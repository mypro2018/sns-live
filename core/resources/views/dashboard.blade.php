@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
  /*.info-box-text{
      font-size: 15px;
      font-weight: bold;
  }
  center{
      padding-top: 20px;
  }
  .well{
      background-color: #2c20201a !important;
  }
  .well-lg {
       padding: 5px !important;
  }
  .panel{
      margin-bottom: 0px !important;
  }*/

  /* admin LTE default */
  .bg-purple-dark{
    background-color: #2a296e;
    color: #fff;
    opacity: 0.9;
  }
  .bg-disabled {
      opacity: .65;
      filter: alpha(opacity=65);
  }
  .bg-disabled-more {
      opacity: .35;
      filter: alpha(opacity=35);
  }
</style>  
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- !!Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <div class="row">
    
    <div class="col-md-3 col-sm-6 col-xs-12">      
      <div class="info-box">
        <span class="info-box-icon bg-purple-dark"><i class="fa fa-gavel"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Total Auctions</span>
          <span class="info-box-number auctions"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div><!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-purple"><i class="fa fa-cubes"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Total Items</span>
          <span class="info-box-number items"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-purple bg-disabled"><i class="fa fa-user"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number users"></span>
        </div>
        <!-- /.info-box-content -->
      </div>      
    </div><!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-purple bg-disabled-more"><i class="fa fa-money"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Total Sale(Rs)</span>
          <span class="info-box-number sale"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div><!-- /.col -->

  </div><!-- /.row -->

  <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12 ">
      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa fa-gavel" aria-hidden="true"></i><b> Auction List</b>
            </div> <!-- /.panel-heading -->
            <div class="panel-body">
              <ul class="list-group auction-list"></ul><!-- /.list-group -->
            </div> <!-- /.panel-body -->
          </div> <!-- /.panel -->
        </div><!-- /.col-md-6 -->
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-list" aria-hidden="true"></i><b> Item List - Auction</b>
            </div> <!-- /.panel-heading -->
            <div class="panel-body">
              <ul class="list-group item-list"></ul><!-- /.list-group -->
            </div><!-- /.panel-body -->
          </div> <!-- /.panel -->
        </div><!-- /.col-md-6 -->
      </div><!-- /.row -->
    </div><!-- /.col-md-6 -->
    <div class="col-md-6 col-sm-6 col-xs-12 ">
      <div class="panel panel-default" style="min-height: 350px;">
        <div class="panel-heading">
          <i class="fa fa-television" aria-hidden="true"></i><b> Live Auction</b>
        </div>
        <div class="panel-body">
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9 live-video">
            <iframe></iframe>
          </div>
        </div>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

</section>
<!-- !!Main content -->
@stop

@section('js')
<script type="text/javascript">
  $(".content").addClass('panel-refreshing');
  $.ajax({
    url: "{{URL::to('data')}}",
    method: 'GET',
    data: {},
    async: false,
    success: function (data) {
      $('.auctions').html(data.auction_count);
      $('.items').html(data.item_count);
      $('.users').html(data.customer_count);
      if(data.revenue.length > 1){
        $('.sale').html(data.revenue[0].revenue);
      }else{
        $('.sale').html('0.00');
      }
      if(data.auction_detail.length > 0){
        $('iframe').attr('src',data.auction_detail[0].auction_url+'?autoplay=1');
      }else{
        $('.live-video').html('No Live Auction');
      }
      var auction_list = '';
      var item_list = '';
      if(data.auction_detail.length > 0){
        for(var i = 0;i < data.auction_detail.length;i++){
          if(data.auction_detail[i].auction_status == 1 || data.auction_detail[i].auction_status == 0){
            auction_list += '<a href="javascript:void(0);" class="list-group-item" data-id="'+data.auction_detail[i].id+'">'+data.auction_detail[i].event_name+
                                '<span class="label label-warning pull-right"><i class="fa fa-clock-o" aria-hidden="true"></i></span>'+
                            '</a>';
          }else if(data.auction_detail[i].auction_status == 2){
            auction_list += '<a href="javascript:void(0);" class="list-group-item" data-id="'+data.auction_detail[i].id+'">'+data.auction_detail[i].event_name+
                                '<span class="label label-success pull-right"><i class="fa fa-video-camera" aria-hidden="true"></i></span>'+
                            '</a>';
          }else{
            auction_list += '<a href="javascript:void(0);" class="list-group-item" data-id="'+data.auction_detail[i].id+'">'+data.auction_detail[i].event_name+
                                '<span class="label label-danger pull-right"><i class="fa fa fa-stop" aria-hidden="true"></i></span>'+
                            '</a>';
          }
        }
        for(var i=0;i < data.auction_detail[0].auction_details.length;i++){
          if(data.auction_detail[0].auction_details[i].highest_bid != null){
            var highest_bid = data.auction_detail[0].auction_details[i].highest_bid.bid_amount;
          }else{
            var highest_bid = '0.00';
          }
          if(data.auction_detail[0].auction_details[i].status == 1 || data.auction_detail[0].auction_details[i].status == 0){
            item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-default pull-right">'+highest_bid+'</span></li>';
          }else if(data.auction_detail[0].auction_details[i].status == 2){
            item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-warning pull-right">'+highest_bid+'</span></li>';
          }else{
            if(data.auction_detail[0].auction_details[i].item_approve != null && data.auction_detail[0].auction_details[i].item_approve.status == 1){
              item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-success pull-right">'+highest_bid+'</span></li>';
            }else{
              item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-danger pull-right">'+highest_bid+'</span></li>';
            }
          }
        }
      }else{
        auction_list = 'No Auctions Available';
        item_list    = 'No Items Available';
      }
      $('.auction-list').html(auction_list);
      $('.item-list').html(item_list);
      $(".content").removeClass('panel-refreshing');
    },error: function () {
      alert('error');
    }
  });

  $('.auction-list a').on('click',function(){
    var item_list = '';
    $.ajax({
    url: "{{URL::to('find-auc')}}",
    method: 'GET',
    data: {auction_id:$(this).data('id')},
    async: false,
    success: function (data) {
      $('iframe').attr('src',data.auction_detail[0].auction_url+'?autoplay=1');
      for(var i=0;i < data.auction_detail[0].auction_details.length;i++){
          if(data.auction_detail[0].auction_details[i].highest_bid != null){
            var highest_bid = data.auction_detail[0].auction_details[i].highest_bid.bid_amount;
          }else{
            var highest_bid = '0.00';
          }
          if(data.auction_detail[0].auction_details[i].status == 1 || data.auction_detail[0].auction_details[i].status == 0){
            item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-default pull-right">'+highest_bid+'</span></li>';
          }else if(data.auction_detail[0].auction_details[i].status == 2){
            item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-warning pull-right">'+highest_bid+'</span></li>';
          }else{
            if(data.auction_detail[0].auction_details[i].item_approve != null && data.auction_detail[0].auction_details[i].item_approve.status == 1){ 
                item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-success pull-right">'+highest_bid+'</span></li>';
              }else{
                item_list += '<li class="list-group-item">'+data.auction_detail[0].auction_details[i].item.name+'<span class="label label-danger pull-right">'+highest_bid+'</span></li>';
              }
          }
          
        }
        $('.item-list').html(item_list);
    },error: function () {
      alert('error');
    }
  });

  });

</script>
@stop

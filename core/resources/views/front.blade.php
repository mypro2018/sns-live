@extends('layouts.sammy_new.front_master') 
@section('current_title','Dashboard')

@section('css')

<style type="text/css">
  .alert-panel{
    height: 100%;
  }

  .alert-list{
    padding-left: 10px;
    color: #777;
    background-color: #fff;
  }

  .alert-list h5{
    /*border-bottom: 1px solid #ddd;*/
    font-size: 12px;
    font-weight: 800;
  }

  .alert-list h6{
    /*border-bottom: 1px solid #ddd;*/
    font-size: 10px;
  }


  .alert-panel> ul{
    list-style: none;
  }

  .alert-panel> ul > li{
    padding: 5px;
    border-bottom: 1px solid #F5F4F4;
  }

  .alert-text{
    padding: 0 0 0 5px;
    margin-top: -7px;
  }

</style>

@stop


@section('content')
<div class="row">
  <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="width: 80%;">
      
  </div>


  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-top: 0;padding-right: 0;width: 20%" >      
        <div class="alert-panel" > 
            <ul class="alert-list" style="height: 800px;">
                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>

                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>

                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>

                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>

                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>

                <li> 
                    <h5>James Merlin</h5>
                    <h6>Moved <a href="#">Inspire Test Deal</a> to Stage Contact Made</h6>    
                </li>
            </ul>
        </div>      
  </div>

</div> 



@stop


@section('js')
 
@stop

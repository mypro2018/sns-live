<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176755680-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176755680-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<!-- Stylesheets ============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        
    </style>

	<!-- Document Title ============================================= -->
	<title>Schokman & Samerawickreme | Register</title>
</head>
<body class="stretched">
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header ============================================= -->
		<header id="header" class="full-header">
			@include('front_ui.includes.header_menu_dark')
		</header><!-- #header end -->
		<!-- Page Title ============================================= -->
		<section id="page-title">
			<div class="container clearfix">
				<h1>Sign Up</h1>
			</div>
		</section><!-- #page-title end -->
		<!-- Content ============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix container-main">
					<div class="col_three_third nobottommargin">
						<h3 style="margin-bottom: 5px;">Don't have an Account? Sign Up Now.</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, vel odio non dicta provident sint ex autem mollitia dolorem illum repellat ipsum aliquid illo similique sapiente fugiat minus ratione.</p>
                        <form id="register-form" name="register-form" class="nobottommargin" action="#" method="post">
                            <div class="col_half">
                                <label for="register-form-name">First Name:</label>
                                <input type="text" id="register-form-name" name="register-form-name" value="" class="form-control" />
                            </div>
                            <div class="col_half col_last">
                                <label for="register-form-email">last Name:</label>
                                <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_half">
                                <label for="register-form-name">Email Address:</label>
                                <input type="text" id="register-form-name" name="register-form-name" value="" class="form-control" />
                            </div>
                            <div class="col_half col_last">
                                <label for="register-form-email">Contact Number:</label>
                                <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_full formfull">
                                <label for="register-form-email">Address 01:</label>
                                <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_full formfull">
                                <label for="register-form-email">Address 02:</label>
                                <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_half">
                                <label for="register-form-username">City:</label>
                                <input type="text" id="register-form-username" name="register-form-username" value="" class="form-control" />
                            </div>
                            <div class="col_half col_last">
                                <label for="register-form-phone">Zip Code / Postal ID:</label>
                                <input type="text" id="register-form-phone" name="register-form-phone" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="bottommargin-sm">
                                <label for="">Country</label>
                                <select class="select-hide form-control bottommargin-sm" style="width:98%;">
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="IN">Indiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                </select>
                            </div>
                            <div class="col_half">
                                <label for="register-form-password">Password:</label>
                                <input type="password" id="register-form-password" name="register-form-password" value="" class="form-control" />
                            </div>
                            <div class="col_half col_last">
                                <label for="register-form-repassword">Re-enter Password:</label>
                                <input type="password" id="register-form-repassword" name="register-form-repassword" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div>
                                <input id="checkbox-1" class="checkbox-style" name="checkbox-1" type="checkbox" checked>
                                <label for="checkbox-1" class="checkbox-style-1-label checkbox-small">I have read and agree to the Schokman & Samerawickreme <a href="{{url('terms-and-conditions')}}" target="_blank">Terms & Conditions</a></label>
                            </div>
                            <div class="clear"></div>
                            <div class="col_full text-right">
                                <button class="button button-border button-rounded s" id="register-form-submit" name="register-form-submit" value="register">Sign Up</button>
                            </div>
                        </form> 
					</div>
				</div>
			</div>
		</section><!-- #content end -->
		<!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

	<!-- Footer Scripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
</body>
</html>
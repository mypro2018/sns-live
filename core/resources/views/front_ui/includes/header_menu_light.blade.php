<div id="header-wrap">
    <div class="container clearfix">
        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        <!-- Logo ============================================= -->
        <div id="logo">
            <a href="{{url('/')}}" class="standard-logo" data-dark-logo="{{asset('assets/front/images/logo-blue.png')}}"><img src="{{asset('assets/front/images/logo-blue.png')}}" alt="Canvas Logo"></a>
            <a href="{{url('/')}}" class="retina-logo" data-dark-logo="{{asset('assets/front/images/logo.png')}}"><img src="{{asset('assets/front/images/logo.png')}}" alt="Canvas Logo"></a>
        </div><!-- #logo end -->
        <!-- Primary Navigation ============================================= -->
        <nav id="primary-menu" class="dark">
            <ul>
                <li >
                    <a href="{{url('/')}}">
                        <div>Home</div>
                    </a>
                </li>
                <li >
                    <a href="categories.html">
                        <div>Categories</div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div>Auctions</div>
                    </a>
                </li>
                <li>
                    <a href="contactus.html">
                        <div>Contact Us</div>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- #primary-menu end -->
    </div>
</div>
<footer id="footer" class="dark">
    <div class="container">
        <!-- Footer Widgets ============================================= -->
        <div class="footer-widgets-wrap clearfix">
            <div class="col_three_third col_last">
                <!--<div class="fancy-title title-border">
                    <h4>Schokman & Samerawickreme (Pvt) Ltd</h4>
                </div>-->
                <div class="col_one_third">
                    <div class="widget clearfix">
                        <address>
                            <strong>Head Office & Showroom:</strong><br>
                            No.24, Torrington Road<br>
                            Kandy.<br>
                            Sri Lanka<br/>
                        </address>
                        <abbr title="Phone Number"><strong>Tel:</strong></abbr> +94 81 222 7593<br>
                        <abbr title="Fax"><strong>Tel/Fax:</strong></abbr> +94 81 222 4371<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> schokmankandy@sltnet.lk
                    </div>
                </div>
                <div class="col_one_third">
                    <div class="widget clearfix">
                        <address>
                            <strong>Flagship Auction & Sales Facility:</strong><br>
                            No.63, Isipathana Mawatha<br>
                            Colombo 05.<br>
                            Sri Lanka<br/>
                        </address>
                        <abbr title="Phone Number"><strong>Tel:</strong></abbr> +94 11 2 588 323<br>
                        <abbr title="Fax"><strong>Tel/Fax:</strong></abbr> +94 11 2 584 721<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> info@samera1892.com
                    </div>
                </div>
                <div class="col_one_third">
                    <div class="widget clearfix">
                        <address>
                            <strong>City Office:</strong><br>
                            No.6A, Fairfield Gardens<br>
                            Colombo 08<br/>
                            Sri Lanka<br/>
                        </address>
                        <abbr title="Phone Number"><strong>Tel:</strong></abbr> +94 11 2 671 467, +94 11 2 671 468<br>
                        <abbr title="Fax"><strong>Tel/Fax:</strong></abbr> +94 11 2 671 469<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> info@sandslanka.com
                    </div>
                </div>
            </div>
            <!--<div class="col_one_third col_last">
                <div class="widget subscribe-widget clearfix">
                    <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                        <div class="input-group divcenter">
                            <span class="input-group-addon"><i class="icon-email2"></i></span>
                            <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">Subscribe</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="widget clearfix" style="margin-bottom: -20px;">
                    <div class="row">
                        <div class="col-md-6 clearfix bottommargin-sm">
                            <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                        </div>
                        <div class="col-md-6 clearfix">
                            <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
                                <i class="icon-rss"></i>
                                <i class="icon-rss"></i>
                            </a>
                            <a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- .footer-widgets-wrap end -->
    </div>
    <!-- Copyrights ============================================= -->
    <div id="copyrights">
        <div class="container clearfix">
            <div class="col_half">
                Copyrights &copy; 2017 Schokman & Samerawickreme.<br>
            <div class="copyright-links"><a href="{{url('terms-and-conditions')}}" target="_blank">Terms of Use</a> / <a href="{{url('terms-conditions-sinhala')}}" target="_blank">Terms of Use(Sinhala)</a>/ <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a></div>
            </div>
            <div class="col_half col_last tright">
                <div class="fright clearfix">
                    <a href="#" class="social-icon si-small si-borderless si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="social-icon si-small si-borderless si-twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>
                    <!--<a href="#" class="social-icon si-small si-borderless si-yahoo">
                        <i class="icon-yahoo"></i>
                        <i class="icon-yahoo"></i>
                    </a>-->
                    <a href="#" class="social-icon si-small si-borderless si-linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>
<div id="header-wrap">
    <div class="container clearfix">
        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        <!-- Logo ============================================= -->
        <div id="logo" style="width:20%;">
            <a href="{{url('/')}}" class="standard-logo" data-dark-logo="{{asset('assets/front/images/logo-dark.png')}}"><img src="{{asset('assets/front/images/logo-dark.png')}}" alt="Canvas Logo"></a>
            <a href="{{url('/')}}" class="retina-logo" data-dark-logo="{{asset('assets/front/images/logo-dark.png')}}"><img src="{{asset('assets/front/images/logo-dark.png')}}" alt="Canvas Logo"></a>
            <a href="{{url('/')}}" class="live-logo hide" data-dark-logo="{{asset('assets/front/images/live-logo.png')}}"><img src="{{asset('assets/front/images/live-logo.png')}}" alt="Canvas Logo"></a>
        </div><!-- #logo end -->
        <!-- Primary Navigation ============================================= -->
        <nav id="primary-menu" class="dark" style="margin-left:20px;">
            <ul>
                @if($user = Sentinel::getUser())
                    @if(isset($event_name) && isset($event_no))
                    <li>
                        <a href="" style="line-height: 35px;cursor:pointer;pointer-events:none"><strong>SALE {{$event_no}} - {{mb_strimwidth($event_name,0,50, '....')}} {{$auction->auction_date}}</strong></a>
                    </li>
                    @endif
                    <li >
                    <a class="my-account" href="{{url('my-account')}}">
                        <div>Hi, {{$user->first_name}} {{$user->last_name}} 
                            @if (isset($card_no))
                                <button type="button" class="btn btn-primary">PADDLE <span style="display:inline;" class="badge">{{$card_no}}</span></button>
                            @endif
                        </div>
                        </a>
                    </li>
                    <li>
                        <a class="my-space" href="{{url('my-account')}}">
                            <div>My Space</div>
                        </a>
                    </li>                
                    <li>
                        <a href="{{url('logout')}}" id="sign_out">
                            <div>Sign Out</div>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{url('register')}}">
                            <div>Sign Up</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('login')}}">
                            <div>Sign In</div>
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- #primary-menu end -->
    </div>
</div>
<script>
    var player = new MediaElementPlayer('#player');
    <!--Start of Tawk.to Script-->
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    <!--End of Tawk.to Script-->
</script>
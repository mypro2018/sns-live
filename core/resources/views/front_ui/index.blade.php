<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/swiper.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.2/mediaelementplayer.min.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .auction-live{
            color: #eb4d4b;
        }
        .auction-upcoming{
            color: #30336b;
        }
    </style>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-2452834730654689",
        enable_page_level_ads: true
      });
    </script>
    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Live Auction Portal</title>
</head>

<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->
        
        <section id="slider" class="slider-parallax swiper_wrapper force-full-screen clearfix">
            <div class="slider-parallax-inner">
                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">
                    @if(count($banners) > 0)
                        @foreach($banners as $value)
                            <div class="swiper-slide" style="background-image: url('core/storage/uploads/{{$value->banner_path}}')">
                                <div class="container clearfix">
                                   <!--  <div class="slider-caption slider-caption-center">
                                        <p data-caption-animate="fadeInUp" data-caption-delay="200">Welcome To</p>
                                        <h2 data-caption-animate="fadeInUp">Schokman <br/>& </br>Samerawickreme</h2>
                                        <p data-caption-animate="fadeInUp" data-caption-delay="200">Exceptional Items from Trusted Auction Partners</p>
                                    </div> -->
                                </div>
                            </div>
                        @endforeach
                    @endif
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Pagination -->
                    <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                    <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                </div>
            </div>
        </section>
        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap" style="padding-top:0;">
                <!-- Auction Categories-->
                <!-- <div style="background: #f1f1f1;padding-bottom: 50px;padding-top: 25px;">
                    <div class="container clearfix container-main">
                        <div class="row">
                            <div class="col-md-4 header-line text-center">
                                <div class="divider"></div>
                            </div>
                            <div class="col-md-4 text-center">
                                <h2 style="margin-bottom: 50px;">Auction Categories</h2>
                            </div>
                            <div class="col-md-4 header-line text-center">
                                <div class="divider"></div>
                            </div>
                        </div>
     
                        @if(count($categories) > 0)
                        <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">
                            @foreach($categories as $category)
                            <div class="oc-item">
                                <div class="iportfolio">
                                    <div class="portfolio-image">
                                        <a href="{{url('auction-categories/'.$category->id)}}">
                                            @if($category->image_path)
                                                @if(File::exists(storage_path('uploads/images/category/'.$category->image_path)))
                                                    <img src="{{ url('core/storage/uploads/images/category/'.$category->image_path) }}" alt="{{ $category->display_name }}">
                                                @else
                                                    <img src="{{url('core/storage/uploads/images/category/empty.jpg')}}">
                                                @endif
                                            @else
                                                <img src="{{url('core/storage/uploads/images/category/empty.jpg')}}">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="portfolio-desc">
                                        <h3><a href="portfolio-single.html">{{ $category->display_name }}</a></h3>
                                        <span>{{ $category->description }}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @else
                            <p class="text-center">No Categories Found.</p>
                        @endif

                    </div>
                </div> -->

                <div class="container">
                    <div class="row padtop20" style="padding-top:50px;">
                        <div class="col-md-4 header-line text-center">
                            <div class="divider"></div>
                        </div>
                        <div class="col-md-4 text-center">
                            <h2 style="margin-bottom: 50px;">Latest Auctions</h2>
                        </div>
                        <div class="col-md-4 header-line text-center">
                            <div class="divider"></div>
                        </div>
                    </div>
                    @if(count($auctions) > 0)
                    <div id="oc-products" class="owl-carousel products-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">
                        @foreach($auctions as $auction)
                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                @if($auction->auction_status == AUCTION_TO_BEGIN)
                                    <a href="{{url('auctions/'.$auction->id)}}">
                                @else
                                <a href="{{url('auctions/'.$auction->id)}}">
                                    <!-- @if(count($auction->live_auction_product) > 0) -->
                                        <!-- <a href="{{url('auctions/'.$auction->id.'/item/'.$auction->live_auction_product->sns_item_id)}}"> -->
                                    <!-- @else -->
                                    
                                    <!-- @endif -->
                                @endif
                                        @if($auction->image)
                                            @if(File::exists(storage_path('uploads/images/auction/'.$auction->image)))
                                                <img src="{{ url('core/storage/uploads/images/auction/'.$auction->image) }}" alt="{{ $auction->event_name }}">
                                            @else
                                                <img src="{{url('core/storage/uploads/images/auction/empty.jpg')}}">
                                            @endif
                                        @else
                                            <img src="{{url('core/storage/uploads/images/auction/empty.jpg')}}">
                                        @endif
                                    </a>
                                </div>
                                <div class="product-desc">
                                    <div class="product-title text-left">
                                        <h3><a href="#">{{ $auction->event_name }}</a></h3>
                                    </div>
                                    <div class="product-price">{{ $auction->auction_date }}    
                                    @if($auction->auction_status == AUCTION_LIVE)
                                        <span class="auction-live"> - Live</span>
                                    @else
                                        <span class="auction-upcoming"> - Upcoming</span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                        <p class="text-center">No Auctions Found.</p>
                    @endif

                </div>
                <!--</div>-->
                <div class="promo promo-light promo-full topmargin-sm ">
                    <div class="container clearfix">
                        <div class="col_full nobottommargin">
                            <!-- <h2>Call us today at <span>+94 716 596 569</span> or Email us at <span>support@shopandsellanka.com</span></h2>
                            <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span> -->
                        </div>
                        @if(!$user)
                        <!-- <div class="col_two_fifth nobottommargin pull-right col_last topmargin-sm text-right">
                            <button class="button button-large button-border button-rounded s" style="width: 45%;" onclick="window.location.href='{{url('register')}}'">Sign Up</button>
                            <button class="button button-large button-border button-rounded s" style="width: 45%;" onclick="window.location.href='{{url('login')}}'">Sign In</button>
                        </div> -->
                        @endif
                    </div>
                </div>
                <div class="row clearfix common-height">
                    <div class="col-md-5 center col-padding" style="background: url('{{asset('assets/front/images/main-bg.jpg')}}') center center no-repeat; background-size: cover;">
                        <div>&nbsp;</div>
                    </div>
                    <div class="col-md-7 center col-padding" style="background-color: #F5F5F5;">
                        <div>
                            <div class="heading-block nobottomborder">
                                <h3>Schokman & Samerawickreme</h3>
                            </div>                            
                            <p class="justyfy lead nobottommargin">We are Schokman & Samerawickreme, Reputed Pioneer Charted Auctioneers, Consultant Valuers and Realtors based in Sri Lanka. Established in 1892 we count over 119 years of professionalism and trust in the trade. We have obtained ISO 9001: 2008 certification for Auctioning, Valuation and Real Estate Brokering. This entitles us to be the first Company in South Asia Pacific to have obtained this certificate in the fields of Auctioning & Real Estate Brokering.</p><br/>
                            <p class="justyfy lead nobottommargin">While maintaining the core business of Auctioning, today the organization has diversified into the areas of valuations of movable and immovable property and is the preferred Realtor amongst diplomatic missions and of varied companies and individuals. Our reputation is based on Professionalism and Service Excellence in serving our valued customers.</p>
                        </div>
                    </div>
                </div>
                <!-- <div class="content-wrap container clearfix">
                    <div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-margin="60" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="5" data-items-lg="6">
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/1.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/2.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/3.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/4.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/5.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/6.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/7.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/8.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/9.png')}}" alt="Clients"></a>
                        </div>
                        <div class="oc-item">
                            <a href="#"><img src="{{asset('assets/front/images/clients/10.png')}}" alt="Clients"></a>
                        </div>
                    </div>
                </div> -->
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->
    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>
    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.2/mediaelement-and-player.min.js"></script>
    <script>
    //CHAT FEATURE
    <!--Start of Tawk.to Script-->
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    <!--End of Tawk.to Script-->

   var mySwiper = new Swiper('.swiper-container', {
       
        pagination: {
            el: '.swiper-pagination',
            clickable: false,
        },
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 0,
        parallax: true,
        autoplay: 2000,
        speed: 1000,
        autoplayDisableOnInteraction: false
    });


    </script>
</body>
</html>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Lt Soft | Admin Panel</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- BOOTSTRAP -->
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">
  <!-- //BOOTSTRAP -->

  <!-- FONTS -->
  <link rel="stylesheet" href="{{asset('assets/fonts/roboto/roboto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
  <!-- //FONTS -->

  <!-- STYLE -->
  <link rel="stylesheet" href="{{asset('assets/adminlte/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('assets/core/css/form-elements-login.css')}}">
  <link rel="stylesheet" href="{{asset('assets/dist/checkbox-master/css/checkbox.css')}}">
  <link rel="stylesheet" href="{{asset('assets/core/css/style-login.css')}}">
  <!-- //STYLE -->
  
  <style type="text/css">
  .control-label i{
    font-size: 20px;
    color: #fff;
  }

  .login-description{
    color: #fff;
    text-align: center;
  }
  a.login-description:hover{
    /*color: #555;*/
  }
  a.login-description.new{
    line-height: 2px;
  }
  .login{
    color: #fff;
  }
  .login-btn{
    border: 1px solid #fff !important;
    color: #fff !important
  }
  </style>

</head>

<body>

  <div class="inner-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-sm-offset-4 form-box">
          <form class="form-horizontal" role="form" action="{{URL::to('user/login')}}" method="post">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

              <h3 class="login-description">Login</h3>
              <p class="login-description">
                Welcome to S & S System Admin Panel
              </p>
              <br>

              @if($errors->has('login'))
                <div class="alert alert-danger">
                  Oh snap! {{$errors->first('login')}}
                </div>
              @endif

              <div class="form-group">
                <label class="col-sm-2 control-label"><i class="fa fa-user"></i></label>
                <div class="col-sm-10">
                  <input type="text" name="username" class="form-control login" placeholder="Username" autocomplete="off" value="{{{Input::old('username')}}}" @if(empty(Input::old('username'))) autofocus @endif>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><i class="fa fa-unlock-alt"></i></label>
                <div class="col-sm-10">
                  <input type="password" name="password" class="form-control login" placeholder="Password" @if(!empty(Input::old('username'))) autofocus @endif>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                  <div class="checkbox-btn">
                    <input type="checkbox" value="{{Input::old('remember')}}" id="remember" name="remember"/>
                    <label for="remember" onclick><span>Remember Me</span></label>
                  </div>
                </div>
              </div>
              <!--<div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                  <a href="#" class="login-description new">new? create one</a>
                  <a href="#" class="login-description pull-right">forgot password?</a>
                </div>
              </div>-->
              <button type="submit" class="btn btn-link login-btn">Let's Go</button>
            </form>
        </div>

      </div>
    </div>
  </div>

  <!-- <footer class="main-footer" style="margin-left: 0;padding: 10px;padding-top:10px">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.1.0
      </div>
      Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Tharindu Lakshan</a>. All rights reserved.
    </footer> -->

  <!-- CORE JS -->
  <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
  <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>  
  <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>
  <!-- //CORE JS -->

  <script type="text/javascript">
    $(document).ready(function(){

    });
  </script>
  <!-- endbuild -->

  <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 100%; width: 100%; z-index: -999999; position: fixed;background: url({{asset('assets/images/login.jpg')}})">
  </div>

</body>

</html>

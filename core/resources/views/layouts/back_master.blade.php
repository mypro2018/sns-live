<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>S&S | Admin Panel</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- BOOTSTRAP -->  
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">  
  <!-- //BOOTSTRAP -->

  <!-- FONTS -->
  <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
  <!-- //FONTS -->

  <!-- Chosen Select -->
  <link rel="stylesheet" href="{{asset('assets/dist/chosen/css/chosen.min.css')}}">

  <!-- STYLE -->
  <link rel="stylesheet" href="{{asset('assets/adminlte/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/adminlte/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" href="{{asset('assets/core/css/style.css')}}">

  <!-- Bootstrap-datetimepicker -->
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap-datetimepicker/dist/css/bootstrap-datetimepicker.css')}}">
  <!-- Bootstrap-datetimepicker -->

  <!-- File Upload -->
  <link rel="stylesheet" href="{{asset('assets/dist/fileinput/css/fileinput.min.css')}}">

  <!-- Jquery Confirm -->
  <link rel="stylesheet" href="{{asset('assets/dist/jquery-confirm/css/jquery-confirm.css')}}">

  <!-- Angular-confirm-master -->
  <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
  <!-- Angular-confirm-master -->
  <!-- Thumbnail Slider Css -->
  <link rel="stylesheet" href="{{asset('assets/dist/jquery-slider/css/jcarousel.ajax.css')}}">
  <!-- Thumbnail Slider Css -->
  <!-- toaster slider -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/toastr/css/toastr.min.css')}}"/>
  <!-- toaster slider -->
  <!-- datepicker -->
  <link rel="stylesheet" href="{{asset('assets/dist/date-picker/css/bootstrap-datepicker.min.css')}}">
  <!-- datepicker -->

  @yield('links')

  <!-- //STYLE -->
  <style type="text/css">
    textarea{
      resize: none;
    }
    .require{
      color: #dd4b39;
      font-weight: normal;
    }
    label.error{
      display: initial;
      color: #dd4b39;
    }

    .form-group.has-error .error {
      font-weight: normal;
    }
    .chosen-container{
      width: 100%!important;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
      display: none !important;
    }

    .main-header .logo .logo-lg {
      margin-top: 1px;
    }

    /* side bar custom style */
    .sidebar .sidebar-background, .off-canvas-sidebar .sidebar-background {
      position: absolute;
      z-index: 1;
      height: 100%;
      width: 100%;
      display: block;
      top: 0;
      left: 0;
      background-size: cover;
      background-position: center center;
    }
    .sidebar .sidebar-background:after, .off-canvas-sidebar .sidebar-background:after {
      position: absolute;
      z-index: 3;
      width: 100%;
      height: 100%;
      content: "";
      display: block;
      background: /*#d50000*/#2a296e;
      opacity: .8;
    }
    .sidebar-menu hr {
      margin: 8px 15px;
      border-color: rgba(255, 255, 255, 0.3);
    }
    .sidebar-menu {
        position: relative;
        z-index: 9999;
    }
    .main-header .sidebar-toggle:before {
      font-size: 14px;
    }
    .main-header>.navbar {
      border-bottom: 1px solid #d2d6de;
    }

    /* custom nav styles */
    .user-panel {
      z-index: 2;
      padding: 10px 15px;
    }
    .user-panel p {
      color: #fff;
    }
    .user-panel>.info {
        padding: 8px 5px 0px 15px;
    }
    .user-panel>.info>p {
      font-weight: normal; 
      margin-bottom: 10px;
    }
    .user-panel>.image>img {
      max-width: 35px;
      margin-top: 5px;
    }

    .skin-black-light .sidebar a {
        color: #fff;
    }
    .skin-black-light .sidebar-menu>li:hover>a, .skin-black-light .sidebar-menu>li.active>a {
         color: #fff; 
        background: rgba(255, 255, 255, 0.14);
    }
    .skin-black-light .sidebar-menu>li.header {
        color: rgba(255, 255, 255, 0.46);
        background: transparent;
    }
    .skin-black-light .sidebar-menu>li>.treeview-menu {
        background: rgba(255, 255, 255, 0.14);
    }
    .skin-black-light .sidebar-menu .treeview-menu>li>a {
        color: #fff;
    }
    .skin-black-light .sidebar-menu .treeview-menu>li.active>a, .skin-black-light .sidebar-menu .treeview-menu>li>a:hover {
        color: rgba(255, 255, 255, 0.64);
    }
    .skin-black-light .main-sidebar {
       border-right: 0; 
    }
    .skin-black-light .sidebar-menu>li>a {
        border-left: 0;
        font-weight: normal; 
    }
    .skin-black-light .main-header .navbar>.sidebar-toggle {
      color: #2a296e;
      border: 2px solid #2a296e;
      border-radius: 50%;
      width: 34px;
      height: 34px;
      padding: 10px;
      margin: 8px;
      position: absolute;
      opacity: 0.9;
    }
    .main-header .sidebar-toggle:before {
      font-size: 14px;
      top: 5px;
      position: absolute;
      left: 9px;
    }
    .skin-black-light .main-header>.logo{
      border-right: 0;
    }
    .skin-black-light .main-header {
      border-bottom: 0; 
    }
    .skin-black-light .main-header .navbar .navbar-custom-menu .navbar-nav>li>a, .skin-black-light .main-header .navbar .navbar-right>li>a {
      border-left: 0;
    }
    .skin-black-light .main-header .navbar .sidebar-toggle:hover {
      color: #2a296e;
      opacity: 0.7;
    }
    .skin-black-light .sidebar-menu>li.active>a {
       font-weight: normal; 
    }
    .sidebar-menu .treeview-menu>li>a {
       font-size: inherit; 
    }

    /* custom nav items */
    .nav li a {
      padding: 0;
    }
    .nav li a div {
      color: #2a296e;
      border: 2px solid #2a296e;
      border-radius: 50%;
      width: 34px;
      height: 34px;
      padding: 10px;
      margin: 8px;
      float: left;
      opacity: 0.9;
    }
    .nav li a div i {
      font-size: 14px;
      position: absolute;
      top: 18px;
    }
    .nav li a:hover {
      opacity: 0.7;
    }
  </style>
  <!-- //STYLE -->

  <!-- INCLUDE css -->
  @yield('css')
  <!-- INCLUDE css -->
  
</head>

  <body class="fixed sidebar-mini-expand-feature skin-black-light">
    <div class="refresh_application"></div>
      <div class="wrapper">

        <header class="main-header">

          <!-- Logo -->
          <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
              <img src="{{url('assets/images/logo/35x35logo.png')}}">
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg pull-left">
              <!-- <img src="{{url('assets/images/logo/230x50.png')}}"> -->
              <img src="{{url('assets/images/logo/40x40.png')}}">              
            </span>
          </a>

          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
              <ul class="nav">
                <li>
                  <!-- <a href="{{url('user/logout')}}">
                    <img src="{{url('assets/icons/logout.png')}}" class="user-image" alt="User Image">
                    <span class="hidden-xs">Sign Out</span>
                  </a> -->
                  <a href="{{url('user/logout')}}">
                    <div>
                      <i class="fa fa-sign-out" aria-hidden="true"></i>
                    </div>                    
                  </a>                  
                </li>
              </ul>
            </div>
          </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
          <!-- sidebar: style can be found in sidebar.less -->
          <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
              <div class="pull-left image">
                <img src="{{url('assets/adminlte/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
              </div>
              <div class="pull-left info">
                <p>{{$user->full_name}}</p>
                <p>Last seen : 2017.06.28</p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
              </div>
            </div>

            <!-- sidebar -->
            @include('includes.menu')
            <!-- /.sidebar -->

            <div class="sidebar-background" style="background-image: url({{url('assets/images/sidebar-1.jpg')}})"></div>
          </section>
          <!-- /.sidebar -->          
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <div class="container-fluid">
            @yield('content')
          </div>
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
          <div class="container-fluid">
            <div class="pull-left hidden-xs">
              <b>Version</b> 1.0.1
            </div>
            <p class="pull-right">Copyright &copy; 2017-2018 <!-- <a href="http://almsaeedstudio.com">Tharindu Lakshan</a> -->. All rights reserved.</p>
          </div>
        </footer>

      </div><!-- ./wrapper -->

      <!-- modernizr -->
      <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>
      <!-- jquery -->      
      <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
      <!-- bootstrap -->      
      <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{asset('assets/adminlte/js/app.min.js')}}"></script>
      <!-- SlimScroll 1.3.0 -->
      <script src="{{asset('assets/dist/slimScroll/jquery.slimscroll.min.js')}}"></script>
      <!-- sweet-alert -->
      <script src="{{asset('assets/dist/sweetalert/js/sweet-alert.min.js')}}"></script>

      <!-- MY-SCRIPTS -->
      <script src="{{asset('assets/core/js/custom_functions.js')}}"></script>

      <!-- File Upload -->
      <script src="{{asset('assets/dist/fileinput/js/fileinput.min.js')}}"></script>

      <!-- Chosen Select -->
      <script src="{{asset('assets/dist/chosen/js/chosen.jquery.min.js')}}"></script>

      <!-- Jquery Confirm -->
      <script src="{{asset('assets/dist/jquery-confirm/js/jquery-confirm.min.js')}}"></script>

      <!-- bootstrap-datetimepicker -->
      <script src="{{asset('assets/dist/moment/min/moment.min.js')}}"></script>  
      <script src="{{asset('assets/dist/bootstrap-datetimepicker/dist/js/bootstrap-datetimepicker.js')}}"></script>
      <!-- bootstrap-datetimepicker -->

      <!-- ANGULAR -->
      <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
      <!-- ANGULAR -->

      <!-- load angular-moment -->
      <script src="{{asset('assets/dist/angular/angular-moment/angular-moment.min.js')}}"></script>
      <!-- load angular-moment -->

      <!-- angular-confirm-master -->
      <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
      <!-- angular-confirm-master -->

      <!-- angular chosen -->
      <script src="{{asset('assets/dist/angular/angular-chosen/angular-chosen.min.js')}}"></script>
      <!-- angular chosen -->

      <!-- thumbnail slider  -->
        <script src="{{asset('assets/dist/jquery-slider/js/jquery.jcarousel.min.js')}}"></script>
        <script src="{{asset('assets/dist/jquery-slider/js/jcarousel.ajax.js')}}"></script>
       <!-- angular chosen -->

      <!-- toaster -->
      <script src="{{asset('assets/dist/toastr/js/toastr.min.js')}}"></script>
      <!-- toaster -->

      <!-- datepicker -->
      <script type="text/javascript" src="{{asset('assets/dist/date-picker/js/bootstrap-datepicker.min.js')}}"></script>
      <!-- datepicker -->
      
      <!-- //CORE JS -->

      <script type="text/javascript">
      $(function(){
        $('.refresh_application').fadeOut(2000);
      })
      $(document).ready(function(){
        //date range
        $('.input-daterange').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });

        // bootstrap file input
        $(".input-group").addClass('input-group-sm');
        $(".file-input .btn-file",".lf-ng-md-file-input .md-primary").addClass('bg-purple').removeClass('btn-primary');

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('.chosen').chosen();

        @if(session('success'))
          $.alert({theme: 'material',title: '{{session('success.title')}}',type: 'green',content: '{{session('success.message')}}'});
        @elseif(session('error'))
          $.alert({theme: 'material',title: '{{session('error.title')}}',type: 'red',content: '{{session('error.message')}}'});
        @elseif(session('warning'))
          $.alert({theme: 'material',title: '{{session('warning.title')}}',type: 'orange',content: '{{session('warning.message')}}'});
        @elseif(session('info'))
          $.alert({theme: 'material',title: '{{session('info.title')}}',type: 'blue',content: '{{session('info.message')}}'});
        @endif


        @if(session('print'))
            $.confirm({
              title: 'Card No : {{session('print.title')}}',
              content: '{{session('print.content')}}'+'<br/>Register ID : {{session('print.customer')}}',
              type: 'green',
              typeAnimated: true,
              buttons: {
                  ok: {
                      text: 'Print Receipt',
                      btnClass: 'btn-success',
                      action: function(){
                        $('.print-register-chit').click();
                      }
                  },
                  cancel : {
                    text    : 'Next Customer',
                    btnClass: 'btn-info',
                    action: function(){}
                  }
              }
          });
        @endif
       
      });
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "500",
        "timeOut": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }



    </script>

    @yield('js')

  </body>
</html>

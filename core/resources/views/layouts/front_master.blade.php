
<!--
##############################################################
#     Admin Panel - Laravel 5.1                              #
#                                                            #
#     Author - Yasith Samarawickrama <yazith11@gmail.com>    #
#     Version 1.0                                            #
#     Copyright Sammy 2015                                   #
##############################################################
-->

<!doctype html>
<html class="no-js" lang="">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176755680-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176755680-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
  <meta charset="utf-8">
  <title>Admin Panel | Zoom</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="{{asset('assets/sammy_new/vendor/sweetalert/lib/sweet-alert.css')}}">
  <link rel="stylesheet" href="{{asset('assets/sammy_new/vendor/datatables/media/css/jquery.dataTables.css')}}">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="{{asset('assets/sammy_new/vendor/bootstrap/dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{asset('assets/sammy_new/styles/roboto.css')}}">

  <link rel="stylesheet" href="{{asset('assets/sammy_new/front/css/style.css')}}">

  <link rel="stylesheet" href="{{asset('assets/sammy_new/styles/font-awesome.css')}}">
  
  <!-- endbuild -->

  <style type="text/css">
 

      .navbar {
        position: relative;
        min-height: 50px;
        margin-bottom: 0;
        border: 1px solid transparent;
        border-radius: 0; 
    }

    .navbar-default {
        background-color: #2b3288;

    }

    .navbar-brand {
        float: left;
        height: 50px;
        padding: 0 40px 0 0 ;
        font-size: 18px;
        line-height: 20px;
    }

    .navbar-brand > img {
      height: 92%;
    }

    .navbar-default .navbar-nav > .active > a {
      color: #fff;
      background-color: transparent;
    }
    
    .navbar-default .navbar-nav > .active > a:hover,
    .navbar-default .navbar-nav > .active > a:focus {
      color: #3596d1;
      background-color: transparent;
    }

    .navbar-default .navbar-nav > li > a {
      color: #fff ;
    }

    .navbar-default .navbar-nav > li > a:hover,
    .navbar-default .navbar-nav > li > a:focus {
      color: #3596d1;
      background-color: transparent;
    }

    #btn-add-deal {
        padding: 4px 20px;
        margin: 10px;
    }

    #profile-panel{
      background-color: #27284a;
    }

    .badge-msg {
        /* margin: 0; */
        margin-top: -7px;
        margin-left: -5px;
        position: absolute;
        background-color: #E84949;
    }

  </style>

  @yield('css')

</head>

<body >

  <div class="app layout-fixed-header" >

    <!-- content panel -->
    <div class="main-panel">

      <!-- top header -->
      <nav class="navbar navbar-default" role="navigation">
        
          <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10"  style="width: 80%">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="{{url('assets/sammy_new/images/logo_main.png')}}" alt="Image"></a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown active">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pipeline <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </li>
                <li><a href="#">Activities</a></li>
                <li class="dropdown active">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-users"></i> Contacts <b class="caret"></b>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#"><i class="fa fa-building"></i> Orginizations</a></li>
                    <li><a href="#"><i class="fa fa-users"></i> People</a></li>
                  </ul>
                </li>
                <li><a href="#">Statistics</a></li>
                <li><a href="#" class="btn btn-info" id="btn-add-deal"><i class="fa fa-plus" style="font-size: 13px"></i> Add Deal</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div>

          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="width: 20%;padding-right: 0">
              <div id="profile-panel">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                  <ul class="nav navbar-nav">
                    <li><a href="#"><i class="fa fa-envelope"></i></i><span class="badge badge-msg">4</span></a></li>
                    <li><a href="#"><i class="fa fa-calendar"></i></a></li>
                    <li class="dropdown active">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i> Tharindu .. <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </li>                   
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div>
          </div>
      </nav>
      <!-- /top header -->



      <!-- main area -->
      <div class="container-fluid">          
        @yield('content')
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <!-- bottom footer -->
    <!-- <footer class="content-footer">
      <a href="javascript:;">Copyright <i class="fa fa-copyright"></i> <span>Orange IT.</span> 2015. All rights reserved</a>
    </footer> -->
    <!-- /bottom footer -->
  </div>

  <!-- build:js({.tmp,app}) {{asset('assets/sammy_new/scripts/app.min.js')}} -->
  <script src="{{asset('assets/sammy_new/scripts/extentions/modernizr.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/jquery/dist/jquery-2.0.1.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/jquery.easing/jquery.easing.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/onScreen/jquery.onscreen.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/jquery-countTo/jquery.countTo.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/accordion.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/animate.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/link-transition.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/panel-controls.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/preloader.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/ui/toggle.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/urban-constants.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/extentions/lib.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/jquery.multiselect/js/jquery.multi-select.js')}}"></script>
  <!-- endbuild -->

  <script src="{{asset('assets/sammy_new/vendor/chosen_v1.4.0/chosen.jquery.min.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/checkbo/src/0.1.4/js/checkBo.min.js')}}"></script>
  <script src="{{asset('assets/sammy_new/vendor/sweetalert/lib/sweet-alert.min.js')}}"></script>

  <script src="{{asset('assets/sammy_new/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{asset('assets/sammy_new/scripts/extentions/bootstrap-datatables.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
  <script src="{{asset('assets/vendor/typehead/typehead.js')}}"></script>
  <!-- Custom DataTable Generator -->
  <script src="{{asset('assets/sammy_new/scripts/custom/custom_functions.js')}}"></script>
  <!-- autocomplete -->

  <!-- angular chosen -->
  <script src="{{asset('assets/dist/angular/angular-chosen/angular-chosen.min.js')}}"></script>
  <!-- angular chosen -->

  <script src="{{asset('assets/sammy_new/scripts/autocomplete/dist/jquery.autocomplete.js')}}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('form').checkBo();

      $('.chosen').chosen();

      @if(session('success'))
        sweetAlert('{{session('success.title')}}', '{{session('success.message')}}',0);
      @elseif(session('error'))
        sweetAlert('{{session('error.title')}}','{{session('error.message')}}',2);
      @elseif(session('warning'))
        sweetAlert('{{session('warning.title')}}','{{session('warning.message')}}',3);
      @elseif(session('info'))
        sweetAlert('{{session('info.title')}}','{{session('info.message')}}',1);
      @endif

      $('#autocomplete').autocomplete('enable');
    });

    

  </script>
  @yield('js')
</body>

</html>


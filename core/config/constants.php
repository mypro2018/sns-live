<?php 
/* ============= BACKEND =================*/

/*display option per page*/
define('SHOW_OPTION',serialize([
	'1' => '150',
	'2' => '300',
	'3' => '500'
]));

define('DEFAULT_STATUS', 1);

/* =============== FRONTEND ===============*/

/*auction status*/
define('AUCTION_TO_BEGIN', 1);
define('AUCTION_LIVE', 2);
define('AUCTION_OVER', 3);

/*auction product status */

define('PRODUCT_TO_BEGIN', 0);
define('PRODUCT_TO_LIVE', 2);
define('PRODUCT_TO_OVER', 3);
define('PRODUCT_NOT_AVAILABLE', 4);

/*default transaction id*/
define('TRANSACTION_ID', '0000');

/*default transaction error code*/
define('TRANSACTION_ERROR_CODE', '00');

/*default transaction message*/
define('TRANSACTION_ERROR_MESSAGE', 'TRANSACTION APPROVED');

/*default transaction status*/
define('TRANSACTION_STATUS', 1);

/*product sold & available status*/
define('PRODUCT_SOLD', 3);
define('PRODUCT_AVILABLE', 0);

define('CALL_1', '1st Call');
define('CALL_2', '2nd Call');
define('CALL_3', '3rd Call');

define('CALL0', '0');
define('CALL1', '1');
define('CALL2', '2');
define('CALL3', '3');

define('OFFER_DEFAULT', '0');
define('OFFER_REQUEST', '1');
define('OFFER_EXPIRED', '2');
define('OFFER_APPROVED', '3');

define('ADMIN_BIDDER', '0');
define('FLOOR_BIDDER', '1');
define('ONLINE_BIDDER', '2');

define('ITEM_PAYMENT_APPROVE', '1');
define('ITEM_PAYMENT_REJECT', '2');
define('ITEM_PAYMENT_SUCCESS', '3');

define('AUCTION_SHOW', '1');
define('AUCTION_NOT_SHOW', '0');

define('ONLINE_CARD', '1');
define('FLOOR_CARD', '2');

define('PATH_TO_BANNER', 'uploads/images/banners');

define('UPLOADS_DIR', 'uploads/');

define('UPLOAD_PATH','core/storage/');

//Customer Type
//default customer type
define('DEFAULT_CUSTOMER_TYPE','1');
define('LAND_CUSTOMER','11');
define('FURNITURE_CUSTOMER','22');
define('VEHICLE_CUSTOMER','33');
define('BLOCK_CUSTOMER','44');

//customer band(block)/unband(unblock) 
define('BAND', '1');
define('UNBAND', '0');

//payment status
define('PAYMENT_SUCCESS', '1');
define('PAYMENT_PENDING', '0');

//PAYMENT METHOD
define('POCKET_BALANCE', '1');
define('CARD_PAYMENT', '2');
define('CASH_PAYMENT', '3');

//PAYMENT TIME - 2 Minutes
define('PAYMENT_TIME', 2);

//UNSOLD ITEM
define('UNSOLD_REASON_MESSAGE', 'Auctioneer decided to not selling this item.');
define('ACCOUNT_SUSPENDED_MESSAGE', 'Sorry, Dear Valued Customer your live auction account has been suspended due to pending payments.');

//APPROVE & REJECT WON ITEM TO MAKE PAYMENT
define('APPROVE', 1);
define('REJECT', 2);

define('BIDDING_ACTIVE', '1');
define('BIDDING_INACTIVE', '0');

define('ADVANCE_RATE', 25);

//CARD ISSUE EXCEED MESSAGE
define('CARD_ISSUE_EXCEED_MESSAGE', 'Sorry, Dear valued customer, we issued register cards has been exceeded.So you cannot join with the auction.');
define('NO_CARD_RANGE_MESSAGE', 'Sorry, Dear valued customer, auctioneer not defined the cards.');
//CARD STATUS
define('NO_CARD_RANGE', 0);
define('NEW_CARD_REGISTER', 1);
define('CARD_LIMIT_EXCEED', 2);
define('CARD_CAN_ISSUE', 3);

//AMOUNT PAYING STATUS
define('FULL_PAID', 0);
define('ADVANCE_PAID', 1);

//CUSTOMER LEVEL VALIDATION MESSAGE
define('LEVEL_VALIDATION_MESSAGE', 'You are not authorized by the admin for registering or bidding at this auction. Please contact the Schokman & Samerawickreme for more Details. City Office: No.6A, Fairfield Gardens Colombo 08 Sri LankaTel: +94 11 2 671 467, +94 11 2 671 468Tel/Fax: +94 11 2 671 469, live@samera1892.com. Head Office & Showroom:No.24, Torrington Road Kandy.Sri Lanka Tel: +94 81 222 7593. Tel/Fax: +94 81 222 4371, schokmankandy@sltnet.lk');
define('WAITING_MESSAGE_START_ITEM', 'Please wait for start to next item.');
define('ONLINE_BID_UNHOLD_MESSAGE', 'Bidding Starting…');
define('ONLINE_BID_HOLD_MESSAGE', 'Bidding Timed Out...');
define('ONLINE_BID_HOLD_MESSAGE2', 'Bidding Not Started');

//Video & Image View
define('VIDEO_VIEW', 1);
define('IMAGE_VIEW', 0);

//currency format
define('LKR', 'Rs. ');



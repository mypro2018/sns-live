<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class PermissonFlush extends Command
{
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permisson:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'turncate entire permisson table CAUTION';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
         parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sure = $this->choice('Are you sure?', ['Y', 'N']); 
        if($sure=="Y"){
            DB::table('permissions')->truncate();
        }  
        $this->info('all permissions flushed'); 
    }
}

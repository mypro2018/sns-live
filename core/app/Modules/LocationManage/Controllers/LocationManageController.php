<?php namespace App\Modules\LocationManage\Controllers;


/**
 * LocationManageController class
 * @author Author <lahirumadhusankha0@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\LocationManage\BusinessLogics\Logic;
use Exception;
use Response;

class LocationManageController extends Controller {

    protected $location;
    public function __construct(Logic $logic){
        $this->location = $logic;
    }
    /**
     * This function is used to show add location form
     *
     * @return Response
     */
	public function index()
	{
		return view("LocationManage::add");
	}


    /**
     * This function used to add location
     * @param
     * @return
     */
    public function addLocation(Request $request){
        //validate user input
        $this->validate($request,[
            'location_code' => 'required|unique:sns_auction_location,location_code',
            'location_name' => 'required',
            'email'          => 'Email',
            'contact'       => 'regex:/(94)[0-9]{9}/'
        ]);
        try {
            $supplier = $this->location->addLocation([
                'name'          => $request->location_name,
                'location_code' => $request->location_code,
                'address'       => $request->address,
                'contact_no'    => $request->contact,
                'email'         => $request->email,
                'remark'        => $request->remark,
                'gps_location'  => $request->point,
                'point_address' => $request->map_address
            ]);

            return redirect('location/add')->with([
                'success' => true,
                'success.message' => 'Location added successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return redirect('location/add')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }

    }



    /**
     * This function is used to display all locations
     * @return Response
     */
    public function listView(){
        $location_details = $this->location->getAllLocation();
        return view("LocationManage::list")->with(['location_details' => $location_details,'location' => '']);
    }


    /**
     * This function is used to search locations
     * @return Response
     */

    public function searchLocation(Request $request){
        $location_details = $this->location->searchLocation($request->get('location'));
        return view("LocationManage::list")->with(['location_details' => $location_details,'location' => $request->get('location')]);
    }


    /**
     * This function is used to display location more details
     * @return Response
     */
    public function getLocationDetails(Request $request){
        if($request->ajax()) {
            $location_details = $this->location->getLocationDetails($request->get('location_id'));
            return Response::json(['details' => $location_details]);
        }else{
            return Response::json([]);
        }
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * This function is used to display the edit fields.
     *
     * @param  int  $location_id
     * @return Response
     */
    public function edit($location_id){
        $location_details = $this->location->getLocationDetails($location_id);
        if($location_details) {
            return view("LocationManage::edit")->with(['location_details' => $location_details]);
        }else{
            return response()->view('errors.404');
        }
    }


    /**
     * This function used to add location
     * @param
     * @return
     */
    public function update($id,Request $request){
        //validate user input
        $this->validate($request,[
            'location_name' => 'required',
            'email'          => 'Email',
            'contact'       => 'regex:/(94)[0-9]{9}/'
        ]);
        try {
            $supplier = $this->location->updateLocation($id,[
                'name'          => $request->location_name,
                'address'       => $request->address,
                'contact_no'    => $request->contact,
                'email'         => $request->email,
                'remark'        => $request->remark,
                'gps_location'  => $request->point,
                'point_address' => $request->map_address
            ]);

            return redirect('location/list')->with([
                'success' => true,
                'success.message' => 'Location updated successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return back()->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

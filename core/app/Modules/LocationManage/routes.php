<?php


Route::group(['middleware' => ['auth']], function() {
    Route::group(array('prefix'=>'location','namespace' => 'App\Modules\LocationManage\Controllers'), function() {
        /**
         * GET Routes
         */

        Route::get('list', [
            'as' => 'location.add', 'uses' => 'LocationManageController@listView'
        ]);

        Route::get('add', [
            'as' => 'location.add', 'uses' => 'LocationManageController@index'
        ]);

        Route::get('edit/{id}', [
            'as' => 'location.add', 'uses' => 'LocationManageController@edit'
        ]);

        Route::get('search', [
            'as' => 'location.add', 'uses' => 'LocationManageController@searchLocation'
        ]);

        Route::get('details',[
            'as' => 'location.add','uses' => 'LocationManageController@getLocationDetails'
        ]);

        /**
         * POST Routes
         */

        Route::post('add', [
            'as' => 'location.add', 'uses' => 'LocationManageController@addLocation'
        ]);

        Route::post('edit/{id}', [
            'as' => 'location.add','uses' => 'LocationManageController@update'
        ]);
    
    });
});
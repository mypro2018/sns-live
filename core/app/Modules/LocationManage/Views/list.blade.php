@extends('layouts.back_master') @section('title','Location List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">

<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}
.box {
    margin-bottom: 5px !important;
}
.fa-lg{
    font-size: 1em;
}
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Location
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Location List</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
	    <div class="box-body">
            <form role="form" method="get" action="{{url('location/search')}}">
               <div class="form-group" style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
                   <div class="row">
                       <div class="col-md-4 col-sm-6 col-xs-12">
                           <div class="form-group">
                               <input type="text" class="form-control input-sm" name="location" placeholder="Search Location" value="{{$location}}">
                           </div>
                       </div>
                   </div>
                    <div class="row">
                      <div class="col-md-3 col-md-offset-9">
                         <div class="pull-right">
                            <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                            <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                         </div>
                      </div>
                  </div>
               </div>
            </form>
        </div>
    </div>
    <div class="col refresh">
        <div class="box box-widget box-list box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Location List</h3>
                <div class="box-tools pull-right">
                <a href="{{url('location/add')}}" class="btn bg-purple btn-sm pull-right" style="margin-top: 2px;">New Location</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="20%">Location Name</th>
                        <th width="15%">Contact No</th>
                        <th width="10%">Created At</th>
                        <th width="5%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                        @if(count($location_details) > 0)
                            @foreach($location_details as $result_val)
                                @include('LocationManage::template.location')
                            <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="6" class="text-center">No data found.</td></tr>
                        @endif
                        </tbody>
                    </table>
                    @if($location_details != null)
                        <div style="float: right;">{!! $location_details->render() !!}</div>
                    @endif
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACR62kf6B4-HUntWXtZQRINFL0D8uyEBI" type="text/javascript"></script>
<script type="text/javascript">
/*This function is used to view location details in popup model*/
function viewDetail(location_id) {
    var content='<div class="row">'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Location Code</label>'+
                        '<input type="text" class="form-control input-sm code" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Location Name</label>'+
                        '<input type="text" class="form-control input-sm name" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Contact No</label>'+
                        '<input type="text" class="form-control input-sm contact" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Address</label>'+
                        '<textarea class="form-control input-sm address" rows="2" readonly></textarea>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Map Address</label>'+
                        '<textarea class="form-control input-sm map_address" rows="1" readonly></textarea>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>E-mail</label>'+
                        '<input type="text" class="form-control input-sm mail" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Note</label>'+
                        '<textarea class="form-control input-sm remark" rows="2" readonly></textarea>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group media-sec">'+
                        '<label>Map</label>'+
                        '<div id="googleMap" style="width:100%;height:200px;"></div>'+
                    '</div>'+
                '</div>';
    $('.refresh').addClass('panel-refreshing');
    $.ajax({
        url: "{{URL::to('location/details')}}",
        method: 'GET',
        data: {'location_id': location_id},
        async: false,
        success: function (data) {   
            $.confirm({
                title: 'Location Details',
                theme: 'material',
                content: content,
                columnClass: 'col-md-5 col-md-offset-4',
                onContentReady: function () {
                    var self = this;
                    self.$content.find('.code').val(data.details[0].location_code);
                    self.$content.find('.name').val(data.details[0].name);
                    self.$content.find('.contact').val(data.details[0].contact_no);
                    self.$content.find('.address').html(data.details[0].address);
                    self.$content.find('.map_address').val(data.details[0].point_address);
                    self.$content.find('.mail').val(data.details[0].email);
                    self.$content.find('.remark').html(data.details[0].remark);
                    var fields = data.details[0].gps_location.split(',');
                    var latlng = new google.maps.LatLng(fields[0],fields[1]);
                    var map = new google.maps.Map(document.getElementById('googleMap'), {
                        center: latlng,
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: 'Set Place...',
                    draggable: false
                });
            },buttons: {
              close: function () {
                  $('.refresh').removeClass('panel-refreshing');
              }
            }
          });
        },
        error: function () {
            alert('error');
        }
    });
}
</script>
@stop

@extends('layouts.back_master') @section('title','Add Location')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">

<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Location
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('location/list')}}}">Location List</a></li>
		<li class="active">Add Location</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Location</h3>
			<!--<div class="box-tools pull-right">
				<a href="{{url('location/list')}}" class="btn btn-warning btn-sm" style="margin-top: 2px;">Location List</a>
			</div>-->
		</div>
		<div class="box-body">
		    <form role="form" class="form-horizontal form-validation" method="post">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('location_code')) has-error @endif">
                    <label class="col-sm-2 control-label">Location Code <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="location_code" placeholder="Location Code" value="{{Input::old('location_code')}}">
                        @if($errors->has('location_code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('location_code')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('location_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Location Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="location_name" placeholder="Location Name" value="{{Input::old('location_name')}}">
                        @if($errors->has('location_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('location_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('contact')) has-error @endif">
                    <label class="col-sm-2 control-label">Contact No</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="contact" placeholder="94711234567" value="{{Input::old('contact')}}">
                        @if($errors->has('contact'))
                            <label id="label-error" class="error" for="label">{{$errors->first('contact')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('email')) has-error @endif">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="email" placeholder="Email Address" value="{{Input::old('email')}}">
                        @if($errors->has('email'))
                            <label id="label-error" class="error" for="label">{{$errors->first('email')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Location Address</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="1" name="address" placeholder="Location Address">{{Input::old('address')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Map Address</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="1" name="map_address" id="map_address" placeholder="Map Address" readonly>{{Input::old('map_address')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Note</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="1" name="remark" placeholder="Note">{{Input::old('remark')}}</textarea>
                    </div>
                </div>
                <input type="hidden" class="form-control" name="point" id="point" value="{{Input::old('point')}}">
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                         <span class="text-muted pull-right"><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                 </div>
                <div class="form-group">
                    <div class="col-sm-10 col-md-offset-2">
                        <div id="googleMap" style="width:100%;height:400px;"></div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <span><em><span class="require">*</span> Indicates required field</em></span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</section>



@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/jquery-multiselect/js/jquery.multi-select.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACR62kf6B4-HUntWXtZQRINFL0D8uyEBI" type="text/javascript"></script>

<script type="text/javascript">
 window.onload = function() {
        var point = '{{Input::old('point')}}';
        if(point != ''){
             var fields = point.split(',');
            var latlng = new google.maps.LatLng(fields[0],fields[1]);
        }else{
             var latlng = new google.maps.LatLng(7.514980942395872,80.7110595703125);
        }

        var map = new google.maps.Map(document.getElementById('googleMap'), {
            center: latlng,
            zoom: 9,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Set Place...',
            draggable: true
        });

        google.maps.event.addListener(marker, 'drag', function(a) {
            markerEvent(a);
        });
        google.maps.event.addListener(marker, 'click', function(a) {
            markerEvent(a);
        });
    };

    function markerEvent(a){
        var latlng = new google.maps.LatLng(a.latLng.lat(),a.latLng.lng());
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    document.getElementById('map_address').value = results[0].formatted_address;
                    document.getElementById('point').value=a.latLng.lat()+','+a.latLng.lng();
                }
            }
        });
    }
</script>
@stop

<?php namespace App\Modules\LocationManage\Models;

/**
 *
 * LocationManage Model
 * @author Author <lahirumadhusankha0@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */

use Illuminate\Database\Eloquent\Model;


class LocationManage extends Model {

    /**
     * The sns_location table associated this model.
     */
    protected $table = 'sns_auction_location';
    public $timestamps = true;
    protected $guarded = ['id'];

}

<?php namespace App\Modules\LocationManage\BusinessLogics;

/**
 * Business Logics
 * Define all the busines logics in here
 * @author Author <lahirumadhusankha0@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */
use Illuminate\Database\Eloquent\Model;
use App\Modules\LocationManage\Models\LocationManage;
use Illuminate\Support\Facades\DB;
use Exception;

class Logic extends Model {

    /**
     * This function is used to add a location
     * @param array $data Array of data that needed to add location
     * @return object Location object if success, exception otherwise
     */

    public function addLocation($data){

        DB::transaction(function() use($data){
            $location = LocationManage::create($data);
            if($location){
                return $location;
            }else{
                throw new Exception("Error occured while adding new location");
            }
        });
    }

    /**
     * This function is used to get location list
     * @parm -
     * @return location object
     */
    public function getLocationList(){
        $location_list = LocationManage::select('id','name','location_code')->get();
        if(count($location_list) > 0){
            return $location_list;
        }else{
            return [];
        }
    }


    /**
     * This function is used to get all location details
     * @parm -
     * @return location object
     */
    public function getAllLocation(){
        return $all_location = LocationManage::paginate(10);
    }


    /**
     * This function is used to search location
     * @param integer $location_id
     * @response location object
     */

    public function searchLocation($location){
        return $search_location = LocationManage::where('name','LIKE', '%' . $location . '%')->paginate(10);
    }


    /**
     * This function is used to get all location details
     * @parm  integer $location_id that need to get location details
     * @return supplier object
     */
    public function getLocationDetails($location_id){
        $location_details = LocationManage::where('id','=',$location_id)->get();
        if(count($location_details) > 0){
            return $location_details;
        }else{
            return false;
        }
    }


    /**
     * This function is used to update location
     * @param integer $location_id and array $data
     * @response is boolean
     */
    public function updateLocation($location_id,$data){
        DB::transaction(function() use($location_id,$data){
            $update_location = LocationManage::where('id',$location_id)->update($data);
            if($update_location){
                return true;
            }else{
                throw new Exception("Error occurred while updating location");
            }
        });
    }

}

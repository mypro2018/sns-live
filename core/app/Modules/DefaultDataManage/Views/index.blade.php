@extends('layouts.back_master') @section('title','Add Default Data')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">

<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Default Data
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Add Default Data</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Default Data</h3>
		</div>
		<div class="box-body">
		    <form role="form" class="form-horizontal form-validation" method="post">
                {!!Form::token()!!}
                @if(sizeof($defaultData) > 0)
                    @foreach($defaultData as $key => $value)
                        <div class="form-group @if($errors->has('btn_value')) has-error @endif">
                            <label class="col-sm-2 control-label">Button #{{$value->id}} <span class="require">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control input-sm" name="btn_value[{{$value->id}}]" placeholder="Value" value="{{$value->amount}}">
                                @if($errors->has('btn_value'))
                                    <label id="label-error" class="error" for="label">{{$errors->first('btn_value')}}</label>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <span><em><span class="require">*</span> Indicates required field</em></span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</section>



@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/jquery-multiselect/js/jquery.multi-select.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACR62kf6B4-HUntWXtZQRINFL0D8uyEBI" type="text/javascript"></script>

<script type="text/javascript">
 window.onload = function() {
        var point = '{{Input::old('point')}}';
        if(point != ''){
             var fields = point.split(',');
            var latlng = new google.maps.LatLng(fields[0],fields[1]);
        }else{
             var latlng = new google.maps.LatLng(7.514980942395872,80.7110595703125);
        }

        var map = new google.maps.Map(document.getElementById('googleMap'), {
            center: latlng,
            zoom: 9,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Set Place...',
            draggable: true
        });

        google.maps.event.addListener(marker, 'drag', function(a) {
            markerEvent(a);
        });
        google.maps.event.addListener(marker, 'click', function(a) {
            markerEvent(a);
        });
    };

    function markerEvent(a){
        var latlng = new google.maps.LatLng(a.latLng.lat(),a.latLng.lng());
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    document.getElementById('map_address').value = results[0].formatted_address;
                    document.getElementById('point').value=a.latLng.lat()+','+a.latLng.lng();
                }
            }
        });
    }
</script>
@stop

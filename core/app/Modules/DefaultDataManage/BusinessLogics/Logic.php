<?php namespace App\Modules\DefaultDataManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Models\ButtonPad;

class Logic extends Model {

	/**
	 * Get bid button values
	 */
	public function getValues(){
		return $values = ButtonPad::all();
	}

	/**
	 * Update bid button values
	 */

	public function saveValues($request){
		if($request && sizeof($request->get('btn_value'))){
			foreach($request->get('btn_value') as $key => $value){
				$saveValues 			= ButtonPad::find($key);
				$saveValues->amount 	= $value;
				$saveValues->created_at = date('Y-m-d H:i:s');
				$saveValues->updated_at = date('Y-m-d H:i:s');
				$saveValues->save();
				if(!$saveValues){
					throw new TransactionException('Button value not updated', 101);
				}
			}
		}else{
			throw new TransactionException('request input cannot be empty', 101);
		}
	}

}

<?php namespace App\Modules\DefaultDataManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\DefaultDataManage\BusinessLogics\Logic AS DefaultData;
use DB;

class DefaultDataManageController extends Controller {

	protected $default;

	public function __construct(DefaultData $default)
	{
        $this->default = $default;
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$defaultData = $this->default->getValues();
		return view("DefaultDataManage::index")->with(compact('defaultData'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{	
		DB::transaction(function () use($request) {
			$saveDefaultData = $this->default->saveValues($request);
		});
		return redirect( 'default-data/index' )->with([
			'success' => true,
			'success.message' => 'Values added successfully!',
			'success.title'   => 'Success..!'
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

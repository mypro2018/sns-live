<?php


Route::group(['middleware' => ['auth']], function() {
    Route::group(array('prefix'=>'default-data', 'namespace' => 'App\Modules\DefaultDataManage\Controllers'), function() {
        
        /**
         * GET Routes
         */

        Route::get('index', [
            'as' => 'default.index', 'uses' => 'DefaultDataManageController@index'
        ]);

        Route::post('index', [
            'as' => 'default.index', 'uses' => 'DefaultDataManageController@store'
        ]);


    });
});
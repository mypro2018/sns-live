<?php namespace App\Modules\CustomerManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\CustomerManage\BusinessLogics\Logic as CustomerLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use App\Modules\FrontEnd\BusinessLogics\Logic AS FrontLogic;
use Illuminate\Http\Request;
use Sentinel;
use Exception;
use Response;
use DB;
class CustomerManageController extends Controller {

	protected $customer;
    protected $auction;
	protected $front;

	public function __construct(CustomerLogic $customer,AuctionLogic $auction, FrontLogic $front)
	{
        $this->customer = $customer;
        $this->auction  = $auction;
		$this->front    = $front;
    }

	/**
	 * Display a customer details.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view("CustomerManage::view");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addCustomer(Request $request)
	{
		try {
			// decode json object
			$form = json_decode($request->register);

			$emailCheck = $this->customer->getCustomerByEmail($form->contact_no, $form->nic);

			if($emailCheck){
				return Response::json([
					'error' => true,
					'message' => 'Email already exists. Please try again.',
					'title' => 'Well done!'
				]);
			}else{
				$customer = $this->customer->add([
	                'fname'          	=> $form->first_name,
	                'lname'  			=> $form->last_name,
	                'email'    			=> $form->email,
	                'contact_no'    	=> $form->contact_no,
	                'address_1'  		=> $form->address_1,
	                'address_2'    		=> (isset($form->address_2))?$form->address_2:'',
	                'city'    			=> $form->city,
	                'zip_postal_code'   => $form->zip_code,
	                'country_id'   		=> $form->country->id,
	                'password'    		=> $form->password
	            ],$form);

				return Response::json([
					'success' => true,
					'message' => 'Registration completed. Please check your email.',
					'title' => 'Well done!'
				]);
			}
		}
		catch(Exception $e){

        	return Response::json([
				'error' => true,
				'message' => $e->getMessage(),
				'title' => 'Ops!'
			]);
        }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addWatchlist(Request $request)
	{
		try {
			$user = Sentinel::getUser();
			
			// decode json object
			$form = json_decode($request->watchlist);

			$watchlist = $this->customer->addWatchlist([
                'user_id'          	=> $user->id,
                'sns_auction_id'  	=> $form->auction_id,
                'sns_item_id'    	=> $form->item_id
            ]);

			return Response::json([
				'success' 	=> true,
				'id' 		=> $watchlist->id
			]);
		}
		catch(Exception $e){
        	return Response::json([
				'error' 	=> true,
				'message' 	=> $e->getMessage(),
				'title' 	=> 'Ops!'
			]);
        }
	}

    /**
     * Display a listing of the customers.
     *
     * @return Response
     */
    public function customerList()
    {
		$customer_list    = $this->customer->customerList();
		$auction_list  	  = $this->auction->getAllAuctions();
		$all_customer  	  = $this->customer->customerCount();
		$online_customer  = $this->customer->onlineCustomerCount();
		$floor_customer   = $this->customer->floorCustomerCount();
        return view("CustomerManage::list")->with([
			'customer_list'  => $customer_list,
			'customer' 		 => '',
			'auction' 		 => $auction_list,
			'select_auction' => '',
			'from' 			 => '',
			'to' 			 => '',
			'register_type'	 => '',
			'all_customer'	 => $all_customer,
			'online_customer'=> $online_customer,
			'floor_customer' => $floor_customer
		]);
    }


	/**
     * This Function is used to search customer
     * @param  
     * @return Response
     */
    public function searchCustomer(Request $request)
    {
		$auction_list 		= $this->auction->getAllAuctions();
		$customer_list 		= $this->customer->searchCustomer($request);
		$all_customer  		= $this->customer->customerCount($request->get('auction'));
		$online_customer  	= $this->customer->onlineCustomerCount($request->get('auction'));
		$floor_customer   	= $this->customer->floorCustomerCount($request->get('auction'));
		return view("CustomerManage::list")->with([
        	'customer_list' 	=> $customer_list,
        	'customer' 			=> $request->get('customer'),
        	'auction' 			=> $auction_list,
			'select_auction' 	=> $request->get('auction'),
			'from' 			    => $request->get('from'),
			'to' 			    => $request->get('to'),
			'register_type'		=> $request->get('register_type'),
			'all_customer'	    => $all_customer,
			'online_customer'	=> $online_customer,
			'floor_customer' 	=> $floor_customer
        ]);
    }


    /**
     * This function is used to customer auction details
     * @param integer $customer_id
     * @return
     */
    public function customerAuctionList($customer_id){
        $customer 	    = $this->customer->getCustomerByUserId($customer_id);
        $customer_list  = $this->customer->getCustomerSummaryAuctionDetails($customer_id);
		$customer_types	= $this->customer->getCustomerTypes();
		$new_arr	    = [];

        foreach($customer_list as $result){
            $array = [];
            foreach($result->get_auction->auction_details_summary as $detail){
                $dd = [];
                if($detail->highest_bid && $detail->highest_bid->win_bid && $customer_id ==  $detail->highest_bid->sns_customer_id){
                    $dd = $detail->highest_bid->win_bid->id;
                    array_push($array,$dd);
                }
            }
            $result->win_count = count($array);
        }
		foreach($customer->customer_allowed_type as $key => $value){
			array_push($new_arr, $value->customer_type_id);
		}
        return view("CustomerManage::view")->with([
			'customer' 		 => $customer, 
			'details'  	 	 => $customer_list,
			'customer_types' => $customer_types,
			'checked_types'	 => $new_arr
		]);
    }

    /**
     * This function is used to auction item details
     * @param integer $customer_id integer $auction_id
     * @return
     */

    public function getItemDetails($auction_id,$customer_id){
        $auction 	  = $this->auction->getAuction($auction_id);
        $item_details = $this->auction->getItemDetails($auction_id,$customer_id);
        return view("CustomerManage::auction")->with(['auction' => $auction,'details' => $item_details,'uid' => $customer_id]);
	}
	

	/**
	 * POCKET BALANCE
	 */

	public function pocketBalance($id = null){

		$pocket_balance = $this->customer->getPocketBalance($id);
		return view('CustomerManage::pocket-balance')->with(['pocket_balance' => $pocket_balance]);
	}


	/**
	 * ADD POCKET BALANCE
	 */
	
	public function addPocketBalance(Request $request, $id){
		$pocket_balance = $this->customer->addPocketBalance($request);
		return view('CustomerManage::pocket-balance')->with(['pocket_balance' => $pocket_balance]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * @param  array  $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		try {
			// decode json object
			$form = json_decode($request->register);
			$user = Sentinel::getUser();
			if(isset($form->password)) {
				$customer = $this->customer->update($form->id, [
	                'fname'          	=> $form->first_name,
	                'lname'  			=> $form->last_name,
	                'email'    			=> $form->email,
	                'telephone_no'    	=> $form->contact_no,
	                'address_1'  		=> $form->address_1,
	                'address_2'    		=> (isset($form->address_2))?$form->address_2:'',
	                'city'    			=> $form->city,
	                'zip_postal_code'   => $form->zip_code,
	                'country_id'   		=> $form->country->id
	            ],$form,$user->id);
			}else {
				$customer = $this->customer->update($form->id, [
	                'fname'          	=> $form->first_name,
	                'lname'  			=> $form->last_name,
	                'email'    			=> $form->email,
	                'telephone_no'    	=> $form->contact_no,
	                'address_1'  		=> $form->address_1,
	                'address_2'    		=> $form->address_2,
	                'city'    			=> $form->city,
	                'zip_postal_code'   => $form->zip_code,
	                'country_id'   		=> $form->country->id
	            ],$form,$user->id);
			}

			if($customer){
				$fileName  = $customer->nic_image_path;
				$fileName2 = $customer->proof_image_path;

				if($request->hasFile('file')) {
					$file 	         = $request->file('file'); //get file
					$extension       = $file->getClientOriginalExtension(); //get extension
					$fileName        = 'customer-' .$form->id.'.'.$extension; //set file name	
					$destinationPath = storage_path('uploads/images/customer/nic-licence'); // destination	
					$file->move($destinationPath, $fileName);//move to file
				}
				//Upload proof image
				if($request->hasFile('file2')) {
					$file2 	          = $request->file('file2'); //get file
					$extension2       = $file2->getClientOriginalExtension(); //get extension
					$fileName2        = 'customer-' .$form->id.'.'.$extension2; //set file name	
					$destinationPath2 = storage_path('uploads/images/customer/proof'); // destination	
					$file2->move($destinationPath2, $fileName2);//move to file
				}
				$customerUpdate = $this->customer->updateCustomerUploadImages($form->id, $fileName, $fileName2);
				if($customerUpdate){
					return Response::json([
						'success' => true,
						'message' => 'Updated successfully.',
						'title'   => 'Job well done!'
					]);
				}else{
					return Response::json([
						'error' => true,
						'message' => 'Error occurred when updating customer.',
						'title' => 'Ops!'
					]);
				}
			}else{
				return Response::json([
					'error' => true,
					'message' => 'Error occurred when updating customer.',
					'title' => 'Ops!'
				]);
			}
        }catch(Exception $e){
        	return Response::json([
				'error' => true,
				'message' => $e->getMessage(),
				'title' => 'Ops!'
			]);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	//band customer
	public function bandCustomer(Request $request){
		if($request->ajax()){
			$band_customer = $this->customer->bandCustomer($request->get('customer_id'), $request->get('status'));
			return response()->json(['status' => 'success', 'result' => $band_customer]);
		}else{
			return response()->json(['status' => 'error']);
		}
	}


	//view customer edit
	public function editCustomer($id){
		if(isset($id)){
			$new_arr = [];
			$customer = $this->customer->getCustomerByUserId($id);
			foreach($customer->customer_allowed_type as $key => $value){
				array_push($new_arr, $value->customer_type_id);
			}
			$customer_types	= $this->customer->getCustomerTypes();
			$countries      = $this->front->allCountries();
			$cities         = $this->front->allCities();
			return view("CustomerManage::edit")->with([
				'customer' 		 => $customer, 
				'customer_types' => $customer_types,
				'checked_types'  => $new_arr,
				'countries'		 => $countries,
				'cities'         => $cities
			]);
		}else{
			return redirect('customer/list')->with([
				'error'   		=> true,
				'error.message' => 'customer should have unique id',
				'error.title' 	=> 'Error..!',
			]); 
		}
	}

	//update customer -backend
	public function updateCustomer(Request $request, $id){
		$this->validate($request,[
            'first_name' => 'required'
        ]);

		try{

			$result = DB::transaction(function() use($request, $id) {
				$customer = $this->customer->updateCustomer($request, $id);
				if($customer){
					//upload customer nic or licence image
					$fileName1 = $customer->nic_image_path;
					$fileName2 = $customer->proof_image_path;
					if($request->hasFile('file1')){
						$file 	         = $request->file('file1'); //get file
						$extension 		 = $file->getClientOriginalExtension(); //get extension
						$fileName1       = 'customer-'.$customer->id.'.'.$extension; //set file name
						$destinationPath = storage_path('uploads/images/customer/nic-licence'); // destination
						$file->move($destinationPath, $fileName1);//move to file
					}
					//upload customer proof file image
					if($request->hasFile('file2')){
						$file 	         = $request->file('file2'); //get file
						$extension 		 = $file->getClientOriginalExtension(); //get extension
						$fileName2  	 = 'customer-'.$customer->id.'.'.$extension; //set file name
						$destinationPath = storage_path('uploads/images/customer/proof'); // destination
						$file->move($destinationPath, $fileName2);//move to file
					}
					$updateFIlePath = $this->customer->updateCustomerUploadImages($customer->id, $fileName1, $fileName2);
					if(!$updateFIlePath){
						throw new Exception("Error occurred while updating uploaded files");
					}
					return $customer;
				}else{
					throw new Exception("Error occurred while updating customer details");
				}
			});

			if($result){
				return redirect('customer/edit/'.$id)->with([
					'success'   	  => true,
					'success.message' => 'Customer updated successfully!',
					'success.title'   => 'Success..!'
				]);
			}else{
				return redirect('customer/edit/'.$id)->with([
					'error'   		=> true,
					'error.message' => 'Error..!',
					'error.title' 	=> 'Error..!',
				]);
			}
		}catch(\Exception $e){
			return redirect('customer/edit/'.$id)->with([
				'error'   		=> true,
				'error.message' => $e->getMessage(),
				'error.title' 	=> 'Error..!',
			]);
		}
		
	}

}

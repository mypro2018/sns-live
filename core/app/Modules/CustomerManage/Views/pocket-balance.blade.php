@extends('layouts.back_master') @section('title','Pocket Balance')
@section('css')
<style>
.card_no{
  padding-left:15px;
  padding-right:15px;
  font-size:10px;
}
</style>
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Pocket Balance
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('customer/list')}}}">Customer List</a></li>
		<li class="active">Pocket Balance</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <form role="form" class="form-horizontal form-validation" method="post" enctype="multipart/form-data">
      <div class="box-body">
        {!!Form::token()!!}
        <div class="form-group form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label">Register No</label>
            <div class="col-sm-4">
              <input type="text" class="form-control input-sm" name="id" placeholder="Customer" value="{{$pocket_balance->id}}" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Customer</label>
            <div class="col-sm-4">
              <input type="text" class="form-control input-sm" name="name" placeholder="Customer" value="{{$pocket_balance->fname}}" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Pocket Balance</label>
            <div class="col-sm-4">
              @if(sizeof($pocket_balance->pocket_balance) > 0)                
                <div class="input-group">
                  <div class="input-group-addon">LKR</div>
                  <input type="text" class="form-control" id="current_amount" name="current_amount" placeholder="Amount" value="{{number_format($pocket_balance->pocket_balance->balance, 2)}}" readonly>
                </div>
              @else
                <div class="input-group">
                  <div class="input-group-addon">LKR</div>
                  <input type="text" class="form-control" id="current_amount" name="current_amount" placeholder="Amount" value="0.00" readonly>
                </div>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Transaction Type</label>
            <div class="col-sm-4">
              <label class="radio-inline">
                <input type="radio" name="type" id="inlineRadio1" value="1" checked> Deposit
              </label>
              <label class="radio-inline">
                  <input type="radio" name="type" id="inlineRadio2" value="0" @if(sizeof($pocket_balance->pocket_balance) == 0) disabled @endif> Withdrawal
              </label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Amount</label>
            <div class="col-sm-4">
              <div class="input-group">
                <div class="input-group-addon">LKR</div>
                  @if(sizeof($pocket_balance->pocket_balance) > 0)
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" required>
                  @else
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" required>
                  @endif
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="box-footer">
          <div class="row">
              <div class="col-sm-6">
                  <span><em><span class="require">*</span> Indicates required field</em></span>
              </div>
              <div class="col-sm-6 text-right">
                  <button type="submit" class="btn btn-sm bg-purple pull-right"><i class="fa fa-floppy-o"></i> Save</button>
              </div>
          </div>
      </div>
    </form>
    <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Tranasaction List</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
            <thead>
              <tr>
                <th class="text-center" width="5%">#</th>
                <th class="text-center">Register No</th>
                <th class="text-center">Customer Name</th>
                <th class="text-center" >Amount</th>
                <th class="text-center">Date</th>
                <th class="text-center">Status</th>                   
              </tr>
            </thead>
            <tbody>
            <?php $i = 1;?>
            @if(sizeof($pocket_balance->pocket_balance) > 0 && sizeof($pocket_balance->pocket_balance->pocket_balance_transaction) > 0)
              @foreach($pocket_balance->pocket_balance->pocket_balance_transaction as $transaction)
              <tr>
                <td class="text-center" widtd="5%">{{$i}}</td>
                <td class="text-center">{{$pocket_balance->id}}</td>
                <td class="text-center" >{{$pocket_balance->fname}}</td>
                <td class="text-center">LKR {{number_format($transaction->transaction_amount, 2)}}</td>
                <td class="text-center">{{$transaction->created_at}}</td>
                @if($transaction->transaction_type == 1)
                  <td class="text-center"><i class="fa fa-plus-circle" aria-hidden="true" style="font-size: 15px;color: green;"></i></td>
                @else
                  <td class="text-center"><i class="fa fa-minus-circle" aria-hidden="true" style="font-size: 15px;color: red;"></i></td>
                @endif                   
              </tr>
              <?php $i++ ?>
              @endforeach
            @else
              <tr><td colspan="7" class="text-center">No data found.</td></tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>

          
 </div>
</section>
@stop


@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/jquery-multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/dist/jquery-slider/jssor.slider-24.1.5.min.js')}}"></script>
<script type="text/javascript">

function viewDetail(auction_id,customer_id,status) {
  $('.content').addClass('panel-refresh');
  var title   = '';
  var content = '';
 
  if(status == '{{UNBAND}}'){
    title   = 'Remove Customer Band';
    content = 'Remove Band of the Customer';
  }

  if(status == '{{BAND}}'){
    title   = 'Customer Band Confirmation';
    content = 'Customer Band to the Auction';
  }

  $.confirm({
    title         : title,
    content       : content,
    icon          : 'fa fa-question-circle',
    animation     : 'scale',
    closeAnimation: 'scale',
    opacity       : 0.5,
      buttons: {
        'confirm': {
            text      : 'Confirm',
            btnClass  : 'btn-sm bg-purple',
            action: function () {
              $(".content").addClass('panel-refreshing');
              $.ajax({
                url   : "{{URL::to('auction/band/aution')}}",
                method: 'GET',
                data  : {
                  'auction_id' : auction_id,
                  'customer_id': customer_id,
                  'status'     : status
                },
                async : false,
                success: function (data) {
                  $(".content").removeClass('panel-refreshing');
                  if(data.status == 'success'){
                    toastr.success('Customer Band to the Auction Sucessfully');
                    location.reload();
                  }else{
                    toastr.error('Please try again.');
                  }

                },
                error: function () {
                  $('.content').removeClass('panel-refresh');
                  alert('error');
                }
              });
            }
          },
          cancel: function () {
            $(".content").removeClass('panel-refreshing');
          }
        }
      });
}
</script>
@stop



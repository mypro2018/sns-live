<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td class=""><span class="label label-default card_no">{{$result_val->id}}</span></td>
     @if($result_val->card_type == ONLINE_CARD)
     <td class="text-center"><span class="label label-info card_no">{{$result_val->card_no}}</span></td>
     @elseif($result_val->card_type == FLOOR_CARD)
     <td class="text-center"><span class="label label-success card_no">{{$result_val->card_no}}</span></td>
     @else
     <td class="text-center">-</td>
     @endif
     <td class="text-center">{{$result_val->auction_count?:'-'}}</td>
     <td>{{$result_val->customer_name}}</td>
     <td>{{$result_val->contact_no?:'-'}}</td>
     <td>{{$result_val->telephone_no?:'-'}}</td>
     <td>{{$result_val->email?$result_val->email:'-'}}</td>
     <td>{{$result_val->address1?$result_val->address1:'-'}} {{$result_val->address2?$result_val->address2:''}}</td>
     <td>{{$result_val->nic?$result_val->nic:'-'}}</td>
     <td>{{$result_val->city?:'-'}}</td>
     <!-- <td>{{$result_val->event_name?$result_val->event_name:'-'}}</td> -->
     <td>{{$result_val->created_at}}</td>
     <!-- <td class="text-center">
        @if($result_val->band_status == BAND)
            <i style="color:red;" class="fa fa-ban view"></i> Yes
        @else
            <i style="color:green;" class="fa fa-check view"></i> No
        @endif
    </td> -->
     <td class="text-center">
        <div class="btn-group">
            <a href="pocket-balance/{{$result_val->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Pocket Balance"><i style="color:#FF8C00;"class="fa fa-dollar pocket-balance"></i></a>
            <a href="view/{{$result_val->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Detail"><i class="fa fa-eye view"></i></a>
            <a href="edit/{{$result_val->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Customer"><i class="fa fa-pencil"></i></a>
            @if($result_val->band_status == BAND)
                <a href="javascript:void(0);" onClick=viewDetail({{$result_val->id}},{{UNBAND}}) class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Band Customer"><i style="color:red;" class="fa fa-lock view"></i></a>
            @else
                <a href="javascript:void(0);" onClick=viewDetail({{$result_val->id}},{{BAND}}) class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Unband Customer"><i style="color:green;" class="fa fa-unlock view"></i></a>
            @endif
        </div>
     </td>
</tr>


@extends('layouts.back_master') @section('title','View Customer')

@section('css')
<!-- toogle -->
<link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" />
<!-- toogle -->
<style>
.card_no{
  padding-left:15px;
  padding-right:15px;
  font-size:10px;
}
.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 50px; }
.toggle.ios .toggle-handle { border-radius: 50px; }
.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
</style>
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Customer
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('customer/list')}}}">Customer List</a></li>
		<li class="active">View Customer</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-body">
      <div class="form-group form-horizontal">
        <div class="form-group">
          <label class="col-sm-2 control-label">S & S Register ID</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="name" placeholder="S & S Register ID" value="{{$customer->id}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">First Name</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="name" placeholder="First Name" value="{{$customer->fname}}" readonly>
          </div>
          <label class="col-sm-2 control-label">Last Name</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="lname" placeholder="Last Name" value="{{$customer->lname}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Contact No</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="contact" placeholder="Contact No" value="{{$customer->contact_no}}" readonly>
          </div>
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="email" placeholder="E-mail" value="{{$customer->email}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Note</label>
          <div class="col-sm-4">
            <textarea class="form-control input-sm" rows="3" name="note" placeholder="Note" readonly>{{$customer->note?:''}}</textarea>
          </div>
          <label class="col-sm-2 control-label">Telephone No</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="telephone_no" placeholder="Telephone No" value="{{$customer->telephone_no?:''}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Address</label>
          <div class="col-sm-4">
            <textarea class="form-control input-sm" rows="3" name="description" placeholder="Customer Address" readonly>{{$customer->address_1}}</textarea>
          </div>
          <label class="col-sm-2 control-label">NIC</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="nic" placeholder="NIC" value="{{$customer->nic}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">City</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="city" placeholder="City" value="{{$customer->city}}" readonly>
          </div>
          <label class="col-sm-2 control-label">Country</label>
          <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="nic" placeholder="NIC" value="{{$customer->country?$customer->country->country_name:''}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Customer Types</label>
          @foreach($customer_types as $key => $value)
            <div class="col-xm-2 col-sm-2 col-md-2 col-lg-2">
                <div class="btn-group btn-group-sm">
                  <input type="checkbox" name="customer_type[]" id="{{ $value->id }}" onclick="return false;" autocomplete="off" value="{{ $value->id }}" @if(in_array($value->id, $checked_types)) checked @endif/>
                  <div class="btn-group btn-group-sm">
                    <label for="{{ $value->id }}" class="btn btn-default">
                      <span class="glyphicon glyphicon-ok"></span>
                      <span></span>
                    </label>
                    <label for="{{ $value->id }}" class="btn btn-default"><!-- active-->
                        {{ $value->name }}
                    </label>
                  </div>
                </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="box">
    <div class="box-header with-border">
       <h3 class="box-title">Customer Auction List</h3>
     </div>
     <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered bordered table-striped table-condensed">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th>Auction</th>
                <th>Card No</th>
                <th>Location</th>
                <th width="8%">Auction Date</th>
                <th>Register Date</th>
                <th>No of Auction Item</th>
                <th>No of Items Won</th>
                <th width="5%">Band</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $i = 1;?>
            @if(count($details) > 0)
              @foreach($details as $result_val)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$result_val->get_auction->event_name}}</td>
                  @if(isset($result_val->card_range) && count($result_val->card_range) > 0)
                    @if(count($result_val->card_range) > 0 && $result_val->card_range->card_type == FLOOR_CARD) 
                      <td><span class="label label-success card_no">{{$result_val->card_no?:'-'}}</span></td>
                    @else
                      <td><span class="label label-info card_no">{{$result_val->card_no?:'-'}}</span></td>
                    @endif
                  @else
                  <td>-</td>
                  @endif
                  <td>
                  @if(count($result_val->get_auction->location) > 0)
                    {{$result_val->get_auction->location->name}}
                  @else
                   {{'-'}} 
                  @endif
                  </td>
                  <td>{{$result_val->get_auction->auction_date}}</td>
                  <td>{{$result_val->created_at}}</td>
                  <td>{{count($result_val->get_auction->auction_details_summary)}}</td>
                  <td>{{$result_val->win_count}}</td>
                  <td class="text-center">@if($result_val->band_status == BAND)
                      <i style="color:red;" class="fa fa-ban view"></i> Yes
                    @else
                      <i style="color:green;" class="fa fa-check view"></i> No
                    @endif
                  </td>
                  <td>
                    <center>
                      <div class="btn-group">
                        <a href="../item/{{$result_val->auction_id}}/{{$result_val->user_id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Detail"><i class="fa fa-eye view"></i></a>
                        @if(isset($result_val->customer) && count($result_val->customer) > 0 && $result_val->customer->band_status == UNBAND)
                          @if($result_val->band_status == BAND)
                            <a href="javascript:void(0);" onClick=viewDetail({{$result_val->auction_id}},{{$result_val->user_id}},{{UNBAND}}) class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Unband Customer"><i style="color:green;" class="fa fa-check view"></i></a>
                          @else
                            <a href="javascript:void(0);" onClick=viewDetail({{$result_val->auction_id}},{{$result_val->user_id}},{{BAND}}) class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Band Customer"><i style="color:red;" class="fa fa-ban view"></i></a>
                          @endif
                        @endif
                      </div>
                    </center>
                  </td>
                </tr>
                <?php $i++;?>
              @endforeach
            @else
              <tr><td colspan="10" class="text-center">No data found.</td></tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @if(count($details) > 0 && count($details) == 10)
    <div class="box-footer">
      <div style="float: right;">{!! $details->render() !!}</div>
    </div>
    @endif
 </div>
</section>
@stop


@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/jquery-multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/dist/jquery-slider/jssor.slider-24.1.5.min.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">

function viewDetail(auction_id,customer_id,status) {
  $('.content').addClass('panel-refresh');
  var title   = '';
  var content = '';
 
  if(status == '{{UNBAND}}'){
    title   = 'Remove Customer Band';
    content = 'Remove Band of the Customer';
  }

  if(status == '{{BAND}}'){
    title   = 'Customer Band Confirmation';
    content = 'Customer Band to the Auction';
  }

  $.confirm({
    title         : title,
    content       : content,
    icon          : 'fa fa-question-circle',
    animation     : 'scale',
    closeAnimation: 'scale',
    opacity       : 0.5,
      buttons: {
        'confirm': {
            text      : 'Confirm',
            btnClass  : 'btn-sm bg-purple',
            action: function () {
              $(".content").addClass('panel-refreshing');
              $.ajax({
                url   : "{{URL::to('auction/band/aution')}}",
                method: 'GET',
                data  : {
                  'auction_id' : auction_id,
                  'customer_id': customer_id,
                  'status'     : status
                },
                async : false,
                success: function (data) {
                  $(".content").removeClass('panel-refreshing');
                  if(data.status == 'success'){
                    toastr.success('Customer Band to the Auction Sucessfully');
                    location.reload();
                  }else{
                    toastr.error('Please try again.');
                  }

                },
                error: function () {
                  $('.content').removeClass('panel-refresh');
                  alert('error');
                }
              });
            }
          },
          cancel: function () {
            $(".content").removeClass('panel-refreshing');
          }
        }
      });
}
</script>
@stop



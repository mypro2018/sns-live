@extends('layouts.back_master') @section('title','Customer List')

@section('css')
<style>
.card_no{
  padding-left:15px;
  padding-right:15px;
  font-size:10px;
  padding-top:5px;
  padding-bottom:5px;
}
</style>
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Customer
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Customer List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
    <form role="form" method="get" action="{{url('customer/search')}}">
      <div class="box-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="customer" placeholder="Search Customer Name,Card No,Contact No,NIC,Email" value="{{$customer}}">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="auction">
                <option value="">-- Search All Auctions --</option>
                @if($auction)
                  @foreach($auction as $key => $val)
                  <option value="{{$val->id}}" @if($val->id == $select_auction) selected @endif>{{$val->event_name}} - {{$val->name}} - {{$val->auction_date}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="register_type">
                <option value="">-- Search Register Type --</option>
                  <option value="{{ONLINE_CARD}}" @if(ONLINE_CARD == $register_type) selected @endif>Online Customer</option>
                  <option value="{{FLOOR_CARD}}"  @if(FLOOR_CARD == $register_type) selected @endif>Floor Customer</option>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" name="from" placeholder="From" value="{{$from}}" autocomplete="off"/>
                <span class="input-group-addon">to</span>
                <input type="text" class="input-sm form-control" name="to" placeholder="To" value="{{$to}}" autocomplete="off"/>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
          <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
        </div>
      </div>
    </form>
  </div>

  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Customer List</h3>
      <div class="pull-right">
        <span class="label label-info card_no">Online Customers ({{count($online_customer)}})</span>
        <span class="label label-success card_no">Floor Customers ({{count($floor_customer)}})</span>
        <span class="label label-warning card_no">All Customers ({{$all_customer}})</span>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
              <thead>
                <tr>
                  <th width="2%">#</th>
                  <th width="3%">No</th>
                  <th width="5%">Card No</th>
                  <th width="10%">Auction Count</th>
                  <th width="10%">Customer Name</th>
                  <th width="5%">Contact No</td>
                  <th width="5%">Telephone No</td>
                  <th width="5%">E-mail</td>
                  <th width="10%">Address</td>
                  <th width="5%">NIC</td>
                  <th width="5%">Note</td>
                  <!-- <th width="10%">Latest Auction</th> -->
                  <th width="10%">Register Date</th>
                  <!-- <th width="5%">Band</th> -->
                  <th width="20%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $i = 1;?>
              @if(count($customer_list) > 0)
                @foreach($customer_list as $key => $result_val)
                  @include('CustomerManage::template.customer')
                <?php $i++;?>
                @endforeach
              @else
                <tr><td colspan="10" class="text-center">No data found.</td></tr>
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    @if(count($customer_list) > 0)
    <div class="box-footer">
      <div style="float: right;">{!! $customer_list->appends($_GET)->render() !!}</div>
    </div>
    @endif
  </div>
</section>
@stop
@section('js')
<script type="text/javascript">

function viewDetail(customer_id,status) {
  $('.content').addClass('panel-refresh');
  var title   = '';
  var content = '';
 
  if(status == '{{UNBAND}}'){
    title   = 'Remove Customer Band';
    content = 'Remove Band of the Customer';
  }

  if(status == '{{BAND}}'){
    title   = 'Customer Band Confirmation';
    content = 'Customer Band';
  }

  $.confirm({
    title         : title,
    content       : content,
    icon          : 'fa fa-question-circle',
    animation     : 'scale',
    closeAnimation: 'scale',
    opacity       : 0.5,
      buttons: {
        'confirm': {
            text      : 'Confirm',
            btnClass  : 'btn-sm bg-purple',
            action: function () {
              $(".content").addClass('panel-refreshing');
              $.ajax({
                url   : "{{URL::to('customer/band')}}",
                method: 'GET',
                data  : {
                  'customer_id': customer_id,
                  'status'     : status
                },
                async : false,
                success: function (data) {
                  $(".content").removeClass('panel-refreshing');
                  if(data.status == 'success'){
                    toastr.success('Customer Band Sucessfully');
                    location.reload();
                  }else{
                    toastr.error('Please try again.');
                  }
                },
                error: function () {
                  $('.content').removeClass('panel-refresh');
                  alert('error');
                }
              });
            }
          },
          cancel: function () {
            $(".content").removeClass('panel-refreshing');
          }
        }
      });
}
</script>
@stop
@extends('layouts.back_master') @section('title','Edit Customer')
@section('css')
<!-- toogle -->
<link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" />
<!-- toogle -->
<style>
.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 50px; }
.toggle.ios .toggle-handle { border-radius: 50px; }
.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
.img-rounded{
    width:100px;
    height:100px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Customer
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('auction/list')}}}">Auction List</a></li>
		<li class="active">Update Customer</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" class="form-horizontal form-validation" method="post"  enctype="multipart/form-data">
    		<div class="box-header with-border">
    			<h3 class="box-title">Edit Customer</h3>
                <a href="{{{url('customer/list')}}}" class="btn btn-sm bg-purple pull-right">Customer List</a>
    		</div>
    		<div class="box-body">
	            {!!Form::token()!!}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-1 notice" style="padding-top:7px;"></div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">S&S Reg No</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="id" placeholder="S&S Reg No" value="{{$customer->id}}" readonly>
                    </div>
                </div>
                <div class="form-group @if($errors->has('first_name')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">First Name <span class="require">*</span></label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{$customer->fname?:''}}">
                        @if($errors->has('first_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('first_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('last_name')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Last Name </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{$customer->lname?:''}}">
                        @if($errors->has('last_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('last_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Address Line 1 </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <textarea class="form-control input-sm" rows="3" name="address1" placeholder="Address">{{$customer->address_1?:''}}</textarea>
                        @if($errors->has('address1'))
                            <label id="label-error" class="error" for="label">{{$errors->first('address1')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Address Line 2</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <textarea class="form-control input-sm" rows="3" name="address2" placeholder="Address">{{$customer->address_2?:''}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">City</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <select name="city" class="form-control chosen iput-sm">
                            <option value="">Select City</option>
                            @if($cities)
                                @foreach ($cities as $city)
                                    <option value="{{$city->name}}" @if($city->name == $customer->city) selected @endif> {{$city->name}} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Country</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <select name="country" class="form-control chosen iput-sm">
                            <option value="">Select Country</option>
                            @if($countries)
                                @foreach ($countries as $country)
                                    <option value="{{$country->id}}" @if($country->id == $customer->country_id) selected @endif> {{$country->country_name}} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group @if($errors->has('contact')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Contact No <span class="require">*</span></label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="contact" placeholder="0712345678" value="{{$customer->contact_no?:''}}" min="0" onkeypress="return event.charCode >= 48">
                        @if($errors->has('contact'))
                            <label id="label-error" class="error" for="label">{{$errors->first('contact')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('telephone_no')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Telephone No </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="number" class="form-control input-sm" name="telephone_no" placeholder="0112345678" value="{{$customer->telephone_no?:''}}" min="0" onkeypress="return event.charCode >= 48">
                        @if($errors->has('telephone_no'))
                            <label id="label-error" class="error" for="label">{{$errors->first('telephone_no')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('nic')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">NIC </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="nic" placeholder="914578945V" value="{{$customer->nic?:''}}">
                        @if($errors->has('nic'))
                            <label id="label-error" class="error" for="label">{{$errors->first('nic')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('note')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Note </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <textarea class="form-control input-sm" rows="3" name="note" placeholder="Note">{{$customer->note?:''}}</textarea>
                        @if($errors->has('note'))
                            <label id="label-error" class="error" for="label">{{$errors->first('note')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('mail')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">E-mail</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="mail" placeholder="E-mail" value="{{$customer->email?:''}}" readonly>
                        @if($errors->has('mail'))
                            <label id="label-error" class="error" for="label">{{$errors->first('mail')}}</label>
                        @endif
                    </div>
                </div>
                @if(sizeof($customer_types) > 0)
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Customer Types</label>
                    @foreach($customer_types as $key => $value)
                        <div class="col-xm-2 col-sm-2 col-md-2 col-lg-2">
                            <label class="radio-inline">
                                <input type="radio" name="customer_type" id="{{ $value->id }}" value="{{ $value->id }}" @if(in_array($value->id, $checked_types)) checked @endif/>{{ $value->name }}
                            </label>
                        </div>
                    @endforeach
                </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Uploaded Files</label>
                    <div class="col-sm-2">
                        @if(!empty($customer->nic_image_path))
                            <img src="{{ url('core/storage/uploads/images/customer/nic-licence/'.$customer->nic_image_path) }}" class="img-rounded" alt="Showing Image"/>
                        @else
                            <img class="img-rounded" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
                        @endif
                        <br/>NIC/Licence
                    </div>
                    <div class="col-sm-2">
                        @if(!empty($customer->proof_image_path))
                            <img src="{{ url('core/storage/uploads/images/customer/proof/'.$customer->proof_image_path) }}" class="img-rounded" alt="Showing Image"/>
                        @else
                            <img class="img-rounded" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
                        @endif
                        <br/>Proof of Address Document
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Customer NIC/Drving Licence Image</label>
                    <div class="col-sm-9">
                        <input id="file1" type="file" name="file1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Proof of Address Document</label>
                    <div class="col-sm-9">
                        <input id="file2" type="file" name="file2">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="com-xm-12 col-sm-12 col-md-6 col-lg-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="com-xm-12 col-sm-12 col-md-6 col-lg-6 text-right">
                        <button type="submit" class="btn btn-sm bg-purple pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
		</form>
	</div>
</section>
@stop

@section('js')
<!-- toogle -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- toogle -->
<script type="text/javascript">
$("#file1, #file2").fileinput({
    uploadUrl: "",
    maxFileSize: 2000,
    allowedFileExtensions: ["jpg", "JPG", "png", "PNG", "Jpeg"],
    allowedFileTypes: ["image"],
    showRemove: false,
    showUpload: false,
    initialPreview: []
});
$(document).ready(function(){
    
});
</script>
@stop

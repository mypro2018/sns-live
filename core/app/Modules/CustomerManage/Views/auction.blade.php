@extends('layouts.back_master') @section('title','View Customer Auction Details')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Customer
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('customer/view/'.$uid)}}}">Customer Auction List</a></li>
		<li class="active">View Item Details</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">   
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">View Auction</h3>
        </div>
        <div class="box-body">
            <div class="form-group form-horizontal">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Auction</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->event_name}}" readonly>
                        </div>
                        <label class="col-sm-2 control-label">Auction Date</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->auction_date}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Auction Item List</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label class="control-label"><b>Auction Item Detail</b></label>
                    <table class="table table-bordered bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th width="5%">#</label></th>
                                <th>Item Code</label></th>
                                <th>Item</label></th>
                                <th>Minimum BID Amount(Rs)</label></th>
                                <th>Withdraw BID Amount(Rs)</label></th>
                                <th>Highest BID Amount(Rs)</label></th>
                                <th>My BID Amount(Rs)</label></th>
                                <th width="10%">Item Status</label></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;?>
                        @if(count($details) > 0)
                        @foreach($details as $result_val)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$result_val->item->item_code}}</td>
                                    <td >{{$result_val->item->name}}</td>
                                    <td class="text-right">{{number_format($result_val->start_bid_price,2)}}</td>
                                    <td class="text-right">{{number_format($result_val->withdraw_amount,2)}}</td>
                                    <td class="text-right">{{number_format($result_val->bid['bid_amount'],2)}}</td>
                                    <td class="text-right">{{number_format($result_val->highest_bid['bid_amount'],2)}}</td>
                                    <td class="text-center">
                                    @if($result_val->status == 0)
                                        <i  aria-hidden="true"></i> Bid Not Start
                                    @elseif($result_val->status == 1 || $result_val->status == 0)
                                        <i class="fa fa-bullhorn" aria-hidden="true"></i> Pending
                                    @elseif($result_val->status == 2)
                                        <i class="fa fa-gavel" aria-hidden="true"></i> Bidding
                                    @elseif($result_val->status == 3)
                                        @if($result_val->bid['bid_amount'] == $result_val->highest_bid['bid_amount'])
                                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Won
                                        @else
                                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Lost
                                        @endif
                                    @endif
                                    </td>
                                </tr>
                        <?php $i++;?>
                        @endforeach
                        @else
                        <tr><td colspan="6" class="text-center">No data found.</td></tr>
                        @endif
                        </tbody>
                    </table>
                @if(count($details) > 0 )
                <div class="box-footer">
                    <div style="float: right;">{!! $details->render() !!}</div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@stop
<?php

Route::group(['middleware' => ['auth']], function() {
    Route::group(array('prefix' => 'customer', 'namespace' => 'App\Modules\CustomerManage\Controllers'), function () {
        /**
         * GET Routes
         */

        Route::get('list', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@customerList'
        ]);

        Route::get('view/{id}', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@customerAuctionList'
        ]);

        Route::get('item/{auction_id}/{customer_id}', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@getItemDetails'
        ]);

        Route::get('search', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@searchCustomer'
        ]);

        Route::get('band', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@bandCustomer'
        ]);

        Route::get('pocket-balance/{id?}', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@pocketBalance'
        ]);

        Route::get('edit/{id}', [
            'as' => 'customer.update', 'uses' => 'CustomerManageController@editCustomer'
        ]);
        /**
        * POST Routes
        */
        Route::post('add', [
            'as' => 'customer.add', 'uses' => 'CustomerManageController@addCustomer'
        ]);
        Route::post('update', [
            'as' => 'customer.update', 'uses' => 'CustomerManageController@update'
        ]);
        Route::post('add-watchlist', [
            'as' => 'customer.add-watchlist', 'uses' => 'CustomerManageController@addWatchlist'
        ]);

        Route::post('pocket-balance/{id?}', [
            'as' => 'customer.list', 'uses' => 'CustomerManageController@addPocketBalance'
        ]);

        Route::post('edit/{id}', [
            'as' => 'customer.update', 'uses' => 'CustomerManageController@updateCustomer'
        ]);

    });
});

// Route::group(array('prefix'=>'customer-out','module' => 'FrontEnd', 'namespace' => 'App\Modules\CustomerManage\Controllers'), function() {
//     Route::get('card-exceed', [
//         'as' => 'front.card.exceed', 'uses' => 'CustomerManageController@checkOnlineCardExceed'
//     ]);
// });
<?php namespace App\Modules\CustomerManage\Models;

/**
*
* CustomerManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class CustomerManage extends Model {

	/**
     * The sns_customer_register table associated this model.
     */
    protected $table = 'sns_customer_register';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
	 * Country
	 * @return object Country
	 */
	public function country() {
		return $this->belongsTo('App\Models\Countries', 'country_id', 'id');
	}

	/**
	 * customer user details
	 * @return object User
	 */
	public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
	}
	
	public function customer_card(){
        return $this->hasOne('App\Modules\CustomerManage\Models\CustomerAuctionManage', 'user_id', 'id');
	}

	public function customer_last_card(){
        return $this->hasOne('App\Modules\CustomerManage\Models\CustomerAuctionManage','user_id','id')->orderBy('id', 'DESC');;
	}

	public function pocket_balance(){
        return $this->hasOne('App\Modules\CustomerManage\Models\PocketBalance', 'customer_id', 'id');
	}

	public function payments(){
		return $this->hasOne('App\Modules\PaymentManage\Models\PaymentManage', 'sns_user_id', 'id');
	}

	public function approve_payments(){
		return $this->hasMany('App\Modules\ProductManage\Models\ItemApproveManage', 'customer_id', 'id');
	}

	public function customer_allowed_type(){
		return $this->hasMany('App\Models\CustomerAllowedType', 'customer_id', 'id');
	}

	public function allowed_customer_type(){
		return $this->hasOne('App\Models\CustomerAllowedType', 'customer_id', 'id');
	}


}

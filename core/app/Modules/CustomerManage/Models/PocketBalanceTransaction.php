<?php namespace App\Modules\CustomerManage\Models;

/**
*
* CustomerManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class PocketBalanceTransaction extends Model {

	/**
     * The sns_customer_register table associated this model.
     */
    protected $table = 'pocket_balance_transaction';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
	 * Country
	 * @return object Country
	 */
	public function country() {
		return $this->belongsTo('App\Models\Countries', 'country_id', 'id');
	}


}

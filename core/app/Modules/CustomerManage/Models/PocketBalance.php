<?php namespace App\Modules\CustomerManage\Models;

/**
*
* CustomerManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class PocketBalance extends Model {

	/**
     * The sns_customer_register table associated this model.
     */
    protected $table = 'pocket_balance';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
	 * Country
	 * @return object Country
	 */
	public function pocket_balance_transaction(){
        return $this->hasMany('App\Modules\CustomerManage\Models\PocketBalanceTransaction', 'pocket_balance_id', 'id')->orderBy('id', 'DESC');
	}


}

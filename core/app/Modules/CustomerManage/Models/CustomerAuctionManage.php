<?php namespace App\Modules\CustomerManage\Models;

/**
*
* CustomerAuctionManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class CustomerAuctionManage extends Model {

	/**
     * The sns_customer_register table associated this model.
     */
    protected $table = 'sns_customer_auction';
    public $timestamps = true;
    protected $guarded = ['id'];


	/**
     * This function is used to get customer auctions
     * @param integer $user_id
     * @return
     */


    /**
     * This function is used to get auction details
     * @param
     * @return object location
     */

    public function get_auction() {
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionManage','auction_id','id');
    }

    /**
     * This function is used to get customer details
     * @param
     * @return object customer
     */

    public function get_customer() {
        //return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage','user_id','id');
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function card_range() {
        return $this->belongsTo('App\Models\CardRange','card_range_id','id');
    }

    public function customer() {
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage','user_id', 'id');
    }

    public function approve_payment(){
        return $this->hasMany('App\Modules\ProductManage\Models\ItemApproveManage', 'card_no', 'card_no')
        ->where('status', ITEM_PAYMENT_APPROVE);
    }

}

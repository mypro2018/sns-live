<?php namespace App\Modules\CustomerManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Nishan Randika <nishanran@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Modules\CustomerManage\Models\CustomerManage;
use App\Modules\CustomerManage\Models\CustomerAuctionManage;
use Core\UserManage\Models\User;
use App\Models\WatchList;
use App\Models\CardRange;
use App\Models\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;
use Sentinel;
use Mail;
use Activation;
use App\Modules\CustomerManage\Models\PocketBalance;
use App\Modules\CustomerManage\Models\PocketBalanceTransaction;
use App\Modules\ProductManage\Models\ItemApproveManage;
use App\Models\CustomerType;
use App\Models\CustomerAllowedType;


class Logic{

	/**
     * This function is used to add a customer
     * @param array $data, array $form
     * @return object customer object if success, exception otherwise
     */
    public function add($data,$form) {

       return DB::transaction(function() use($data, &$form) {
            $customer = CustomerManage::create($data);
            if($customer) {
                //Allocate default user type for new registering user
                $allocate_user_type = CustomerAllowedType::create([
                    'customer_id'       => $customer->id,
                    'customer_type_id'  => DEFAULT_CUSTOMER_TYPE,
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
                if(!$allocate_user_type){
                    throw new TransactionException('New user not allocated with default user type', 101);
                }
            	// Create a New User Account
            	$newUser = Sentinel::register([
					'first_name' 	=> $form->first_name,
					'last_name' 	=> $form->last_name,
					'email' 		=> $form->email,
					'username' 		=> $form->email,
                    'password'      => $form->password,
					'status' 		=> 1
				]);

				if (!$newUser) {
					throw new TransactionException('Record user add wasn\'t updated', 101);
				}

				$role = Sentinel::findRoleBySlug('registered');
				$role->users()->attach($newUser);

				$customer->user_id = $newUser->id;
				$customer->save();

				if($newUser) {
					$activation = Activation::create($newUser);

					$body = [
			            'email' 	=> $newUser->email,
                        'fname'     => $newUser->first_name,
			            'name' 		=> $newUser->full_name,
			            'subject' 	=> 'Activate your account',
			            'code' 		=> $activation->code,
			            'id' 		=> $newUser->id
			        ];

					Mail::queue('CustomerManage::template.activation-email-template', $body, function($message) use ($body) {
			            $message->to($body['email'], $body['name'])
                        ->bcc('sandslivewebportal10@gmail.com', 'Sands Live')
                        ->from('test@email.com')
                        ->subject($body['subject']);
			        });
				}
                return $customer;
            }else {
                throw new Exception("Error occured while adding new customer");
            }
        });
        return true;
    }

    /**
     * This function is used to add a watchlist
     * @param array $data
     * @return object watchlist object if success, exception otherwise
     */
    public function addWatchlist($data) {
        return $watchlist = WatchList::create($data);
    }

    /**
     * This function is used to get customer details
     * @param integer $id that need to get customer details
     * @return customer object
     */
    public function getCustomerByUserId($id) {
        $customer = CustomerManage::with(['user','country', 'customer_allowed_type'])->where('id', '=', $id)->first();
        if($customer) {
            return $customer;
        }
        else {
            return [];
        }
    }

    /**
     * This function is used to update customer
     * @param integer $id, array $data, array $form
     * @return object customer object if success, exception otherwise
     */
    public function update($id, $data, $form, $user_id) {
        return DB::transaction(function() use($id, $data, $form, $user_id) {
            // update customer details
            $customer = CustomerManage::find($id)->update($data);
            if($customer){
                // update user details
                $user               = User::find($user_id);
                $user->first_name   = $form->first_name;
                $user->last_name    = $form->last_name;
                $user->email        = $form->email;
                $user->username     = $form->email;
                if(isset($form->password)){                   
                    $user->password     = \Hash::make($form->password);
                }
                $user->save();
                if(!$user) {
                    throw new Exception("Error occurred while updating user details");
                }
                return $customer = CustomerManage::find($id);
            }else {
                throw new Exception("Error occurred while updating customer");
            }
        });
    }

    /**
     * This function is used to get customer list
     * @param
     * @return array customer_list
     */
    public function customerList(){
        $customer_list = DB::table('sns_customer_register')
        ->leftJoin(DB::raw('(SELECT * FROM sns_customer_auction WHERE id in (SELECT max(id) FROM sns_customer_auction group by sns_customer_auction.user_id)) AS customer_auction'), function($join) {
                $join->on('sns_customer_register.id', '=', 'customer_auction.user_id')
                        ->whereNull('customer_auction.deleted_at');
        })
        ->leftJoin('sns_auction', function($join){
            $join->on('sns_auction.id', '=', 'customer_auction.auction_id')
                    ->whereNUll('sns_auction.deleted_at');
        })
        ->leftJoin('sns_customer_auction', function($join){
            $join->on('sns_auction.id', '=', 'sns_customer_register.user_id')
                    ->whereNUll('sns_customer_auction.deleted_at');
        })
        ->leftJoin('card_range', function($join){
            $join->on('card_range.id', '=', 'customer_auction.card_range_id')
                    ->whereNUll('card_range.deleted_at');
        })
        ->select(
            'sns_customer_register.id',    
            'sns_customer_register.fname AS customer_name',
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'sns_customer_register.created_at',
            'sns_auction.event_name',
            'sns_customer_register.email',
            'sns_customer_register.nic',
            'customer_auction.card_no',
            'card_range.card_type',
            'customer_auction.auction_id',
            'sns_customer_register.band_status',
            'sns_customer_register.city',
            'address_1 AS address1',
            'address_2 AS address2',
            DB::raw('(select COUNT(id) from sns_customer_auction where user_id = sns_customer_register.id) as auction_count')
        )
        ->whereNUll('sns_customer_register.deleted_at')
        ->orderBy('customer_auction.id', 'desc')
        ->orderBy('sns_customer_register.id', 'desc')
        ->groupBy('sns_customer_register.id')
        ->paginate(unserialize(SHOW_OPTION)[1]);
        return $customer_list;
    }


    /**
     * This function is used to get customer details
     * @param $customer_id
     * @return customer_details
     */
    public function getCustomerAuctionDetails($customer_id){
        return $list = CustomerAuctionManage::with(['customer','card_range',
            'get_auction.location',
            'get_auction.auction_details.highest_bid.win_bid.bidding_winner' => function($sql) use($customer_id){
                $sql->where('id', $customer_id);
            }])
            ->where('user_id',$customer_id)
            ->orderBy('id', 'DESC')
            ->whereNull('deleted_at')
            ->paginate(unserialize(SHOW_OPTION)[1]);
    }

    /**
     * This function is used to get auction details by customer
     * @param $customer_id
     * @return auctions
     */
    public function getAuctionsByUser($customer_id){
        return $list = CustomerAuctionManage::with(['get_auction','get_customer'])
                        ->where('user_id',$customer_id)
                        ->get();
    }

    /**
     * This function is used to get customer list
     * @param
     * @return customer_list
     */
    public function customer(){
        return $user = User::select(DB::raw('CONCAT(first_name," ",last_name," - ",username) AS customer'),'id')->where('user_type',1)->lists('customer','id');
    }


    /**
     * This function is used to search customer with auction
     * @param integer $customer_id, integer $auction_id
     * @return customer object
     */
    public function searchCustomer($searchInput){
        $search_customer = DB::table('sns_customer_register')
            ->leftJoin('sns_customer_auction',function($join){
                $join->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id');
            })
            ->leftJoin('sns_auction', function($join){
                $join->on('sns_auction.id', '=', 'sns_customer_auction.auction_id');
            })
            ->leftJoin('card_range', function($join){
                $join->on('card_range.id', '=', 'sns_customer_auction.card_range_id');
            })
            ->select('sns_customer_register.id',
                'sns_customer_register.fname AS customer_name',
                'sns_customer_register.contact_no',
                'sns_customer_register.telephone_no',
                'sns_customer_register.created_at',
                'sns_auction.event_name',
                'sns_customer_register.email',
                'sns_customer_register.nic',
                'sns_auction.id as auction_id',
                'sns_customer_auction.card_no',
                'card_range.card_type',
                'sns_customer_auction.auction_id',
                'sns_customer_register.band_status',
                'address_1 AS address1',
                'address_2 AS address2',
                'sns_customer_register.city',
                DB::raw('(select COUNT(id) from sns_customer_auction where user_id = sns_customer_register.id) as auction_count')
            );
             
        if(!empty($searchInput->customer)){
            $search_customer = $search_customer->where(function($search) use($searchInput) {
                $search->where('sns_customer_register.fname', 'LIKE', '%'.trim($searchInput->customer).'%')
                       ->orWhere('sns_customer_register.id', trim($searchInput->customer))
                       ->orWhere('sns_customer_register.contact_no', trim($searchInput->customer))
                       ->orWhere('sns_customer_register.email', trim($searchInput->customer))
                       ->orWhere('sns_customer_register.nic', trim($searchInput->customer))
                       ->orWhere('sns_customer_register.telephone_no', trim($searchInput->customer))
                       ->orWhere('sns_customer_auction.card_no', trim($searchInput->customer));
            });
        }
        if($searchInput->auction > 0){
            $search_customer = $search_customer->where(function($search) use($searchInput){
                $search->where('sns_customer_auction.auction_id', '=', $searchInput->auction);
            });
        }

        if($searchInput->register_type  > 0){
            $search_customer = $search_customer->where(function($search) use($searchInput){
                $search->where('card_range.card_type', '=', $searchInput->register_type);
            });
        }

        if(!empty($searchInput->from) && !empty($searchInput->to)){
            $search_customer = $search_customer->where(function($search) use($searchInput){
                $search->whereDate('sns_customer_register.created_at', '>=', $searchInput->from)
                       ->whereDate('sns_customer_register.created_at', '<=', $searchInput->to);
            });
        }
        
        if($searchInput->auction > 0){
            $search_customer->orderBy('sns_customer_auction.id','asc');
        }else{

            $search_customer->orderBy('sns_customer_register.id','desc');
        }
        return $search_customer->paginate(unserialize(SHOW_OPTION)[1]);
    }

    /**
     * This function is used to get Customer Payments
     * @param integer $auction_id that need to get customer payment details
     * @return customer object
     */
    public function getPaymentDetails($auction_id, $customer_id){
        $payment = CustomerAuctionManage::select('payments.*')
                    ->leftJoin('sns_payment as payments', function($payment_join) {
                        $payment_join->on('sns_customer_auction.auction_id', '=', 'payments.sns_auction_id');
                    })
                    ->where('sns_customer_auction.auction_id', $auction_id)
                    ->where('sns_customer_auction.user_id', $customer_id)
                    ->where('payments.sns_item_id', '0')
                    ->where('payments.payment_status', '1')
                    ->get();
        
        if(sizeof($payment) > 0){
            return $payment;
        }else{
            return [];
        }
        
    }

    /**
     * This function is used to get Customer Payments
     * @param integer $auction_id that need to get customer payment details
     * @return customer object
     */
    public function getPaymentDetailsToValidate($auction_id, $customer_id = null){
        $user = Sentinel::getUser();

        // get current date & time
        $date = date('Y-m-d');
        $time = date('h:i');

        $data = CustomerAuctionManage::select('payments.*')
            ->leftJoin('sns_payment as payments', function($payment_join) {
                $payment_join->on('sns_customer_auction.auction_id', '=', 'payments.sns_auction_id');
            })
            ->leftJoin('sns_auction as auctions', function($auction_join) {
                $auction_join->on('sns_customer_auction.auction_id', '=', 'auctions.id');
            })
            ->where('sns_customer_auction.auction_id', $auction_id)
            ->where('sns_customer_auction.user_id', $customer_id)
            ->where('payments.sns_item_id', '=', 0)
            ->where('payments.payment_status', '=', PAYMENT_SUCCESS)
            // ->where('auctions.auction_date', '<=', $date)
            // ->whereRaw("TIME_FORMAT(auctions.auction_start_time,'%H:%i') <= '".$time."'")
            ->get();

        return $data;
    }

    /**
     * This function is used to get customer count
     * @param 
     * @return customer_count 
     */    
    public function customerCount($auction_id = null){
        $customer = CustomerManage::whereNull('deleted_at')
        ->get();
        return count($customer);
    }

    /**
     * This function is used to get customer count
     * @param 
     * @return customer_count 
     */    
    public function onlineCustomerCount($auction_id = null){
        $online_customer = CustomerAuctionManage::leftJoin('card_range',function($join){
            $join->on('sns_customer_auction.card_range_id', '=', 'card_range.id');
        })
        ->leftJoin('sns_customer_register',function($join){
            $join->on('sns_customer_register.id', '=', 'sns_customer_auction.user_id');
        })
        ->where('card_range.card_type', '=', ONLINE_CARD)
        ->whereNull('sns_customer_auction.deleted_at')
        ->whereNull('card_range.deleted_at')
        ->groupBy('sns_customer_auction.card_no');
        if($auction_id > 0){
            $online_customer = $online_customer->where('sns_customer_auction.auction_id', '=', $auction_id);
        }
        $online_customer = $online_customer->get();    
        return $online_customer;
    }

    /**
     * This function is used to get customer count
     * @param 
     * @return customer_count 
     */    
    public function floorCustomerCount($auction_id = null){
        $floor_customer = CustomerAuctionManage::join('card_range',function($join){
            $join->on('sns_customer_auction.card_range_id', '=', 'card_range.id');
        })
        ->leftJoin('sns_customer_register',function($join){
            $join->on('sns_customer_register.id', '=', 'sns_customer_auction.user_id');
        })
        ->where('card_range.card_type', '=', FLOOR_CARD)
        ->whereNull('sns_customer_auction.deleted_at')
        ->groupBy('sns_customer_register.nic')
        ->whereNull('card_range.deleted_at')
        ->groupBy('sns_customer_auction.card_no');
        if($auction_id > 0){
            $floor_customer = $floor_customer->where('sns_customer_auction.auction_id', '=', $auction_id);
        }
        $floor_customer = $floor_customer->get();    
        return $floor_customer;
    }

    /**
     * This function is used to add a customer
     * @param array $data, array $form
     * @return object customer object if success, exception otherwise
     */
    public function addCustomerAuction($auction_id, $card_no, $customer_id, $data) {
    
        DB::beginTransaction();
        $customer_auction_check = CustomerAuctionManage::where('auction_id', '=', $auction_id)
        ->where('card_no', '=', $card_no)
        ->whereNull('deleted_at')
        ->first();

        if($customer_auction_check){
            $customer_exist_auction = CustomerAuctionManage::where('auction_id', '=', $auction_id)
            ->where('user_id', '=', $customer_id)
            ->whereNull('deleted_at')
            ->first();
            if($customer_exist_auction){
                $customer_auction = CustomerAuctionManage::where('auction_id', '=', $auction_id)
                ->where('user_id', '=', $customer_id)
                ->whereNull('deleted_at')
                ->update([
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                DB::commit();
                return $customer_exist_auction;
            }else{
                $customer_auction = CustomerAuctionManage::create($data);
                if(!$customer_auction) {
                    DB::rollBack();
                    throw new Exception("Error occurred while adding customer to auction");
                }
                DB::commit();
                return $customer_auction;
            }    
        }else{
            $customer_auction = CustomerAuctionManage::create($data);
            if(!$customer_auction) {
                DB::rollBack();
                throw new Exception("Error occurred while adding customer to auction");
            }
            DB::commit();
            return $customer_auction;
        }
    }

    /**
     * This function is used to get customer by email
     * @param 
     * @return customer 
     */    
    public function getCustomerByEmail($nic, $contact){
        return $customer = CustomerManage::where(function($sql) use ($nic, $contact){
            $sql->where('user_id', '=', 0)
                ->orWhere('nic', '=', strtoupper(trim($nic)))
                ->orWhere('telephone_no', '=', trim($contact));
        })
        ->first();
    }


    //CHECK EMAIL ALREADY EXIST
    public function emailExist($email){
        return $userExist = User::where('username', $email)->where('status', DEFAULT_STATUS)->first();
    }

    //floor customer register
    public function registerCustomer($request){
        DB::beginTransaction();
        $customer_type = null;
        $getCustomer = CustomerManage::where('id', '=', $request->id)
        ->whereNull('deleted_at')
        ->first();
        
        if($getCustomer){
            $customerUpdate                = CustomerManage::find($getCustomer->id);
            $customerUpdate->register_type = FLOOR_BIDDER;
            $customerUpdate->fname         = $request->first_name;
            $customerUpdate->lname         = $request->last_name;
            $customerUpdate->address_1     = $request->address;
            $customerUpdate->contact_no    = $request->contact;
            $customerUpdate->city          = $request->city;
            $customerUpdate->country_id    = $request->country;
            $customerUpdate->email         = $request->mail;
            $customerUpdate->nic           = $request->nic;
            $customerUpdate->note          = $request->note;
            $customerUpdate->status        = DEFAULT_STATUS;
            $customerUpdate->updated_at    = date('Y-m-d H:i:s');
            $customerUpdate->save();
            if(sizeof($customerUpdate) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while updating new customer");
            }
            //get checked customer_types
            if($request->get('customer_type')){
                $customer_allowed_types = CustomerAllowedType::updateOrCreate([
                    'customer_id'       => $getCustomer->id 
                ],[
                    'customer_id'       => $getCustomer->id,
                    'customer_type_id'  => $request->get('customer_type'),
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
                if(!$customer_allowed_types){
                    DB::rollBack();
                    throw new Exception("Error occurred while allocating customer types");
                }
            }
            DB::commit();
            return $customerUpdate;
        }else{
            $newUser = User::create([
                'first_name' 	=> $request->first_name,
                'last_name' 	=> $request->last_name,
                'email' 		=> $request->mail,
                'status' 		=> DEFAULT_STATUS
            ]);
            if (!$newUser) {
                throw new TransactionException('Record user add wasn\'t updated', 101);
            }
            $customer = CustomerManage::create([
                'user_id'       => $newUser->id,
                'register_type' => FLOOR_BIDDER,
                'fname'         => $request->first_name,
                'lname'         => $request->last_name,
                'address_1'     => $request->address,
                'contact_no'    => $request->contact,
                'city'          => $request->city,
                'country_id'    => $request->country,
                'email'         => $request->mail,
                'nic'           => $request->nic,
                'note'          => $request->note,
                'status'        => DEFAULT_STATUS,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
            if(!$customer){
                DB::rollBack();
                throw new Exception("Error occurred while adding new customer");
            }
            //get checked customer_types
            if($request->get('customer_type')){
                $customer_allowed_types = CustomerAllowedType::create([
                    'customer_id'       => $customer->id,
                    'customer_type_id'  => $request->get('customer_type'),
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
                if(!$customer_allowed_types){
                    DB::rollBack();
                    throw new Exception("Error occurred while allocating customer types");
                }
            }
            DB::commit();
            return $customer;
        }
    }

    public function getCustomer($user_id){
        return $customer_details = CustomerManage::with('customer_last_card', 'allowed_customer_type.customer_types')
        ->where('user_id', '=', $user_id)
        ->first();
    }

    //get last auction register customer card issued details
    public function getLastRegisterDetails($auction_id, $user_type){
        $card_check = CardRange::where('auction_id', '=', $auction_id)
        ->where('card_type', '=', $user_type)
        ->whereNull('deleted_at')
        ->first();

        if($card_check){
            $customer_auction_check = DB::table('sns_customer_auction')
            ->join('card_range', function($join){
                $join->on('sns_customer_auction.card_range_id', '=', 'card_range.id')
                ->whereNull('card_range.deleted_at');
            })
            ->select(
                'sns_customer_auction.card_no', 
                'sns_customer_auction.card_range_id'
            )
            ->where('sns_customer_auction.auction_id', $auction_id)
            ->where('card_range.card_type', $user_type)
            ->whereNull('sns_customer_auction.deleted_at')
            ->orderBy('sns_customer_auction.id', 'DESC')
            ->first();
            
            if(sizeof($customer_auction_check) > 0){
                //new card no
                $cardNo = ($customer_auction_check->card_no + 1);
                return [
                    'card_no'  => $cardNo,
                    'range_id' => $customer_auction_check->card_range_id
                ];
            }else{
                return [
                    'card_no'  => $card_check->card_range_from, 
                    'range_id' => $card_check->id
                ];
            }
        }else{
            return [];
        }
    }

     //band customer
     public function bandCustomer($customer_id, $status){
        DB::beginTransaction();
        // $check_bid_have = AuctionBidManage::join('sns_auction_detail',function($join){
        //     $join->on('sns_auction_bid.sns_auction_detail_id', '=', 'sns_auction_detail.id');
        // })
        // ->where('sns_auction_detail.sns_auction_id', $auction_id)
        // ->where('sns_auction_bid.sns_customer_id', $customer_id)
        // ->whereNUll('sns_auction_detail.deleted_at')
        // ->whereNUll('sns_auction_bid.deleted_at')
        // ->first();
        // if(count($check_bid_have) > 0){
        //     return $check_bid_have;
        // }else{
            $band_auction = CustomerAuctionManage::where('user_id', $customer_id)
            ->update([
                'band_status'=> $status,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            if(count($band_auction) == 0){
                DB::rollBack();
                throw new Exception("Error occured while updating customer auction band");
            }

            $band_customer = CustomerManage::where('id', $customer_id)
            ->update([
                'band_status'=> $status,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            if(count($band_customer) == 0){
                DB::rollBack();
                throw new Exception("Error occured while updating customer band");
            }

 
            DB::commit();   
            return $band_customer;

        //}
    }
    
    public function loadCustomer($user_name){
        return $customer_details = CustomerManage::with(['payments' => function($query){
            $query->select('id','sns_user_id','payment_status');
            //->where('payment_status', 0)
           // ->orWhere('payment_status', -1);
        } ,'approve_payments' => function($query){
            $query->select('id', 'detail_id', 'customer_id', 'card_no', 'auction_id');
           // ->where('status', APPROVE)
           // ->where('item_approve_sold_status', PAYMENT_PENDING);
        }])->where('email', '=', $user_name)->first();
    }

    //check valid card no
    public function validCardNo($request){
        return $card_no_available = CustomerAuctionManage::where('auction_id', $request->auction_id)
        ->where('card_no', $request->customer_card_no)
        ->whereNull('deleted_at')
        ->first();
    }

    //pocket balance

    public function getPocketBalance($customer_id = null){

        return $pocket_balance = CustomerManage::with('pocket_balance.pocket_balance_transaction')->
        where('id', $customer_id)->first();

    }

    //add pocket balance
    public function addPocketBalance($input){

        $check_pocket_balance = PocketBalance::where('customer_id', '=', $input->id)
        ->whereNull('deleted_at')
        ->first();
        $pocket_balance    = null;
        $amount            = null;
        $pocket_balance_id = null;
        
        if(sizeof($check_pocket_balance) > 0){
            $pocket_balance_id = $check_pocket_balance->id;
            DB::transaction(function () use(&$pocket_balance, $input, &$amount, &$check_pocket_balance){
                
                if($input->type == 1){
                    $amount = $check_pocket_balance->balance + $input->amount;
                    PocketBalance::where('customer_id', '=', $input->id)
                    ->update(['balance' => $amount]);
                }else{
                    $amount = $check_pocket_balance->balance - $input->amount;
                    PocketBalance::where('customer_id', '=', $input->id)
                    ->update(['balance' => $amount]);
                }

            });
        }else{
            DB::transaction(function () use(&$pocket_balance, $input){
                $pocket_balance = PocketBalance::create([
                    'customer_id'   =>  $input->id,
                    'balance'       =>  $input->amount,
                    'status'        => DEFAULT_STATUS,
                    'balance_date'  => date('Y-m-d H:i:s'),
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            });
            $pocket_balance_id = $pocket_balance->id;
        }

        DB::transaction(function () use(&$pocket_balance, $input, $pocket_balance_id){
            $pocket_balance = PocketBalanceTransaction::create([
                'pocket_balance_id'   => $pocket_balance_id,
                'transaction_type'    => $input->type,
                'transaction_amount'  => $input->amount,
                'status'              => DEFAULT_STATUS,
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s'),
            ]);
        });


        return $pocket_balance = CustomerManage::with('pocket_balance.pocket_balance_transaction')->
        where('id', $input->id)->first();

    }


    //GET CURRENT POCKET BALANCE AMOUNT
    public function customerPocketBalance($customer_id = null){

        return $pocket_balance = PocketBalance::where('customer_id', $customer_id)->first();

    }


    //POCKET BALANCE DEDUCTION
    public function pocketBalanceDeduction($customer_id, $amount){

        $pocket_balance_id    = null;
        $balance_amount       = 0;

        $check_pocket_balance = PocketBalance::where('customer_id', '=', $customer_id)
        ->whereNull('deleted_at')
        ->first();

        if(sizeof($check_pocket_balance) > 0){
            $pocket_balance_id = $check_pocket_balance->id;
            DB::transaction(function () use($amount, $check_pocket_balance, $customer_id, $pocket_balance_id, &$balance_amount){
                $balance_amount = ($check_pocket_balance->balance - $amount);
                    PocketBalance::where('customer_id', '=', $customer_id)
                    ->update(['balance' => $balance_amount]);
                
                $pocket_balance = PocketBalanceTransaction::create([
                    'pocket_balance_id'   => $pocket_balance_id,
                    'transaction_type'    => 0,
                    'transaction_amount'  => $amount,
                    'status'              => DEFAULT_STATUS,
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s'),
                ]);

            });
        }

    }


    //GET SUMMARY CUSTOMER AUCTION DETAILS
    /**
     * This function is used to get customer details
     * @param $customer_id
     * @return customer_details
     */
    public function getCustomerSummaryAuctionDetails($customer_id)
    {
        return $list = CustomerAuctionManage::with(['customer','card_range',
            'get_auction.location',
            'get_auction.auction_details_summary.highest_bid.win_bid.bidding_winner' => function($sql) use($customer_id){
                $sql->where('id', $customer_id);
            }])
            ->where('user_id', $customer_id)
            ->orderBy('id', 'DESC')
            ->whereNull('deleted_at')
            ->paginate(unserialize(SHOW_OPTION)[1]);
    }

    public function updateCustomer($request, $id){

        $customer = CustomerManage::find($id);
        $customer->fname        = $request->input('first_name');
        $customer->lname        = $request->input('last_name');
        $customer->address_1    = $request->input('address1');
        $customer->address_2    = $request->input('address2');
        $customer->contact_no   = $request->input('contact');
        $customer->telephone_no = $request->input('telephone_no');
        $customer->email        = $request->input('mail');
        $customer->nic          = $request->input('nic');
        $customer->note         = $request->input('note');
        $customer->city         = $request->input('city');
        $customer->country_id   = $request->input('country');
        $customer->save();
        if(sizeof($customer) > 0){
            //update checked customer_types
            if($request->get('customer_type')){
                $customer_allowed_types = CustomerAllowedType::updateOrCreate([
                    'customer_id'       => $customer->id
                ], [
                    'customer_id'       => $customer->id,
                    'customer_type_id'  => $request->get('customer_type'),
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
                if(!$customer_allowed_types){
                    DB::rollBack();
                    throw new Exception("Error occured while allocating customer types");
                }
            }
        }else{
            throw new TransactionException('Record customer wasn\'t updated', 101);
        }
        return $customer;
    }


    /** This function is used to find user has pending payments
     * @param $user_id 
     */
    public function pendingPayments($user_id){
        if(isset($user_id)){
            return $pending_payments = ItemApproveManage::where('customer_id', $user_id)
            ->where('status', 1)
            ->where('item_approve_sold_status', 0)
            ->get();
        }else{
            throw new Exception('Invalid arguments passed');
        }
    }

    /**
     * This function is used to get customer types
     * @return array $customer_type
     */
    public function getCustomerTypes(){
        return $customer_type = CustomerType::where('status', DEFAULT_STATUS)
        ->whereNull('deleted_at')
        ->get();
    }

    /**
     * This function is used to check & update register card limit has been exceed or not
     * @param integer live_auction_id
     */
    public function checkOnlineCardExceed($request){
        /**
         * No define card range - return NO_CARD_RANGE
         * Card range defined & no registered customers - return NEW_CARD_REGISTER
         * Card range defined & issued card no's exceed - return CARD_LIMIT_EXCEED
         * Card range defined & issued card no's can issue - return CARD_CAN_ISSUE
         */
        $card_check = CardRange::where('auction_id', '=', $request->live_auction_id)
        ->where('card_type', '=', $request->user_type)
        ->whereNull('deleted_at')
        ->first();
        if($card_check){
            $customer_auction_check = DB::table('sns_customer_auction')
            ->join('card_range', function($join){
                $join->on('sns_customer_auction.card_range_id', '=', 'card_range.id')
                ->whereNull('card_range.deleted_at');
            })
            ->select(
                'sns_customer_auction.card_no', 
                'sns_customer_auction.card_range_id'
            )
            ->where('sns_customer_auction.auction_id', $request->live_auction_id)
            ->where('card_range.card_type', $request->user_type)
            ->whereNull('sns_customer_auction.deleted_at')
            ->orderBy('sns_customer_auction.card_no', 'desc')
            ->first();
            if($customer_auction_check){
                if($card_check->card_range_to == $customer_auction_check->card_no){
                    $update_card_range_exceed_status = CardRange::where('auction_id', $request->live_auction_id)
                    ->where('card_type', $request->user_type)
                    ->update([
                        'exceed_status' => DEFAULT_STATUS
                    ]);
                    return CARD_LIMIT_EXCEED;
                }else{
                    return CARD_CAN_ISSUE;
                }
            }else{
                return NEW_CARD_REGISTER;
            }
        }else{
            return NO_CARD_RANGE;
        }
    }

    /**
     * Update customer allocated_type
     * @param integer customer_id
     * @param integer customer_type
     * @return 
     */
    public function updateCustomerAllocatedType($request){
        if($request){
            $update_type = CustomerAllowedType::updateOrCreate([
                'customer_id' => $request->customer_id
            ],
            [
                'customer_id'      => $request->customer_id,
                'customer_type_id' => $request->customer_type,
                'status'           => DEFAULT_STATUS,
                'created_at'       => date('Y-m-d H:i:s'),
                'updated_at'       => date('Y-m-d H:i:s')
            ]);
            return $update_type;
        }else{
            throw new Exception('Parameters cannot be empty');
        }
    }

    /**
     * Update customer upload images 
     * @param integer customer_id
     * @param file file_name1
     * @param file file_name1
     * 
     */
    public function updateCustomerUploadImages($customer_id, $file_name1, $file_name2){
        return DB::transaction(function() use($customer_id, $file_name1, $file_name2) {
            $customer = CustomerManage::find($customer_id);
            $customer->nic_image_path   = $file_name1;
            $customer->proof_image_path = $file_name2;
            $customer->save();
            return $customer;
        });
    }
}

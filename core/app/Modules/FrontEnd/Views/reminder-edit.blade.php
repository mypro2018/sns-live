<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="angularApp">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<!-- Stylesheets ============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />

    <!-- jquery confirm -->
    <link rel="stylesheet" href="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}">
    <!-- jquery confirm -->

	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>

    .dark {
        color: inherit;
    }

    a {
        color: inherit;
    }

    .padding0{
        padding: 0;
    }

    .jconfirm-content hr{
        margin: 5px 0 25px;
    }

    .margin-b-0{
      margin-bottom: 0;
    }
        
    </style>

    <script type="text/javascript">
        var BASE_URL = "<?php echo url() ?>";
    </script>

	<!-- Document Title ============================================= -->
	<title>Schokman & Samerawickreme | Password Reset</title>
</head>
<body class="stretched">
	<!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap nopadding">

                <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0;"></div>

                <div class="section nobg full-screen nopadding nomargin">
                    <div class="container vertical-middle divcenter clearfix">

                        <div class="row center">
                            <a href="{{url('/')}}"><img src="{{asset('assets/front/images/logo-dark.png')}}" alt="S&S Logo"/></a>
                        </div>

                        <div class="panel panel-default divcenter noradius noborder" style="max-width: 400px;">
                            <div class="panel-body" style="padding: 40px;">                                
                              {!! Form::open(array('class'=>'form-layout margin-b-0','reminder_edit')) !!}
                                <div class="form-process" style="display: none;"></div>
                                <h3>Reset your password</h3>

                                @if(isset($msg))
                                    <div class="alert alert-danger">
                                        <strong>Oh snap!</strong> {{$msg}}
                                    </div>
                                @endif
                                
                                <div class="col_full">
                                  <label>New Password:</label>
                                  {!! Form::password('password', array('placeholder'=>'New Password', 'class'=>'form-control not-dark')) !!}
                                </div>

                                <div class="col_full">
                                  <label>Confirm Password:</label>
                                  {!! Form::password('password_confirmation', array('placeholder'=>'Password Confirmation', 'class'=>'form-control not-dark')) !!}
                                </div>

                                {!! Form::hidden('id',  $id) !!}
                                {!! Form::hidden('code', $code) !!}

                                <div class="col_full nobottommargin">
                                {!! Form::submit('Reset', array('class'=>'button button-3d button-black nomargin btn-block')) !!}
                                </div>
                              {!! Form::close() !!}
                            </div>
                        </div>

                        <div class="row center dark"><small>Copyrights &copy; 2017 Schokman & Samerawickreme (Pvt) Ltd.</small></div>

                    </div>
                </div>

            </div>

        </section><!-- #content end -->

    </div><!-- #wrapper end -->

	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- jquery confirm -->
    <script src="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
    <!-- jquery confirm -->

	<!-- Footer Scripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){      
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.forgot-pwd', function () {
                reset();
            });
        });

        var reminder = '<form id="password-form" method="post">'
        +'<div class="row" style="margin:0;">'
          +'<div class="col-lg-1 col-xs-12 text-center padding0">'
            +'<img style="margin: -21px 0px -5px;" src="'+BASE_URL+'/assets/front/images/logo-dark.png">'
          +'</div>'
          +'<div class="col-lg-11 col-xs-12 text-center">'
            +'<h3>Forgot Your Password?</h3>'
          +'</div>'
        +'</div>'
        +'<hr>'
        +'<div class="row" style="margin:0;">'
          +'<div class="col-lg-12 col-xs-12 padding0">'
            +'<p>Hey, it happens to everyone. Just let us know what email address you use to login and we\'ll send you an email with instructions.</p>'
          +'</div>'
          +'<div class="col-lg-12 col-xs-12 padding0">'
            +'<label>Email Address:</label>'
            +'<input autofocus type="email" class="form-control input-sm" id="email">'
            +'<p class="text-error help-block" style="display:none"></p>'
          +'</div>'
        +'</div>'
        +'</form>';

        function reset(){
          $.confirm({
              theme: 'material',
              title: '',
              columnClass: 'col-md-6 col-md-offset-3',
              content: reminder,
              buttons: {
                confirm: {
                  text: 'Send',
                  // btnClass: 'btn-dark',
                  action: function () {
                    var email = this.$content.find('input#email');
                    var errorText = this.$content.find('.text-error');

                    if (email.val() == '') {
                      errorText.text('Please enter your registered email address.').show();
                      email.focus();
                      return false;
                    } else {
                      var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
                      if(!pattern.test(email.val())){
                        errorText.text('Invalid email address.').show();
                        email.focus();
                        return false;
                      }
                    }

                    $(".form-process").show();

                    $.ajax({
                        url: "{{url('reminder-email-sender')}}" ,
                        type: 'POST',
                        data: {'email' : email.val() },
                        success: function(data) {
                            if(data.status=='success'){
                                $.alert({
                                  theme: 'material',
                                  // type: 'green',
                                  title: 'You\'ve got mail!',
                                  content: 'We sent you an email with instructions on how to reset your password.',
                                  buttons: {
                                    ok: {
                                      // btnClass: 'btn-green',
                                      action: function () {}
                                    }
                                  }
                                });
                            }
                            else{
                                $.alert({
                                  theme: 'material',
                                  // type: 'red',
                                  title: 'Error!',
                                  content: 'Email not found on our Database.',
                                  buttons: {
                                    ok: {
                                      // btnClass: 'btn-red',
                                      action: function () {
                                        reset();
                                      }
                                    }
                                  }
                                });
                            }
                            $(".form-process").hide();
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(thrownError);
                        }
                    });
                  }
                },
                cancel: function () {                    
                }
              }
          });
        }

        //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->


    </script>
</body>
</html>
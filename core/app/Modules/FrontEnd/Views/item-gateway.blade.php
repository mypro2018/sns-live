<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }

        div.formContainer{
            height: 100%!important;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Payment</title>
</head>
<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>Item Payment</h1>
                <!-- <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    @if(isset($category_id))
                        <li><a href="{{ url('auction-categories/'.$category_id) }}">{{ $item->category->display_name }}</a></li>
                        <li><a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                        <li><a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-payment') }}">{{$item->name}} Payment</a></li>
                    @elseif(isset($auction_id))
                        <li><a href="{{ url('auctions/'.$auction_id) }}">{{ $item->auc_detail->auction->event_name }}</a></li>
                        <li><a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                        <li><a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-payment') }}">{{$item->name}} Payment</a></li>
                    @else
                        <li><a href="{{ url('my-account') }}">My Account</a></li>
                        <li><a href="{{ url('my-account/item/'.$item->id) }}">{{$item->name}}</a></li>
                        <li><a href="{{ url('my-account/item/'.$item->id.'/Item Payment') }}">{{$item->name}} Payment</a></li>
                    @endif
                    </li>
                    <li class="active">Auction Payment</li>
                </ol> -->
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="col_one_fifth nobottommargin">&nbsp;</div>
                    <div class="col_three_fifth col_last nobottommargin">
                        <div class="panel panel-default nobottommargin">
                            <div class="panel-body" style="padding: 40px 40px 0;">
                                <h4 class="text-center" id="approved_time"></h5>
                                <h4 class="text-center" id="monitor"></h5>
                                <h5 class="textcenter"><div id="defaultCountdown"></div></h5>
                                <h3 class="text-center">Please fill your card information</h3>
                                <iframe name='iframe1' id="iframe1" style="margin-left:auto;margin-right:auto;border:none;" class="col-lg-12"  height="350px" width="500px" src="{{ $initResponse->getPaymentPageUrl() }}"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col_one_fifth col_last nobottommargin">&nbsp;</div>
                </div>
            </div>
        </section>
        <!-- payment time over -->
        <form id="band_customer" method="post">
            {!!Form::token()!!}
            <input type="hidden" name="band_status" value="1">
            <button class="button button-border button-rounded button-small band_customer" style="margin:0;display: none;" type="submit" id="band_customer">Band</button>
        </form>
        <!-- payment time over -->
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.plugin.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.countdown.min.js')}}"></script> 

    <script type="text/javascript">
        $(document).ready(function(){
        $(document).find('.my-space').hide();
        $(document).find(".my-account, .standard-logo").attr('href', '').css({'cursor': 'pointer', 'pointer-events' : 'none'});
            var approved_time = new Date('{{$item->won_item_detail->approved->created_at}}');
            //calculate remaning time to make payment
            var date = new Date(approved_time.setDate(approved_time.getDate()));
                date = date.setMinutes(date.getMinutes() + 5);
            var time = new Date(date);
            
            $('#defaultCountdown').countdown({
                until      : time, 
                padZeroes  : true,
                onExpiry   : timeOver, 
                onTick     : timeCountDown
            });
           
        });

        //time count down to make payment
        function timeCountDown(periods) { 

            if(periods[4] == 0){
                if(periods[5] > 0){
                    $('#monitor').text('More ' + periods[5] + ' minutes and ' + periods[6] + ' seconds to complete your payment');
                }
                else{
                    $('#monitor').text('More ' + periods[6] + ' seconds to complete your payment');
                }
            }
        }

        //time over - logout user and block account
        function timeOver() { 
            $('#band_customer').submit();
        }

    </script>

</body>

</html>
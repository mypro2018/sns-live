<div class="pricing-box pricing-extended  {{($i == count($won_item_list))?'':'bottommargin'}} clearfix">
    <div class="pricing-action-area">
        @if($list->image_path)
            @if(File::exists(storage_path('uploads/images/item/'.$list->image_path)))
                <img width="100%" alt="100%x180" src="{{ url('core/storage/uploads/images/item/'.$list->image_path) }}" />
            @else
                <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
            @endif
        @else
        <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
        @endif
    </div>
    <div class="pricing-desc">
        <div class="pricing-title">
            <h3>{{$list->display_name}}
            {{--@if($list->item->status == PRODUCT_TO_OVER)<span class="floatright bidlost">Auction Expired</span>@else <span class="floatright bidwin">Bid Now !</span> @endif</h3>--}}
        </div>
        <div class="pricing-features">
            <ul class="iconlist-color clearfix">
                <li><i class="icon-desktop"></i> Auction - {{$list->event_name}}</li>
                <li><i class="icon-desktop"></i> Item Code - {{$list->item_code}}</li>
                <!-- <li><i class="icon-magic"></i> Minimum Biding Price - {{number_format($list->start_bid_price,2)}}</li> -->
                <li><i class="icon-bullhorn"></i> Wining Bid Price - {{number_format($list->win_amount,2)}}</li>
                <!-- @if($list->amount && $list->amount < $list->win_amount)
                    <li style="color:red;"><i class="icon-repeat"></i> Due Amount - {{number_format($list->win_amount - $list->amount,2)}}</li>
                @endif -->
            </ul>
        </div>
        <!-- <div class="col_full nobottommargin textright divider-right">
            @if($list->amount && $list->amount < $list->win_amount)
                <a href="{{url('my-account/item/'.$list->id.'/item-payment')}}" class="button button-border button-small button-rounded">Complete due amount of Rs {{($list->win_amount - $list->amount)}}</a>
            @else
                <button type="button" onclick="item_receipt({{$list->id}})" class="button button-border button-small button-rounded"><span><i class="icon-mail"></i> Email Receipt</button>
            @endif
            <button type="button" class="button button-border button-small button-rounded s" onclick="location.href='{{url('my-account/item/'.$list->id)}}'">View Details</button>
        </div> -->
    </div>
</div>

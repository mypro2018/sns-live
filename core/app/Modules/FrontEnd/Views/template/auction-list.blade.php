<div class="pricing-box pricing-extended  {{($a == count($auctions))?'':'bottommargin'}} clearfix">
    <div class="pricing-action-area">
        @if($list->get_auction->image)
            @if(File::exists(storage_path('uploads/images/auction/'.$list->get_auction->image)))
                <img width="100%" alt="100%x180" src="{{ url('core/storage/uploads/images/auction/'.$list->get_auction->image) }}" />
            @else
                <img alt="100%x180" src="{{ url('core/storage/uploads/images/auction/empty.jpg') }}" />
            @endif
        @else
        <img alt="100%x180" src="{{ url('core/storage/uploads/images/auction/empty.jpg') }}" />
        @endif
    </div>
    <div class="pricing-desc">
        <div class="pricing-title">
            <h3>{{$list->get_auction->event_name}}
        </div>
        <div class="pricing-features">
            <ul class="iconlist-color clearfix">
                <li>Auction Date/Time - {{$list->get_auction->auction_date}} {{$list->get_auction->auction_start_time}}</li>
                <li>Auction End Date/Time - {{$list->get_auction->auction_end_time}}</li>
                <li>Registration Fee - {{number_format($list->get_auction->registration_fee,2)}}</li>
            </ul>
        </div>
        <!-- <div class="col_full nobottommargin textright divider-right">
            <button type="button" onclick="receipt({{$list->get_auction->id}})" class="button button-border button-small button-rounded"><span><i class="icon-mail"></i> Email Receipt</button>
            @if($list->get_auction->auction_status <> 3)
            <a href="{{url('auctions/'.$list->get_auction->id)}}" class="button button-border button-small button-rounded">View Details</a>
            @endif
        </div> -->
    </div>
</div>

<div class="pricing-box pricing-extended {{($i == count($bid_lists))?'':'bottommargin'}} clearfix">
    <div class="pricing-action-area">
        @if($bid_list->image_path)
            @if(File::exists(storage_path('uploads/images/item/'.$bid_list->image_path)))
                <img width="100%" alt="100%x180" src="{{ url('core/storage/uploads/images/item/'.$bid_list->image_path) }}" />
            @else
                <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
            @endif
        @else
            <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
        @endif
    </div>
    <div class="pricing-desc">
        <div class="pricing-title">
            <h3>{{$bid_list->item_name}}
            @if($bid_list->item_status == PRODUCT_TO_OVER)
                @if($bid_list->user_id == Sentinel::getUser()->id)
                    <span class="floatright bidwin">Item Won</span>
                @else
                    <span class="floatright bidlost">Item Lost</span>
                @endif
            @else
                <span class="floatright">Bid Now</span>
            @endif
            </h3>
        </div>
        <div class="pricing-features">
            <ul class="iconlist-color clearfix">
                <li><i class="icon-desktop"></i> Item Code - {{$bid_list->item_code}}</li>
                <li><i class="icon-magic"></i> Minimum Biding Price - {{$bid_list->start_bid}}</li>
                <li><i class="icon-legal"></i> Highest Biding Price - {{$bid_list->highest_bid}}</li>
            </ul>
        </div>
       <!-- <div class="col_full nobottommargin textright divider-right">
           <button type="button" class="button button-border button-small button-rounded s" onclick="location.href='{{url('my-account/item/'.$bid_list->item_id)}}'">View Details</button>
       </div> -->
    </div>
</div>

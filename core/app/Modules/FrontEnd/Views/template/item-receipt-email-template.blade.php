<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    
    <style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }

    .pull-right{
        float: right;
    }

    .txt-right{
        text-align: right;
    }

    .txt-center{
        text-align: center;
    }

    .border-top{
        border-top:2px solid #eee;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ asset('assets/images/logo/logo.png') }}" alt="logo">
                            </td>                            
                            <td class="txt-right">
                                Created: {{date("F j, Y")}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                Schokman & Samerawickreme.<br>
                                No.24, Torrington Road<br>
                                Kandy<br>
                            </td>                            
                            <td class="txt-right pull-right">
                                {{$customer->fname}} {{$customer->lname}}<br>
                                {{$customer->address_1}}<br>                                
                                {{$customer->email}}<br>
                                {{$customer->contact_no}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Auction
                </td>
                <td>
                    Auction Date/Time
                </td>
                <td>
                    Item
                </td>                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    {{$item->auction_detail->auction->event_name}}
                </td> 
                <td>
                    {{$item->auction_detail->auction->auction_date}} {{$item->auction_detail->auction->auction_start_time}}
                </td> 
                <td>
                    {{$item->name}}({{$item->item_code}})
                </td>                
                <td class="txt-right">
                    Rs {{$item->auction_detail->highest_bid->win_bid->win_amount}}
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Transaction #
                </td>
                <td>
                    Reference #
                </td>
                <td>
                    Date/Time
                </td>                
                <td>
                    Amount
                </td>
            </tr>            

            <?php $i = 1; $total = 0; ?>
            @foreach($item->payment as $payment)
            <tr class="item {{ (count($item->payment) == $i)?'last':'' }}">
                <td>
                    {{$payment->payment_transaction->transaction_id}}
                </td>
                <td>
                    {{$payment->reference_no}}
                </td> 
                <td>
                    {{$payment->payment_transaction->transaction_time}}
                </td>                 
                <td class="txt-right">
                    Rs {{number_format($payment->payment_transaction->paid_amount,2)}}
                </td>
            </tr>
            <?php $i++; $total = $total + $payment->payment_transaction->paid_amount; ?>
            @endforeach
            
            <tr class="total">
                <td colspan="3"></td>                
                <td class="txt-right">
                   <b>Total</b>: Rs {{number_format($total,2)}}
                </td>
            </tr>
        </table>
    </div>
</body>
</html>

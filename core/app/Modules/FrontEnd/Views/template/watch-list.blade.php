<div class="pricing-box pricing-extended  {{($i == count($watch_list))?'':'bottommargin'}} clearfix">
    <div class="pricing-action-area">
        @if($list->item->single_image && $list->item->single_image->image_path)
            @if(File::exists(storage_path('uploads/images/item/'.$list->item->single_image->image_path)))
                <img width="100%" alt="100%x180" src="{{ url('core/storage/uploads/images/item/'.$list->item->single_image->image_path) }}" />
            @else
                <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
            @endif
        @else
            <img alt="100%x180" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
        @endif
    </div>
    <div class="pricing-desc">
        <div class="pricing-title">
            <h3>{{$list->item->name}}
            @if(sizeof($list->item->auction_detail) > 0)
                @if($list->item->auction_detail->status == PRODUCT_TO_OVER)
                    @if(!empty($list->item->auction_detail->bid) && $list->item->auction_detail->bid->sns_customer_id == Sentinel::getUser()->id)
                        <span class="floatright bidwin">Item Won</span>
                    @else
                        <span class="floatright bidlost">Item Lost</span>
                    @endif
                @else
                    <span class="floatright">Bid Now</span>
                @endif
                <span class="floatright">Item Over</span>
            @endif
            </h3>
        </div>
        <div class="pricing-features">
            <ul class="iconlist-color clearfix">
                <li><i class="icon-desktop"></i> Item Code - {{$list->item->item_code}}</li>
                @if($list->item->auction_detail)
                    <li><i class="icon-magic"></i> Minimum Biding Price - {{number_format($list->item->auction_detail->start_bid_price,2)}}</li>
                @endif
                @if($list->item && $list->item->auction_detail && $list->item->auction_detail->bid != null)
                    <li><i class="icon-bullhorn"></i> Current Biding Price - {{number_format($list->item->auction_detail->bid->bid_amount,2)}}</li>
                @endif
            </ul>
        </div>
    </div>
</div>

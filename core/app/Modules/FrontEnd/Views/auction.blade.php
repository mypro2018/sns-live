<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="myApp">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />

<!-- Stylesheets ============================================= -->
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
<!-- Angular-confirm-master -->
<link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
<!-- Angular-confirm-master -->
<style>
    address{
        margin-bottom:5px;
        font-size:11px;
    }

    abbr{
        font-size:11px;
    }

    label{
        text-transform:none;
    }

    .container-main{
        padding-top: 25px;
    }

    .pricing-box.pricing-extended {
        height: 100%;
    }
    .pricing-action-area {
        position: relative !important;
    }
    @media only screen and (max-width: 450px) {
        .customize{
            margin-top: 25px!important;
            margin-bottom:0px !important;
        }
        .textright{
            text-align: center !important;
        }

    }
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
    .auction_name{
        font-size:14px;
        font-weight:bold;
    }
    
    .banner_img{
        height:50px;
    }
    .box {
  position: relative;
  border: 1px solid #BBB;
  background: #EEE;
}
.ribbon {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#F70505 0%, #8F0808 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #8F0808;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #8F0808;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.bid_now_btn{
    border-radius:3px;
    padding:5px;
    padding-left:40px;
    padding-right:40px;
    border: 2px solid #444;
    background-color: transparent;
    color: #333;
    line-height: 36px;
    font-weight: 600;
    text-shadow: none;
}
.bid_now_btn:hover {
    background-color: #444;
    color: #FFF;
    border-color: transparent !important;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
}
.bid_now_btn:active{
    color: #ffffff;
    background-color: #444;
    border-color: #444;
}
.bid_now_btn:active:focus {
    color: #ffffff;
    background-color: #444;
    border-color: #444;
}
.now_price{
    font-size:20px;
    font-weight:bold;
}
.box{
    background:#FFF;
}
.item-img{
    height: 100px;
    width: 130px;
}
#example2 {
  padding: 5px;
  background-repeat: no-repeat;
  background-size: 300px 500px;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- Document Title ============================================= -->
<title>Schokman & Samerawickreme | Auctions</title>
</head>
<body class="stretched" ng-cloak>
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->
        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{ $auction->event_name }}</h1>
                <!-- <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">{{ $auction->event_name }}</li>
                </ol> -->
            </div>
        </section>
        <!-- #page-title end -->
        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <!-- MyController -->
                    <form role="form" method="get" id="form">
                        <div ng-controller="MyController" class="my-controller">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <div class="form-group">    
                                        <span><strong>Page: @{{ currentPage }}</strong></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <select class="form-control input-sm chosen" name="category">
                                            @if(count($category) > 0)
                                                <option value="">-- Search All Categories --</option>
                                                @foreach($category as $key => $val)
                                                    <option value="{{$key}}" @if($key == $category_search) selected @endif>{{$val}}</option>
                                                @endforeach
                                            @else
                                                <option value="">-- No Categories --</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input ng-model="q" id="search" class="form-control input-sm" autocomplete="off" placeholder="Search Item">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                    <div class="form-group">
                                        <input type="number" min="1" max="100" class="form-control input-sm" ng-model="pageSize">
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="clear"></div>
                            <!-- live now area -->
                            @if(isset($auction) && sizeof($auction) > 0 && $auction->live_section_display_status == BIDDING_ACTIVE)
                                <div class="panel panel-default box">
                                    <div class="ribbon"><span>LIVE NOW</span></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            @if(isset($auction) && sizeof($auction) > 0 && $auction->auction_status == AUCTION_LIVE)
                                                <div class="col-xm-3 col-sm-3 col-md-3 col-lg-3">
                                                    <span class="auction_name"><h5>{{$auction->event_name?:''}} <br/>{{$auction->auction_date}}</h5></span>
                                                </div>
                                                @if(!empty($live_item[0]->item_id))
                                                    <div class="col-md-4">
                                                        <div class="media">
                                                            <div class="media-body">
                                                                <h4 class="media-heading">{{$live_item[0]->item_name}}</h4>
                                                                @if($live_item[0]->show_hide_code == BIDDING_ACTIVE)
                                                                    <i class="icon-desktop"></i> Item Code - {{$live_item[0]->item_code}}
                                                                @else
                                                                    <i class="icon-desktop"></i> Item Code - {{ substr($live_item[0]->item_code, 0, -3) . 'XXX' }}
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        @if(!empty($live_item[0]->image_path))
                                                            <img class="img-rounded item-img" src="{{ url('core/storage/uploads/images/item/'.$live_item[0]->image_path) }}" />
                                                        @else
                                                            <img class="img-rounded item-img" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                        @endif
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span class="now_price">Rs {{number_format($live_item[0]->bid_amount,2)}}</span><br/>
                                                        <span>Now Price</span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <!-- check live section bid now button enable disable section -->
                                                        @if($auction->bid_now_active_status_1 == BIDDING_ACTIVE)
                                                            <!-- Check customer register with auction -->
                                                            @if(Sentinel::check())
                                                                @if(sizeof($payment) == 0)
                                                                    @if($live_item[0]->auction_status == AUCTION_TO_BEGIN || $live_item[0]->auction_status == AUCTION_LIVE)
                                                                        <button style="cursor: pointer;" class="button button-border button-small button-rounded check-card-exceed" data-href="{{ URL::to('auctions/'.$live_item[0]->auction_id.'/item/'.$live_item[0]->item_id.'/payment') }}">Register Now <i class="icon-circle-arrow-right"></i></button>
                                                                    @endif
                                                                @else
                                                                    @if($live_item[0]->item_detail_status == PRODUCT_TO_LIVE)
                                                                        <button style="cursor: pointer;" class="button button-border button-small button-rounded check-card-exceed" data-href="{{ URL::to('auctions/'.$live_item[0]->auction_id.'/item/'.$live_item[0]->item_id.'/item-bid/'.$live_item[0]->item_id) }}">Bid Now <i class="icon-circle-arrow-right"></i></a>                                                  
                                                                    @endif
                                                                @endif
                                                                <!-- <a style="cursor: pointer;" href="{{ url('auctions/'.$live_item[0]->auction_id.'/item/'.$live_item[0]->item_id) }}" class="button button-border button-small button-rounded">Bid Now</a> -->
                                                            @else
                                                                <a style="cursor: pointer;" href="{{ url('login')}}?d={{Crypt::encrypt('auctions/'.$live_item[0]->auction_id.'/item/'.$live_item[0]->item_id.'/item-bid/'.$live_item[0]->item_id) }}" class="button button-border button-small button-rounded">Register Now <i class="icon-circle-arrow-right"></i></a>
                                                            @endif
                                                        @endif
                                                    </div>
                                                @else
                                                <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                                                    <img class="img-rounded" src="{{ url('core/storage/uploads/images/banners/live-section/waiting-to-item.gif') }}" />
                                                </div>
                                                @endif
                                            @else
                                                <div class="col-xm-12 col-sm-12 col-md-12 col-lg-12">
                                                    <img class="img-rounded img-responsive center-block" src="{{ url('core/storage/uploads/images/banners/live-section/waiting-auction.gif') }}" />
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <!-- live now area -->
                            <hr/>
                            <div dir-paginate="product in filtered = (products | filter:q | itemsPerPage: pageSize)" current-page="currentPage" class="row pricing-box pricing-extended bottommargin clearfix">
                                <div class="pricing-action-area">
                                    <a href="@{{ product.link }}" target="_blank">
                                        <div id="example2">                                
                                            <img alt="item image" src="@{{ product.image }}" class="img-responsive" alt="Responsive image"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="pricing-desc">
                                    <div class="pricing-title">
                                    <a href="@{{ product.link }}" target="_blank"><h3>@{{ product.name }}</h3></a>
                                    </div>
                                    <div class="pricing-features">
                                        <ul class="iconlist-color clearfix">
                                            <li ng-if="product.show_hide_code == 0"><i class="icon-desktop"></i> Item Code - @{{ product.item_code | limitTo: 3 }}@{{product.item_code.length > 3 ? 'XXX' : ''}}</li>
                                            <li ng-if="product.show_hide_code == 1"><i class="icon-desktop"></i> Item Code - @{{ product.item_code }}</li>
                                            <!-- <li><i class="icon-magic"></i> Bid value - Rs @{{ product.start_bid_price }}</li> -->
                                        </ul>
                                    </div>
                                    <div class="col_full nobottommargin textright divider-right customize">
                                        <a class="button button-border button-small button-rounded" target="_blank" href="@{{ product.link }}">View details</a>
                                        <!-- <div ng-if="product.watchlist == null">
                                            <br><a href="{{url('login')}}?d={{Crypt::encrypt('auctions/'.$auction->id)}}"><u>Sign in</u></a> to add to watchlist
                                        </div> -->
                                        <!-- <a style="cursor: pointer;" ng-show="product.watchlist == '0'" class="button button-border button-small button-rounded" ng-click="WatchList($index,product.id,product.sns_auction_id)"><i class="icon-eye-open"></i> Add to watchlist</a> -->
                                        <!-- <div ng-show="product.watchlist > 0">
                                            <br>Added to the watchlist
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <h5 class="text-center" ng-hide="filtered.length">No items found.</h5>
                            <h4>Showing @{{filtered.length}} items</h4>
                        </div>
                    </form>
                    <!-- End MyController -->
                    <!-- OtherController -->
                    <div ng-controller="OtherController" class="other-controller">
                        <div class="text-right">
                            <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('assets/angular/front-end/template/dirPagination.tpl.html') }}"></dir-pagination-controls>
                        </div>
                    </div>
                    <!-- End OtherController -->
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <!-- ANGULAR -->
    <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
    <!-- ANGULAR -->

    <script src="{{asset('assets/dist/angular/angular-dir-pagination/dirPagination.js')}}"></script>

    <!-- angular-confirm-master -->
    <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
    <!-- angular-confirm-master -->
    
    <!-- pusher -->
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <!-- pusher -->
    <script type="text/javascript">
        
        'use strict';

        var myApp = angular.module('myApp', ['angularUtils.directives.dirPagination','cp.ngConfirm']);
        /* puser event*/
        Pusher.logToConsole = false;
        var pusher = new Pusher('55845bb31c3702ef4dd0', {
          cluster: 'ap2',
          forceTLS: true
        });

        var channel = pusher.subscribe('auction-channel');

         //bid event
        channel.bind('offer-confirm-event', function(data) {
            location.reload();
        });

        function MyController($scope,$ngConfirm,$http,BASE_URL) {

            $scope.currentPage = 1;
            $scope.pageSize    = 10;

            $scope.products = {!! $products !!};
          
            $scope.pageChangeHandler = function(num) {
                // console.log('Auction item page changed to ' + num);
            };

            $scope.WatchList = function(index,item_id,auction_id=null) {                
                var watchlist = {
                    item_id     : item_id,
                    auction_id  : auction_id
                };
                var formData = new FormData();
                formData.append('watchlist', JSON.stringify(watchlist));
                $http.post(BASE_URL+"customer/add-watchlist", formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function(response){
                    if(response.data.success == true) {
                        $scope.products[index].watchlist = response.data.id;
                    } else if(response.data.error == true) {
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'red', closeIcon: false, buttons: {ok: function () {}}});
                    }
                },function(error){
                    console.log(error);
                });
            };
        }

        function OtherController($scope) {
          $scope.pageChangeHandler = function(num) {
            console.log('going to page ' + num);
          };
        }

        myApp.controller('MyController', MyController);
        myApp.controller('OtherController', OtherController);
        myApp.constant('BASE_URL', "{{asset('/')}}");
        toastr.options = {
             "positionClass"    : "toast-top-full-width",
             "closeButton"      : true,
             "timeOut"          : 0,
             "progressBar"      : true,
             "preventDuplicates": true,
        };
        $(document).ready(function(){
            $('select[name="category"]').on('change', function(){
                if($(this).val() > 0){
                    $('#form').submit();
                }
            });  
            //Display message when issued registered card has been exceed & when not defined by auctioneer.
            $('.check-card-exceed').on('click', function(){
                var array       = {!! json_encode($live_item) !!};
                var redirect    = $(this).data('href');
                if(array.length > 0){
                    var live_auction_id = array[0].auction_id;
                    $.ajax({
                        url     : "{{URL::to('auction/card-exceed')}}",
                        method  : 'GET',
                        data    : {
                            'live_auction_id': live_auction_id,
                            'user_type'      : '{{ONLINE_CARD}}'
                        },
                        async   : false,
                        success : function (data) { 
                            if(data.response == '{{CARD_LIMIT_EXCEED}}'){
                                toastr.error('{{CARD_ISSUE_EXCEED_MESSAGE}}');
                            }else if(data.response == '{{NO_CARD_RANGE}}'){
                                toastr.error('{{NO_CARD_RANGE_MESSAGE}}');
                            }else{
                                if(data.allowRegister > 0){
                                    window.location.href = redirect
                                }else{
                                    toastr.error('{{LEVEL_VALIDATION_MESSAGE}}'); 
                                }
                            }
                        },error: function () {
                            toastr.error('Error...Please try again');
                        }
                    });
                }
            });     
        });
    </script>
</body>
</html>
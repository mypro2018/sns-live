<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="angularApp">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />
<title>Schokman & Samerawickreme | Item Bid</title>
<!-- Stylesheets ============================================= -->
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
<!-- toaster -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" type="text/css" />
<!-- toogle -->
<link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" />
<!-- toaster -->
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Angular-confirm-master -->
<link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
    <!-- Angular-confirm-master -->
<link rel="stylesheet" type="text/css" href="https://craftpip.github.io/jquery-confirm/css/jquery-confirm.css">
<style>
.line, .double-line{
  margin: 35px 0px 35px 0px;
}

.container-main{
  padding-bottom: 25px;
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
    display: none !important;
}
@media only screen and (max-width: 600px) {
  .breadcrumb{
      display: none;
  }
}
.col_half{
    margin-bottom: 0px !important;
}
.item_name{
  font-size: 18px;
  color: #444;
  font-weight: 600;
  line-height: 1.5;
  margin: 0 0 0 0 !important; 
}
.item_code{
  font-size: 24px;
  color: #444;
  font-weight: 600;
  line-height: 1.5;
  margin: 0 0 0 0 !important; 
}
.custom-btn{
    width: 100%;
}
.btn-table{
    width: 50%;
}
.tag {
  font: 20px/1.5 'PT Sans', serif;
   margin: 25px;
  font-weight:bold;
  background: crimson;
  border-radius: 3px 0 0 3px;
  color: #fff;
  display: inline-block;
  height: 26px;
  line-height: 25px;
  padding: 0 20px 0 23px;
  position: relative;
  margin: 0 0 0 0;
  text-decoration: none;
  -webkit-transition: color 0.2s;
}

.tag-highest {
  font: 15px/1.5 'PT Sans', serif;
  margin: 25px;
  font-weight:bold;
  background: #ddd;
  border-radius: 0 3px 3px 0;
  color: #000;
  display: inline-block;
  height: 34px;
  line-height: 30px;
  padding: 0 20px 0 23px;
  position: relative;
  margin: 0 0 0 0;
  text-decoration: none;
  -webkit-transition: color 0.2s;
}

.tag::before {
  background: #fff;
  border-radius: 10px;
  box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
  content: '';
  height: 6px;
  left: 10px;
  position: absolute;
  width: 6px;
  top: 12px;
}

.tag::after {
  background: #fff;
  border-bottom: 13px solid transparent;
  border-left: 10px solid crimson;
  border-top: 13px solid transparent;
  content: '';
  position: absolute;
  right: 0;
  top: 0;
}
.btn-default:active{
  background-color: #fff !important;
  color: #000 !important;
}
.btn-default:focus{
  background-color: #fff !important;
  color: #000 !important;
}
.call-image{
  max-width: 50% !important;
}
.toggle.android { border-radius: 0px;}
.toggle.android .toggle-handle { border-radius: 0px; }
.notice{
  font-size: 10px;
  color: #000;
}
.call-notice{
  font-size: 20px;
  text-align: center;
  font-weight: bold;
}
.highest_bid{
  font-size: 50px !important;
}
.text-red{
  color : red;
}
#page-title span{
  font-size       : 50px !important;
  font-family     : "Times New Roman", Times, serif;
  text-transform  : capitalize;
  font-weight     : bold; 
}
.itemprice{
  color: #000;
  font-weight: bold;
}
.unhold-bid{
  background-color: green !important;
  border: 2px solid green !important;
}
.hold-bid{
  background-color: red !important;
  border: 2px solid red !important;
}
.iframe-container{
  width: 100%
}
.responsive-iframe{
  width: 100%;
  height: 350px;
}
@media (min-width: 1200px){
  .container {
      width: 1350px;
  }
}
</style>
</head>
<body class="stretched" ng-controller="ItemBidController" ng-cloak>
  <!-- Document Wrapper ============================================= -->
  <div id="wrapper" class="clearfix">
    <!-- Header ============================================= -->
    <header id="header" class="full-header">
      <?php 
        $card_no = '';
        $auction = '';
      ?>
      @if(isset($customer_detail) && sizeof($customer_detail) > 0 && isset($customer_detail->customer_last_card) && sizeof($customer_detail->customer_last_card) > 0)
        <?php $card_no = $customer_detail->customer_last_card->card_no ?>
      @endif
      @if(isset($item) && sizeof($item->auction_detail) > 0 && $item->auction_detail->auction)
        <?php $auction = $item->auction_detail->auction ?>
      @endif
      @include('front_ui.includes.header_menu_dark', [
        'event_name' => $auction->event_name,
        'card_no'    => $card_no,
        'event_no'   => $auction->auction_no
      ]);
    </header>
    <!-- #header end -->
    <!-- Page Title ============================================= -->
    <section id="page-title">
      <div class="container clearfix">
        @if(isset($item) && sizeof($item->auction_detail) > 0 && $item->auction_detail->request_offer == OFFER_EXPIRED)
          <h1 class="nomargin text-center highest_bid">
            @if(isset($item->auction_detail->highest_bid) && 
              sizeof($item->auction_detail->highest_bid) > 0 &&
              isset($item->auction_detail->highest_bid->bid_user) &&
              sizeof($item->auction_detail->highest_bid->bid_user) &&
              Sentinel::getUser()->id == $item->auction_detail->highest_bid->bid_user->user_id)
                <span class="highest_price" style="color:green;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span>
            @else
                <span class="highest_price" style="color:red;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span>
            @endif
          </h1>
          <h1 class="nomargin text-center auctioneer_offer hide"><span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span></h4>
          <input type="hidden" name="request_offer" id="request_offer" value="{{$item->auction_detail->request_offer}}">
        @else
          <!-- <h4 class="nomargin text-center highest_bid hide"><span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span></h4> -->
          <h4 class="nomargin text-center auctioneer_offer highest_bid"><span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span></h4>
        @endif
        <input type="hidden" name="highest_price" id="highest_price" value="@{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price}}">
        @if(isset($item) && sizeof($item) > 0 && isset($item->auction_detail) && sizeof($item->auction_detail) > 0)
          <input type="hidden" name="request_offer" id="request_offer" value="{{$item->auction_detail->request_offer}}">
        @else
          <input type="hidden" name="request_offer" id="request_offer" value="0">
        @endif
        <input type="hidden" name="bid_count" id="bid_count" value="{{$count->bid_count}}">
        <!-- <ol class="breadcrumb text"> -->
       <!--    <li><a href="{{ url('/') }}">Home</a></li>
          @if(isset($category_id))
            <li>
              <a href="{{ url('auction-categories/'.$category_id) }}">{{ $item->category->display_name }}</a>
            </li>
            <li>
              <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}">{{$item->name}}</a>
            </li>
          @elseif(isset($auction_id))
            <li>
              <a href="{{ url('auctions/'.$auction_id) }}">{{ $item->auction_detail->auction->event_name }}</a>
            </li>
            <li>
              <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}">{{$item->name}}</a>
            </li>
          @else
            <li><a href="{{ url('my-account') }}">My Account</a></li>
            <li><a href="{{ url('my-account/item/'.$item->id) }}">{{$item->name}}</a></li>
          @endif
            <li class="active">{{ $item->name }} Bid</li>
        </ol> -->
      </div>
    </section>
    <!-- #page-title end -->
    <!-- Content ============================================= -->
    <section id="content">
      <div class="content-wrap">
        <div class="container clearfix container-main">
         <!--  <div class="single-product"> -->
            <!--  <div class="product"> -->
                  <!-- <div class="col_half"> -->
                      <!-- Product Single - Gallery ============================================= -->
                      <!-- Place somewhere in the <body> of your page -->
                      <!-- <div class="product-image"> -->
            <div class="row">
              <div class="col-xm-12 col-sm-12 col-md-6 col-lg-6">
                <!-- video view --> 
                <div class="form-process image-video-view" style="display: none;"></div>
                <div class="video-view @if($customer_detail && $customer_detail->customer_last_card && $customer_detail->customer_last_card->video_image_view_status == IMAGE_VIEW) hide @endif">
                  <div class="iframe-container">
                    <iframe class="responsive-iframe" id="iframe" src="{{$item->auction_detail->auction->auction_url}}" frameborder="0"></iframe>
                  </div>
                </div>
                <!-- image view -->
                <section class="slider image-view @if($customer_detail && $customer_detail->customer_last_card && $customer_detail->customer_last_card->video_image_view_status == VIDEO_VIEW) show @endif">
                  <div id="slider" class="flexslider">
                    <ul class="slides">
                      @if(sizeof($item->images) > 0)
                        @foreach($item->images as $image)
                          @if(File::exists(storage_path('uploads/images/item/'.$image->image_path)))
                            <li>
                              <img src="{{ url('core/storage/uploads/images/item/'.$image->image_path) }}" />
                            </li>
                          @else
                            <li>
                              <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                            </li>
                          @endif
                        @endforeach
                      @else
                        <li>
                          <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                        </li>
                      @endif
                    </ul>
                  </div>
                  <div id="carousel" class="flexslider">
                    <ul class="slides">
                      @if(sizeof($item->images) > 0)
                        @foreach($item->images as $image)
                          @if(File::exists(storage_path('uploads/images/item/'.$image->image_path)))
                            <li>
                                <img src="{{ url('core/storage/uploads/images/item/'.$image->image_path) }}" />
                            </li>
                            @else
                            <li>
                                <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                            </li>
                          @endif   
                        @endforeach
                      @else
                        <li>
                            <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                        </li>
                      @endif
                    </ul>
                  </div>
                </section>
                <!-- end image view -->
                <div class="form-group">
                    <div class="col-sm-12">
                      <div class="checkbox">
                        <label>
                          @if($customer_detail && $customer_detail->customer_last_card && $customer_detail->customer_last_card->video_image_view_status == IMAGE_VIEW)
                            <input type="checkbox" data-toggle="toggle" data-on="Go to Images" data-off="Go to Live Video" id="request_view" data-size="small" data-onstyle="warning" data-offstyle="info" data-style="quick" data-style="android" data-width="160">
                          @elseif($customer_detail->customer_last_card->video_image_view_status == VIDEO_VIEW)
                            <input type="checkbox" data-toggle="toggle" data-on="Go to Images" data-off="Go to Live Video" id="request_view" data-size="small" data-onstyle="warning" data-offstyle="info" data-style="quick" data-style="android" data-width="160" checked>
                          @else
                            <input type="checkbox" data-toggle="toggle" data-on="Go to Images" data-off="Go to Live Video" id="request_view" data-size="small" data-onstyle="warning" data-offstyle="info" data-style="quick" data-style="android" data-width="160">
                          @endif
                        </label>
                      </div>
                    </div>
                </div>
              </div>
              <!-- @if(isset($customer_detail) && sizeof($customer_detail) > 0 
              && isset($customer_detail->customer_last_card) && sizeof($customer_detail->customer_last_card) > 0)
              <center><button type="button" class="btn btn-primary">Your Auction Registration No <span class="badge">{{$customer_detail->customer_last_card->card_no}}</span></button></center>
              @endif -->
                  <!-- </div> -->
                 <!--  <div class="col_half col_last"> --><!-- product-desc -->
              <div class="col-xm-12 col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                  <div class="col-xm-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="text-center nomargin mobile-show-current-bid-price">
                    @if(isset($item->auction_detail->highest_bid) && 
                      sizeof($item->auction_detail->highest_bid) > 0 &&
                      isset($item->auction_detail->highest_bid->bid_user) &&
                      sizeof($item->auction_detail->highest_bid->bid_user) &&
                      Sentinel::getUser()->id == $item->auction_detail->highest_bid->bid_user->user_id)
                        <span class="highest_price" style="color:green;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span>
                    @else
                        <span class="highest_price" style="color:red;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | number:"" }}</span>
                    @endif
                    </h3>
                    <!-- <p class="item_code text-center" ng-if="item.show_hide_code == 1">({{$item->item_code}})</p>
                    <p class="item_code text-center" ng-if="item.show_hide_code == 0">(@{{ item.item_code | limitTo: 3 }}@{{item.item_code.length > 3 ? 'XXX' : ''}})</p> -->
                    <p class="item_code text-center">({{$item->item_code}})</p>
                    <p class="item_name text-center">{{$item->name}}</p>
                    <div class="form-process" style="display: none;"></div>
                    <!-- <h4 class="bottommargin-20">Enter Your Bid</h4> -->
                    <div class="col_full nobottompadding notoppadding col-padding nobottommargin">
                      <!-- <div class="row" ng-show="call > 0" ng-if="call < 3"> -->
                        @if(isset($item) && sizeof($item) > 0 && isset($item->auction_detail) && sizeof($item->auction_detail) > 0 && isset($item->auction_detail->auction) && sizeof($item->auction_detail->auction) > 0)
                          @if($item->auction_detail->auction->bid_now_active_status_2 == BIDDING_ACTIVE)
                            <div class="input-group input-group-lg bottommargin-20">
                              <span class="input-group-btn"><button class="btn" type="button">Next Bid <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button></span>
                              <input type="text" ng-model="price" id="bid_input" class="form-control bid-input input-lg" placeholder="Enter your bid" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" autofocus ng-disabled="call == 3" price/>
                              <span class="input-group-btn"><button class="btn btn-default" type="button" ng-disabled="!price || call == 3 ||hold == 0" ng-click="increment()" id="plus_button">+<i class="fa fa-plus"></i></button></span>
                            </div>
                            <div class="table-responsive">
                              <table class="table btn-table" align="center" border="0">
                                <tbody>
                                  <tr>
                                    <td ng-repeat="value in amounts">
                                      <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="setBid(value.amount)" ng-disabled="call == 3 || hold == 0" id="bid_button">@{{ showButtonAmount(value.amount) | number }}</button>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <p class="text-center">
                              <button class="button button-border button-rounded @if($item->auction_detail->auction->hold_online_bidding == BIDDING_ACTIVE) unhold-bid @else hold-bid @endif" type="button" ng-click="addBid(1,price,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" id="bid" ng-disabled="call == 3 || hold == 0">Bid</button>
                            </p>
                          @endif
                        @endif
                      <!-- </div> -->
                      <!-- Calling NOTICE -->
                      <div class="text-center">
                        <div class="tag" ng-show="call > 0" ng-if="call == 1"><b ng-bind="call_message"></b></div>
                        <div class="tag" ng-show="call > 0" ng-if="call == 2"><b ng-bind="call_message"></b></div>
                        <div class="tag" ng-show="call > 0" ng-if="call == 3"><b ng-bind="call_message"></b></div>
                      </div>
                      <!-- End Calling Notice -->
                      <br/>

                      <!-- @if(isset($item) && sizeof($item->auction_detail) > 0 && $item->auction_detail->request_offer == OFFER_EXPIRED)
                        <h4 class="nomargin text-center highest_bid">Highest Offer : 
                          @if(isset($item->auction_detail->highest_bid) && 
                              sizeof($item->auction_detail->highest_bid) > 0 &&
                              isset($item->auction_detail->highest_bid->bid_user) &&
                              sizeof($item->auction_detail->highest_bid->bid_user) &&
                              Sentinel::getUser()->id == $item->auction_detail->highest_bid->bid_user->user_id)
                            <span class="highest_price" style="color:green;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span>
                          @else
                            <span class="highest_price" style="color:red;">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span>
                          @endif
                        </h4>
                        <h4 class="nomargin text-center auctioneer_offer hide">Highest Offer : <span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span></h4>
                        <input type="hidden" name="request_offer" id="request_offer" value="{{$item->auction_detail->request_offer}}">
                      @else
                        <h4 class="nomargin text-center highest_bid hide">Highest Offer : <span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span></h4>
                        <h4 class="nomargin text-center auctioneer_offer highest_bid">Highest Offer : <span class="highest_price">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price | currency:"" }}</span></h4>
                      @endif
                      <input type="hidden" name="highest_price" id="highest_price" value="@{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price}}">
                      @if(isset($item) && sizeof($item) > 0 && isset($item->auction_detail) && sizeof($item->auction_detail) > 0)
                        <input type="hidden" name="request_offer" id="request_offer" value="{{$item->auction_detail->request_offer}}">
                      @else
                        <input type="hidden" name="request_offer" id="request_offer" value="0">
                      @endif
                      <input type="hidden" name="bid_count" id="bid_count" value="{{$count->bid_count}}"> -->
                    </div>
                    <div class="row">
                      <div class="col-xm-12 col-sm-12 col-md-12 col-lg-12">
                          <div class="form-group">
                              <p class="item_name text-center"><u>Details</u></p>
                              <p class="item_name text-center">
                                @if(isset($item))
                                  {{$item->description?:''}}<br/>
                                  {{$item->sns_opinion?:''}}
                                @endif
                              </p>
                          </div>
                          <!-- <div class="form-group">
                              <label for="exampleInputEmail1">Your Comment</label>
                              <textarea class="form-control" rows="2" placeholder="Place Your Comment Here !"></textarea>
                          </div> -->
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">                                    
                          <thead>
                              <tr>
                                  <th>Bidder</th>
                                  <th>Bid Amount(Rs)</th>
                                  <th>Bid Time</th>
                              </tr>
                          </thead>
                          <tbody class="bid-list">
                              <tr ng-repeat="item in bids | limitTo: 5 ">
                                  <td ng-if="item.sns_customer_id != bid_user_id && show_hide == 1">@{{item.bidder.fname }}</td>
                                  <td ng-if="item.sns_customer_id != bid_user_id && show_hide == 0">@{{item.bidder.fname | limitTo: 6 }}@{{item.bidder.fname.length > 5 ? '...####' : ''}}</td>
                                  <td ng-if="item.sns_customer_id == bid_user_id">You</td>
                                  <td class="text-right">@{{item.bid_amount | currency : "Rs " }}</td>
                                  <td class="text-center">@{{item.bid_time}}</td>
                              </tr>
                              <tr ng-show="bids.length == 0">
                                  <td colspan="3">No bids.</td>
                              </tr>
                          </tbody>
                      </table>
                    </div>
                    <!--<div class="table-responsive">
                      <table class="table btn-table" align="center">
                        <tbody>
                          <tr>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,10,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">10</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,20,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">20</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,50,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">50</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,100,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">100</button>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,500,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">500</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,1000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">1,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,2000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">2,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,5000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">5,000</button>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,10000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">10,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,20000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">20,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,50000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">50,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-click="addBid(0,100000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">100,000</button>
                            </td>
                          </tr>
                         
                          <tr>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" ng-click="addBid(0,200000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">200,000</button>
                            </td>
                             <td>
                               <button class="btn btn-default btn-sm custom-btn click-btn" ng-click="addBid(0,500000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">500,000</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" ng-click="addBid(0,1000000,((bids[0].bid_amount)?bids[0].bid_amount:start_price))" ng-disabled="call == 3">1,000K</button>
                            </td>
                            <td>
                              <button class="btn btn-default btn-sm custom-btn click-btn" type="submit" ng-disabled="call == 3">Clear </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <p class="notice text-center">Noted : When you click amount buttons it will increase current bid by default.</p>
                    </div> -->
                    <!-- end table-responsive -->
                    
                  </div>
                </div>
              </div>
            </div>
            <hr/>
            <!-- <div class="row" ng-show="call > 0" ng-if="call == 3">
              <div class="col-lg-12 text-center">
                <div class="well">
                  <h4 class="nomargin">Highest Bidder Amount</h4>
                  <div class="itemprice bottommargin-20">Rs @{{ (bids[0].bid_amount)?bids[0].bid_amount:start_price }}</div>
                  <h4 class="nomargin text-uppercase">Congratulations! <br/><small>@{{ (bids[0])?bids[0].bidder.first_name+' '+bids[0].bidder.last_name:'' }}</small></h4>
                  <p class="nomargin"><small></small></p>
                  <div class="clear"></div>
                  @if(Sentinel::check() && sizeof($item->auction_detail->highest_bid) > 0 && sizeof($item->auction_detail->highest_bid->bid_user) > 0 && $user->id == $item->auction_detail->highest_bid->bid_user->user_id)
                    @if(isset($item->auction_detail) && sizeof($item->auction_detail) && isset($item->auction_detail->approved) && sizeof($item->auction_detail->approved) > 0)
                      @if(isset($item->payment) && sizeof($item->payment) > 0)      

                        @if(isset($category_id))
                            <a ng-show="bids[0] && bids[0].sns_customer_id == uid" href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                        @elseif(isset($auction_id))
                            <a ng-show="bids[0] && bids[0].sns_customer_id == uid" href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                        @else
                            <a ng-show="bids[0] && bids[0].sns_customer_id == uid" href="{{ url('my-account/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                        @endif
                      @else
                        @if(isset($category_id))
                            <a ng-show="bids[0] && bids[0].sns_customer_id == uid" href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Please Complete Payment</a>
                        @elseif(isset($auction_id))
                            <a  href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Please Complete Payment</a>
                        @else
                            <a ng-show="bids[0] && bids[0].sns_customer_id == uid" href="{{ url('my-account/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Please Complete Payment</a>
                        @endif
                      @endif
                    @else
                        <button type="button" class="button button-3d button-rounded button-white button-light">Waiting for payment approval</button>
                    @endif
                  @else
                    <a href="{{url('auctions/'.$item->auction_detail->auction->id)}}" class="button button-sm button-border button-rounded">Go to Next Item</a>
                  @endif
                </div>
              </div>
            </div> -->
            <div class="row">
              <!-- <div class="col-xm-12 col-sm-6 col-md-6 col-lg-6">
              </div> -->
              <div class="col-xm-12 col-sm-12 col-md-12 col-lg-12">
                @if(isset($customer_detail) && sizeof($customer_detail) > 0 
                && isset($customer_detail->customer_last_card) && sizeof($customer_detail->customer_last_card) > 0)
                <p class="text-center"><button type="button" class="btn btn-primary">PADDLE <span class="badge">{{$customer_detail->customer_last_card->card_no}}</span></button></p>
                @endif
                  <!-- <div class="form-group">
                      <label for="exampleInputEmail1"><u>About This Item</u></label>
                      <p class="">
                        @if(isset($item))
                          {{$item->description}}<br/>
                          {{$item->sns_opinion}}
                        @endif
                      </p>
                  </div> -->
                  <!-- <div class="form-group">
                      <label for="exampleInputEmail1">Your Comment</label>
                      <textarea class="form-control" rows="2" placeholder="Place Your Comment Here !"></textarea>
                  </div> -->
              </div>
            </div>  
        </div>
      </div>
    </section>
    <!-- #content end -->
    <!-- Footer ============================================= @ include('front_ui.includes.footer') -->
    
    <!-- #footer end -->
  </div>
  <!-- #wrapper end -->
  <!-- Go To Top
  ============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>
<script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>
<!-- Footer Scripts ============================================= -->
<script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
<!-- External JavaScripts ============================================= -->
<script defer src="{{asset('assets/front/js/jquery.flexslider.js')}}"></script>
<!-- ANGULAR -->
<script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
<!-- ANGULAR -->
<!-- angular-confirm-master -->
<script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
<!-- angular-confirm-master -->
<!-- pusher library -->
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<!-- pusher -->
<!-- toaster -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!-- toaster -->
<!-- toogle -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- toogle -->
<!-- accounting js -->
<script src="{{asset('assets/dist/accounting/accounting.min.js')}}"></script>
<!-- accounting js -->
<script>
window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')
</script>
<script type="text/javascript">
var a              = 1;
var detail         = $('#request_offer').val();
var request_window = null;
var log_user_id    = '{{$customer_detail->id}}';
var user_id        = '{{$logged_user->id}}';
var dialog_box1    = '';
var bid_count      = '{{$count->bid_count}}';
var hold_status;
var wining_confirm_window;
var auction_id     = '{{$item->auction_detail->sns_auction_id}}';
/** set enter key press for bid button */
$(document).bind('keypress', function(e) {
  if(hold_status == '{{ BIDDING_ACTIVE }}'){
    if(e.keyCode == 13){
      $('#bid').trigger('click');
    }
  }
});

//Image Slider
$(window).load(function() {
  $(document).find('.my-space, .standard-logo, .retina-logo').hide();
  $(document).find(".my-account, .standard-logo, .live-logo").attr('href', '').css({'cursor': 'pointer', 'pointer-events' : 'none'});
  $(document).find('.live-logo').removeClass('hide');
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    // animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel",
    start: function(slider) {
        $('body').removeClass('loading');
    }
  });
});
      
//request to show video or image
$('#request_view').change(function() {
  $(".image-video-view").show();
  var viewStatus = 0;
  if(this.checked) {
    $('.video-view').removeClass('hide');
    $(".image-video-view").hide();
    viewStatus = 1;
    $('#iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');  
  }else{
    $('.video-view').addClass('hide');
    $('.image-view').removeClass('hide'); 
    $(".image-video-view").hide();
    viewStatus = 0;
    $('#iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');  
  }
  changeImageVideoView(auction_id, log_user_id, viewStatus);
  window.scrollTo(0,0);      
});

'use strict';
var app = angular.module('angularApp',['cp.ngConfirm']);
app.controller('ItemBidController', function ($scope,$ngConfirm,$http,BASE_URL,$filter) {

  $scope.uid              = {!! Sentinel::getUser()->id !!};        
  $scope.item             = {!! $item !!};
  $scope.call             = $scope.item.auction_detail.call;
  $scope.status           = $scope.item.auction_detail.status;
  $scope.hold             = $scope.item.auction_detail.auction.hold_online_bidding;
  hold_status             = $scope.item.auction_detail.auction.hold_online_bidding;
  $scope.show_hide        = $scope.item.auction_detail.auction.show_hide_bidder;
  var withdrawal_price    = Number($scope.item.auction_detail.withdraw_amount||0);
  var auction_id          = $scope.item.auction_detail.sns_auction_id;
  $scope.amounts          = {!! $buttonValues !!};

  if($scope.call == '{{CALL1}}'){
    $scope.call_message = 'Going Once';
  }

  if($scope.call == '{{CALL2}}'){
    $scope.call_message = 'Going Twise';
  }

  if($scope.call == '{{CALL3}}'){
    if($scope.uid == '{{$user->id}}'){
      $scope.call_message = 'Sold to You';
    }else{
      $scope.call_message = 'Sold to another bidder';
    }
  }

  //Show online bidding hold message
  if(hold_status == '{{BIDDING_INACTIVE}}' && bid_count == 1){
    toastr.error('{{ONLINE_BID_HOLD_MESSAGE2}}');
  }

  if($scope.item.auction_detail.approved != null){
    $scope.item_final_status = $scope.item.auction_detail.approved.status;
  }
  
  $scope.bid_user_id  = {!! $customer_detail['id'] !!};
  $scope.detail_id    = {!! $detail_id !!};
  $scope.bids         = {!! $bids !!};           
  $scope.start_price  = {!! $start_price !!};
  var current_price   = Number(($scope.bids[0].bid_amount)?$scope.bids[0].bid_amount:$scope.start_price || 0);
  $scope.price        = current_price + withdrawal_price;

  Pusher.logToConsole = false;
  var pusher = new Pusher('55845bb31c3702ef4dd0', {
    encrypted: true,
    cluster  : 'ap2',
    forceTLS : true
  });
  var channel = pusher.subscribe('auction-channel');
  
  //bid event
  channel.bind('bid-event', function(data) {
    if(data.list.auction_detail.sns_item_id ==  "{{Request::route('bid_item_id')}}"){
      $scope.$apply(function () {
        $scope.call = data.list.auction_detail.call;
        $scope.bids.splice(0, 0, data.list);
        current_price   = Number((data.list.bid_amount)?data.list.bid_amount:$scope.start_price || 0);
        $scope.price    = current_price + withdrawal_price;
      });

      var value  = parseInt(data.list.bid_amount);
      var amount = 'Rs ' + value.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      //toastr.info('New bid on this item.'+' '+amount);
      $('.highest_price').html(amount);
      if(data.list.bid_user.user_id == {!! Sentinel::getUser()->id !!}){
        $('.highest_price').css('color', 'green');//display highest bid price
      }else{
        $('.highest_price').css('color', 'red');//display highest bid price
      }

      $('#request_offer').val(data.list.auction_detail.request_offer);
        
      if(data.list.auction_detail.request_offer == '{{OFFER_EXPIRED}}'){
        $('.auctioneer_offer').addClass('hide');
        $('.highest_bid').removeClass('hide');
      }

    }
  });

  //call request event
  channel.bind('call-event', function(data) {
    //if(auction_id === data?.call.detail?.customer_auction?.auction_id){
      $scope.$apply(function () {
        $scope.call   = data.call.call_status;
        if(data.call.call_status == '{{CALL1}}'){
          $scope.call_message = 'Going Once';
          $ngConfirm({
            theme: 'bootstrap',
            content:'<center><span class="call-notice">'+$scope.call_message+'</span></center><br/><p class="text-center"></p>'+
            '<p></p>',
            title:"Auctioneer's Notice",
            autoClose: 'close|1000',
            closeIcon: false,
            animation: 'scale',
            type: 'blue',
            buttons: {
              close: function () {}
            }
          });
        }
        if(data.call.call_status == '{{CALL2}}'){
          $scope.call_message = 'Going Twice';
          $ngConfirm({
            theme: 'bootstrap',
            content:'<center><span class="call-notice">'+$scope.call_message+'</span></center><br/><p class="text-center"></p>',
            title:"Auctioneer's Notice",
            autoClose: 'close|1000',
            closeIcon: false,
            animation: 'scale',
            type: 'blue',
            buttons: {
              close: function () {}
            }
          });
        }
        if(data.call.call_status == '{{CALL3}}'){
          $scope.status = data.call.call_status;
          if(data.call.detail.bidder.user_id == '{{$user->id}}'){
            $scope.call_message = 'Sold to You';
            dialog_box1 = '<div class="row">'+
                    '<div class="col-lg-12 text-center">'+
                      '<div class="well">'+
                        '<h4 class="nomargin">Winning Bid Amount</h4>'+
                        '<div class="itemprice bottommargin-20">'+accounting.formatMoney(data.call.detail.bid_amount, "Rs ")+'</div>'+
                        '<h4 class="nomargin text-uppercase">Congratulations! <br/><small>"{{$user->first_name}}"</small></h4>'+
                        '<p class="nomargin"><h3>You Won This Item<h3></p>'+
                        '<div class="clear"></div>'+
                        '<button type="button" class="button button-3d button-rounded button-white button-light">Waiting for the Auctioneer`s Approval</button>'
                      '</div>'+
                    '</div>'+
                  '</div>';
          }else{
            $scope.call_message = 'Sold to another bidder';
            dialog_box1 = '<div class="row">'+
                    '<div class="col-lg-12 text-center">'+
                      '<div class="well">'+
                        '<h4 class="nomargin">Winning Bid Amount</h4>'+
                        '<div class="itemprice bottommargin-20">'+accounting.formatMoney(data.call.detail.bid_amount, "Rs ")+'</div>'+
                        '<h1 class="nomargin text-uppercase text-red"> SOLD <br/></h1>'+
                        '<div class="clear"></div>'+
                        // '<a href="{{url("auctions/1")}}" class="button button-sm button-border button-rounded">Go to Next Item</a>'+
                      //  '<h3>Please Wait untill the next item is starting</h3>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
          }
          wining_confirm_window = $ngConfirm({
            theme       : 'bootstrap',
            content     : dialog_box1,
            title       : "Auctioneer's Message",
            closeIcon   : false,
            animation   : 'scale',
            columnClass : 'medium',
            type        : 'blue',
            buttons     :{
              close: function () {}
            }
          });
        }
      });
    //}
  });

  //ITEM PAYMENT EVENT
  channel.bind('sold-event', function(data) {
    var con = '';
    $scope.status = data.call.call_status;
    
    if(data.call.bidder.user_id == '{{$user->id}}'){
      window.location.href ="{{URL::to('auctions')}}/"+data.detail.sns_auction_id+"/item/"+data.detail.sns_item_id+"/item-payment/";
      // con = '<div class="row">'+
      //         '<div class="col-lg-12 text-center">'+
      //           '<div class="well">'+
      //             '<h4 class="nomargin">Highest Bidding Value</h4>'+
      //             '<div class="itemprice bottommargin-20">Rs '+data.call.bid_amount+'</div>'+
      //             '<h4 class="nomargin text-uppercase">Congratulations! <br/><small>"{{$user->first_name}}"</small></h4>'+
      //             '<p class="nomargin"><h3>You Won this item<h3></p>'+
      //             '<div class="clear"></div>'+
      //             '<button type="button" class="button button-3d button-rounded button-white button-light">Waiting for payment approval</button>'
      //           '</div>'+
      //         '</div>'+
      //       '</div>';
    }else{
      con = '<div class="row">'+
              '<div class="col-lg-12 text-center">'+
                '<div class="well">'+
                  '<h4 class="nomargin">Highest Bidding Value</h4>'+
                  '<div class="itemprice bottommargin-20">'+accounting.formatMoney(data.call.bid_amount, "Rs ")+'</div>'+
                  '<h4 class="nomargin text-uppercase">You Lost This Item<br/></h4>'+
                  '<div class="clear"></div>'+
                  // '<a href="{{url("auctions/1")}}" class="button button-sm button-border button-rounded">Go to Next Item</a>'+
                 '<h3>Please Wait untill the next item is starting</h3>'+
                '</div>'+
              '</div>'+
            '</div>';
      $ngConfirm({
        theme     : 'bootstrap',
        content   :con,
        title     :"Auctioneer's Notice",
        closeIcon : false,
        animation : 'scale',
        type      : 'blue',
        buttons   : {
          close : function () {}
        }
      });
    }
    call = 'Final Call';
    
  });

  /** REDIRECT TO PAYMENT METHOD SELECT PAGE
   * Rule : Customer allocated New | Approved customer types, after won the item redirect to payment page
   * Rule : Customer allocated Gold | Platinum customer types, after won the item do not redirect to payment page
   * 
   */
  channel.bind('payment_event', function(data) {
    var payment_redirection = '{{DEFAULT_STATUS}}';
    if(log_user_id == data.customer_id){
      $.ajax({
        url   : "{{URL::to('front/customer')}}",
        method: 'GET',
        data  : {
          user_id : user_id
        },
        async : false,
        success: function (data) {
          if(data && data.customer && data.customer.allowed_customer_type && data.customer.allowed_customer_type.customer_types){
            payment_redirection = data.customer.allowed_customer_type.customer_types.payment_redirection;
          }else{
            payment_redirection = '{{DEFAULT_STATUS}}';
          }
        },
        error: function () {
          toastr.error('Error...');
        }
      });
      
      if(payment_redirection > 0){
        var url = '{{url('auctions')}}';
        window.location.href = url+"/"+data.auction_id+"/item/"+data.item_id+"/item-payment";
      }else{
        wining_confirm_window.close();
        toastr.info('{{WAITING_MESSAGE_START_ITEM}}');
      }
    }
  });

  //ITEM NOT SOLD EVENT
  channel.bind('sold-reject-event', function(data) {
    //if(auction_id === data?.sns_auction_id){
      $scope.status = data.call.call_status;
      var next_url  = '{{url('auctions')}}'+'/'+data.sns_auction_id;
      //wining_confirm_window.close();
      var con = `<div class="row">
              <div class="col-lg-12 text-center">
                <div class="well">
                  <h4 class="nomargin">Highest Bidding Value</h4>
                  <div class="itemprice bottommargin-20">`+accounting.formatMoney(data.highest_bid.bid_amount, "Rs ")+`</div>
                  <p class="nomargin"><h2> Sorry, Not Sold <h2></p>
                  <div class="clear"></div>
                </div>
              </div>
            </div>`;
      $ngConfirm({
        theme     : 'bootstrap',
        content   : con,
        title     : "Auctioneer's Notice",
        closeIcon : false,
        animation : 'scale',
        type      : 'blue',
        buttons: {
          close: function () {}
        }
      });
    //}
  });

  // offer request event
  channel.bind('offer-request-event', function(data) {
    $('#request_offer').val('{{OFFER_REQUEST}}');
    $scope.$apply(function () {
      toastr.info('Notice : Offer Requested');
      request_window = $ngConfirm({
        title: 'Offer Requested',
        icon: 'fa fa-gavel',
        autoClose: 'cancel|5000',
        content: '<strong>Auctioneer wating for your offer.</strong>' +
        '<br/><span><strong>Auctioneer Offer : '+accounting.formatMoney(data.bid_amount, "Rs ")+'</strong></span>',
        // '<input type="text" class="form-control input-sm" placeholder="Your Offer" ng-model="price" ng-focus>',
        theme: 'bootstrap',
        animation: 'scale',
        buttons: {
          okay: {
            btnClass: "btn-blue",
            text : "Offer Now",
            action: function(scope, button){
                // if(data.bid_amount < scope.price){
                //   $scope.addBid(1,scope.price,0);
                // }else{
                //   toastr.error('Please enter highest price.');
                //   return false;
                // }
            }
          }
        },
      })
    });
  });

  // offer bid event
  channel.bind('offer-event', function(data) {
    toastr.info('Customer Higher Offer is '+accounting.formatMoney(data.offer.offer_amount, "Rs "));
  });

  //next item
  channel.bind('next-item-event', function(data) {
    //if(auction_id === data?.auction_id){
      window.location.href = "{{URL::to('auctions')}}/"+data.auction_id+"/item/"+data.item_id+"/item-bid/"+data.item_id+"";
    //}
  });

  //offer confirm event
  channel.bind('offer-confirm-event', function(data) {
    if(data.list.auction_detail.sns_item_id ==  "{{Request::route('bid_item_id')}}"){
        $scope.call = data.list.auction_detail.call;
        $scope.bids.splice(0, 0, data.list);
        var value  = parseInt(data.list.bid_amount);
        var amount = 'Rs ' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        //toastr.info('New bid on this item.'+' '+amount);
        $('.highest_price').html(amount);
        $('#request_offer').val(data.list.auction_detail.request_offer);
        
        if(data.list.auction_detail.request_offer == '{{OFFER_EXPIRED}}'){
          $('.auctioneer_offer').addClass('hide');
          $('.highest_bid').removeClass('hide');
        }
        location.reload();
    }
  });
  
  //validate & disabled over bidding item to rebidding & give message to user
  channel.bind('over-item-rebid-disabled', function(data) {
    if(log_user_id == data[1]){
      toastr.info('Bidding over for this item. Please make your mayment.');
      $ngConfirm({
        title   : "<strong>Auctioneer's Notice</strong>",
        content : 'Bidding over for this item. Please make your payment.',
        buttons : {
          confirm : {
            text : 'Make Payment',
            action: function(){
              var url = '{{url('auctions')}}';
              window.location.href = url+"/"+data[0].auction_id+"/item/"+data[0].item_id+"/item-payment";
            }
          }
          // cancel: function () {}
        }
      });
    }  
  });

  //Online bidding hold event from backend auctioneer
  channel.bind('hold-bidding', function(data) {
    //if(auction_id == data.auctionId){
      $scope.hold = data.holdStatus;
      hold_status = data.holdStatus;
      if(data.holdStatus == '{{BIDDING_ACTIVE}}'){
        $('#bid').prop('disabled', false);
        $('#bid').addClass('unhold-bid').removeClass('hold-bid');
        toastr.success('{{ONLINE_BID_UNHOLD_MESSAGE}}');
      }else if(data.holdStatus == '{{BIDDING_INACTIVE}}'){
        toastr.error('{{ONLINE_BID_HOLD_MESSAGE}}');
        $('#bid').prop('disabled', true);
        $('#bid').addClass('hold-bid').removeClass('unhold-bid');
      }
    //}
  });

  //Show & Hider Online Bidder Name
  channel.bind('show-hide-bidder', function(data) {
    //if(auction_id == data.auctionId){
      $scope.show_hide = data.showHideStatus;
      $scope.$watch($scope.show_hide, function(){
        $scope.show_hide = data.showHideStatus
      })
      if(data.showHideStatus == '{{BIDDING_ACTIVE}}'){
        toastr.success('Showing Online Bidder');
      }else{
        toastr.warning('Hiding Online Bidder');
      }
    //}
  });
  
  /*****Pusher End******/

  $scope.addBid = function(type, amount, topAmount){

    //$ngConfirm({
    //  title   : "<strong>Auctioneer's Notice</strong>",
    //  content : 'Please Confirm Your Bid <strong>'+accounting.formatMoney(amount, "Rs ")+'</strong>',
    //  buttons : {
    //    confirm: function () {
          $(".form-process").show();
          if(type > 0){
            amount = amount;
          }else{
            if($('#request_offer').val() == '{{OFFER_DEFAULT}}' || $('#request_offer').val() == '{{OFFER_EXPIRED}}'){
              amount = parseInt(amount) + parseInt(topAmount);
              if(amount <= topAmount){
                $('#bid_input').select();
                $ngConfirm({
                  theme     : 'material',
                  content   :'Please enter highest price.',
                  title     :'Notice!',
                  closeIcon : false,
                  buttons: {
                    ok  : function () {} 
                  }
                });

                return false;
              }
            }else{
              amount = parseInt(amount);
              $(".form-process").show();
            }
          }
          if(amount > 0){
            $.ajax({
              url: "{{URL::to('auction/bid')}}",
              method: 'GET',
              data: {
                bid_count   : $('#bid_count').val(),
                amount      :amount,
                detail_id   :$scope.detail_id,
                auction     :$scope.item.auction_detail.sns_auction_id,
                offer_status:$('#request_offer').val(),
                bidder_type :'{{ONLINE_BIDDER}}'
              },
              async: false,
              success: function (data) {
                $(".form-process").hide();
                if(data.list != 0){
                  toastr.success('Your Bid Sent');
                  if($('#request_offer').val() == '{{OFFER_REQUEST}}'){
                    $scope.call    = '{{CALL0}}';                   
                    $scope.price   = "";
                    $('#request_offer').val('{{OFFER_REQUEST}}');
                    $('#bid_count').val(data.count.bid_count);
                    $('#bid_input').val('');
                  }else{ 
                    $scope.call    = data.list.auction_detail.call;                   
                    $scope.price   = "";
                    $('#request_offer').val(data.list.auction_detail.request_offer);
                    $('#bid_count').val(data.count.bid_count);
                    $('#bid_input').val('');
                  }
                }else{
                  toastr.error('You’ve been outbidded');
                  $scope.price = (current_price + withdrawal_price);
                }
              },
              error: function () {
                toastr.error('Please try again');
              }
            });
          }else{
            $(".form-process").hide();
            $scope.price = (current_price + withdrawal_price);
            toastr.error('You’ve been outbidded');
          }     
        //},
       // cancel: function () {}
      //}
    //});
  };

  //input value bid increments by item withdrawal_price
  $scope.increment = function(){
    $scope.price   = Number($scope.price||0) + withdrawal_price;
  };

  $scope.setBid = function(value){
    $scope.price = Number(current_price) + Number(value);
  };

  $scope.showButtonAmount = function(value) {
    return Number(current_price) + Number(value);
  }

});
app.constant('BASE_URL', "{{asset('/')}}");
app.directive('price', function ($filter) {
  return {
      require: '?ngModel',
      link: function (scope, elem, attrs, ctrl) {
          if (!ctrl) {
              return;
          }

          ctrl.$formatters.unshift(function () {
              return $filter('number')(ctrl.$modelValue);
          });

          ctrl.$parsers.unshift(function (viewValue) {
              var plainNumber = viewValue.replace(/[\,\.]/g, ''),
                  b = $filter('number')(plainNumber);

              elem.val(b);

              return plainNumber;
          });
      }
  };
});

//Change Video & Image View 
function changeImageVideoView(auction_id, customer_id, status){
  $.ajax({
    url   : "{{URL::to('auction/change-image-video')}}",
    method: 'GET',
    data  : {
      auction_id : auction_id,
      customer_id: customer_id,
      status     : status
    },
    async : false,
    success: function (data) {
    },
    error: function () {
      toastr.error('Error...');
    }
  });
}

/*app.directive('price', [function () {
  return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
          attrs.$set('ngTrim', "false");
          var formatter = function(str, isNum) {
              str = String( Number(str || 0) / (isNum?1:100) );
              str = (str=='0'?'0.0':str).split('.');
              str[1] = str[1] || '0';
              return str[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + '.' + (str[1].length==1?str[1]+'0':str[1]);
          }
          var updateView = function(val) {
              scope.$applyAsync(function () {
                  ngModel.$setViewValue(val || '');
                  ngModel.$render();
              });
          }
          var parseNumber = function(val) {
              var modelString = formatter(ngModel.$modelValue, true);
              var sign = {
                  pos: /[+]/.test(val),
                  neg: /[-]/.test(val)
              }
              sign.has = sign.pos || sign.neg;
              sign.both = sign.pos && sign.neg;
              
              if (!val || sign.has && val.length==1 || ngModel.$modelValue && Number(val)===0) {
                  var newVal = (!val || ngModel.$modelValue && Number()===0?'':val);
                  if (ngModel.$modelValue !== newVal)
                      updateView(newVal);
                  
                  return '';
              }
              else {
                  var valString = String(val || '');
                  var newSign = (sign.both && ngModel.$modelValue>=0 || !sign.both && sign.neg?'-':'');
                  var newVal = valString.replace(/[^0-9]/g,'');
                  var viewVal = newSign + formatter(angular.copy(newVal));

                  if (modelString !== valString)
                      updateView(viewVal);

                  return (Number(newSign + newVal) / 100) || 0;
              }
          }
          var formatNumber = function(val) {
              if (val) {
                  var str = String(val).split('.');
                  str[1] = str[1] || '0';
                  val = str[0] + '.' + (str[1].length==1?str[1]+'0':str[1]);
              }
              return parseNumber(val);
          }
          
          ngModel.$parsers.push(parseNumber);
          ngModel.$formatters.push(formatNumber);
      }
  };
}]);*/
//CHAT FEATURE
//Start of Tawk.to Script
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
//     var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
//     s1.async=true;
//     s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
//     s1.charset='UTF-8';
//     s1.setAttribute('crossorigin','*');
//     s0.parentNode.insertBefore(s1,s0);
// })();
//End of Tawk.to Script
</script>    
</body>
</html>
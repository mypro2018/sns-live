<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="myApp">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- jquery confirm -->
    <link rel="stylesheet" href="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}">
    <!-- jquery confirm -->

    <!-- Angular-confirm-master -->
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
    <!-- Angular-confirm-master -->
    <!-- Angular Material File Input -->
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/angular-material.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.css')}}">
    <!-- Angular Material File Input -->
    <style>

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }

        .bottommargin-sm{
            margin-bottom:15px !important;
        }

        .userprofile-image-area{
            padding: 0;
        }

        .thumbnail{
            width: 100%;
            height: 100%;
            margin: 0;
            max-width: 242px;
            border-radius: 0;
        }

        .nomarginbottom{
            margin-bottom: 0;
        }

        #content p{
            margin-bottom: 0;
        }

        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
        .img-rounded{
            width:100px;
            height:100px;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | My Account</title>
</head>
<body class="stretched" ng-cloak>
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>My Account</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">My Account</li>
                </ol>
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="col-md-7 col-md-offset-2">
                        <div class="pricing-box pricing-extended bottommargin clearfix">
                            <div class="userprofile-image-area">
                                <a href="#">
                                    <img alt="100%x180" src="{{asset('assets/images/avatar.png')}}" style="display: block; margin: 37px;">
                                </a>
                            </div>
                            <div class="userprofile-desc">
                                <div class="pricing-title">
                                    <h3>{{ $customer->fname }} {{ $customer->lname }}</h3>
                                    <h3>Account No : {{ $customer->id }}</h3>
                                </div>
                                <div class="topmargin20">
                                    <p><strong>Contact No:</strong> {{ $customer->telephone_no }}</p>
                                    <p><strong>Address:</strong> {{ $customer->address_1 }}</p>
                                    <p><strong>E-Mail:</strong> {{ $customer->email }}</p>
                                    <p><strong>Last Login at:</strong> {{ $customer->user->last_login }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col_full nomarginbottom">
                        <div class="tabs tabs-bordered clearfix nomarginbottom" id="tab-2">
                            <div class="form-process" style="display: none;"></div>
                            <ul class="tab-nav clearfix">
                                <li><a href="#tabs-5"><i class="icon-user norightmargin"></i>  My Profile</a></li>
                                <!-- <li><a href="#tabs-6"><i class="icon-legal norightmargin"></i>  Bidding History</a></li> -->
                                <li><a href="#tabs-7"><i class="icon-file-text norightmargin"></i>  My Watchlist</a></li>
                                <li><a href="#tabs-8"><i class="icon-money norightmargin"></i>  My Winning History</a></li>
                                <li><a href="#tabs-9"><i class="icon-money norightmargin"></i>  My Auctions</a></li>
                            </ul>
                            <div class="tab-container">
                                <!-- MyController -->
                                <div ng-controller="MyController" class="tab-content clearfix" id="tabs-5">
                                    <form class="nobottommargin" name="myForm" method="post" enctype="multipart/form-data">
                                        {!!Form::token()!!}                                        
                                        <div class="col_half" id="first_name_error">
                                            <label for="first_name">First Name:</label>
                                            <input type="text" name="first_name" ng-model="first_name" class="form-control validate" readonly/>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="col_half col_last" id="last_name_error">
                                            <label for="last_name">Last Name:</label>
                                            <input type="text" name="last_name" ng-model="last_name" class="form-control validate" readonly/>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col_half" id="email_error">
                                            <label for="email">Email Address:</label>
                                            <input type="email" name="email" ng-model="email" autocomplete="off" class="form-control validate" ng-disabled="true"/>
                                            <label class="error" for="label"></label>
                                            <span class="error" ng-show="myForm.email.$error.email" style="color: red; line-height: 2;">
                                                Invalid email address.
                                            </span>
                                        </div>
                                        <div class="col_half col_last" id="contact_no_error">
                                            <label for="contact_no">Contact Number:</label>
                                            <input type="text" name="contact_no" ng-model="contact_no" class="form-control validate" readonly/>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col_full formfull" id="address_1_error">
                                            <label for="address_1">Address 01:</label>
                                            <textarea rows="1" name="address_1" ng-model="address_1" class="form-control validate" readonly></textarea>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col_full formfull">
                                            <label for="address_2">Land number / Special Note:</label>
                                            <textarea rows="1" name="address_2" ng-model="address_2" class="form-control" readonly></textarea>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col_half" id="city_error">
                                            <label for="city">City:</label>
                                            <input type="text" name="city" ng-model="city" class="form-control validate" readonly/>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="col_half col_last" id="zip_code_error">
                                            <label for="zip_code">Zip Code / Postal ID:</label>
                                            <input type="text" name="zip_code" ng-model="zip_code" class="form-control validate" readonly/>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="col_full formfull" id="country_error">
                                            <label for="country">Country</label>
                                            <select class="form-control input-sm validate"
                                                name="country"
                                                option="countries"
                                                ng-model="country"
                                                ng-options="country.country_name for country in countries track by country.id" readonly>
                                                <option value="">- Select Country -</option>
                                            </select>
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="col_half" id="password_error">
                                            <label for="password">Password:</label>
                                            <input type="password" name="password" ng-model="password" class="form-control optional-validate" />
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="col_half col_last" id="confirm_password_error">
                                            <label for="confirm_password">Re-enter Password:</label>
                                            <input type="password" name="confirm_password" ng-model="confirm_password" class="form-control optional-validate" />
                                            <label class="error" for="label"></label>
                                        </div>
                                        <div class="col_half">
                                            @if($customer->nic_image_path != null)
                                                <img class="img-rounded nic-file" src="{{ url('core/storage/uploads/images/customer/nic-licence')}}/{{$customer->nic_image_path}}"/><br/>NIC/Licence
                                            @else
                                                <img class="img-rounded proof-file" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/><br/>NIC/Licence
                                            @endif
                                        </div>
                                        <div class="col_half col_last">
                                            @if($customer->proof_image_path != null)
                                                <img class="img-rounded nic-file" src="{{ url('core/storage/uploads/images/customer/proof')}}/{{$customer->proof_image_path}}"/><br/>Proof of Address Document
                                            @else
                                                <img class="img-rounded proof-file" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/><br/>Proof of Address Document
                                            @endif
                                            </div>
                                            <div class="col_half">
                                                <label for="password">Upload Customer NIC/Drving Licence Image</label>
                                                <lf-ng-md-file-input lf-files="file" lf-browse-label="Browse..." lf-api="lfApi" ng-disabled="img_upload1" preview></lf-ng-md-file-input>
                                            </div>
                                            <div class="col_half col_last">
                                                <label for="password">Upload Proof of Address Document</label>
                                                <lf-ng-md-file-input lf-files="file2" lf-browse-label="Browse..." lf-api="lfApi1" ng-disabled="img_upload2" preview></lf-ng-md-file-input>
                                            </div>
                                        <div class="clear"></div>
                                        <div class="text-right" style="padding-right: 18px;">
                                            <button class="button button-border button-rounded s" ng-disabled="myForm.$invalid" ng-click="save()">Update</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- My Bidding History -->
                                <!-- <div class="tab-content clearfix" id="tabs-6">
                                    
                                </div> -->
                                <!-- End My Bidding History -->
                                <!-- Start My Watch List -->
                                <div class="tab-content clearfix" id="tabs-7">
                                    <?php $i = 1;?>
                                    @if(count($watch_list) > 0)
                                        @foreach($watch_list as $list)
                                            @include('FrontEnd::template.watch-list')
                                            <?php $i++;?>
                                        @endforeach
                                    @else
                                        <center>No Item added to Watch List.</center>
                                    @endif                                    
                                </div>
                                <!-- End -->
                                <!-- Start My Win List -->
                                <div class="tab-content clearfix" id="tabs-8">
                                    <?php $i = 1;?>
                                    @if(count($won_item_list) > 0)
                                        @foreach($won_item_list as $list)
                                            @include('FrontEnd::template.won-item-list')
                                            <?php $i++;?>
                                        @endforeach
                                    @else
                                        <center>No Item in Wining History.</center>
                                    @endif                                    
                                </div>
                                <!-- End -->
                                <!-- Start My Win List -->
                                <div class="tab-content clearfix" id="tabs-9">
                                    <!-- <div class="form-process form-process-auction" style="display: none;"></div> -->
                                    <?php $a = 1;?>
                                    @if(count($auctions) > 0)
                                        @foreach($auctions as $list)
                                            @include('FrontEnd::template.auction-list')
                                            <?php $a++;?>
                                        @endforeach
                                    @else
                                        <center>No Registered Auctions.</center>
                                    @endif
                                </div>
                                <!-- End -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->
    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>
    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>
    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
    <!-- ANGULAR -->
    <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
    <!-- ANGULAR -->
    <!-- jquery confirm -->
    <script src="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
    <!-- jquery confirm -->
     <!-- Angular Material File Input -->
     <script src="{{asset('assets/dist/angular/angular-material/angular-animate.min.js')}}"></script>
     <script src="{{asset('assets/dist/angular/angular-material/angular-aria.min.js')}}"></script>
     <script src="{{asset('assets/dist/angular/angular-material/angular-material.min.js')}}"></script>
     <script src="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.js')}}"></script>
     <!-- Angular Material File Input -->

    <script type="text/javascript">
        function receipt(id){
            $(".form-process").show();
            $.ajax({
                url: "{{url('auction-receipt')}}" ,
                type: 'GET',
                data: {'id' : id },
                success: function(data) {
                    if(data.status=='success'){
                        $.alert({
                          theme: 'material',
                          title: 'You\'ve got mail!',
                          content: 'We sent you an email with auction registration receipt.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    } else {
                        $.alert({
                          theme: 'material',
                          title: 'Error!',
                          content: 'Auction data not found.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    }
                    $(".form-process").hide();
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(thrownError);
                }
            });
        }
        function item_receipt(id){
            $(".form-process").show();

            $.ajax({
                url: "{{url('item-receipt')}}" ,
                type: 'GET',
                data: {'id' : id },
                success: function(data) {
                    if(data.status=='success'){
                        $.alert({
                          theme: 'material',
                          title: 'You\'ve got mail!',
                          content: 'We sent you an email with auction item receipt.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    } 
                    else if(data.status=='error') {
                        $.alert({
                          theme: 'material',
                          title: 'Error!',
                          content: 'Auction data not found.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    }
                    else {
                        $.alert({
                          theme: 'material',
                          title: 'Due Amount!',
                          content: 'Dear customer please complete the due amount of Rs '+data.due+' to proceed.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    }
                    $(".form-process").hide();
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(thrownError);
                }
            });
        }
    </script>
    <!-- angular-confirm-master -->
    <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
    <!-- angular-confirm-master -->
    <script type="text/javascript">
        // 'use strict';
        var myApp = angular.module('myApp', ['cp.ngConfirm', 'ngAnimate','ngMaterial','lfNgMdFileInput']);
        function MyController($scope,$ngConfirm,$http,BASE_URL) {
            $scope.countries    = {!! $countries !!};
            $scope.details      = {!! $customer !!};
            $scope.id           = $scope.details.id;        
            $scope.first_name   = $scope.details.fname;        
            $scope.last_name    = $scope.details.lname;
            $scope.email        = $scope.details.email;
            $scope.contact_no   = $scope.details.telephone_no;
            $scope.address_1    = $scope.details.address_1;
            $scope.address_2    = $scope.details.address_2;
            $scope.city         = $scope.details.city;
            $scope.zip_code     = $scope.details.zip_postal_code;
            $scope.country      = $scope.details.country;
            if($scope.details.nic_image_path == '' || $scope.details.nic_image_path == null){
                $scope.img_upload1 = false; 
            }else{
                $scope.img_upload1 = true;
            }
            if($scope.details.proof_image_path == '' || $scope.details.proof_image_path == null){
                $scope.img_upload2 = false; 
            }else{
                $scope.img_upload2 = true;
            }
            //add auction details
            $scope.save = function (){
                var $i = 0;
                // form validation
                $("form .validate").each(function() {
                    var $this = $(this);
                    var $name = String($this.attr('name')).split('_');
                    if($this.val() == "" || $this.val() == null) {
                        $('#' + $this.attr('name') + '_error').addClass('chosen-error');
                        $('#' + $this.attr('name') + '_error').find('input').addClass('error');
                        $('#' + $this.attr('name') + '_error').find('label.error').show().text('The '+ $name[0] + (($name[1] && !$.isNumeric($name[1]))?' '+$name[1]:'') +' field is required.'); 
                        $i++;
                    }
                    else{
                        $('#' + $this.attr('name') + '_error').removeClass('chosen-error');
                        $('#' + $this.attr('name') + '_error').find('input').removeClass('error');
                        $('#' + $this.attr('name') + '_error').find('label.error').hide();
                    }
                });

                if($i > 0) {
                    return false;
                }

                if($scope.password && $scope.confirm_password) {
                    if($scope.password != $scope.confirm_password){
                        $ngConfirm({theme: 'material', content:'Password and confirm password not matched.', title:'Alert!', closeIcon: false, buttons: {ok: function () {}}});
                        return false;
                    }
                    $('#confirm_password_error,#password_error').find('input').removeClass('error');
                    $('#confirm_password_error,#password_error').find('label.error').hide();
                }
                else if($scope.password && !$scope.confirm_password) {
                    $('#confirm_password_error').find('input').addClass('error');
                    $('#confirm_password_error').find('label.error').show().text('The confirm password field is required.'); 
                    
                    $('#password_error').find('input').removeClass('error');
                    $('#password_error').find('label.error').hide();
                    return false;
                }
                else if(!$scope.password && $scope.confirm_password) {
                    $('#password_error').find('input').addClass('error');
                    $('#password_error').find('label.error').show().text('The password field is required.');

                    $('#confirm_password_error').find('input').removeClass('error');
                    $('#confirm_password_error').find('label.error').hide();
                    return false;
                }
                else {
                    $('#confirm_password_error,#password_error').find('input').removeClass('error');
                    $('#confirm_password_error,#password_error').find('label.error').hide();
                }

                var register = {
                    id          : $scope.id,
                    first_name  : $scope.first_name,
                    last_name   : $scope.last_name,
                    email       : $scope.email,
                    contact_no  : $scope.contact_no,
                    address_1   : $scope.address_1,
                    address_2   : $scope.address_2,
                    city        : $scope.city,
                    zip_code    : $scope.zip_code,
                    country     : $scope.country,
                    password    : $scope.password
                };

                var formData = new FormData();

                formData.append('register', JSON.stringify(register));
                formData.append('file', ($scope.file)?($scope.file.length > 0)?$scope.file[0].lfFile:'':'');
                formData.append('file2', ($scope.file2)?($scope.file2.length > 0)?$scope.file2[0].lfFile:'':'');
                $(".form-process").show();
                $http.post(BASE_URL+"customer/update", formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function(response){
                    if(response.data.success == true) {
                        $scope.password         = "";
                        $scope.confirm_password = "";
                        $scope.lfApi.removeAll();
                        $scope.lfApi1.removeAll();
                        
                        $('.error').hide();
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'green', closeIcon: false, buttons: {ok: function () {location.reload()}}});
                    } else if(response.data.error == true) {
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'red', closeIcon: false, buttons: {ok: function () {}}});
                    }
                    $(".form-process").hide();
                },function(error){
                    console.log(error);
                });
            }
        }
        myApp.controller('MyController', MyController);
        myApp.constant('BASE_URL', "{{asset('/')}}");
         //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->

    </script>

</body>

</html>
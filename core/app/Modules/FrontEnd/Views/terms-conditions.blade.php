<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Terms & Conditions</title>
</head>
<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>Terms & Conditions</h1>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Terms & Conditions</li>
                </ol>
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="col_full">
                    •	All users of the Schokman & Samerawickreme (Hereinafter referred to as S&S)
On line Trading facility of – a.) Shopping Cart b.) On Line Bidding c.) Live Auctioning, need to register themselves by duly perfecting the provided S&S application form. (One Time registration).However those intending to take part in the Live Auctioning Process, would be required to further register by making the nominal registration fee on each occasion you intend taking part in the Auctioning process.
					</div>
					<div class="col_full">
                    •	The “User” and “Password” created by you at the time of registration, should be kept confidential and not divulged to anyone else as this could be used to log into the S&S Online Trading facility. S&S would not be responsible for any third party logging in with your “User and “Password” and contracting business of any sort.
					</div>
					<div class="col_full">
                    •	Any items you have committed to buy, needs to be paid for. As such you need to satisfy yourself prior to bidding / purchasing as all items are auctioned / sold on <b>“as it is condition.”</b> Any item / items once purchased cannot be returned and will not be taken back or payment returned by S&S.
                    </div>
                    <div class="col_full">
                    •	In the case of upfront purchases (Shopping Cart), you could purchase, price marked items displayed in the web portal and your Credit Card would be debited immediately on confirmation of purchase. “On Line Bidding” is similar to the Live Auction process, allowing you to bid on an item within a stipulated time frame. Each bid you make is a binding contract to buy, if you win. The same is true for “Live Auctions”. SS will commence the credit card debiting process immediately on confirmation of purchase or on winning a bid. The purchaser would be required to complete the process within 5 minutes by confirming with the Bank the verification requested for the Bank to complete the process. Failure to do so would result in the item being re auctioned or withdrawn and the person concerned would be blacklisted. For this buyers are expected to possess recognized credit cards acceptable to S&S with adequate credit limit or make alternate arrangements with S&S. (Please check for acceptable Credit Cards in the registration form.)
                    </div>
                    <div class="col_full">
                    •	Those who intend taking part in the On Line Trading facility without using their Credit Card could do so by making a deposit to their Pocket account in consultation with S&S
                    </div>
                    <div class="col_full">
                    •	If a number of similar items are listed separately and you require only one item, ensure that you bid only for one item. Because if you bid for several similar items when your requirement is only one and you are the winning bidder you need to purchase all the items you have won even if they are the same or similar.
                    </div>
                    <div class="col_full">
                    •	Ensure that you read the listing description carefully before bidding. For example, some items such as certain Antique Furniture, artifacts, Items of Historic value cannot be exported due to existing Government regulations. Therefore only bid on or buy an item only if you can meet the existing export regulations, if you intend exporting the items. S&S will not be responsible for the consequences if you bid for an item with the intention of exporting and not be able to export due to local regulations. Therefore only bid or buy items where you can meet the local legal requirements for purchasing / Export.
                    </div>
                    <div class="col_full">
                    •	In the case of Credit Card payments and the card being rejected, the item concerned would be re-auctioned and the customer blacklisted.
                    </div>
                    <div class="col_full">
                    •	If it is found that false information has been provided in the registration form, your registration would be discontinued forthwith.
                    </div>
                    <div class="col_full">
                    •	Bidding for your own items is not permitted
                    </div>
                    <div class="col_full">
                    •	Bidding by any individual with the intention of increasing the Bid price artificially is not accepted. The registration of any person indulging in such practice would be cancelled and would be Blacklisted and debarred from taking part in any future auctions.
                    </div>
                    <div class="col_full">
                    •	Retracting on a bid is not permitted and allowed only under special circumstances where S&S is satisfied the cause to be a genuine error by the buyer. Unless otherwise the original intended bid would be taken as correct.
In the case the bid retraction request is due to having manipulated the bidding process, such retractions would not be agreed to. The decision of which would be at the sole discretion of the management of S&S.

                    </div>
                    <div class="col_full">
                    •	The auctioneer has the option to refuse any bid without giving any explanation/ reason.
                    </div>
                    <div class="col_full">
                    •	As a general rule the highest bidder becomes the purchaser. Should any dispute arise the decision of the auctioneers / S&S would be final and conclusive.
                    </div>
                    <div class="col_full">
                    •	If required S&S would facilitate the transportation of purchased items through a third party at a cost agreed with the buyer. 
                    </div>
                    <div class="col_full">
                    •	In the case of the Buyer arranging his/her own transport, the hard copy or the soft copy of the invoice, needs to be produced as proof of payment at the time of collection of the purchased items. All items purchased and paid for, would have to be removed from the auction site on the following working day. 
                    </div>
                    <div class="col_full">
                    •	We confirm that all customer information and all transaction details would be kept confidential.
                    </div>
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <script>
    //CHAT FEATURE
    <!--Start of Tawk.to Script-->
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    <!--End of Tawk.to Script-->
    </script>

</body>

</html>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Item Won</title>
</head>

<body class="stretched">

    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>2017 Honda Civic Type R</h1>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">Auction</a></li>
                    <li><a href="#">Bid</a></li>
                    <li class="active">Won</li>
                </ol>
            </div>
        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="single-product">
                        <div class="product">
                            <div class="col_half" style="margin-bottom:0;">

                                <!-- Product Single - Gallery ============================================= -->
                                <!-- Place somewhere in the <body> of your page -->
                                <div class="product-image">
                                    <section class="slider">
                                        <div id="slider" class="flexslider">
                                            <ul class="slides">
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="carousel" class="flexslider">
                                            <ul class="slides">
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                                <li>
                                                    <img src="{{asset('assets/front/images/home/bank.jpg')}}" />
                                                </li>
                                            </ul>
                                        </div>
                                    </section>
                                </div>
                                <!-- Product Single - Gallery End -->
                            </div>
                            <div class="col_half col_last product-desc" style="margin-bottom:0;">
                                <!-- Product Single - Short Description ============================================= -->
                                <h4 class="nomargin">Description</h4>
                                <div class="clear"></div>
                                <div class="line"></div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero velit id eaque ex quae laboriosam nulla optio doloribus! Perspiciatis, libero, neque, perferendis at nisi optio dolor!</p>
                                <p>Perspiciatis ad eveniet ea quasi debitis quos laborum eum reprehenderit eaque explicabo assumenda rem modi.</p>
                                <!-- Product Single - Short Description End -->

                                <div class="col_half" style="margin-bottom:0;">
                                    <h4 class="nomargin textcenter">Registration Fee</h4>
                                    <div class="itemprice bottommargin-sm textcenter">RS 1,200.00</div>
                                </div>
                                <div class="col_half col_last" style="margin-bottom:0;">
                                    <h4 class="nomargin textcenter">Minimum Bidding Price</h4>
                                    <div class="itemprice bottommargin-sm textcenter">RS 1,999.99</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_full nobottommargin textcenter topmargin-sm">
                        <div class="promo promo-light promo-small topmargin-sm ">
                            <div class="container clearfix">
                                <h3>Current Highest Bid Value : <span>Rs 1080.00</span></h3>
                                <h1 class="topmargin20">Congratulations <br/> John Doe</h1>
                                <h2>You Won the Auction</h2>
                                <button class="button button-large button-border button-rounded " type="submit" id="template-reviewform-submit" name="template-reviewform-submit" value="submit">Payment</button>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script defer src="{{asset('assets/front/js/jquery.flexslider.js')}}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')
    </script>

    <script type="text/javascript">
        $(function() {
            SyntaxHighlighter.all();
        });
        $(window).load(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel",
                start: function(slider) {
                    $('body').removeClass('loading');
                }
            });
        });

        //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->
        
    </script>
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
    
</body>

</html>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Payment Receipt</title>
</head>
<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>Payment</h1>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Auction</a></li>
                    <li class="active">Payment Receipt</li>
                </ol>
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="col-md-8 col-md-offset-2 nobottommargin textcenter">
                        <div class="receiptbg promo promo-light promo-small">
                            <div class="clearfix">
                                <div class="receipttable">
                                    <h3 class="text-center">Payment Receipt</h3><br>
                                    <div class="row">
                                        <div class="col-md-4 text-left"><p><strong>Date: </strong> 2017/06/06</p></div>
                                        <div class="col-md-4 text-center"><p><strong>Transaction ID: </strong> #12345456576774</p></div>
                                        <div class="col-md-4 text-right"><p>#12345456576774</p></div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Item Name</th>
                                                    <th>Price(RS)</th>
                                                    <th>Total Price(RS)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">Item 01-XXXXX</td>
                                                    <td class="text-right">5,000.00</td>
                                                    <td class="text-right">50,000.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-2 padtop20 text-right">
                        <button type="button" class="button button-border button-small button-rounded" style="margin:0;">Print</button>
                    </div>
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

</body>

</html>
<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="angularApp">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<!-- Stylesheets ============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Angular-confirm-master -->
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
    <!-- Angular-confirm-master -->
    <!-- Chosen Select -->
    <!-- <link rel="stylesheet" href="{{asset('assets/dist/chosen/css/chosen.min.css')}}"> -->
    <!-- Angular Material File Input -->
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/angular-material.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.css')}}">
    <!-- Angular Material File Input -->
    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

	<!-- Document Title ============================================= -->
	<title>Schokman & Samerawickreme | Register</title>
</head>
<body class="stretched" ng-controller="RegisterController" ng-cloak>
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header ============================================= -->
		<header id="header" class="full-header">
			@include('front_ui.includes.header_menu_dark')
		</header><!-- #header end -->
		<!-- Page Title ============================================= -->
		<section id="page-title">
			<div class="container clearfix">
				<h1>Sign Up</h1>
			</div>
		</section><!-- #page-title end -->
		<!-- Content ============================================= -->
		<section id="content">
			<div class="content-wrap" style="padding-bottom: 40px;">
				<div class="container clearfix container-main">
					<div class="col_three_third nobottommargin">

                        <div class="panel panel-default nobottommargin">
                            <div class="panel-body" style="padding: 40px;">

        						<h4>Don't have an Account? Sign Up Now.</h4>
                                <div class="clear"></div>
                                <form class="nobottommargin" name="myForm" method="post" enctype="multipart/form-data" autocomplete="off">
                                    {!!Form::token()!!}
                                    <div class="form-process" style="display: none;"></div>
                                    <div class="col_half" id="first_name_error">
                                        <label for="first_name">First Name: *</label>
                                        <input type="text" name="first_name" ng-model="first_name" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half col_last" id="last_name_error">
                                        <label for="last_name">Last Name: *</label>
                                        <input type="text" name="last_name" ng-model="last_name" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_half" id="email_error">
                                        <label for="email">Email Address: *</label>
                                        <input type="email" name="email" ng-model="email" autocomplete="off" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                        <span class="error" ng-show="myForm.email.$error.email" style="color: red; line-height: 2;">
                                            Invalid email address.
                                        </span>
                                    </div>
                                    <div class="col_half col_last" id="contact_no_error">
                                        <label for="contact_no">Contact Number: *</label>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">94</span>
                                            <input type="text" name="contact_no" ng-model="contact_no" class="form-control validate" placeholder="71234567" />
                                        </div>
                                        <label>(Hint :94711234567)</label>
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_full formfull" id="address_1_error">
                                        <label for="address_1">Address 01: *</label>
                                        <textarea rows="1" name="address_1" ng-model="address_1" class="form-control validate"></textarea>
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_full formfull">
                                        <label for="address_2">Land number / Special Note</label>
                                        <textarea rows="1" name="address_2" ng-model="address_2" class="form-control"></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_half" id="city_error">
                                        <label for="city">City: *</label>
                                        <select chosen class="form-control validate"
                                            name="city"
                                            option="cities"
                                            ng-model="city"
                                            ng-options="city.name for city in cities track by city.name">
                                            <option value="">- Select City -</option>
                                        </select>
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half col_last" id="zip_code_error">
                                        <label for="zip_code">Zip Code / Postal ID: *</label>
                                        <input type="text" name="zip_code" ng-model="zip_code" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_half" id="country_error">
                                        <label for="country">Country: *</label>
                                        <select class="form-control input-sm validate"
                                            name="country"
                                            option="countries"
                                            ng-model="country"
                                            ng-options="country.country_name for country in countries track by country.id">
                                            <option value="">- Select Country -</option>
                                        </select>
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half col_last" id="nic_error">
                                        <label for="zip_code">NIC: *</label>
                                        <input type="text" name="nic" ng-model="nic" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half" id="password_error">
                                        <label for="password">Password: *</label>
                                        <input type="password" name="password" ng-model="password" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half col_last" id="confirm_password_error">
                                        <label for="confirm_password">Re-enter Password: *</label>
                                        <input type="password" name="confirm_password" ng-model="confirm_password" class="form-control validate" />
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="col_half">
                                        <label for="password">Upload Customer NIC/Drving Licence Image</label>
                                        <lf-ng-md-file-input lf-files="file" lf-browse-label="Browse..." lf-api="lfApi" preview></lf-ng-md-file-input>
                                    </div>
                                    <div class="col_half col_last">
                                        <label for="password">Upload Proof of Address Document</label>
                                        <lf-ng-md-file-input lf-files="file2" lf-browse-label="Browse..." lf-api="lfApi1" preview></lf-ng-md-file-input>
                                    </div>
                                    <div class="clear"></div>
                                    <div id="agreement_error">
                                        <input id="agreement" class="checkbox-style" name="agreement" ng-model="agreement" type="checkbox">
                                        <label for="agreement" class="checkbox-style-1-label">I have read and agree to the Schokman & Samerawickreme <a href="{{url('terms-and-conditions')}}" target="_blank">Terms & Conditions</a></label>
                                        <label class="error" for="label"></label>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="text-right" style="padding-right: 18px;">
                                        <button class="button button-border button-rounded s" ng-disabled="myForm.$invalid" ng-click="save()">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</section><!-- #content end -->
		<!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

	<!-- Footer Scripts ============================================= -->
	<script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <!-- ANGULAR -->
    <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
    <!-- ANGULAR -->

    <!-- load angular-moment -->
    <!-- <script src="{{asset('assets/dist/angular/angular-moment/angular-moment.min.js')}}"></script> -->
    <!-- load angular-moment -->

    <!-- angular-confirm-master -->
    <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
    <!-- angular-confirm-master -->

    <!-- Chosen Select -->
    <!-- <script src="{{asset('assets/dist/chosen/js/chosen.jquery.min.js')}}"></script> -->

    <!-- angular chosen -->
    <!-- <script src="{{asset('assets/dist/angular/angular-chosen/angular-chosen.min.js')}}"></script> -->
    <!-- angular chosen -->

    <script src="{{asset('assets/dist/angular/angular-material/angular-messages.min.js')}}"></script>

    <!-- Angular Material File Input -->
    <script src="{{asset('assets/dist/angular/angular-material/angular-animate.min.js')}}"></script>
    <script src="{{asset('assets/dist/angular/angular-material/angular-aria.min.js')}}"></script>
    <script src="{{asset('assets/dist/angular/angular-material/angular-material.min.js')}}"></script>
    <script src="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.js')}}"></script>
    <!-- Angular Material File Input -->

    <script type="text/javascript">
        'use strict';

        var app = angular.module('angularApp',[/*'localytics.directives',*/'cp.ngConfirm','ngMessages', 'ngAnimate','ngMaterial','lfNgMdFileInput']);

        app.controller('RegisterController', function ($scope,$ngConfirm,$http,BASE_URL) {

            $scope.countries = {!! $countries !!};
            $scope.cities    = {!! $cities !!};

            //add auction details
            $scope.save = function (){
                var $i = 0;

                // form validation
                $("form .validate").each(function() {
                    var $this = $(this);
                    var $name = String($this.attr('name')).split('_');
                    if($this.val() == "" || $this.val() == null) {
                        $('#' + $this.attr('name') + '_error').addClass('chosen-error');
                        // $('#' + $this.attr('name') + '_error').find('input').addClass('error');
                        $('#' + $this.attr('name') + '_error').find('label.error').show().text('The '+ $name[0] + (($name[1] && !$.isNumeric($name[1]))?' '+$name[1]:'') +' field is required.'); 
                        $i++;
                    }
                    else{
                        $('#' + $this.attr('name') + '_error').removeClass('chosen-error');
                        // $('#' + $this.attr('name') + '_error').find('input').removeClass('error');
                        $('#' + $this.attr('name') + '_error').find('label.error').hide();
                    }
                });

                if($i > 0) {
                    return false;
                }

                if($scope.password != $scope.confirm_password){
                    $ngConfirm({theme: 'material', content:'Password and confirm password not matched.', title:'Alert!', closeIcon: false, buttons: {ok: function () {}}});
                    return false;
                }

                if(!$scope.agreement){
                    $ngConfirm({theme: 'material', content:'Please check Terms & Conditions.', title:'Alert!', closeIcon: false, buttons: {ok: function () {}}});
                    return false;
                }
                
                var register = {
                    first_name  : $scope.first_name,
                    last_name   : $scope.last_name,
                    email       : $scope.email,
                    contact_no  : '94'+$scope.contact_no,
                    address_1   : $scope.address_1,
                    address_2   : $scope.address_2,
                    city        : $scope.city,
                    zip_code    : $scope.zip_code,
                    country     : $scope.country,
                    nic         : $scope.nic,
                    password    : $scope.password
                };

                var formData = new FormData();
                formData.append('file', ($scope.file.length > 0)?$scope.file[0].lfFile:'');
                formData.append('file2', ($scope.file2.length > 0)?$scope.file2[0].lfFile:'');
                formData.append('register', JSON.stringify(register));

                $(".form-process").show();

                $http.post(BASE_URL+"register", formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function(response){
                    if(response.data.success == true) {
                        $scope.first_name       = "";
                        $scope.last_name        = "";
                        $scope.email            = "";
                        $scope.contact_no       = "";
                        $scope.address_1        = "";
                        $scope.address_2        = "";
                        $scope.city             = "";
                        $scope.zip_code         = "";
                        $scope.country          = "";
                        $scope.nic          = "";
                        $scope.password         = "";
                        $scope.confirm_password = "";
                        $scope.lfApi.removeAll();
                        $scope.lfApi1.removeAll();
                        $('.error').hide();
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'green', closeIcon: false, buttons: {ok: function () {}}});
                    } else if(response.data.error == true) {
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'red', closeIcon: false, buttons: {ok: function () {}}});
                    }
                    $(".form-process").hide();
                },function(error){
                    console.log(error);
                });
            }
        });
        app.constant('BASE_URL', "{{asset('/')}}");
        //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->
    </script>
</body>
</html>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/dist/jquery-confirm/css/jquery-confirm.css')}}" type="text/css" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }
        #clockdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
  }
  #clockdiv > div{
    padding: 10px;
    border-radius: 3px;
    background: #00BF96;
    display: inline-block;
    margin-right: 10px;
  }

  #clockdiv div > span{
    padding: 15px;
    border-radius: 3px;
    background: #00816A;
    display: inline-block;
  }

  .smalltext{
    padding-top: 5px;
    font-size: 16px;
  }

  #highestdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
  }

   #highestdiv > div{
    padding: 10px;
    border-radius: 3px;
    background: #c2c6c6;
    display: inline-block;
    margin-right: 10px;
  }

  #highestdiv div > span{
    padding: 15px;
    border-radius: 3px;
    background: #36397e;
    display: inline-block;
  }

  .smalltext{
    padding-top: 5px;
    font-size: 16px;
  }
  /*COUNT DOWN TIMER */
  .countdown-rtl {
  direction: rtl;
}
.countdown-holding span {
  color: #888;
}
.countdown-row {
  clear: both;
  width: 100%;
  padding: 0px 2px;
  text-align: center;
}
.countdown-show1 .countdown-section {
  width: 98%;
}
.countdown-show2 .countdown-section {
  width: 48%;
}
.countdown-show3 .countdown-section {
  width: 32.5%;
}
.countdown-show4 .countdown-section {
  width: 24.5%;
}
.countdown-show5 .countdown-section {
  width: 19.5%;
}
.countdown-show6 .countdown-section {
  width: 16.25%;
}
.countdown-show7 .countdown-section {
  width: 14%;
}
.countdown-section {
  display: block;
  float: left;
  font-size: 75%;
  text-align: center;
}
.countdown-amount {
    font-size: 200%;
}
.countdown-period {
    display: block;
}
.countdown-descr {
  display: block;
  width: 100%;
}
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Payment</title>
</head>
<body class="stretched">
    <!-- onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="" -->
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>Payment Details</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    @if(isset($category_id))
                        <li><a href="{{ url('auction-categories/'.$category_id) }}">{{ $item->category->display_name }}</a></li>
                        <li><a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @elseif(isset($auction_id))
                        <li><a href="{{ url('auctions/'.$auction_id) }}">{{ $item->won_item_detail->auction->event_name }}</a></li>
                        <li><a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @else
                        <li><a href="{{ url('my-account') }}">My Account</a></li>
                        <li><a href="{{ url('my-account/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @endif
                    <li class="active">Payment Details</li>
                </ol>
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            @if(sizeof($item->payment) > 0 && sizeof($item->payment[0]) > 0)
                @if($item->payment[0]->payment_status == PAYMENT_SUCCESS && $winner['win_amount'] > $item->payment[0]->amount)
                <div class="content-wrap">
                    <div class="container clearfix container-main">
                        <div class="col_one_fifth nobottommargin">&nbsp;</div>
                        <div class="col_three_fifth col_last nobottommargin">
                            <form id="form" method="post">
                                {!!Form::token()!!}
                                <input type="hidden" name="band_status" value="0">
                                <div class="well well-lg bottommargin ">
                                    <div class="col_full">
                                        <div class="col_half textcenter">
                                            <label for="login-form-username">Total Amount:</label>
                                        </div>
                                        <div class="col_half textcenter">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rs</span>
                                                <input type="text" id="amount_display" name="amount_display" value="{{number_format($winner->win_amount,'2')}}" class="form-control" readonly/>
                                                <input type="hidden" id="amount" name="amount" value="{{$winner->win_amount}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php
                                        $amount  = $winner->win_amount;
                                        $full    = $amount;
                                        $advance = '';
                                        // if($amount <= 10000){
                                        //     $full    =  $amount;
                                        //     $advance = '';
                                        // }elseif ($amount > 10000 && $amount <= 100000) {
                                        //     $full    =  $amount;
                                        //     $advance =  (50 / 100) * $amount;
                                        // } else{
                                        //     $full    =  $amount;
                                        //     $advance =  (25 / 100) * $amount;
                                        // }
                                    ?>

                                    <input type="hidden" name="cid" value="{{$winner->sns_user_id}}">
                                    <input type="hidden" name="full_amount" value="{{$full}}">
                                    <input type="hidden" name="advance_amount" value="{{$advance}}">

                                    @if($paidAmount > 0)
                                        <div class="col_full">
                                            <div class="col_half textcenter">
                                                <label for="login-form-username">Paid Amount:</label>
                                            </div>
                                            <div class="col_half textcenter">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs</span>
                                                    <input style="color: green;" type="text" id="paid" name="paid" value="{{number_format($paidAmount,'2')}}" readonly class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_full">
                                            <div class="col_half textcenter">
                                                <label for="login-form-username">Due Amount:</label>
                                            </div>
                                            <div class="col_half textcenter">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs</span>
                                                    <input style="color: red;" type="text" id="due" name="due" value="{{number_format($paidAmount,'2')}}" readonly class="form-control" /> 
                                                </div>                                            
                                            </div>                                        
                                        </div>
                                        <div class="col_full textcenter">
                                            <div class="col_half">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" checked value="2"> Full Payment
                                                </label>
                                            </div>
                                        </div>
                                        <!-- <p class="textcenter nobottommargin">Please make a payment for the due amount.</p> -->
                                    @else
                                        <div class="col_full textcenter">
                                            <div class="col_half">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" {{($winner->payment_type && $winner->payment_type == 1)?'checked':''}} {{ ($advance)?'':'disabled=disabled' }} value="1"> Advance Payment
                                                </label>
                                            </div>
                                            <div class="col_half col_last">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" value="2" checked> Full Payment
                                                    <!-- <input type="radio" name="type" id="type" {{($winner->payment_type && $winner->payment_type == 2)?'checked':''}} value="2"> Full Payment -->
                                                </label>
                                            </div>
                                            <label class="type_error error" for="label"></label>
                                        </div>
                                    @endif
                                </div>
                                <div class="well well-lg nobottommargin">
                                    <!-- <h3>User Details</h3>
                                    <div class="col_full">
                                        <label for="login-form-username">Name *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                        <input type="text" id="winner_name" name="winner_name" value="{{$winner->bidding_winner->fname}} {{$winner->bidding_winner->lname}}" class="form-control" />
                                        @else
                                        <input type="text" id="winner_name" name="winner_name" value="" class="form-control" />
                                        @endif
                                    </div>
                                    <div class="col_full" id="winner_contact_no_error">
                                        <label for="login-form-password">Contact No *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <input type="text" id="winner_contact_no" name="winner_contact_no" value="{{$winner->bidding_winner->contact_no}}" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @else
                                            <input type="text" id="winner_contact_no" name="winner_contact_no" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @endif
                                    </div>
                                    <div class="col_full" id="winner_email_error">
                                        <label for="login-form-password">E-mail *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <input type="text" id="winner_email" name="winner_email" value="{{$winner->bidding_winner->email}}" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @else
                                            <input type="text" id="winner_email" name="winner_email" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @endif
                                    </div>
                                    <div class="col_full">
                                        <label for="login-form-password">Address</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <textarea id="winner_address" name="winner_address" class="form-control" rows="1">{{$winner->bidding_winner->address_1}} {{$winner->bidding_winner->address_2}}</textarea>
                                        @else
                                            <textarea id="winner_address" name="winner_address" class="form-control" rows="1"></textarea>
                                        @endif
                                    </div> -->
                                    <div class="col_full textcenter">
                                        <div class="col_half">
                                            <label class="radio-inline">
                                                <input type="radio" name="payment_method" id="pocket_balance" value="1"
                                                    @if(sizeof($pocket_balance) > 0) 
                                                        @if($winner->win_amount <= $pocket_balance->balance) 
                                                            checked
                                                        @else
                                                            disabled
                                                        @endif 
                                                    @else
                                                        disabled
                                                    @endif> 
                                                    Pocket Balance (<strong> LKR @if(sizeof($pocket_balance) > 0){{number_format($pocket_balance->balance, 2)}} @else 0.00 @endif </strong>)
                                                
                                            </label>
                                        </div>
                                        <div class="col_half col_last">
                                            <label class="radio-inline">
                                                <input type="radio" name="payment_method" id="card_payment" value="0" 
                                                @if(sizeof($pocket_balance) > 0) 
                                                    @if($winner->win_amount > $pocket_balance->balance) 
                                                        checked
                                                    @endif
                                                @else
                                                    checked 
                                                @endif> Card Payment
                                            </label>
                                        </div>
                                        <label class="type_error error" for="label"></label>
                                    </div>
                                    <div class="col_full nobottommargin textright">
                                        <button class="button button-border button-rounded button-small" style="margin:0;" type="submit" id="submit">Proceed</button>
                                        @if(isset($category_id))
                                            <!-- <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @elseif(isset($auction_id))
                                            <!-- <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @else
                                            <!-- <a href="{{ url('my-account/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col_one_fifth col_last nobottommargin">&nbsp;</div>
                    </div>
                </div>
                @else
                <div class="content-wrap">
                    <div class="container clearfix container-main">
                        <div class="col_one_fifth nobottommargin">&nbsp;</div>
                        <div class="col_three_fifth col_last nobottommargin">
                            <div class="col text-center">
                                Dear Valued Customer , You can join with with the auction again here.<br/><br/><a href="{{ url('auctions/'.$auction_id) }}" class="btn btn-default" role="button">Join With Auction</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            @else
                <div class="content-wrap">
                    <div class="container clearfix container-main">
                        <div class="col_one_fifth nobottommargin">&nbsp;</div>
                        <div class="col_three_fifth col_last nobottommargin">
                            <form id="form" method="post">
                                {!!Form::token()!!}
                                <input type="hidden" name="band_status" value="0">
                                <div class="well well-lg bottommargin ">
                                    <div class="col_full">
                                        <div class="col_half textcenter">
                                            <label for="login-form-username">Total Amount:</label>
                                        </div>
                                        <div class="col_half textcenter">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rs</span>
                                                <input type="text" id="amount_display" name="amount_display" value="{{number_format($winner->win_amount,'2')}}" class="form-control" readonly/>
                                                <input type="hidden" id="amount" name="amount" value="{{$winner->win_amount}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php
                                        $amount  = $winner->win_amount;
                                        $full    =  $amount;
                                        $advance = '';

                                        // if($amount <= 10000){
                                        //     $full    =  $amount;
                                        //     $advance = '';
                                        // }elseif ($amount > 10000 && $amount <= 100000) {
                                        //     $full    =  $amount;
                                        //     $advance =  (50 / 100) * $amount;
                                        // } else{
                                        //     $full    =  $amount;
                                        //     $advance =  (25 / 100) * $amount;
                                        // }
                                    ?>

                                    <input type="hidden" name="cid" value="{{$winner->sns_user_id}}">
                                    <input type="hidden" name="full_amount" value="{{$full}}">
                                    <input type="hidden" name="advance_amount" value="{{$advance}}">

                                    @if($paidAmount > 0)
                                        <div class="col_full">
                                            <div class="col_half textcenter">
                                                <label for="login-form-username">Paid Amount:</label>
                                            </div>
                                            <div class="col_half textcenter">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs</span>
                                                    <input style="color: green;" type="text" id="paid" name="paid" value="{{number_format($paidAmount,'2')}}" readonly class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_full">
                                            <div class="col_half textcenter">
                                                <label for="login-form-username">Due Amount:</label>
                                            </div>
                                            <div class="col_half textcenter">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs</span>
                                                    <input style="color: red;" type="text" id="due" name="due" value="{{number_format($paidAmount,'2')}}" readonly class="form-control" /> 
                                                </div>                                            
                                            </div>                                        
                                        </div>
                                        <div class="col_full textcenter">
                                            <div class="col_half">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" checked value="2"> Full Payment
                                                </label>
                                            </div>
                                        </div>
                                        <!-- <p class="textcenter nobottommargin">Please make a payment for the due amount.</p> -->
                                    @else
                                        <div class="col_full textcenter">
                                            <div class="col_half">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" {{($winner->payment_type && $winner->payment_type == 1)?'checked':''}} {{ ($advance)?'':'disabled=disabled' }} value="1"> Advance Payment
                                                </label>
                                            </div>
                                            <div class="col_half col_last">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" id="type" value="2" checked> Full Payment
                                                    <!-- <input type="radio" name="type" id="type" {{($winner->payment_type && $winner->payment_type == 2)?'checked':''}} value="2"> Full Payment -->
                                                </label>
                                            </div>
                                            <label class="type_error error" for="label"></label>
                                        </div>
                                    @endif
                                </div>
                                <div class="well well-lg nobottommargin">
                                    <!-- <h3>User Details</h3> -->
                                     <!-- <div class="col_full">
                                        <label for="login-form-username">Name *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                        <input type="text" id="winner_name" name="winner_name" value="{{$winner->bidding_winner->fname}} {{$winner->bidding_winner->lname}}" class="form-control" />
                                        @else
                                        <input type="text" id="winner_name" name="winner_name" value="" class="form-control" />
                                        @endif
                                    </div>
                                    <div class="col_full" id="winner_contact_no_error">
                                        <label for="login-form-password">Contact No *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <input type="text" id="winner_contact_no" name="winner_contact_no" value="{{$winner->bidding_winner->contact_no}}" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @else
                                            <input type="text" id="winner_contact_no" name="winner_contact_no" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @endif
                                    </div>
                                    <div class="col_full" id="winner_email_error">
                                        <label for="login-form-password">E-mail *</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <input type="text" id="winner_email" name="winner_email" value="{{$winner->bidding_winner->email}}" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @else
                                            <input type="text" id="winner_email" name="winner_email" class="form-control validate" />
                                            <label class="error" for="label"></label>
                                        @endif
                                    </div>
                                    <div class="col_full">
                                        <label for="login-form-password">Address</label>
                                        @if(isset($winner) && sizeof($winner) > 0 && isset($winner->bidding_winner) && sizeof($winner->bidding_winner) > 0)
                                            <textarea id="winner_address" name="winner_address" class="form-control" rows="1">{{$winner->bidding_winner->address_1}} {{$winner->bidding_winner->address_2}}</textarea>
                                        @else
                                            <textarea id="winner_address" name="winner_address" class="form-control" rows="1"></textarea>
                                        @endif
                                    </div> -->
                                    <div class="col_full textcenter">
                                        <div class="col_half">
                                            <label class="radio-inline">
                                                <input type="radio" name="payment_method" id="pocket_balance" value="1"
                                                @if(sizeof($pocket_balance) > 0) 
                                                    @if($winner->win_amount <= $pocket_balance->balance) 
                                                        checked
                                                    @else
                                                        disabled
                                                    @endif
                                                @else
                                                    disabled 
                                                @endif> Pocket Balance (<strong> LKR @if(sizeof($pocket_balance) > 0){{number_format($pocket_balance->balance, 2)}} @else 0.00 @endif </strong>)
                                            </label>
                                        </div>
                                        <div class="col_half col_last">
                                            <label class="radio-inline">
                                                <input type="radio" name="payment_method" id="card_payment" value="0" 
                                                @if(sizeof($pocket_balance) > 0) 
                                                    @if($winner->win_amount > $pocket_balance->balance) 
                                                        checked
                                                    @endif
                                                @else
                                                    checked 
                                                @endif> Card Payment
                                            </label>
                                        </div>
                                        <label class="type_error error" for="label"></label>
                                    </div>
                                    <div class="col_full nobottommargin textright">
                                        <h4 class="text-center" id="approved_time"></h5>
                                        <h4 class="text-center" id="monitor"></h5>
                                        <h5 class="textcenter"><div id="defaultCountdown"></div></h5>

                                        <center><button class="button button-border button-rounded button-small" style="margin-top:20px;" type="submit" id="submit">Proceed</button></center>
                                        @if(isset($category_id))
                                            <!-- <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @elseif(isset($auction_id))
                                            <!-- <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @else
                                            <!-- <a href="{{ url('my-account/item/'.$item->id) }}" class="button button-border button-rounded button-small" style="margin:0;">Cancel</a> -->
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col_one_fifth col_last nobottommargin">&nbsp;</div>
                    </div>
                </div>
            @endif
        </section>
        <!-- #content end -->

        <!-- payment time over -->
        <form id="band_customer" method="post">
            {!!Form::token()!!}
            <input type="hidden" name="band_status" value="1">
            <button class="button button-border button-rounded button-small band_customer" style="margin:0;display: none;" type="submit" id="band_customer">Band</button>
        </form>
        <!-- payment time over -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <script src="{{asset('assets/dist/jquery-confirm/js/jquery-confirm.js')}}"></script>  
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.plugin.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.countdown.min.js')}}"></script>  
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).find('.my-space').hide();
        $(document).find(".my-account, .standard-logo").attr('href', '').css({'cursor': 'pointer', 'pointer-events' : 'none'});
        @if(session('success'))
            $.confirm({
                title: '{{session('success.title')}}',
                content: '{{session('success.message')}}',
                type: 'green',
                backgroundDismiss: false,
                buttons: {
                    omg: {
                        text: 'Ok',
                        btnClass: 'btn-green',
                    }
                }
            });
        @endif

        $('#form').submit(function(){
            var $i = 0;

            // form validation
            $("#form .validate").each(function() {

                var $this = $(this);

                var $name = String($this.attr('name')).split('_');
                if($this.val() == "" || $this.val() == null) {
                    $('#' + $this.attr('name') + '_error').find('label.error').show().text('The '+ $name[0] + (($name[1] && !$.isNumeric($name[1]))?' '+$name[1]:'') +' field is required.'); 
                    $i++;
                }
                else{
                    $('#' + $this.attr('name') + '_error').find('label.error').hide();
                }
            });

            if($i > 0) {
                return false;
            }

            if(!$('input[name=type]:checked').val() ){
                $('.type_error').text('Please select an option').show();
                return false;
            }
        }); 

        //calculate remaning time to make payment
        var approved_time = new Date('{{$item->won_item_detail->approved->created_at}}');
        var date = new Date(approved_time.setDate(approved_time.getDate()));
            date = date.setMinutes(date.getMinutes() + 2);
        var time = new Date(date);

        $('#defaultCountdown').countdown({
            until      : time, 
            padZeroes  : true, 
            onExpiry   : timeOver, 
            onTick     : timeCountDown,
            timezone   : +5.30
        });
        //when page is load & remaining time over, then band the customer.
        var nw_date = new Date().toLocaleString("en-US", {timeZone: "Asia/Colombo"});
        nw_date     = new Date(nw_date)
        nw_date     = nw_date.setMinutes(nw_date.getMinutes());
        if(date < nw_date){
            toastr.error('Payment time is over for this item & your account will be suspended !');
            //logout user and block account
           timeOver();
        }

    });

    //time count down to make payment
    function timeCountDown(periods) { 
        if(periods[4] == 0){
            if(periods[5] > 0){
                $('#monitor').text('More ' + periods[5] + ' minutes and ' + periods[6] + ' seconds to complete your payment');
            }
            else{
                $('#monitor').text('More ' + periods[6] + ' seconds to complete your payment');
            }
        }
    }

    //time over - logout user and block account
    function timeOver() { 
        $('#band_customer').submit();
    }
     //CHAT FEATURE
     <!--Start of Tawk.to Script-->
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    <!--End of Tawk.to Script-->

    </script>

</body>

</html>
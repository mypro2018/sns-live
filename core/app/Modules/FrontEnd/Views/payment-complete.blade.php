<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- jquery confirm -->
    <link rel="stylesheet" href="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}">
    <!-- jquery confirm -->

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }

        .container-main{
            padding-bottom: 25px;
        }
    </style>

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Auction Payment</title>
</head>
<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>Auction Payment</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    @if(isset($category_id))
                        <li><a href="{{ url('auction-categories/'.$category_id) }}">{{ $item->category->display_name }}</a></li>
                        <li><a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @elseif(isset($auction_id))
                        <li><a href="{{ url('auctions/'.$auction_id) }}">{{ $item->auction_detail->auction->event_name }}</a></li>
                        <li><a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @else
                        <li><a href="{{ url('my-account') }}">My Account</a></li>
                        <li><a href="{{ url('my-account/item/'.$item->id) }}">{{$item->name}}</a></li>
                    @endif
                    </li>
                    <li class="active">Auction Payment Complete</li>
                </ol>
            </div>

        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="col_one_fifth nobottommargin">&nbsp;</div>
                    <div class="col_three_fifth col_last nobottommargin">
                        <div class="panel panel-default nobottommargin">
                            <div class="form-process" style="display: none;"></div>
                            <div class="panel-body" style="padding: 40px">
                                <?php
                                    if($response['responseCode'] == '00' || $response['responseCode'] == '10'){
                                ?>
                                    <p style="color:#3EA73E;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Transaction was processed successfully</p>
                                    <p>Transaction Reference : <?php echo $response['txnReference'] ?></p>
                                    <p>Transaction Time : <?php echo $response['txnTime'] ?></p>
                                    <p>Merchant Reference : <?php echo $response['clientRef'] ?></p>
                                    <div class="textright divider-right">
                                        <button class="button button-border button-small button-rounded" onclick="receipt({{$response['paymentId']}})"><span><i class="icon-mail"></i> Email receipt</span></button>
                                        @if(isset($category_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id) }}">
                                        @elseif(isset($auction_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-bid/'.$item->id) }}">
                                        @else
                                            <a class="button button-border button-small button-rounded" href="{{ url('my-account/item/'.$item->id) }}">
                                        @endif
                                            Continue
                                        </a>
                                    </div>
                                <?php 
                                    }elseif($response['responseCode'] == '91' ||
                                        $response['responseCode'] == '92' ||
                                        $response['responseCode'] == 'A4' ||
                                        $response['responseCode'] == 'C5' ||
                                        $response['responseCode'] == 'T3' ||
                                        $response['responseCode'] == 'T4' ||
                                        $response['responseCode'] == 'U9' ||
                                        $response['responseCode'] == 'X1' ||
                                        $response['responseCode'] == 'X3' ||
                                        $response['responseCode'] == '-1' ||
                                        $response['responseCode'] == 'C0' ||
                                        $response['responseCode'] == 'A6'){
                                ?>
                                    <p style="color:#FF0000;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Banking Network Temporarily Unavailable -Please try again later.</p>

                                    <p>Transaction Reference : <?php echo $response['txnReference'] ?></p>
                                    <p>Transaction Time : <?php echo $response['txnTime'] ?></p>
                                    <p>Merchant Reference : <?php echo $response['clientRef'] ?></p>
                                    <div class="textright divider-right">
                                        @if(isset($category_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/payment') }}">
                                        @elseif(isset($auction_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/payment') }}">
                                        @else
                                            <a class="button button-border button-small button-rounded" href="{{ url('my-account/item/'.$item->id.'/payment') }}">
                                        @endif
                                            Try Again
                                        </a>
                                    </div>
                                <?php }else{ ?>
                                    <p style="color:#FF0000;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Payment Declined - <?php echo $response['responseText'] ?> <br/> Please try an alternative card.</p>

                                    <p>Transaction Reference : <?php echo $response['txnReference'] ?></p>
                                    <p>Transaction Time : <?php echo $response['txnTime'] ?></p>
                                    <p>Merchant Reference : <?php echo $response['clientRef'] ?></p>
                                    <div class="textright divider-right"    >
                                        @if(isset($category_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/payment') }}">
                                        @elseif(isset($auction_id))
                                            <a class="button button-border button-small button-rounded" href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/payment') }}">
                                        @else
                                            <a class="button button-border button-small button-rounded" href="{{ url('my-account/item/'.$item->id.'/payment') }}">
                                        @endif
                                            Alternative Card
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col_one_fifth col_last nobottommargin">&nbsp;</div>
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <!-- jquery confirm -->
    <script src="{{asset('assets/dist/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
    <!-- jquery confirm -->

    <script type="text/javascript">
        function receipt(id){
            $(".form-process").show();

            $.ajax({
                url: "{{url('auction-receipt')}}" ,
                type: 'GET',
                data: {'id' : id },
                success: function(data) {
                    if(data.status=='success'){
                        $.alert({
                          theme: 'material',
                          title: 'You\'ve got mail!',
                          content: 'We sent you an email with auction registration receipt.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    } else {
                        $.alert({
                          theme: 'material',
                          title: 'Error!',
                          content: 'Auction data not found.',
                          buttons: {
                            ok: {
                              action: function () {}
                            }
                          }
                        });
                    }
                    $(".form-process").hide();
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(thrownError);
                }
            });
        }
    </script>

</body>

</html>
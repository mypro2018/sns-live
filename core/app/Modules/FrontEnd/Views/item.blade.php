<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/dist/jquery-countdown/jquery.countdown.css')}}" type="text/css" />
    <!-- toaster -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" type="text/css" />
    <!-- toaster -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <style>
        .line, .double-line{
            margin: 35px 0px 35px 0px;
        }
        #defaultCountdown { width: 100%; height: 45px; }
        .customer-center{
            margin-top:20px;
        }
    </style>
    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Item Detail</title>
</head>
<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->
        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{ $item->name }}</h1>
            </div>
        </section>
        <!-- #page-title end -->
        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">
                    <div class="single-product">
                        <div class="product">
                            <div class="col_half" style="margin-bottom:0;">
                                <?php $date_time = date('Y-m-d H:i');  ?>
                                <!-- Product Single - Gallery ============================================= -->
                                <!-- Place somewhere in the <body> of your page -->
                                <div class="product-image">
                                    <section class="slider">
                                        <div id="slider" class="flexslider">
                                            @if($item->auction_detail && $item->auction_detail->auction->auction_status == AUCTION_LIVE && $item->auction_detail->auction && $date_time < date('Y-m-d H:i',strtotime($item->auction_detail->auction->auction_date.' '.$item->auction_detail->auction->auction_start_time)))
                                                <!-- <iframe width="600" height="300" src="{{$item->auction_detail->auction->auction_url}}" frameborder="0"></iframe> -->
                                                <ul class="slides">
                                                    @if(count($item->images) > 0)
                                                        @foreach($item->images as $image)
                                                            @if(File::exists(storage_path('uploads/images/item/'.$image->image_path)))
                                                            <li>
                                                                <img src="{{ url('core/storage/uploads/images/item/'.$image->image_path) }}" />
                                                            </li>
                                                            @else
                                                            <li>
                                                                <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                            </li>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <li>
                                                            <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                        </li>
                                                    @endif
                                                </ul>
                                            @else
                                                <ul class="slides">
                                                    @if(count($item->images) > 0)
                                                        @foreach($item->images as $image)
                                                            @if(File::exists(storage_path('uploads/images/item/'.$image->image_path)))
                                                            <li>
                                                                <img src="{{ url('core/storage/uploads/images/item/'.$image->image_path) }}" />
                                                            </li>
                                                            @else
                                                            <li>
                                                                <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                            </li>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <li>
                                                            <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                        </li>
                                                    @endif
                                                </ul>
                                            @endif
                                        </div>
                                        <div id="carousel" class="flexslider">
                                            <ul class="slides">
                                                @if(count($item->images) > 0)
                                                    @foreach($item->images as $image)
                                                        @if(File::exists(storage_path('uploads/images/item/'.$image->image_path)))
                                                        <li>
                                                            <img src="{{ url('core/storage/uploads/images/item/'.$image->image_path) }}" />
                                                        </li>
                                                        @else
                                                        <li>
                                                            <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                        </li>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <li>
                                                        <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </section>
                                </div>
                                <!-- Product Single - Gallery End -->
                            </div>
                            <div class="col_half col_last product-desc" style="margin-bottom:0;">
                            @if($item->show_hide_code == BIDDING_ACTIVE)
                                <h5>ITEM CODE : {{ $item->item_code }}</h5>
                            @else
                                <h5>ITEM CODE : {{ substr($item->item_code, 0, -3) . 'XXX' }}</h5>
                            @endif
                                <!-- Product Single - Short Description ============================================= -->
                                <h4 class="nomargin">Details</h4>
                                <div class="clear"></div>
                                <div class="line"></div>
                                
                                <p>{{ $item->description?:'' }}</p>
                                <!-- Product Single - Short Description End -->
                                @if($item->auction_detail)
                                    <!-- <div class="col_full text-center" style="margin-bottom:0;">
                                        @if(count($payment) == 0)
                                            @if(isset($item->auction_detail->auction->registration_fee) && $item->auction_detail->auction->registration_fee > 0)
                                                <h4 class="nomargin">*** Registration Fee ***</h4>
                                                <div class="itemprice bottommargin-sm">Rs {{ $item->auction_detail->auction->registration_fee }}</div>
                                            @else
                                                <h4 class="nomargin">*** Registration Free ***</h4>
                                            @endif
                                        @endif
                                    </div><br/> -->
                                    <div class="clear"></div>
                                    @if(isset($item->auction_detail) && sizeof($item->auction_detail) > 0 && isset($item->auction_detail->highest_bid) && sizeof($item->auction_detail->highest_bid) > 0 && isset($item->auction_detail->highest_bid->win_bid) && sizeof($item->auction_detail->highest_bid->win_bid) > 0)
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <div class="well">
                                                    <h4 class="nomargin">Highest Bid Amount</h4>
                                                    <div class="itemprice bottommargin-20">Rs {{ number_format($item->auction_detail->highest_bid->win_bid->win_amount, 2) }}</div>
                                                    <h4 class="nomargin text-uppercase">Congratulations ! <br/><small>{{ $item->auction_detail->highest_bid->win_bid->bidding_winner->lname }} Customer</small></h4>
                                                    <p class="nomargin"><small>Won the Auction</small></p>
                                                    <div class="clear"></div>
                                                    @if(Sentinel::check() && $user->id == $item->auction_detail->highest_bid->bid_user->user_id)
                                                        @if($item->auction_detail->approved)
                                                            @if(!$item->payment)
                                                                @if(isset($category_id))
                                                                    <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                                                                @elseif(isset($auction_id))
                                                                    <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                                                                @else
                                                                    <a href="{{ url('my-account/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Payment</a>
                                                                @endif
                                                            @else
                                                                @if($amount < $item->auction_detail->highest_bid->bid_amount)
                                                                    @if(isset($category_id))
                                                                        <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Complete due amount of Rs {{($item->auction_detail->highest_bid->bid_amount - $amount)}}</a>
                                                                    @elseif(isset($auction_id))
                                                                        <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Complete due amount of Rs {{($item->auction_detail->highest_bid->bid_amount - $amount)}}</a>
                                                                    @else
                                                                        <a href="{{ url('my-account/item/'.$item->id.'/item-payment') }}" class="button button-sm button-border button-rounded">Complete due amount of Rs {{($item->auction_detail->highest_bid->bid_amount - $amount)}}</a>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @else
                                                            <button type="button" class="button button-sm button-border button-rounded">Waiting for payment approval</button>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @if($date_time < date("Y-m-d H:i", strtotime($item->auction_detail->auction->auction_date.' '.$item->auction_detail->auction->auction_start_time)))
                                            
                                            <h5 class="text-center" id="monitor">More</h5>
                                            <h4 class="textcenter"><div id="defaultCountdown"></div></h4>
                                            <h5 class="text-center" id="monitor">To Start Auction</h5>
                                            <div class="text-center bottommargin-20">
                                                <button class="button button-3d button-rounded button-white button-light">
                                                     {{date("Y-m-d H:i", strtotime($item->auction_detail->auction->auction_date.' '.$item->auction_detail->auction->auction_start_time))}}
                                                </button>
                                            </div>
                                        @else
                                            <div class="text-center bottommargin-20">
                                            @if(count($payment) == 0)
                                                @if($item->auction_detail->auction->auction_status == AUCTION_TO_BEGIN && $item->auction_detail->status == PRODUCT_TO_BEGIN)
                                                    <button class="button button-3d button-rounded button-white button-light">
                                                        Auction Is Starting. Please Wait to Bid
                                                    </button>
                                                @endif
                                            @else
                                                @if($date_time >= date("Y-m-d H:i", strtotime($item->auction_detail->auction->auction_date.' '.$item->auction_detail->auction->auction_start_time)) && $item->auction_detail->auction->auction_status == AUCTION_TO_BEGIN && $item->auction_detail->status == PRODUCT_TO_BEGIN)
                                                    <button class="button button-3d button-rounded button-white button-light">
                                                        Auction Is Starting. Please Wait to Bid
                                                    </button>
                                                @endif
                                                @if($item->auction_detail->auction->auction_status == AUCTION_LIVE)
                                                    <button class="button button-3d button-rounded button-white button-light">
                                                    Auction is Live Now <i class="icon-ok"></i>
                                                    </button>
                                                @elseif($item->auction_detail->auction->auction_status == AUCTION_OVER)
                                                    <button class="button button-3d button-rounded button-white button-light">
                                                    Auction Over
                                                    </button>
                                                @endif
                                            @endif
                                            </div>
                                        @endif
                                        @if($item->auction_detail->auction->bid_now_active_status_1 == BIDDING_ACTIVE)
                                            <div class="text-center customer-center">                                        
                                                @if(Sentinel::check())
                                                    @if(count($payment) == 0)
                                                        @if($item->auction_detail->auction->auction_status == AUCTION_TO_BEGIN || $item->auction_detail->auction->auction_status == AUCTION_LIVE)
                                                            @if(isset($category_id))
                                                                <!-- <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/payment') }}" class="button button-border button-rounded">Start online Customer Seat</a> -->
                                                            @elseif(isset($auction_id))
                                                                <!-- <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/payment') }}" class="button button-border button-rounded">Start online Customer Seat</a> -->
                                                            @else
                                                                <!-- <a href="{{ url('my-account/item/'.$item->id.'/payment') }}" class="button button-border button-rounded">Start online Customer Seat</a> -->
                                                            @endif                                                    
                                                        @endif
                                                    @else
                                                        @if($date_time >= date("Y-m-d H:i", strtotime($item->auction_detail->auction->auction_date.' '.$item->auction_detail->auction->auction_start_time)))
                                                            @if($item->auction_detail->status == PRODUCT_TO_LIVE)
                                                                @if(isset($category_id))
                                                                    <!-- <a href="{{ url('auction-categories/'.$category_id.'/item/'.$item->id.'/item-bid/'.$item->id) }}" class="button button-border button-rounded">Bid Now <i class="icon-circle-arrow-right"></i></a> -->
                                                                @elseif(isset($auction_id))
                                                                    <!-- <a href="{{ url('auctions/'.$auction_id.'/item/'.$item->id.'/item-bid/'.$item->id) }}" class="button button-border button-rounded">Bid Now <i class="icon-circle-arrow-right"></i></a> -->
                                                                @else
                                                                    <!-- <a href="{{ url('my-account/item/'.$item->id.'/item-bid/'.$item->id) }}" class="button button-border button-rounded">Bid Now <i class="icon-circle-arrow-right"></i></a> -->
                                                                @endif
                                                            @else
                                                                <!-- <button class="button button-3d button-rounded button-white button-light">This item not started yet. Please wait... <i class="icon-repeat"></i></button> -->
                                                            @endif                                                        
                                                        @endif
                                                    @endif
                                                @else
                                                    <h5 class="text-center"> 
                                                        @if(isset($category_id))
                                                            <!-- <a style="cursor: pointer;" href="{{url('login')}}?d={{Crypt::encrypt('auction-categories/'.$category_id.'/item/'.$item->id)}}" class="button button-border button-rounded"> -->
                                                        @else
                                                            <!-- <a style="cursor: pointer;" href="{{url('login')}}?d={{Crypt::encrypt('auctions/'.$auction_id.'/item/'.$item->id)}}" class="button button-border button-rounded"> -->
                                                        @endif
                                                        <!-- BID NOW</a> -->
                                                    </h5>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="line"></div>
                    <div class="col_full nobottommargin">                        
                        <?php $i = 0; ?>
                        @if($relatedItems)
                        <h4>Related Items</h4>
                        <div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30" data-nav="true" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">
                            @foreach($relatedItems as $r_item)
                                @if($r_item->id != $item->id)
                                <div class="oc-item">
                                    <div class="product iproduct clearfix">
                                        <div class="product-image">

                                            @if(isset($category_id))
                                                <a href="{{ url('auction-categories/'.$category_id.'/item/'.$r_item->id) }}">
                                            @elseif(isset($auction_id))
                                                <a href="{{ url('auctions/'.$auction_id.'/item/'.$r_item->id) }}">
                                            @else
                                                <a href="{{ url('my-account/item/'.$r_item->id) }}">
                                            @endif
                                                @if($r_item->image_path)
                                                    @if(File::exists(storage_path('uploads/images/item/'.$r_item->image_path)))
                                                        <img src="{{ url('core/storage/uploads/images/item/'.$r_item->image_path) }}" />
                                                    @else
                                                        <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                    @endif
                                                @else
                                                    <img src="{{ url('core/storage/uploads/images/item/empty.jpg') }}" />
                                                @endif
                                            </a>
                                        </div>
                                        <div class="product-desc center">
                                            <div class="product-title">
                                                <h3><a href="#">{{ $r_item->name }}</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </div>
                        @endif

                        @if($i < 2)
                            <h5 class="text-center">No Related Items</h5>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')
    </script>
    <script defer src="{{asset('assets/front/js/jquery.flexslider.js')}}"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.plugin.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.countdown.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>
    <!-- pusher -->
    <script type="text/javascript" src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <!-- pusher -->
    <!-- toaster -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- toaster -->
    <script type="text/javascript">
        $(function() {
            // SyntaxHighlighter.all();
        });
        $(window).load(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: true,
                slideshow: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                //animation: "slide",
                controlNav: true,
                animationLoop: true,
                slideshow: false,
                sync: "#carousel",
                start: function(slider) {
                    $('body').removeClass('loading');
                }
            });
        });
        var item_status    = {!! ($item->auction_detail)?$item->auction_detail->status:'' !!} 
        if(item_status != ''){
            if(item_status != 2){
                var pusher = new Pusher('bd44e8ba54aa707e490d', {
                        encrypted: true
                    });
                var channel = pusher.subscribe('auction-channel');
                    channel.bind('bid-event', function(data) {
                        toastr.info('First bid added on this item.')
                        location.reload();
                });
            }else{
                var pusher = new Pusher('bd44e8ba54aa707e490d', {
                        encrypted: true
                    });
                var channel = pusher.subscribe('auction-channel');
                    channel.bind('bid-event', function(data) {
                        toastr.info('New bid added on this item.')
                });
            }
        }    

        $(function () {
            var date = '{!! (($item->auction_detail)?$item->auction_detail->auction->auction_date:'').' '.(($item->auction_detail)?$item->auction_detail->auction->auction_start_time:'') !!}';
            var austDay = new Date(date);
            $('#defaultCountdown').countdown({until: austDay, padZeroes: true, onExpiry: liftOff, onTick: watchCountdown});

            function liftOff() { 
                location.reload();
            }

            function watchCountdown(periods) {
                if(periods[4] == 0){
                    if(periods[5] > 0){
                        $('#monitor').text('Just ' + periods[5] + ' minutes and ' + periods[6] + ' seconds to go');
                    }
                    else{
                        $('#monitor').text('Just ' + periods[6] + ' seconds to go');
                    }
                }
            }

            var status = {!! ($item->auction_detail)?$item->auction_detail->auction->auction_status:0 !!}        

            if(status == 0 || status == 1){
                setInterval(function(){
                    $.ajax({
                        url: "{{URL::to('auction/get-auction-status')}}",
                        method: 'GET',
                        data: {item_id:{!! ($item)?$item->id:0 !!}},
                        async: false,
                        success: function (response) {
                            console.log(response);
                            if(response.status == 2){
                                location.reload();
                            }
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                }, 10000)
            }
        });  

        /* puser event*/
        Pusher.logToConsole = false;
        var pusher = new Pusher('55845bb31c3702ef4dd0', {
          cluster: 'ap2',
          forceTLS: true
        });

        var channel = pusher.subscribe('auction-channel');

        //bid event
        channel.bind('bid-event', function(data) {
            toastr.info('New Item on the Live Now');
        });  

        //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->

    </script>

</body>

</html>
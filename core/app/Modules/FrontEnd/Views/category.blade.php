<!DOCTYPE html>
<html dir="ltr" lang="en-US" ng-app="myApp">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56433362-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-56433362-2');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}" type="text/css" />

    <!-- Angular-confirm-master -->
    <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
    <!-- Angular-confirm-master -->

    <style>
        .pricing-box.pricing-extended {
            height: 215px;
        }

        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title ============================================= -->
    <title>Schokman & Samerawickreme | Categories</title>
</head>
<body class="stretched" ng-cloak>
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header ============================================= -->
        <header id="header" class="full-header">
            @include('front_ui.includes.header_menu_dark')
        </header>
        <!-- #header end -->

        <!-- Page Title ============================================= -->
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{ $category->name }}</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">{{ $category->name }}</li>
                </ol>
            </div>
        </section>
        <!-- #page-title end -->

        <!-- Content ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix container-main">

                    <!-- MyController -->
                    <div ng-controller="MyController" class="my-controller">

                        <div class="row">
                            <div class="col-lg-8">
                                <h4>Page: @{{ currentPage }}</h4>
                            </div>
                            <div class="col-lg-3">
                                <input ng-model="q" id="search" class="form-control" autocomplete="off" placeholder="Search...">
                            </div>
                            <div class="col-lg-1">
                                <input type="number" min="1" max="100" class="form-control" ng-model="pageSize">
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div dir-paginate="product in filtered = (products | filter:q | itemsPerPage: pageSize)" current-page="currentPage" class="pricing-box pricing-extended bottommargin clearfix">
                            <div class="pricing-action-area">                                
                                <img width="100%" src="@{{ product.image }}" />
                            </div>
                            <div class="pricing-desc">
                                <div class="pricing-title">
                                    <h3>@{{ product.name }}</h3>
                                </div>
                                <div class="pricing-features">
                                    <ul class="iconlist-color clearfix">
                                        <li><i class="icon-desktop"></i> Item Code - @{{ product.item_code }}</li>
                                        <li ng-if="product.start_bid_price"><i class="icon-magic"></i> Bid value - Rs @{{ product.start_bid_price }}</li>
                                    </ul>
                                </div>
                                <div class="col_full nobottommargin textright divider-right">
                                    <a class="button button-border button-small button-rounded s" style="margin-right:0;margin-bottom: 7px;" href="@{{ product.link }}">View Details</a>
                                    <div ng-if="product.watchlist == null">
                                        <br><a href="{{url('login')}}?d={{Crypt::encrypt('auction-categories/'.$category->id)}}"><u>Sign in</u></a> to add to watchlist
                                    </div>
                                    <a style="cursor: pointer;" ng-show="product.watchlist == '0'" class="button button-border button-small button-rounded" ng-click="WatchList($index,product.id,product.sns_auction_id)"><i class="icon-eye-open"></i> Add To Watchlist</a>
                                    <div ng-show="product.watchlist > 0">
                                        <br>Added to the watchlist
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-center" ng-hide="filtered.length">No items found.</h5>

                        <h4>Showing @{{filtered.length}} items</h4>

                    </div>
                    <!-- End MyController -->

                    <!-- OtherController -->
                    <div ng-controller="OtherController" class="other-controller">
                        <div class="text-right">
                            <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="{{asset('assets/angular/front-end/template/dirPagination.tpl.html') }}"></dir-pagination-controls>
                        </div>
                    </div>
                    <!-- End OtherController -->                    
                    
                </div>
            </div>
        </section>
        <!-- #content end -->

        <!-- Footer ============================================= -->
        @include('front_ui.includes.footer')
        <!-- #footer end -->
    </div>
    <!-- #wrapper end -->

    <!-- Go To Top ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script>

    <!-- Footer Scripts ============================================= -->
    <script type="text/javascript" src="{{asset('assets/front/js/functions.js')}}"></script>

    <!-- ANGULAR -->
    <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
    <!-- ANGULAR -->

    <script src="{{asset('assets/dist/angular/angular-dir-pagination/dirPagination.js')}}"></script>

    <!-- angular-confirm-master -->
    <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
    <!-- angular-confirm-master -->


    <script type="text/javascript">
        'use strict';

        /*var app = angular.module('angularApp',['angularUtils.directives.dirPagination','localytics.directives','cp.ngConfirm']);

        app.controller('AuctionController', function ($scope,$ngConfirm,$http,BASE_URL) {

            

        });*/

        var myApp = angular.module('myApp', ['angularUtils.directives.dirPagination','cp.ngConfirm']);

        function MyController($scope,$ngConfirm,$http,BASE_URL) {

            $scope.currentPage = 1;
            $scope.pageSize = 10;

            $scope.products = {!! $products !!};
          
            $scope.pageChangeHandler = function(num) {
                console.log('Category item page changed to ' + num);
            };

            $scope.WatchList = function(index,item_id,auction_id=null) {
                var watchlist = {
                    item_id     : item_id,
                    auction_id  : auction_id
                };

                var formData = new FormData();

                formData.append('watchlist', JSON.stringify(watchlist));

                $http.post(BASE_URL+"customer/add-watchlist", formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function(response){
                    if(response.data.success == true) {
                        $scope.products[index].watchlist = response.data.id;
                    } else if(response.data.error == true) {
                        $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'red', closeIcon: false, buttons: {ok: function () {}}});
                    }
                },function(error){
                    console.log(error);
                });
            };
        }

        function OtherController($scope) {
          $scope.pageChangeHandler = function(num) {
            console.log('going to page ' + num);
          };
        }

        myApp.controller('MyController', MyController);
        myApp.controller('OtherController', OtherController);

        myApp.constant('BASE_URL', "{{asset('/')}}");

        //CHAT FEATURE
        <!--Start of Tawk.to Script-->
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cf5f02ab534676f32ad3857/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        <!--End of Tawk.to Script-->
    </script>
</body>

</html>
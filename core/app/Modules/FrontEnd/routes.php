<?php

Route::group(array('prefix'=>'','module' => 'FrontEnd', 'namespace' => 'App\Modules\FrontEnd\Controllers'), function() {

    // Front Login
    Route::get('login', [
      'as' => 'front.login', 'uses' => 'FrontEndController@loginView'
    ]);

    Route::get('register', [
        'as' => 'front.register', 'uses' => 'FrontEndController@register'
    ]);

    Route::get('privacy-policy', [
        'as' => 'front.privacy', 'uses' => 'FrontEndController@privacyPolicy'
    ]);

    Route::get('terms-and-conditions', [
        'as' => 'front.privacy', 'uses' => 'FrontEndController@termsConditions'
    ]);

    Route::get('terms-conditions-sinhala', [
        'as' => 'front.privacy', 'uses' => 'FrontEndController@termsConditionsSinhala'
    ]);

    Route::get('auction-categories/{id}', [
        'as' => 'front.categories', 'uses' => 'FrontEndController@categories'
    ]);

    Route::get('auctions/{id}', [
        'as' => 'front.auctions', 'uses' => 'FrontEndController@auctions'
    ]);

    Route::get('auction-categories/{id}/item/{item_id}', [
        'as' => 'front.item', 'uses' => 'FrontEndController@categoryItem'
    ]);

    Route::get('auctions/{id}/item/{item_id}', [
        'as' => 'front.item', 'uses' => 'FrontEndController@auctionItem'
    ]);

    Route::get('login/{id}/{code}', [
        'as' => 'front.login', 'uses' => 'FrontEndController@activationView'
    ]);

    Route::get('reminder-edit', [
        'as' => 'front.reminder.edit', 'uses' => 'FrontEndController@reminderEditView'
    ]);

    /**
    * POST Routes
    */    
    Route::post('login', [
        'as' => 'front.login', 'uses' => 'FrontEndController@login'
    ]);    
    
    Route::post('register', [
        'as' => 'front.register', 'uses' => 'FrontEndController@addCustomer'
    ]);

    Route::post('login/{id}/{code}', [
        'as' => 'front.login_activate', 'uses' => 'FrontEndController@activation'
    ]);

    Route::post('reminder-email-sender', [
        'as' => 'front.reminder.email.sender', 'uses' => 'FrontEndController@reminderEmailSender'
    ]);

    Route::post('reminder-edit', [
        'as' => 'front.reminder.edit', 'uses' => 'FrontEndController@reminderEdit'
    ]);
});


Route::group(['middleware' => ['auth_front']], function() {
	Route::group(array('prefix'=>'','module' => 'FrontEnd', 'namespace' => 'App\Modules\FrontEnd\Controllers'), function() {

        Route::get('auctions/{id}/item/{item_id}/item-bid/{bid_item_id}', [
            'as'   => 'front.item-bid', 'uses' => 'FrontEndController@auctionItemBid',
        ]);

        Route::get('auction-categories/{id}/item/{item_id}/item-bid/{bid_item_id}', [
            'as' => 'front.item-bid.category', 'uses' => 'FrontEndController@categoryItemBid'
        ]);

        Route::get('my-account/item/{item_id}/item-bid/{bid_item_id}', [
            'as' => 'front.item-bid.account', 'uses' => 'FrontEndController@myAccountItemBid'
        ]);

		Route::get('my-account/item/{id}', [
			'as' => 'front.item', 'uses' => 'FrontEndController@myAccountItem'
		]);        

        /* ################## auction payment ################################################*/
        Route::get('auctions/{id}/item/{item_id}/payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionPayment'
        ]);

        Route::get('auction-categories/{id}/item/{item_id}/payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryPayment'
        ]);

        Route::get('my-account/item/{item_id}/payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountPayment'
        ]);        

        Route::get('auctions/{id}/item/{item_id}/payment-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionPaymentComplete'
        ]);

        Route::get('auction-categories/{id}/item/{item_id}/payment-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryPaymentComplete'
        ]);

        Route::get('my-account/item/{item_id}/payment-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountPaymentComplete'
        ]);
        /* ################## end auction payment #############################################*/

        /* ################## item payment ####################################################*/
        Route::get('auctions/{id}/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionItemPayment'
        ]);

        Route::get('auction-categories/{id}/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryItemPayment'
        ]);
        Route::get('my-account/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountItemPayment'
        ]);

        Route::get('auctions/{id}/item/{item_id}/item-payment/gateway', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionItemGateway'
        ]);

        Route::get('auction-categories/{id}/item/{item_id}/item-payment/gateway', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryItemGateway'
        ]);
        Route::get('my-account/item/{item_id}/item-payment/gateway', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountItemGateway'
        ]);

        Route::get('auctions/{id}/item/{item_id}/gateway-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionItemPaymentComplete'
        ]);
        Route::get('auction-categories/{id}/item/{item_id}/gateway-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryItemPaymentComplete'
        ]);
        Route::get('my-account/item/{item_id}/gateway-complete', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountItemPaymentComplete'
        ]);
        /* ################## end item payment ################################################*/

        Route::get('my-account', [
            'as' => 'front.my-account', 'uses' => 'FrontEndController@myAccount'
        ]);        

        Route::get('logout', [
            'as' => 'front.logout', 'uses' => 'FrontEndController@logout'
        ]);

        /* ################## payment receipts ################################################*/
        Route::get('auction-receipt', [
            'as' => 'front.auction.receipt', 'uses' => 'FrontEndController@auctionReceipt'
        ]);

        Route::get('item-receipt', [
            'as' => 'front.item.receipt', 'uses' => 'FrontEndController@itemReceipt'
        ]);

        Route::get('front/customer', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@getCustomerTypes'
        ]);

        /* ################## end auction payment ############################################*/

        /*Route::get('item-win', [
            'as' => 'front.item-bid', 'uses' => 'FrontEndController@itemWin'
        ]);*/

        /*Route::get('my-watch', [
            'as' => 'front.watch', 'uses' => 'FrontEndController@watchList'
        ]);*/

        /*Route::get('payment-receipt', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@paymentReceipt'
        ]);*/

        /**
        * POST Routes
        */ 
        Route::post('auctions/{id}/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@auctionItemPaymentDetails'
        ]);
        Route::post('auction-categories/{id}/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@categoryItemPaymentDetails'
        ]);
        
        Route::post('my-account/item/{item_id}/item-payment', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@myAccountItemPaymentDetails'
        ]);

        Route::post('auctions/{id}/item/{item_id}/item-payment/gateway', [
            'as' => 'front.payment', 'uses' => 'FrontEndController@blockCustomer'
        ]);  



	});
});
<?php namespace App\Modules\FrontEnd\BusinessLogics;

use App\Models\Countries;
use App\Models\City;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author<author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Models\WatchList;
use App\Modules\AuctionLiveManage\Models\AuctionBidManage;
use DB;

class Logic extends Model {

	/**
     * This function is used to get all countries
     * @return auction object
     */
    public function allCountries() {
        return $countries = Countries::all();
    }

    /**
     * This function is used to get all cities
     * @return auction object
     */
    public function allCities() {
      return $cities = City::all();
    }


    /**
     * This function is used to get user watch list details
     * @param integer $user_id
     * @response
     */

    public function getWatchList($user_id){
        $watch_list = WatchList::with('item.single_image','auction','item.auction_detail.bid')->where('user_id',$user_id)->get();
        if($watch_list){
            return $watch_list;
        }else{
            return [];
        }
    }

    /**
     * This function is used to get user bidding details
     * @param integer $user_id
     * @response
     */

    public function getBiddingList($user_id){

        $auction_bid = DB::select(DB::raw('SELECT
                                              i.id AS item_id,
                                              i.display_name AS item_name,
                                              i.item_code AS item_code,
                                              aw.sns_user_id,
                                              scr.user_id,
                                            (
                                            SELECT
                                              MIN(bid_amount)
                                            FROM
                                              sns_auction_bid
                                            WHERE
                                              sns_auction_detail_id = ad.id
                                            ) AS start_bid,
                                            (
                                            SELECT
                                              MAX(bid_amount)
                                            FROM
                                              sns_auction_bid
                                            WHERE
                                              sns_auction_detail_id = ad.id
                                            ) AS highest_bid,
                                            ad.withdraw_amount AS withdraw_amount,
                                            ad.status AS item_status,
                                            (
                                            SELECT
                                              image_path
                                            FROM
                                              sns_item_image
                                            WHERE
                                              sns_item_id = i.id
                                            LIMIT 1
                                            ) AS image_path
                                            FROM
                                              sns_auction_bid ab
                                            INNER JOIN
                                              sns_auction_detail ad ON ab.sns_auction_detail_id = ad.id
                                            INNER JOIN
                                              sns_item i ON ad.sns_item_id = i.id
                                            LEFT JOIN
                                              sns_auction_winner aw ON ab.id = aw.sns_auction_bid_id
                                            INNER JOIN
                                              sns_customer_register scr ON aw.sns_user_id = scr.id
                                            WHERE
                                              ab.sns_customer_id = '.$user_id.'
                                            GROUP BY
                                              ab.sns_auction_detail_id'));
        if($auction_bid){
            return $auction_bid;
        }else{
            return [];
        }
    }


    /**
     * This function is used to get won item list
     * @param integer $user_id
     * @return win_item_list
     */

    public function getWinList($user_id){

        $won_item_list = DB::select('SELECT
                                        aw.win_amount,
                                        ad.start_bid_price,
                                        i.display_name,
                                        i.item_code,
                                        i.id,
                                        ii.image_path,
                                        p.amount,
                                        sa.event_name
                                    FROM
                                        sns_auction_winner aw                                    
                                    INNER JOIN sns_auction_bid ab ON aw.sns_auction_bid_id = ab.id
                                    INNER JOIN sns_auction_detail ad ON ad.id = ab.sns_auction_detail_id
                                    INNER JOIN sns_auction sa ON ad.sns_auction_id = sa.id
                                    INNER JOIN sns_item i ON ad.sns_item_id = i.id
                                    LEFT JOIN sns_item_image ii ON ii.sns_item_id = i.id
                                    LEFT JOIN (SELECT 
                                        sns_auction_id, sns_item_id, SUM(amount) AS amount
                                        FROM
                                        sns_payment
                                        WHERE
                                        payment_status = 1
                                        GROUP BY sns_auction_id , sns_item_id) p ON ad.sns_auction_id = p.sns_auction_id AND i.id = p.sns_item_id
                                    WHERE
                                        aw.sns_user_id = :user_id 
                                    GROUP BY 
                                        i.id',['user_id' => $user_id]);
        if($won_item_list){
            return $won_item_list;
        }else{
            return [];
        }
    }


    // This function is used to get latest live section item.
    public function getLiveSectionItem($user = null){
    
      $live_item = DB::table('sns_auction_detail')
        ->join('sns_item', function($join){
          $join->on('sns_auction_detail.sns_item_id', '=', 'sns_item.id')
            ->where('sns_item.status', '=', PRODUCT_TO_LIVE)
            ->whereNull('sns_item.deleted_at');
        })
        ->rightJoin('sns_auction', function($join){
          $join->on('sns_auction_detail.sns_auction_id', '=', 'sns_auction.id')
            ->whereNull('sns_auction.deleted_at');
        })
        ->leftJoin('sns_auction_bid', function($join){
          $join->on('sns_auction_detail.id', '=', 'sns_auction_bid.sns_auction_detail_id')
            ->whereNull('sns_auction_bid.deleted_at');
        })
        ->leftJoin('sns_item_image', function($join){
          $join->on('sns_item.id', '=', 'sns_item_image.sns_item_id')
            ->whereNull('sns_item_image.deleted_at');
        })
        ->select('sns_auction.id as auction_id',
                 'sns_auction.event_name as auction_name',
                 'sns_auction.auction_date as auction_date',
                 'sns_item.id as item_id',
                 'sns_item.name as item_name',
                 'sns_item.item_code as item_code',
                 'sns_auction_bid.bid_amount as bid_amount',
                 'sns_item_image.image_path',
                 'sns_auction.auction_status',
                 'sns_auction_detail.status as item_detail_status',
                 'sns_item.show_hide_code'
                )
        ->where('sns_auction.auction_status', '=', AUCTION_LIVE)
        ->where('sns_auction_detail.status', '=', PRODUCT_TO_LIVE)
        ->where('sns_auction_detail.call', '!=', CALL3)
        ->orderBy('sns_auction_bid.bid_time', 'desc')
        ->limit(1)
        ->get();
      
      return $live_item;

    }

}

<?php namespace App\Modules\FrontEnd\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Modules\ProductManage\BusinessLogics\Logic as ProductLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use App\Modules\CategoryManage\BusinessLogics\Logic as CategoryLogic;
use App\Modules\FrontEnd\BusinessLogics\Logic as FrontLogic;
use App\Modules\AuctionLiveManage\BusinessLogics\Logic as AuctionLiveLogic;
use App\Modules\CustomerManage\BusinessLogics\Logic as CustomerLogic;
use App\Modules\PaymentManage\BusinessLogics\Logic as PaymentLogic;
use App\Modules\DefaultDataManage\BusinessLogics\Logic as DefaultDataLogic;


use App\Modules\CustomerManage\Models\CustomerAuctionManage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;
use Sentinel;
use Session;
use Exception;
use Response;
use Activation;
use Permission;
use Reminder;
use Mail;
use Input;


// payment
use App\Classes\PayCorp\Config\ClientConfig;
use App\Classes\PayCorp\GatewayClient;
use App\Classes\PayCorp\Payment\PaymentInitRequest;
use App\Classes\PayCorp\Component\TransactionAmount;
use App\Classes\PayCorp\Component\Redirect;
use App\Classes\PayCorp\Enums\TransactionType;
use App\Classes\PayCorp\Payment\PaymentCompleteRequest;

use Crypt;

class FrontEndController extends Controller {

	protected $product;
	protected $auction;
	protected $category;
	protected $front;
	protected $auction_live;
	protected $customer;
	protected $payment;
	protected $defaultData;

	public function __construct(ProductLogic $product, AuctionLogic $auction, CategoryLogic $category, FrontLogic $front, AuctionLiveLogic $auction_live, CustomerLogic $customer, PaymentLogic $payment, DefaultDataLogic $defaultData)
	{
        $this->product 		= $product;
        $this->auction 		= $auction;
        $this->category 	= $category;
        $this->front 		= $front;
        $this->auction_live = $auction_live;
        $this->customer 	= $customer;
        $this->payment 		= $payment;
		$this->defaultData  = $defaultData;

		if(Sentinel::check()){
			$user = Sentinel::getUser();
			view()->share('user', $user);
		}
    }

    /**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function loginView(Request $request)
	{
		try{
			if(!Sentinel::check()){
				return view('FrontEnd::login');
			}else{
				if($request->has('d')){
					$redirect = Crypt::decrypt($request->d);
				}else{
					$redirect = Session::get('loginRedirect', $request->url());
				}
				Session::forget('loginRedirect');
				return redirect($redirect);
			}
		}catch(\Exception $e){
			return view('FrontEnd::login')->withErrors(['login' => $e->getMessage()]);
		}
	}

	/**
     * add new FrontEndUser data to database
     * @param $request
     * @return Redirect to type add
     */
    public function login(Request $request)
    {
      	$credentials = array(
	        'username' => $request->username,
	        'password' => $request->password
	    );

	    if ($request->remember) {
	        $remember = true;
	    } else {
	        $remember = false;
	    }

	    try {

		//check customer band or not
		$customer_detail = $this->customer->loadCustomer($request->username);
		//if(count($customer_detail) > 0){
			//if($customer_detail->band_status == BAND || $customer_detail->payments || sizeof($customer_detail->approve_payments) > 0){
			if($customer_detail && $customer_detail->band_status == BAND){
				return redirect()->route('front.login')->withErrors(array('login' => ACCOUNT_SUSPENDED_MESSAGE));
			}
		//}
		$user = Sentinel::authenticate($credentials, $remember);
		if ($user) {
			if ($user->status == 1) {
				if($request->has('d')){
					$redirect = Crypt::decrypt($request->d);
				}else{
					$redirect = Session::get('loginRedirect', '');
				}
				Session::forget('loginRedirect');
				return redirect($redirect);
			} else {
				Sentinel::logout();
				$msg = 'Temporarily Deactivate User!';
			}

		} else {
			$msg = 'Invalid username/password. Try again!';
		}

	    } catch (\Exception $e) {
	        $msg = $e->getMessage();
	    }

        return redirect()->route('front.login')->withErrors(array('login' => $msg));
    }

    /** 
	*	@method logout()
	*	@description Logging out the logged in user
	*	@return URL redirection
	*/
	public function logout(){
		Sentinel::logout();
		return redirect()->route('front.login');
	}

	/**
	 * @method blockUser()
	 * @description user blocking and sign out 
	 */
	public function blockUser($customer_id = null){
		return $customer_detail = $this->customer->bandCustomer($customer_id, 1);
	}


    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addCustomer(Request $request){
		try{
			//decode json object
			$form       = json_decode($request->register);
			$emailExist = $this->customer->emailExist($form->email);
			if($emailExist){
				return Response::json([
					'error'   => true,
					'message' => 'Registering email address already exist.',
					'title'   => 'Oops!'
				]);
			}else{
				$nic_contact_exist = $this->customer->getCustomerByEmail($form->nic, $form->contact_no);
				if($nic_contact_exist){
					if($nic_contact_exist->telephone_no == $form->contact_no){
						return Response::json([
							'error'   => true,
							'message' => 'Registering contact no already exist',
							'title'   => 'Oops!'
						]);
					}elseif($nic_contact_exist->nic == strtoupper(trim($form->nic))){
						return Response::json([
							'error'   => true,
							'message' => 'Registering nic no already exist',
							'title'   => 'Oops!'
						]);
					}
				}else{
					$customer = $this->customer->add([
						'register_type' 	=> ONLINE_BIDDER,
						'fname'          	=> $form->first_name,
						'lname'  			=> $form->last_name,
						'email'    			=> trim($form->email),
						'telephone_no'    	=> trim($form->contact_no),
						'address_1'  		=> $form->address_1,
						'address_2'    		=> (isset($form->address_2))?$form->address_2:'',
						'city'    			=> $form->city->name,
						'zip_postal_code'   => $form->zip_code,
						'country_id'   		=> $form->country->id,
						'nic'				=> strtoupper(trim($form->nic))
					], $form);
					if($customer){
						//Upload NIC or Driving Licence Image
						if($request->hasFile('file')) {
							$file 	         = $request->file('file'); //get file
							$extension       = $file->getClientOriginalExtension(); //get extension
							$fileName        = 'customer-' .$customer->id.'-'.date('YmdHis').'.'.$extension; //set file name	
							$destinationPath = storage_path('uploads/images/customer/nic-licence'); // destination	
							$file->move($destinationPath, $fileName);//move to file
						}else {
							$fileName = '';
						}
						//Upload proof image
						if($request->hasFile('file2')) {
							$file2 	          = $request->file('file2'); //get file
							$extension2       = $file->getClientOriginalExtension(); //get extension
							$fileName2        = 'customer-' .$customer->id.'-'.date('YmdHis').'.'.$extension2; //set file name	
							$destinationPath2 = storage_path('uploads/images/customer/proof'); // destination	
							$file2->move($destinationPath2, $fileName2);//move to file
						}else {
							$fileName2 = '';
						}
						$customerUpdate = $this->customer->updateCustomerUploadImages($customer->id, $fileName, $fileName2);
						if($customerUpdate){
							return Response::json([
								'success' => true,
								'message' => 'Registration completed.Please verify your email address.',
								'title'   => 'Job well done!'
							]);
						}else{
							return Response::json([
								'error' => true,
								'message' => 'Error occurred when registering',
								'title' => 'Ops!'
							]);
						}
					}else{
						return Response::json([
							'error' => true,
							'message' => 'Error occurred when registering',
							'title' => 'Ops!'
						]);
					}
				}
			}
		}catch(Exception $e){
        	return Response::json([
				'error' => true,
				'message' => $e->getMessage(),
				'title' => 'Ops!'
			]);
    	}
	}

	/** 
	*	@method reminderEditView()
	*	@description use view of the password reset
	*	@return html UI
	*/
	public function activationView($id, $code) {
		$user = Sentinel::findById($id);

        if (Activation::exists($user)) {
        	if (Activation::complete($user, $code))
			{
			    // Activation was successfull
			    return view('FrontEnd::login', ['id' => $id, 'code' => $code]);
			}else{
				// activation code error
				return view('FrontEnd::login', ['error' => 'Activation code error!', 'id' => $id, 'code' => $code]);
			}
        }
        else {	
            //incorrect info was passed
            return view('FrontEnd::login');
        }
	}

	/** 
	*	@method reminderEdit()
	*	@description reset password to new password
	*	@return to login page
	*/
	public function activation(Request $request) 
	{
        $user = Sentinel::findById(Input::get('id'));

    	if ($activation = Activation::completed($user)) {
		    // Activation was successfull
		    $credentials = array(
		        'username' => $request->username,
		        'password' => $request->password
		    );

		    if ($request->remember) {
		        $remember = true;
		    } else {
		        $remember = false;
		    }

		    try {
		        $loggedUser = Sentinel::authenticate($credentials, $remember);

		        if ($loggedUser) {
		            if ($loggedUser->status == 1) {
		                $redirect = Session::get('loginRedirect', '');
		                Session::forget('loginRedirect');
		                return redirect($redirect);
		            } else {
		                $msg = 'Temporarily Deactivate User!';
		            }

		        } else {
		            $msg = 'Invalid username/password. Try again!';
		        }

		    } catch (\Exception $e) {
		        $msg = $e->getMessage();
		    }
	        return redirect()->route('front.login')->withErrors(array('login' => $msg, 'id' => Input::get('id'), 'code' =>Input::get('code')));
		} else {
		    $msg = "Activation not found or not completed!";
            return view('front.login', ['login' => $msg]);
		}
    }

    /**
     * reset password
     * @param  user email
     * @return user confirmation mail
     */
    public function reminderEmailSender(Request $request){

		$user = Sentinel::getUserRepository()->where('email',$request->get('email'))->first();

		if($user){
            $reminder = Reminder::create($user);

            $data = [
	            'email' => $user->email,
	            'fname' => $user->first_name,
	            'name' => $user->full_name,
	            'subject' => 'Reset Your Password',
	            'code' => $reminder->code,
	            'id' => $user->id
	        ];

	        Mail::queue('FrontEnd::template.reminder-email-template', $data, function($message) use ($data) {
	            $message->to($data['email'], $data['name'])->from('test@email.com')->subject($data['subject']);
	        });

            return response()->json(['status' => 'success']);
        }
        else{
            return response()->json(['status' => 'error']);
        }
    }

    /** 
	*	@method reminderEditView()
	*	@description use view of the password reset
	*	@return html UI
	*/
	public function reminderEditView(Request $request) {

		$user = Sentinel::findById($request->get('id'));
        
        if (Reminder::exists($user, $request->get('code'))) {
            return view('FrontEnd::reminder-edit', ['id' => $request->get('id'), 'code' => $request->get('code')]);
        }
        else {
            //incorrect info was passed
            return view('FrontEnd::login');
        }
	}

	/** 
	*	@method reminderEdit()
	*	@description reset password to new password
	*	@return to login page
	*/
	public function reminderEdit(Request $request) {
        
        $password = Input::get('password');
        $passwordConf = Input::get('password_confirmation');

       	$user = Sentinel::findById(Input::get('id'));
        $reminder = Reminder::exists($user, Input::get('code'));

        //incorrect info was passed.
        if ($reminder == false) {
            return view('FrontEnd::login');
        }
 
		if(empty($password) || empty($passwordConf)){
			return view('FrontEnd::reminder-edit', ['id' => Input::get('id'), 'code' =>Input::get('code'), 'msg' => 'Please fill all the requred fields.']);
		} else if($password != $passwordConf) {
            return view('FrontEnd::reminder-edit', ['id' => Input::get('id'), 'code' =>Input::get('code'), 'msg' => 'Passwords must match.']);
        }

        Reminder::complete($user,Input::get('code'), $password);

        $credentials = array(
	        'username' => $user->username,
	        'password' => $password
	    );

	    if ($request->remember) {
	        $remember = true;
	    } else {
	        $remember = false;
	    }

        $user = Sentinel::authenticate($credentials, $remember);

	        if ($user) {
	            if ($user->status == 1) {
	                if($request->has('d')){
						$redirect = Crypt::decrypt($request->d);
					}else{
						$redirect = Session::get('loginRedirect', '');
					}
	                Session::forget('loginRedirect');
	                return redirect($redirect);
	            } else {
	            	Sentinel::logout();
	                $msg = 'Temporarily Deactivate User!';
	            }

	        } else {
	            $msg = 'Invalid username/password. Try again!';
	        }

	     return redirect()->route('front.login')->withErrors(array('login' => $msg));

    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("FrontEnd::index");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function categories($id)
	{
		$user 	  = Sentinel::getUser();
		$category = $this->category->getCategoryDetails($id);
		$products = $this->product->getItemsByCategory($id);
		
		foreach ($products as $key => $product) {
			if($product->image_path && File::exists(storage_path('uploads/images/item/'.$product->image_path))) {
                $product->image = url('core/storage/uploads/images/item/'.$product->image_path);
            }
            else {
                $product->image = url('core/storage/uploads/images/item/empty.jpg');
            }
            $product->link = url('auction-categories/'.$id.'/item/'.$product->id);

            $product->watchlist = null;
            if(isset($user)) {
            	$product->watchlist = 0;
            	if(count($product->watchList)>0) {
	            	foreach ($product->watchList as $key => $list) {
						if($user->id == $list->user_id){
							$product->watchlist = $list->id;
						}            		
	            	}
	            }
            }
		}

		return view("FrontEnd::category")->with([
			'category'	=> $category[0],
			'products'	=> $products
		]);
	}

	/**
     * This function is used to display user auction list
     * @param
     * @response
     */
	public function auctions($id, Request $request)
	{
		$customer_id 		= null;
		$user 	   	 		= Sentinel::getUser();
		$auction   	 		= $this->auction->getAuction($id);
		$live_section_item 	= $this->front->getLiveSectionItem($user);
		$live_auction_id	= null;
		$card_exceed		= 0;
	
		if(sizeof($user) > 0){
			$customer_detail = $this->customer->getCustomer($user->id);
			if($customer_detail){
				$customer_id = $customer_detail->id;
			}
		}

		if(sizeof($live_section_item) > 0){
			$live_auction_id = $live_section_item[0]->auction_id;
		}

		if(Sentinel::check()){
			$payment = $this->payment->getPaymentDetails($live_auction_id, $customer_id);
		}else{
			$payment = [];
		}

		if($request->get('category') > 0){
			$products = $this->auction->getAuctionCategoryFilterDetails($id, $request->get('category'));
		}else{
			$products = $this->auction->getAuctionDetails($id);
		}
		
		$category = $this->category->getCategoryName();

		foreach ($products as $key => $product) {
			if($product->image_path && File::exists(storage_path('uploads/images/item/'.$product->image_path))) {
                $product->image = url('core/storage/uploads/images/item/'.$product->image_path);
            }
            else {
                $product->image = url('core/storage/uploads/images/item/empty.jpg');
            }
            $product->link = url('auctions/'.$id.'/item/'.$product->id);
            $product->watchlist = null;

            if(isset($user)) {
            	$product->watchlist = 0;
            	if(count($product->watchList)>0) {	            	
	            	foreach ($product->watchList as $key => $list) {
						if($user->id == $list->user_id){
							$product->watchlist = $list->id;
						}            		
	            	}
	            }
            }
		}

		if(sizeof($auction) > 0){
			if(sizeof($auction[0]->card_range) > 0){
				$card_exceed = $auction[0]->card_range[0]->exceed_status;
			}
		}else{
			return response()->view('errors.404');
		}

		return view("FrontEnd::auction")->with([
			'auction'			=> $auction[0],
			'products'			=> $products,
			'category'  		=> $category,
			'category_search'	=> $request->get('category'),
			'live_item'			=> $live_section_item,
			'payment'			=> $payment,
			'exceed_status'		=> $card_exceed
		]);
	}

	/**
     * This function is used to display user auction list
     * @param
     * @response
     */
	public function searchItemCategory(Request $request,$id)
	{
		$user 	  = Sentinel::getUser();
		$auction  = $this->auction->getAuction($id);
		$products = $this->auction->getAuctionCategoryFilterDetails($id, $request->get('category'));
		$category = $this->category->getCategoryName();

		foreach ($products as $key => $product) {
			if($product->image_path && File::exists(storage_path('uploads/images/item/'.$product->image_path))) {
                $product->image = url('core/storage/uploads/images/item/'.$product->image_path);
            }
            else {
                $product->image = url('core/storage/uploads/images/item/empty.jpg');
            }
            $product->link = url('auctions/'.$id.'/item/'.$product->id);

            $product->watchlist = null;
            if(isset($user)) {
            	$product->watchlist = 0;
            	if(count($product->watchList)>0) {	            	
	            	foreach ($product->watchList as $key => $list) {
						if($user->id == $list->user_id){
							$product->watchlist = $list->id;
						}            		
	            	}
	            }
            }
		}

		return view("FrontEnd::auction")->with([
			'auction'	=> $auction[0],
			'products'	=> $products,
			'category' => $category
		]);
	}

	/**
     * This function is used to display auctionItem
     * @param
     * @response
     */
	public function auctionItem($id, $item_id){
		$customer_id = null;
		$user 		 = Sentinel::getUser();
		$item 		 = $this->product->getItemDetails($item_id);

		$amount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$amount = $payment->amount + $amount;
			}
		}

		$relatedItems = $this->auction->getAuctionDetails($id);
		if(sizeof($user) > 0){
			$customer_detail = $this->customer->getCustomer($user->id);
			if(count($customer_detail) > 0){
				$customer_id = $customer_detail->id;
			}
		}
		
		if(Sentinel::check()){
			$payment = $this->customer->getPaymentDetails($id, $customer_id);
		}else{
			$payment = [];
		}

		// check customer auction band or not
		// $customer_detail = $this->customer->customerAuction($customer_id);
		// if(count($customer_detail) > 0){
		// 	if($customer_detail->band_status == BAND){
		// 		$msg = 'Sorry,Your live auction account has been temporarily suspended.';
		// 		return redirect()->route('front.login')->withErrors(array('login' => $msg));
		// 	}
		// }

		return view("FrontEnd::item")->with([
			'item'			=> $item[0],
			'relatedItems'	=> $relatedItems,
			'auction_id' 	=> $id,
			'payment'		=> $payment,
			'amount'		=> $amount
		]);
	}

	/**
     * This function is used to display categoryItem
     * @param
     * @response
     */
	public function categoryItem($id, $item_id){		
		$item = $this->product->getItemDetails($item_id);

        $auction_id = ($item[0]->auction_detail)?$item[0]->auction_detail->sns_auction_id:0;
			
		$amount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$amount = $payment->amount + $amount;
			}
		}

		$relatedItems = $this->product->getItemsByCategory($id);

        if(Sentinel::check()){
	        $payment = $this->customer->getPaymentDetails($auction_id);
	    }
	    else{
	    	$payment = [];
	    }

		return view("FrontEnd::item")->with([
			'item'			=> $item[0],
			'relatedItems'	=> $relatedItems,
			'category_id' 	=> $id,
			'payment'		=> $payment,
			'amount'		=> $amount
		]);
	}

    public function myAccountItem($id){

		$customer_id = null;
        $item        = $this->product->getItemDetails($id);
        $auction_id  = ($item[0]->auction_detail)?$item[0]->auction_detail->sns_auction_id:0;
		$user        = Sentinel::getUser();
		$amount = 0;
		
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$amount = $payment->amount + $amount;
			}
		}

		$relatedItems = $this->auction->getAuctionDetails($auction_id);

		if(count($user) > 0){
			$customer_detail = $this->customer->getCustomer($user->id);
			if(count($customer_detail) > 0){
				$customer_id = $customer_detail->id;
			}
		}

        if(Sentinel::check()){
			$payment = $this->customer->getPaymentDetails($id, $customer_id);
		}else{
			$payment = [];
		}

        return view("FrontEnd::item")->with([
            'item'			=> $item[0],
            'relatedItems'	=> $relatedItems,
            'payment'		=> $payment,
            'amount'		=> $amount
        ]);
    }

	/**
     * This function is used to display auctionItemBid
     * @param
     * @response
     */
	public function auctionItemBid($id, $item_id){
		$user 		  = Sentinel::getUser();
		$item 		  = $this->product->getItemDetails($item_id);
		$buttonValues = $this->defaultData->getValues($id);
		$customer_id  = null;
		$amount 	  = 0;
		$request      = (object)[];

		//Get Customer id
		$customer_detail = $this->customer->getCustomer($user->id);
		if($customer_detail){
			$customer_id = $customer_detail->id;
		}

		//User validation for issued card & user levels rules when fail
		$request->live_auction_id = $id;
		$request->user_type       = ONLINE_CARD;
		$customer_card 			  = $this->customer->checkOnlineCardExceed($request);
		if($customer_card == CARD_LIMIT_EXCEED){
			return redirect()->route('front.auctions', [$id]);
		}else if($customer_card == NO_CARD_RANGE){
			return redirect()->route('front.auctions', [$id]);
		}else{
			$request->customer_id = $customer_id;
			$customer_level = $this->auction->getAllowedAuction($request);
			if($customer_level == 0){
				return redirect()->route('front.auctions', [$id]);
			}
		}

		//check customer has pending payments
		//check customer is block or unblock
		$isBlockCustomer = $this->customer->loadCustomer($customer_detail->email);
		if($isBlockCustomer && $isBlockCustomer->band_status != BAND){
			if(isset($item[0]->payment) && sizeof($item[0]->payment) > 0){
				foreach ($item[0]->payment as $key => $payment) {
					$amount = ($payment->amount + $amount);
				}
			}

			if(isset($item[0]) && sizeof($item[0]) > 0 &&
			   isset($item[0]->auction_detail) && sizeof($item[0]->auction_detail) > 0){

				$validate   = $this->customer->getPaymentDetailsToValidate($id, $customer_id);

				$bid_count  = $this->auction_live->bidCount($item[0]->auction_detail->id);

				if(sizeof($validate) > 0){

					$detail = $this->auction_live->getItemBids($item_id, $id);
					if(sizeof($detail) > 0){
						return view("FrontEnd::item-bid")->with([
							'item'			 => $item[0],
							'auction_id' 	 => $id,
							'detail_id'		 => $detail[0]->id,
							'bids'			 => $detail[0]->bids,
							'start_price'	 => $detail[0]->start_bid_price,
							'amount'		 => $amount,
							'count'          => $bid_count,
							'customer_detail'=> $customer_detail,
							'logged_user'	 => $user,
							'buttonValues'	 => $buttonValues
						]);
					}else{
						return redirect()->route('front.auctions', [$id]);
					}
				}else{
					return redirect()->route('front.auctions', [$id]);
				}
			}else{
				return redirect()->route('front.auctions', [$id]);
			}
		}else{
			//$blockCustomer = $this->blockUser($customer_id);
			$blockCustomer = $this->customer->bandCustomer($customer_id, BAND);
			if($blockCustomer){
				Sentinel::logout();
				return redirect()->route('front.login')->withErrors(array('login' => ACCOUNT_SUSPENDED_MESSAGE));
			}
		}
	}

	/**
     * This function is used to display categoryItemBid
     * @param
     * @response
     */
	public function categoryItemBid($id, $item_id){

		$item = $this->product->getItemDetails($item_id);

		$amount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$amount = $payment->amount + $amount;
			}
		}

		$auction_id = $item[0]->auction_detail->sns_auction_id;

		$validate = $this->customer->getPaymentDetailsToValidate($auction_id);

		$bid_count = $this->auction_live->bidCount($item[0]->auction_detail->id);

		if(count($validate) > 0){
			$detail = $this->auction_live->getItemBids($item_id,$auction_id);

			return view("FrontEnd::item-bid")->with([
				'item'			=> $item[0],
				'category_id' 	=> $id,
				'detail_id'		=> $detail[0]->id,
				'bids'			=> $detail[0]->bids,
				'start_price'	=> $detail[0]->start_bid_price,
				'amount'		=> $amount,
				'count'         => $bid_count
			]);
		}
		else{
			return response()->view('errors.404');
		}
	}

	/**
     * This function is used to display myAccountItemBid
     * @param
     * @response
     */
	public function myAccountItemBid($item_id){

		$item = $this->product->getItemDetails($item_id);

		$amount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$amount = $payment->amount + $amount;
			}
		}

		$auction_id = $item[0]->auction_detail->sns_auction_id;

		$validate = $this->customer->getPaymentDetailsToValidate($auction_id);

		$bid_count = $this->auction_live->bidCount($item[0]->auction_detail->id);

		if(count($validate) > 0){
			$detail = $this->auction_live->getItemBids($item_id,$auction_id);
			
			return view("FrontEnd::item-bid")->with([
				'item'			=> $item[0],
				'detail_id'		=> $detail[0]->id,
				'bids'			=> $detail[0]->bids,
				'start_price'	=> $detail[0]->start_bid_price,
				'amount'		=> $amount,
				'count'         => $bid_count
			]);
		}else{
			return response()->view('errors.404');
		}
	}	
	

	public function auctionRegistrationPayment($item, $customer_id){

		$payment_list = $this->payment->getPayment($item[0]->auction_detail->auction->id, $customer_id);

		if(sizeof($payment_list) > 0){
			$payment = $payment_list;
		}else{
			$payment = $this->payment->add([
				'sns_auction_id' => $item[0]->auction_detail->auction->id,
				'amount'  		 => $item[0]->auction_detail->auction->registration_fee,
				'sns_user_id'    => $customer_id,
				'payment_method' => CARD_PAYMENT
			]);
		}

        $reference_no = str_pad($payment->id, 6, "0", STR_PAD_LEFT);

        $payment->reference_no = $reference_no;
        $payment->save();

		date_default_timezone_set('Asia/Colombo');
		error_reporting(E_ALL);
		ini_set('display_errors', 0);
		/*------------------------------------------------------------------------------
		STEP1: Build ClientConfig object
		------------------------------------------------------------------------------*/
		$clientConfig = new ClientConfig();
		$clientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
		$clientConfig->setAuthToken("af791e78-469e-4f73-bd94-b8dc3348a772");
		$clientConfig->setHmacSecret("IHqpmhM0S0KHMKoE");
		/*------------------------------------------------------------------------------
		STEP2: Build Client object
		------------------------------------------------------------------------------*/
		$client = new GatewayClient($clientConfig);
		/*------------------------------------------------------------------------------
		STEP3: Build PaymentInitRequest object
		------------------------------------------------------------------------------*/
		$initRequest = new PaymentInitRequest();
		$initRequest->setClientId(14000413);
		$initRequest->setTransactionType(TransactionType::$PURCHASE);
		$initRequest->setClientRef($reference_no); //reference no
		$initRequest->setComment("merchant_additional_data"); //reference data
		$initRequest->setExtraData(array("payment_id" => $payment->id, "amount" => $payment->amount)); //additional data
		// sets transaction-amounts details (all amounts are in cents)
		$transactionAmount = new TransactionAmount();
		$transactionAmount->setTotalAmount(0);
		$transactionAmount->setServiceFeeAmount(0);
		$transactionAmount->setPaymentAmount($item[0]->auction_detail->auction->registration_fee * 100);
		$transactionAmount->setCurrency("LKR");
		$initRequest->setTransactionAmount($transactionAmount);
		// sets redirect settings
		$redirect = new Redirect();
		$redirect->setReturnUrl(url('auctions/'.$item[0]->auction_detail->auction->id.'/item/'.$item[0]->id.'/payment-complete'));
		$redirect->setReturnMethod("CLOSEGET");
		$initRequest->setRedirect($redirect);

		/*------------------------------------------------------------------------------
		STEP4: Process PaymentInitRequest object
		------------------------------------------------------------------------------*/
		$initResponse = $client->payment()->init($initRequest);

		return $initResponse;
	}

	/**
     * This function is used to display auctionItemPayment
     * @response
	 * @param $id as auction_id
	 * @param $item_id as item id
     */

	public function auctionPayment($id = null, $item_id = null){

		$card                  = 0;
		$customer              = 0;
		$user                  = Sentinel::getUser();
		$item                  = $this->product->getItemDetails($item_id);
		$range_id              = 0;
		$customer_access_level = null;
		$registration_fee      = 0;

		//get customer detail
		$customer_detail = $this->customer->getCustomer($user->id);
		if($customer_detail){
			$customer = $customer_detail->id;
			if($customer_detail->allowed_customer_type && $customer_detail->allowed_customer_type->customer_types){
				$customer_access_level = $customer_detail->allowed_customer_type->customer_types;
			}
		}
		/** check customer level with redirect to payment gateway
		*   Customer Type New | Approved | Gold - should pay the registration fee if available
		*/
		if($customer_access_level != null && $customer_access_level->registration_fee > 0){
			if(sizeof($item[0]->auction_detail) > 0 && sizeof($item[0]->auction_detail->auction) > 0){
				$registration_fee = $item[0]->auction_detail->auction->registration_fee;
			}
		}
		//get last card no of auction
		$last_customer_card = $this->customer->getLastRegisterDetails($item[0]->auction_detail->auction->id, ONLINE_CARD);
		if(sizeof($last_customer_card) > 0){
			$card 	  = $last_customer_card['card_no'];
			$range_id = $last_customer_card['range_id'];
		}
		//$card 	  += 1;
		#If Registration Fee have for this auction.
		if(sizeof($item[0]->auction_detail) > 0 && sizeof($item[0]->auction_detail->auction) > 0 && $registration_fee > 0 && $customer_access_level->registration_fee > 0){

			$initResponse = $this->auctionRegistrationPayment($item, $customer);

			return view("FrontEnd::payment")->with([
				'id'			=> $item_id,
				'auction_id' 	=> $id,
				'item'			=> $item[0],
				'initResponse'  => $initResponse
			]);
		}else{
			if(isset($item) && isset($item[0]) && sizeof($item[0]) > 0 && sizeof($item[0]->auction_detail) > 0 && sizeof($item[0]->auction_detail->auction) > 0){
				//add payment
				$payment = $this->payment->add([
					'sns_auction_id'    => $item[0]->auction_detail->auction->id,
					'amount'  			=> $item[0]->auction_detail->auction->registration_fee,
					'sns_user_id'    	=> $customer,
					'payment_date'		=> date('Y-m-d H:i:s')
				]);
				//add payment transaction
				$payment_transaction = $this->payment->update($payment->id,[
					'payment_status'	=> PAYMENT_SUCCESS,
					'reference_no'		=> '0000'.$payment->id	
				],[
					'sns_payment_id'	=> $payment->id,
					'transaction_id'	=> TRANSACTION_ID,
					'transaction_time'	=> date('Y-m-d H:i:s'),
					'paid_amount'		=> $item[0]->auction_detail->auction->registration_fee,
					'error_code'		=> TRANSACTION_ERROR_CODE,
					'error_message'		=> TRANSACTION_ERROR_MESSAGE,
					'status'			=> TRANSACTION_STATUS
				]);

				//generate reference no
				$reference_no = $this->generateReferenceNo($customer, $item[0]->auction_detail->auction->id, $card);
				//register user with auction
				$customerAuction = $this->customer->addCustomerAuction($item[0]->auction_detail->auction->id, $card, $customer, [
					'user_id'		=> $customer,
					'auction_id'	=> $item[0]->auction_detail->auction->id,
					'card_no'		=> $card,
					'card_range_id' => $range_id,
					'invoice_no'    => $reference_no
				]);

				//get auction only
				$auction = $this->auction->getAuctionOnly($id);

				//check auction start or not
				if(date("Y-m-d H:i") >= date("Y-m-d H:i", strtotime($auction->auction_date.' '.$auction->auction_start_time)) && $auction->auction_status == AUCTION_LIVE && $item[0]->status == PRODUCT_TO_LIVE){
					return redirect()->route('front.item-bid',[
						'auctions' => $id, 
						'item' => $item_id, 
						'item-bid' => $item_id
					]);
				}else{
					return back()->with([ 
						'success' => true,
						'success.message' => 'Registered Successfully!',
						'success.title'   => 'Success..!'
					]);
				}
			}else{
				return response()->view('errors.404');
			}
		}
	}

	/**
     * This function is used to display categoryItemPayment
     * @response
     */
	public function categoryPayment($id, $item_id){
		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);
		if($item[0]->auction_detail->auction->registration_fee > 0){
			$initResponse = $this->auctionRegistrationPayment($item, $user);

			return view("FrontEnd::payment")->with([
				'id'			=> $item_id,
				'category_id' 	=> $id,
				'item'			=> $item[0],
				'initResponse'  => $initResponse
			]);
		}else{
			//add payment
			$payment = $this->payment->add([
				'sns_auction_id'    => $item[0]->auction_detail->auction->id,
				'amount'  			=> $item[0]->auction_detail->auction->registration_fee,
				'sns_user_id'    	=> $user->id
			]);
			//add payment transaction
			$payment_trancaction = $this->payment->update($payment,[
	    		'payment_status'	=> 1
	    	],[
	    		'sns_payment_id'	=> $payment,
	    		'transaction_id'	=> '00000',
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> '0.00',
	    		'error_code'		=> '00',
	    		'error_message'		=> 'TRANSACTION APPROVED',
	    		'status'			=> 1
	    	]);
	    	//register user with auction
			$customerAuction = $this->customer->addCustomerAuction([
					'user_id'		=> $user->id,
					'auction_id'	=> $item[0]->auction_detail->auction->id
				]);
	    	return back()->with([ 
                'success' => true,
                'success.message' => 'Registered Successfully!',
                'success.title'   => 'Success..!'
            ]);
		}
	}

	/**
     * This function is used to display myAccountItemPayment
     * @response
     */
	public function myAccountPayment($item_id){
		
		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);

		if($item[0]->auction_detail->auction->registration_fee > 0){
			$initResponse = $this->auctionRegistrationPayment($item, $user);
			return view("FrontEnd::payment")->with([
				'id'			=> $item_id,
				'item'			=> $item[0],
				'initResponse'  => $initResponse
			]);
		}else{
			//add payment
			$payment = $this->payment->add([
				'sns_auction_id'    => $item[0]->auction_detail->auction->id,
				'amount'  			=> $item[0]->auction_detail->auction->registration_fee,
				'sns_user_id'    	=> $user->id
			]);
			//add payment transaction
			$payment_trancaction = $this->payment->update($payment,[
	    		'payment_status'	=> 1
	    	],[
	    		'sns_payment_id'	=> $payment,
	    		'transaction_id'	=> '00000',
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> '0.00',
	    		'error_code'		=> '00',
	    		'error_message'		=> 'TRANSACTION APPROVED',
	    		'status'			=> 1
	    	]);
	    	//register user with auction
			$customerAuction = $this->customer->addCustomerAuction([
					'user_id'		=> $user->id,
					'auction_id'	=> $item[0]->auction_detail->auction->id
				]);
	    	return back()->with('message','Successfully Registered !');
		}
	}

	public function auctionRegistrationPaymentComplete(){

		date_default_timezone_set('Asia/Colombo');
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		/*------------------------------------------------------------------------------
		STEP1: Build ClientConfig object
		------------------------------------------------------------------------------*/
		$clientConfig = new ClientConfig();
		$clientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
		$clientConfig->setAuthToken("af791e78-469e-4f73-bd94-b8dc3348a772");
		$clientConfig->setHmacSecret("IHqpmhM0S0KHMKoE");
		/*------------------------------------------------------------------------------
		STEP2: Build Client object
		------------------------------------------------------------------------------*/
		$client = new GatewayClient($clientConfig);
		/*------------------------------------------------------------------------------
		STEP3: Build PaymentCompleteRequest object
		------------------------------------------------------------------------------*/
		$completeRequest = new PaymentCompleteRequest();
		$completeRequest->setClientId(14000117);
		$completeRequest->setReqid($_GET['reqid']);
		/*------------------------------------------------------------------------------
		STEP4: Process PaymentCompleteRequest object
		------------------------------------------------------------------------------*/
		return $completeResponse = $client->payment()->complete($completeRequest);
	}

	public function auctionPaymentComplete(Request $request, $id, $item_id){

		$last_online_card_no    = null;
		$response         		= [];
		$user 			  		= Sentinel::getUser();
		$item             		= $this->product->getItemDetails($item_id);
		$completeResponse 		= $this->auctionRegistrationPaymentComplete();
		$data 			  		= $completeResponse->getExtraData();
		$customer_id			= 0;
		
		if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
			$payment_status = 1;
			//Get Last Online Registered Card No 
			//Online card range id
			$online_card_range = $this->auction->getLastRegisterCard($id, ONLINE_CARD);
			//Assign last online card no
			if(sizeof($online_card_range) > 0 && sizeof($online_card_range->last_customer_card) > 0){
				$last_online_card_no = $online_card_range->last_customer_card->card_no;
				$last_online_card_no = $last_online_card_no + 1;
			}else{
				$last_online_card_no =  $online_card_range->card_range_from;
			} 
			//Get customer id
			if(sizeof($user) > 0){
				$customer_detail = $this->customer->getCustomer($user->id);
				if($customer_detail){
					$customer_id = $customer_detail->id;
				}
			}
			//generate reference no
			$reference_no = $this->generateReferenceNo($customer_id, $id, $last_online_card_no);

			$customer_auction = [
				'user_id' 	  	=> $customer_id,
				'auction_id'  	=> $id,
				'card_range_id' => $online_card_range->id,
				'card_no'	  	=> $last_online_card_no,
				'invoice_no'    => $reference_no
			];

			//add customer to auction
			$customerAuction = $this->customer->addCustomerAuction($id, $last_online_card_no, $customer_id, $customer_auction);
			
		}else{
			$payment_status = -1;
		}

		$payment = $this->payment->update($data[0]['payment_id'],[
			'payment_status'	=> $payment_status
		],[
			'sns_payment_id'	=> $data[0]['payment_id'],
			'transaction_id'	=> $completeResponse->getTxnReference(),
			'transaction_time'	=> date('Y-m-d H:i:s'),
			'paid_amount'		=> $data[0]['amount'],
			'error_code'		=> $completeResponse->getResponseCode(),
			'error_message'		=> $completeResponse->getResponseText(),
			'status'			=> $payment_status
		]);
		
		$response['responseCode'] 	= $completeResponse->getResponseCode();
		$response['txnReference'] 	= $completeResponse->getTxnReference();
		$response['clientRef']		= $completeResponse->getClientRef();
		$response['txnTime']		= date('Y-m-d H:i:s');
		$response['paymentId']		= $data[0]['payment_id'];
		$response['responseText']	= $completeResponse->getResponseText();
			
		return view("FrontEnd::payment-complete")->with([
			'id'			=> $item_id,
			'auction_id' 	=> $id,
			'item'			=> $item[0],
			'response'  	=> $response,
			'payment'  		=> $payment
		]);
	}

	public function categoryPaymentComplete(Request $request, $id, $item_id){
		$response = [];
		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);
		$completeResponse = $this->auctionRegistrationPaymentComplete();

		$data = $completeResponse->getExtraData();

		if(is_null($data[0])){
			$payment = $this->payment->getPaymentByRef($request->clientRef);

			$response['responseCode'] 	= $payment->payment_transaction->error_code;
			$response['txnReference'] 	= $payment->payment_transaction->transaction_id;
			$response['clientRef']		= $payment->reference_no;
			$response['txnTime']		= $payment->payment_transaction->transaction_time;
			$response['paymentId']		= $payment->id;
		}
		else{
			if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
				$payment_status = 1;
				$customerAuction = $this->customer->addCustomerAuction([
					'user_id'		=> $user->id,
					'auction_id'	=> $item[0]->auction_detail->auction->id
				]);
			}
			else{
				$payment_status = -1;
			}

			$payment = $this->payment->update($data[0]['payment_id'],[
	    		'payment_status'	=> $payment_status
	    	],[
	    		'sns_payment_id'	=> $data[0]['payment_id'],
	    		'transaction_id'	=> $completeResponse->getTxnReference(),
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> $data[0]['amount'],
	    		'error_code'		=> $completeResponse->getResponseCode(),
	    		'error_message'		=> $completeResponse->getResponseText(),
	    		'status'			=> $payment_status
	    	]);

	    	$response['responseCode'] 	= $completeResponse->getResponseCode();
			$response['txnReference'] 	= $completeResponse->getTxnReference();
			$response['clientRef']		= $completeResponse->getClientRef();
			$response['txnTime']		= date('Y-m-d H:i:s');
			$response['paymentId']		= $data[0]['payment_id'];
		}

		return view("FrontEnd::payment-complete")->with([
			'id'			=> $item_id,
			'category_id' 	=> $id,
			'item'			=> $item[0],
			'response'  	=> $response
		]);
	}

	public function myAccountPaymentComplete(Request $request, $item_id){
		$response = [];
		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);
		$completeResponse = $this->auctionRegistrationPaymentComplete();

		$data = $completeResponse->getExtraData();

		if(is_null($data[0])){
			$payment = $this->payment->getPaymentByRef($request->clientRef);

			$response['responseCode'] 	= $payment->payment_transaction->error_code;
			$response['txnReference'] 	= $payment->payment_transaction->transaction_id;
			$response['clientRef']		= $payment->reference_no;
			$response['txnTime']		= $payment->payment_transaction->transaction_time;
			$response['paymentId']		= $payment->id;
		}
		else{
			if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
				$payment_status = 1;
				$customerAuction = $this->customer->addCustomerAuction($user->id, $item[0]->auction_detail->auction->id, '', []);
			}
			else{
				$payment_status = -1;
			}

			$payment = $this->payment->update($data[0]['payment_id'],[
	    		'payment_status'	=> $payment_status
	    	],[
	    		'sns_payment_id'	=> $data[0]['payment_id'],
	    		'transaction_id'	=> $completeResponse->getTxnReference(),
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> $data[0]['amount'],
	    		'error_code'		=> $completeResponse->getResponseCode(),
	    		'error_message'		=> $completeResponse->getResponseText(),
	    		'status'			=> $payment_status
	    	]);

	    	$response['responseCode'] 	= $completeResponse->getResponseCode();
			$response['txnReference'] 	= $completeResponse->getTxnReference();
			$response['clientRef']		= $completeResponse->getClientRef();
			$response['txnTime']		= date('Y-m-d H:i:s');
			$response['paymentId']		= $data[0]['payment_id'];
		}

		return view("FrontEnd::payment-complete")->with([
			'id'		=> $item_id,
			'item'		=> $item[0],
			'response'  => $response,
			'payment'  	=> $payment
		]);
	}

	/**
     * This function is used to display auctionItemPayment
     * @response
     */
	public function auctionItemPayment($id, $item_id){
		
		$customer  	= 0;
		$user 	  	= Sentinel::getUser();
		$paidAmount = 0;
		//GET CUSTOMER DETAILS
		$customer_detail = $this->customer->getCustomer($user->id);
		if($customer_detail){
			$customer = $customer_detail->id;
		}

		$item = $this->product->wonItemDetails($item_id, $id, $customer);
		
		if($item && sizeof($item) > 0 && $item[0]->auc_detail && $item[0]->won_item_detail->highest_bid && $item[0]->won_item_detail->highest_bid->win_bid){

			if(count($item[0]->payment) > 0){
				foreach ($item[0]->payment as $key => $payment) {
					$paidAmount = $payment->amount + $paidAmount;
				}
			}

			//GET CUSTOMER POCKET BALANCE
			$pocket_balance = $this->customer->customerPocketBalance($customer);

			//return $item[0];
			return view("FrontEnd::payment-item")->with([
				'id'			=> $item_id,
				'auction_id' 	=> $id,
				'item'			=> $item[0],
				'winner'		=> $item[0]->won_item_detail->highest_bid->win_bid,
				'paidAmount'	=> $paidAmount,
				'pocket_balance'=> $pocket_balance
			]);

		}else{

			return response()->view('errors.404');
		}
	}

	/**
     * This function is used to display categoryItemPayment
     * @response
     */
	public function categoryItemPayment($id, $item_id){
		$item = $this->product->getItemDetails($item_id);

		$paidAmount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paidAmount = $payment->amount + $paidAmount;
			}
		}

		return view("FrontEnd::payment-item")->with([
			'id'			=> $item_id,
			'category_id' 	=> $id,
			'item'			=> $item[0],
			'winner'		=> $item[0]->auction_detail->highest_bid->win_bid,
			'paidAmount'	=> $paidAmount
		]);
	}

	/**
     * This function is used to display myAccountItemPayment
     * @response
     */
	public function myAccountItemPayment($item_id){
		$item = $this->product->getItemDetails($item_id);

		$paidAmount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paidAmount = $payment->amount + $paidAmount;
			}
		}

		return view("FrontEnd::payment-item")->with([
			'id'			=> $item_id,
			'item'			=> $item[0],
			'winner'		=> $item[0]->auction_detail->highest_bid->win_bid,
			'paidAmount'	=> $paidAmount
		]);
	}

	/**
     * This function is used to save auctionItemPaymentDetails
     * @response
     */
	public function auctionItemPaymentDetails(Request $request, $id, $item_id){

		$user 	  	= Sentinel::getUser();
		$customer 	= 0;
		//GET CUSTOMER DETAILS
		$customer_detail = $this->customer->getCustomer($user->id);
		if(count($customer_detail) > 0){
			$customer = $customer_detail->id;
		}

		if($request->get('band_status') == 0){	

			$detail = $this->auction_live->updateAuctionWinner($request->cid, [
				'winner_name' 		=> $request->winner_name,
				'winner_contact_no' => $request->winner_contact_no,
				'winner_email' 		=> $request->winner_email,
				'winner_address' 	=> $request->winner_address,
				'payment_type' 		=> $request->type
			]); 

			if($request->payment_method == POCKET_BALANCE){

				//ADD NEW PAYMENT
				$payment  = $this->payment->add([
					'sns_auction_id'    => $id,
					'amount'  			=> $request->amount,
					'sns_item_id'    	=> $item_id,
					'sns_user_id'    	=> $customer,
					'payment_method'	=> POCKET_BALANCE
				]);

				//ADD PAYMENT TRANSACTION
				$payment_trancaction = $this->payment->update($payment->id,[
		    		'payment_status'	=> PAYMENT_SUCCESS,
		    		'reference_no'		=> '0000'.$payment->id	
		    	],[
		    		'sns_payment_id'	=> $payment->id,
		    		'transaction_id'	=> TRANSACTION_ID.date('Ymdhis'),
		    		'transaction_time'	=> date('Y-m-d H:i:s'),
		    		'paid_amount'		=> $request->amount,
		    		'error_code'		=> TRANSACTION_ERROR_CODE,
		    		'error_message'		=> TRANSACTION_ERROR_MESSAGE,
		    		'status'			=> TRANSACTION_STATUS
				]);
				
				//find auction item details
				$auction_item_detail = $this->auction->findAuctionItemDetails($item_id);
				if($auction_item_detail){
					//update approved item as paid
					$update_sold_status = $this->auction_live->updateSoldStatus($id, $customer, $auction_item_detail->id, FULL_PAID);
				}else{
					throw new Exception('Cannot find auction item');
				}		

				//POCKET BALANCE DEDUCTION
				$customer_detail = $this->customer->pocketBalanceDeduction($customer, $request->amount);

				return redirect('auctions/'.$id.'/item/'.$item_id.'/item-payment')->with([
	                'success' 		  => true,
	                'success.message' => $request->amount.' LKR paid for the item.Thank you for your payment.' ,
	                'success.title'   => 'Payment Success..!'
				]);
				
			}else{
				return redirect('auctions/'.$id.'/item/'.$item_id.'/item-payment/gateway');
			}
		}else{
			//customer suspended & sign out.
			$customer_detail = $this->customer->bandCustomer($customer, 1);
			Sentinel::logout();
			return redirect()->route('front.login')->withErrors(array('login' => ACCOUNT_SUSPENDED_MESSAGE));
		}

	}


	public function blockCustomer(){
		//block customer
		$user 	  	     = Sentinel::getUser();
		$customer 		 = 0;

		$customer_detail = $this->customer->getCustomer($user->id);
		if(count($customer_detail) > 0){
			$customer = $customer_detail->id;
		}

		$band_customer = $this->customer->bandCustomer($customer, 1);
		Sentinel::logout();
		$msg = 'Sorry,Your live auction account has been suspended.';
		return redirect()->route('front.login')->withErrors(array('login' => $msg));
	}

	/**
     * This function is used to save categoryItemPaymentDetails
     * @response
     */
	public function categoryItemPaymentDetails(Request $request, $id, $item_id){
		$detail = $this->auction_live->updateAuctionWinner($request->cid, [
			'winner_name' 		=> $request->winner_name,
			'winner_contact_no' => $request->winner_contact_no,
			'winner_email' 		=> $request->winner_email,
			'winner_address' 	=> $request->winner_address,
			'payment_type' 		=> $request->type
		]);

		return redirect('auction-categories/'.$id.'/item/'.$item_id.'/item-payment/gateway');
	}

	/**
     * This function is used to save myAccountItemPaymentDetails
     * @response
     */
	public function myAccountItemPaymentDetails(Request $request, $item_id){
		$detail = $this->auction_live->updateAuctionWinner($request->cid, [
			'winner_name' 		=> $request->winner_name,
			'winner_contact_no' => $request->winner_contact_no,
			'winner_email' 		=> $request->winner_email,
			'winner_address' 	=> $request->winner_address,
			'payment_type' 		=> $request->type
		]);

		return redirect('my-account/item/'.$item_id.'/item-payment/gateway');
	}

	/**
     * This function is used get initResponse
     * @response
     */
	public function itemRegistrationPayment($item, $user, $paid_amount){

		$newAmount = 0;
		if($paid_amount > 0){
			$newAmount = $paid_amount;
	    }else{
	    	if($item[0]->auc_detail && $item[0]->auc_detail->highest_bid && $item[0]->auc_detail->highest_bid->win_bid){

	    		$amount = $item[0]->auc_detail->highest_bid->win_bid->win_amount?:'0';

		        if($amount <= 10000){
		            $full =  $amount;
		            $advance = '';
		        }
		        elseif ($amount > 10000 && $amount <= 100000) {
		            $full =  $amount;
		            $advance =  (50 / 100) * $amount;
		        }
		        else{
		            $full =  $amount;
		            $advance =  (25 / 100) * $amount;
		        }

		        $newAmount = ($item[0]->auc_detail->highest_bid->win_bid->payment_type == 1)?$advance:$full;
		    }   
	        
	    }

		if($payment_list = $this->payment->getPayment($item[0]->auc_detail->auction->id, $user, $newAmount, $item[0]->id, $item[0]->auc_detail->highest_bid->win_bid->payment_type)){

			$payment = $payment_list;

		}else{

			$payment = $this->payment->add([
				'sns_auction_id'    => $item[0]->auc_detail->auction->id,
				'amount'  			=> $newAmount,
				'sns_item_id'    	=> $item[0]->id,
				'sns_user_id'    	=> $user,
				'payment_method'	=> CARD_PAYMENT
			]);
		}

        $reference_no = str_pad($payment->id, 6, "0", STR_PAD_LEFT);

        $payment->reference_no = $reference_no;
        $payment->save();

		date_default_timezone_set('Asia/Colombo');
		error_reporting(E_ALL);
		ini_set('display_errors', 0);
		/*------------------------------------------------------------------------------
		STEP1: Build ClientConfig object
		------------------------------------------------------------------------------*/
		$clientConfig = new ClientConfig();
		$clientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
		$clientConfig->setAuthToken("af791e78-469e-4f73-bd94-b8dc3348a772");
		$clientConfig->setHmacSecret("IHqpmhM0S0KHMKoE");
		/*------------------------------------------------------------------------------
		STEP2: Build Client object
		------------------------------------------------------------------------------*/
		$client = new GatewayClient($clientConfig);
		/*------------------------------------------------------------------------------
		STEP3: Build PaymentInitRequest object
		------------------------------------------------------------------------------*/
		$initRequest = new PaymentInitRequest();
		$initRequest->setClientId(14000413);
		$initRequest->setTransactionType(TransactionType::$PURCHASE);
		$initRequest->setClientRef($reference_no); //reference no
		$initRequest->setComment("merchant_additional_data"); //reference data
		$initRequest->setExtraData(array("payment_id" => $payment->id, "amount" => $payment->amount)); //additional data
		// sets transaction-amounts details (all amounts are in cents)
		$transactionAmount = new TransactionAmount();
		$transactionAmount->setTotalAmount(0);
		$transactionAmount->setServiceFeeAmount(0);
		$transactionAmount->setPaymentAmount($payment->amount * 100);
		$transactionAmount->setCurrency("LKR");
		$initRequest->setTransactionAmount($transactionAmount);
		// sets redirect settings
		$redirect = new Redirect();
		$redirect->setReturnUrl(url('auctions/'.$item[0]->auc_detail->auction->id.'/item/'.$item[0]->id.'/gateway-complete'));
		$redirect->setReturnMethod("CLOSEGET");
		$initRequest->setRedirect($redirect);

		/*------------------------------------------------------------------------------
		STEP4: Process PaymentInitRequest object
		------------------------------------------------------------------------------*/
		$initResponse = $client->payment()->init($initRequest);
		return $initResponse;
	}

	/**
     * This function is used to auctionItemGateway
     * @response
     */
	public function auctionItemGateway(Request $request, $id, $item_id){

		//check user band or not
		$user 	  		 = Sentinel::getUser();
		$customer 		 = 0;
		$customer_detail = $this->customer->getCustomer($user->id);
		$paidAmount      = 0;

		if(count($customer_detail) > 0){
			$customer = $customer_detail->id;
		}

		$item = $this->product->getAucDetails($item_id, $id, $customer);
		
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paidAmount = $payment->amount + $paidAmount;
			}
		}

		if($item[0]->auc_detail && $item[0]->auc_detail->highest_bid && $item[0]->auc_detail->highest_bid->win_bid){

			$initResponse = $this->itemRegistrationPayment($item, $customer, $paidAmount);

			return view("FrontEnd::item-gateway")->with([
				'id'			=> $item_id,
				'auction_id' 	=> $id,
				'item'			=> $item[0],
				'initResponse'  => $initResponse
			]);
		}else{
			return response()->view('errors.404');
		}
	}

	/**
     * This function is used to categoryItemGateway
     * @response
     */
	public function categoryItemGateway($id, $item_id){

		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);
		
		$paidAmount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paidAmount = $payment->amount + $paidAmount;
			}
		}

		$initResponse = $this->itemRegistrationPayment($item, $user, $paidAmount);

		return view("FrontEnd::item-gateway")->with([
			'id'			=> $item_id,
			'category_id' 	=> $id,
			'item'			=> $item[0],
			'initResponse'  => $initResponse
		]);
	}

	/**
     * This function is used to myAccountItemGateway
     * @response
     */
	public function myAccountItemGateway($item_id){
		$user = Sentinel::getUser();
		$item = $this->product->getItemDetails($item_id);
		
		$paidAmount = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paidAmount = $payment->amount + $paidAmount;
			}
		}

		$initResponse = $this->itemRegistrationPayment($item, $user, $paidAmount);

		return view("FrontEnd::item-gateway")->with([
			'id'			=> $item_id,
			'item'			=> $item[0],
			'initResponse'  => $initResponse
		]);
	}

	/**
     * This function is used for auctionItemPaymentComplete
     * @response
     */
	public function auctionItemPaymentComplete(Request $request, $id, $item_id){
		$response = [];
		$completeResponse = $this->auctionRegistrationPaymentComplete();

		$data = $completeResponse->getExtraData();
		if(is_null($data[0])){
			$payment = $this->payment->getPaymentByRef($request->clientRef);

			$response['responseCode'] 	= $payment->payment_transaction->error_code;
			$response['txnReference'] 	= $payment->payment_transaction->transaction_id;
			$response['clientRef']		= $payment->reference_no;
			$response['txnTime']		= $payment->payment_transaction->transaction_time;
			$response['paymentId']		= $payment->id;
		}
		else{
			if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
				$payment_status = 1;
			}
			else{
				$payment_status = -1;
			}

			$payment = $this->payment->update($data[0]['payment_id'],[
	    		'payment_status'	=> $payment_status
	    	],[
	    		'sns_payment_id'	=> $data[0]['payment_id'],
	    		'transaction_id'	=> $completeResponse->getTxnReference(),
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> $data[0]['amount'],
	    		'error_code'		=> $completeResponse->getResponseCode(),
	    		'error_message'		=> $completeResponse->getResponseText(),
	    		'status'			=> $payment_status
	    	]);

	    	$response['responseCode'] 	= $completeResponse->getResponseCode();
			$response['txnReference'] 	= $completeResponse->getTxnReference();
			$response['clientRef']		= $completeResponse->getClientRef();
			$response['txnTime']		= date('Y-m-d H:i:s');
			$response['paymentId']		= $data[0]['payment_id'];
		}

		$item = $this->product->getAucDetails($item_id, $id);

		$paid = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paid = $payment->amount + $paid;
			}
		}
		if($item && sizeof($item) > 0){
			if(isset($item[0]) && sizeof($item[0]) > 0 &&
			   isset($item[0]->auc_detail) && sizeof($item[0]->auc_detail) > 0 && 
			   isset($item[0]->auc_detail->highest_bid) && sizeof($item[0]->auc_detail->highest_bid) > 0 && isset($item[0]->auc_detail->highest_bid->win_bid) && sizeof($item[0]->auc_detail->highest_bid->win_bid) > 0)
			//Get Paid Customer ID
			$customer_id = $item[0]->auc_detail->highest_bid->win_bid->sns_user_id?:0;

		}else{
			throw new Exception('Cannot find data');
		}
		//find auction item details
		$auction_item_detail = $this->auction->findAuctionItemDetails($item_id);
		if($auction_item_detail){
			//update approved item as paid
			$update_sold_status = $this->auction_live->updateSoldStatus($id, $customer_id, $auction_item_detail->id, FULL_PAID);
		}else{
			throw new Exception('Cannot find auction item');
		}

		return view("FrontEnd::item-gateway-complete")->with([
			'id'			=> $item_id,
			'auction_id' 	=> $id,
			'item'			=> $item[0],
			'response'  	=> $response,
			'payment'  		=> $payment,
			'paid'  		=> $paid,
			'next_id'		=> (($item[0]->next)?$item[0]->next->sns_item_id:'')
		]);
	}

	/**
     * This function is used for categoryItemPaymentComplete
     * @response
     */
	public function categoryItemPaymentComplete(Request $request, $id, $item_id){
		$item = $this->product->getItemDetails($item_id);
		$completeResponse = $this->auctionRegistrationPaymentComplete();

		$data = $completeResponse->getExtraData();

		if(is_null($data[0])){
			$payment = $this->payment->getPaymentByRef($request->clientRef);

			$response['responseCode'] 	= $payment->payment_transaction->error_code;
			$response['txnReference'] 	= $payment->payment_transaction->transaction_id;
			$response['clientRef']		= $payment->reference_no;
			$response['txnTime']		= $payment->payment_transaction->transaction_time;
			$response['paymentId']		= $payment->id;
		}
		else{
			if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
				$payment_status = 1;
			}
			else{
				$payment_status = -1;
			}

			$payment = $this->payment->update($data[0]['payment_id'],[
	    		'payment_status'	=> $payment_status
	    	],[
	    		'sns_payment_id'	=> $data[0]['payment_id'],
	    		'transaction_id'	=> $completeResponse->getTxnReference(),
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> $data[0]['amount'],
	    		'error_code'		=> $completeResponse->getResponseCode(),
	    		'error_message'		=> $completeResponse->getResponseText(),
	    		'status'			=> $payment_status
	    	]);

	    	$response['responseCode'] 	= $completeResponse->getResponseCode();
			$response['txnReference'] 	= $completeResponse->getTxnReference();
			$response['clientRef']		= $completeResponse->getClientRef();
			$response['txnTime']		= date('Y-m-d H:i:s');
			$response['paymentId']		= $data[0]['payment_id'];
		}

		return view("FrontEnd::item-gateway-complete")->with([
			'id'			=> $item_id,
			'category_id' 	=> $id,
			'item'			=> $item[0],
			'response'  	=> $response,
			'payment'  		=> $payment,
			'next_id'		=> (($item[0]->next)?$item[0]->next->sns_item_id:'')
		]);
	}

	/**
     * This function is used for myAccountItemPaymentComplete
     * @response
     */
	public function myAccountItemPaymentComplete(Request $request, $item_id){
		$item = $this->product->getItemDetails($item_id);
		$completeResponse = $this->auctionRegistrationPaymentComplete();

		$data = $completeResponse->getExtraData();

		if(is_null($data[0])){
			$payment = $this->payment->getPaymentByRef($request->clientRef);

			$response['responseCode'] 	= $payment->payment_transaction->error_code;
			$response['txnReference'] 	= $payment->payment_transaction->transaction_id;
			$response['clientRef']		= $payment->reference_no;
			$response['txnTime']		= $payment->payment_transaction->transaction_time;
			$response['paymentId']		= $payment->id;
		}
		else{
			if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
				$payment_status = 1;
			}
			else{
				$payment_status = -1;
			}

			$payment = $this->payment->update($data[0]['payment_id'],[
	    		'payment_status'	=> $payment_status
	    	],[
	    		'sns_payment_id'	=> $data[0]['payment_id'],
	    		'transaction_id'	=> $completeResponse->getTxnReference(),
	    		'transaction_time'	=> date('Y-m-d H:i:s'),
	    		'paid_amount'		=> $data[0]['amount'],
	    		'error_code'		=> $completeResponse->getResponseCode(),
	    		'error_message'		=> $completeResponse->getResponseText(),
	    		'status'			=> $payment_status
	    	]);

	    	$response['responseCode'] 	= $completeResponse->getResponseCode();
			$response['txnReference'] 	= $completeResponse->getTxnReference();
			$response['clientRef']		= $completeResponse->getClientRef();
			$response['txnTime']		= date('Y-m-d H:i:s');
			$response['paymentId']		= $data[0]['payment_id'];
		}

		return view("FrontEnd::item-gateway-complete")->with([
			'id'		=> $item_id,
			'item'		=> $item[0],
			'response'  => $response,
			'payment'  	=> $payment,
			'next_id'	=> (($item[0]->next)?$item[0]->next->sns_item_id:'')
		]);
	}

	/**
     * email customer receipt
     * @param  
     * @return customer mail receipt
     */
    public function auctionReceipt(Request $request){

		$payment = $this->payment->paymentDetails($request->id);

		if($payment){

            $data = [
	            'email' 	=> $payment->customer->user->email,
	            'fname' 	=> $payment->customer->user->first_name,
	            'name' 		=> $payment->customer->user->full_name,
	            'subject' 	=> 'Auction Registration Receipt',
	            'payment' 	=> $payment
	        ];

	        Mail::queue('FrontEnd::template.auction-receipt-email-template', $data, function($message) use ($data) {
	            $message->to($data['email'], $data['name'])->from('test@email.com')->subject($data['subject']);
	        });

            return response()->json(['status' => 'success']);
        }
        else{
            return response()->json(['status' => 'error']);
        }
    }

    /**
     * email item receipt
     * @param  
     * @return customer item receipt
     */
    public function itemReceipt(Request $request){

    	$item = $this->product->getItemDetails($request->id);

    	$highest_bid = $item[0]->auction_detail->highest_bid->bid_amount;
    	$paid = 0;
		if(count($item[0]->payment) > 0){
			foreach ($item[0]->payment as $key => $payment) {
				$paid = $payment->amount + $paid;
			}
		}

		if($highest_bid > $paid){
			$due = ($highest_bid - $paid); 
			return response()->json(['status' => 'warning', 'due' => $due]);
		}
		else{
			$user = Sentinel::getUser();
	    	$customer = $this->customer->getCustomerByUserId($user->id);

			if($item){

	            $data = [
		            'email' 	=> $user->email,
		            'name' 		=> $user->full_name,
		            'subject' 	=> 'Auction Item Receipt',
		            'item' 		=> $item[0],
		            'customer'	=> $customer[0]
		        ];

		        Mail::queue('FrontEnd::template.item-receipt-email-template', $data, function($message) use ($data) {
		            $message->to($data['email'], $data['name'])->cc('nishanran@gmail.com')->from('test@email.com')->subject($data['subject']);
		        });

	            return response()->json(['status' => 'success']);
	        }
	        else{
	            return response()->json(['status' => 'error']);
	        }
		}
    }

	/*public function paymentReceipt()
	{
		return view("FrontEnd::payment-receipt");
	}*/

	public function privacyPolicy(){
		$filename = 'privacy-policy.pdf';
		$path 	  = storage_path($filename);

		return Response::make(file_get_contents($path), 200, [
			'Content-Type' => 'application/pdf',
			'Content-Disposition' => 'inline; filename="'.$filename.'"'
		]);
	}
	
	public function termsConditions(){
		$filename = 'terms-condition.pdf';
		$path 	  = storage_path($filename);

		return Response::make(file_get_contents($path), 200, [
			'Content-Type' => 'application/pdf',
			'Content-Disposition' => 'inline; filename="'.$filename.'"'
		]);
	}

	public function termsConditionsSinhala(){
		$filename = 'terms-condition-sinhala.pdf';
		$path 	  = storage_path($filename);

		return Response::make(file_get_contents($path), 200, [
			'Content-Type' => 'application/pdf',
			'Content-Disposition' => 'inline; filename="'.$filename.'"'
		]);
	}

	public function myAccount(){
        if (Sentinel::check()) {
        	$customer_id = '';
			$user = Sentinel::getUser();
			//get customer id
			$customer_detail = $this->customer->getCustomer($user->id);

			if(count($customer_detail) > 0){
				$customer_id = $customer_detail->id;
			}

            $watch_list 	= $this->front->getWatchList($user->id);
            $bidding_list 	= $this->front->getBiddingList($customer_id);
            $countries 		= $this->front->allCountries();
            $customer 		= $this->customer->getCustomerByUserId($customer_id);
            $won_item_list  = $this->front->getWinList($customer_id);
            $auctions       = $this->customer->getAuctionsByUser($customer_id);
        }

        if(count($customer) > 0){
			return view("FrontEnd::my-account")->with([
				'watch_list' 	=> $watch_list,
				'bid_lists' 	=> $bidding_list,
				'countries'		=> $countries,
				'customer'		=> (count($customer)>0)?$customer:[],
	            'won_item_list' => $won_item_list,
	            'auctions' 		=> $auctions
			]);
		}
		else{
			return redirect('/');
		}
	}
    /**
     * This function is used to display user watchlist
     * @param
     * @response
     */
    public function watchList(){
        if (Sentinel::check()) {
            $user_id = Sentinel::getUser()->id;
            $this->front->getWatchList($user_id);
        }
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function register(){
		$countries = $this->front->allCountries();
		$cities    = $this->front->allCities();
		return view("FrontEnd::register")->with([
			'countries'	=> $countries,
			'cities'    => $cities
		]);
	}

	/**
	 * Get allocated user type for logged user
	 * @param integer user_id
	 * @return 
	 */
	public function getCustomerTypes(Request $request){
		if($request->ajax()){
			$customer = $this->customer->getCustomer($request->get('user_id'));
			return response()->json(['customer' => $customer]);
		}else{
			return response()->json([]);
		}
	}

	/**
	 * Generate reference no
	 */
	public function generateReferenceNo($customer_id, $auction_id, $card_no){
		return $referenceNo = $customer_id.$auction_id.$card_no;
	}

	/**
	 * ORDER CONFIRMATION FORM FOR POCKET BALANCE PAYMENT
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function orderConfirmation($id){
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

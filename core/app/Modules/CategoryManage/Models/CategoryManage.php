<?php namespace App\Modules\CategoryManage\Models;

/**
*
* CategoryManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class CategoryManage extends Model {

    /**
     * The sns_item_category table associated this model.
     */
    protected $table = 'sns_item_category';
    public $timestamps = true;
    protected $guarded = ['id'];

}

<?php namespace App\Modules\CategoryManage\Controllers;

/**
* CategoryManageController class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\CategoryManage\BusinessLogics\Logic;
use Intervention\Image\Facades\Image;

use Illuminate\Http\Request;
use Exception;
use Response;

class CategoryManageController extends Controller {
    protected $category;

    public function __construct(Logic $logic){
        $this->category = $logic;
    }

	/**
	 * This function is used to show add category form
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("CategoryManage::add");
	}


    /**
     * This function used to add category
     */
    public function addCategory(Request $request){
        //validate user input
        $this->validate($request,[
            'code'          => 'required|unique:sns_item_category,code,NULL,id,deleted_at,NULL',
            'category_name' => 'required|unique:sns_item_category,name'
        ]);
        try {
        	
            //check image is browse
            if($request->hasFile('file')){
            	$file     = $request->file('file'); //get file
            	$extn     = $file->getClientOriginalExtension(); //get extension
            	$fileName = 'category-' . $request->category_name.date('Ymdhis').'.' . $extn; //set file name
            	$destinationPath = storage_path('uploads/images/category'); // destination	
            	$file->move($destinationPath, $fileName);//move to file
                //resize image
                $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
                    $c->upsize();
                },"top");
                //save
                $img->save($destinationPath . '/' . $fileName);
            }else{
            	$fileName = '';
            }

            //check category display name empty & if empty then assign name otherwise keep as it is.
        	empty($request->dis_name) ? $display_name = $request->category_name : $display_name = $request->dis_name;
            
            $category = $this->category->addCategory([
                'code'          => $request->code,
                'name'          => $request->category_name,
                'display_name'  => $display_name,
                'description'   => $request->description,
                'image_path'    => $fileName
            ]);

            return redirect( 'category/add' )->with([
                'success' => true,
                'success.message' => 'Category added successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return redirect('category/add')->with([
            	'error' => true,
            	'error.title' => 'Error..!',
            	'error.message' => $e->getMessage()
        	])->withInput();
        }
    }


    /**
     * This function is used to display all categories
     * @return Response
     */
    public function listView(){
        $category_name = $this->category->getCategoryName();
        $all_category = $this->category->getAllCategory();
        return view("CategoryManage::list")->with(['category' => '','all_category' => $all_category]);
    }


    /**
     * This function is used to display category image and other details
     * @return Response
     */
    public function getCategoryDetails(Request $request){
        if($request->ajax()) {
            $category_details = $this->category->getCategoryDetails($request->get('category_id'));
            return Response::json(['details' => $category_details]);
        }else{
            return Response::json([]);
        }
    }


    /**
     * This function is used to search category
     * @return Response
     */

    public function searchCategory(Request $request){
        $search_category = $this->category->searchCategory($request->get('category_name'));
        return view("CategoryManage::list")->with(['category' => $request->get('category_name'),'all_category' => $search_category]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * This function is used to display the edit fileds.
	 *
	 * @param  int  $category_id
	 * @return Response
	 */
	public function edit($category_id){
        $category_details = $this->category->getCategoryDetails($category_id);
        if($category_details) {
            return view("CategoryManage::edit")->with(['category_details' => $category_details]);
        }else{
            return response()->view('errors.404');
        }
    }


    /**
     * This function is used to temporary delete added category images.
     *
     * @return Response
     */

    public function deleteImage(Request $request){
        $this->category->deleteCategoryImage($request->key);
        return $request->key;
    }



	/**
	 * This function is used to update category
	 *
	 * @param  int  $category_id
	 * @return Response
	 */
	public function update(Request $request,$category_id)
	{   
        $category_details = $this->category->getCategoryDetails($category_id);
        
        //validate user input
        $this->validate($request,[
            'code'          => 'required|unique:sns_item_category,code,'.$category_id.',id,deleted_at,NULL',
            'category_name' => 'required|unique:sns_item_category,name,'.$category_id.',id,deleted_at,NULL',
        ]);

        try {
            //check category display name empty & if empty then assign name otherwise keep as it is.
            empty($request->dis_name) ? $display_name = $request->category_name : $display_name = $request->dis_name;
            //get this category records
            $category_details = $this->category->getCategoryDetails($category_id);
            //check image is browse
            if($request->hasFile('file')){
                //remove previous uploaded image file
                if(!empty($category_details[0]->image_path)){
                    $file_path = storage_path('uploads/images/category/'.$category_details[0]->image_path);
                
                    if (!unlink($file_path)) {
                        return redirect('category/add')->with([
                            'error' => true,
                            'error.title' => 'Error..!',
                            'error.message' => 'Error occurred when uploading image !'
                        ])->withInput();
                    }
                }
                //get new image file & upload
                $file = $request->file('file'); //get file
                $extn = $file->getClientOriginalExtension(); //get extension
                $fileName = 'category-' . $request->category_name.date('Ymdhis').'.' . $extn; //set file name
                $destinationPath = storage_path('uploads/images/category'); // destination
                $file->move($destinationPath, $fileName);//move to file
                //resize image
                $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
                    $c->upsize();
                },"top");
                //save
                $img->save($destinationPath . '/' . $fileName);
                return $request->code;
                $category = $this->category->updateCategoy($category_id,[
                    'code'          => $request->code,
                    'name'          => $request->category_name,
                    'display_name'  => $display_name,
                    'description'   => $request->description,
                    'image_path'    => $fileName
                ]);
            }else{

                $category = $this->category->updateCategoy($category_id,[
                    'code'          => $request->code,
                    'name'          => $request->category_name,
                    'display_name'  => $display_name,
                    'description'   => $request->description
                ]);
            }
            if(!empty($request->get('search'))){
                return redirect('category/search?category_name='.$request->get('search'))->with([
                    'success' => true,
                    'success.message' => 'Category Updated Successfully!',
                    'success.title'   => 'Success..!'
                ]); 
            }

            return redirect('category/list')->with([
                'success' => true,
                'success.message' => 'Category Updated Successfully!',
                'success.title'   => 'Success..!'
            ]);
            
        }catch(Exception $e){
            return back()->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

}

<?php

Route::group(['middleware' => ['auth']], function(){
    Route::group(array('prefix'=>'category','namespace' => 'App\Modules\CategoryManage\Controllers'), function() {
        /**
         * GET Routes
         */
        /**
         * Category Routes
         */
        Route::get('add', [
            'as' => 'category.add', 'uses' => 'CategoryManageController@index'
        ]);

        Route::get('list', [
            'as' => 'category.list', 'uses' => 'CategoryManageController@listView'
        ]);

        Route::get('details', [
            'as' => 'category.list', 'uses' => 'CategoryManageController@getCategoryDetails'
        ]);

        Route::get('search', [
            'as' => 'category.list', 'uses' => 'CategoryManageController@searchCategory'
        ]);

        Route::get('edit/{id}/search/{search?}', [
            'as' => 'category.edit', 'uses' => 'CategoryManageController@edit'
        ]);



        /**
        * POST Routes
        */
        Route::post('add', [
            'as' => 'category.add', 'uses' => 'CategoryManageController@addCategory'
        ]);

        Route::post('img-delete', [
            'as' => 'category.list', 'uses' => 'CategoryManageController@deleteImage'
        ]);

        Route::post('edit/{id}/search/{search?}', [
            'as' => 'category.add', 'uses' => 'CategoryManageController@update'
        ]);

    });
}); 
@extends('layouts.back_master') @section('title','Edit Main Category')

@section('css')
@stop

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Main Category
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('category/list')}}}">Main Category List</a></li>
		<li class="active">Edit Main Category</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
        <form role="form" class="form-horizontal form-validation" method="post" enctype="multipart/form-data">
    		<div class="box-header with-border">
    			<h3 class="box-title">Edit Main Category</h3>
    		</div>
    		<div class="box-body">		    
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('code')) has-error @endif">
                    <label class="col-sm-2 control-label">Code <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="code" placeholder="Category Code" value="{{$category_details[0]->code}}">
                        @if($errors->has('code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('code')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('category_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="category_name" placeholder="Category Name" value="{{$category_details[0]->name}}">
                        @if($errors->has('category_name'))
                            <label id="label-error" class="error" for="label">This {{Input::old('category_name')}} has already been taken.</label>
                        @endif
                    </div>
                </div>
                <!-- hidden variable -->
                <input type="hidden" name="search" value="{{Request::route('search')}}">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Display Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="dis_name" placeholder="Category Display Name" value="{{$category_details[0]->display_name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="3" name="description" placeholder="Write Something About Category">{{$category_details[0]->description}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Image</label>
                    <div class="col-sm-10">
                        <input id="input-id" name="file" type="file" class="file-loading input-sm" data-allowed-file-extensions='["jpg","png","jpeg"]' data-upload-url="#">
                    </div>
                </div>
    		</div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-sm bg-purple pull-right" id="editForm"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
	</div>
</section>

@stop

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    var image = '{{$category_details[0]->image_path}}';
    if(image){
       image = '{{url()}}'+'/core/storage/uploads/images/category/'+'{{$category_details[0]->image_path}}';
    }else{
       image = '{{url()}}'+'/core/storage/uploads/images/category/empty.jpg';
    }
    $("#input-id").fileinput({
        uploadUrl: "", // server upload action
        uploadAsync: true,
        showUpload:false,
        overwriteInitial: true,
        initialPreview: [
            image
        ],
        maxFileSize: 2000,
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: 'image',
        initialPreviewConfig: [
            {caption: "{{$category_details[0]->image_path}}",url: "{{url()}}/category/img-delete", key:"{{$category_details[0]->id}}"}

        ]
    });
    $('#input-id').on('filecleared', function(event) {
        $('#editForm').attr('disabled',false);
    });
    $('#input-id').on('fileuploaderror', function(event, file, previewId, index, reader) {
        $('#editForm').attr('disabled',true);
    });
    $('#input-id').on('fileselect', function(event, numFiles, label) {
        $('#editForm').attr('disabled',false);
    });
});
</script>
@stop

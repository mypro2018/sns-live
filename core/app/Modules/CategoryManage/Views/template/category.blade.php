<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val['code']}}</td>
     <td>{{$result_val['name']}}</td>
     <td>{{$result_val['display_name']}}</td>
     <td>{{$result_val['created_at']}}</td>
     <td>{{$result_val['updated_at']}}</td>
     <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Detail" onclick="viewDetail({{$result_val['id']}})"><i class="fa fa-eye view"></i></a>
            <a href="{{ route('category.edit', ['id' => $result_val['id'],'search' => Request::get('category_name')]) }}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Category"><i class="fa fa-pencil"></i></a>
            <a href="{{'edit/'.$result_val['id']}}" class="btn btn-xs btn-default disabled" data-toggle="tooltip" data-placement="top" title="Delete Category"><i class="fa fa-trash-o"></i></a>
        </div>
     </td>
</tr>


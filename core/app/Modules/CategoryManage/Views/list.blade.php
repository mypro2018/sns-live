@extends('layouts.back_master') @section('title','Main Category List')

@section('css')
@stop

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
  	Main Category
  	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Main Category List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
    <form role="form" method="get" action="{{url('category/search')}}">      
      <div class="box-body">        
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
             <div class="form-group">
               <input type="text" class="form-control input-sm" name="category_name" placeholder="Search Category" value="{{$category}}">
             </div>
           </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
          <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
        </div>
      </div>
    </form>
  </div>
  <!-- <div class="col refresh"> -->        
  <!-- </div> -->

  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Main Category List</h3>        
      <a href="{{url('category/add')}}" class="btn bg-purple btn-sm pull-right">New Category</a>        
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped table-condensed" id="orderTable">
        <thead>
          <tr>
            <th width="5%">#</th>
            <th width="10%">Code</td>
            <th width="25%">Name</td>
            <th width="20%">Display Name</td>
            <th width="15%">Created At</th>
            <th width="15%">Updated At</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php $i = ($all_category->currentpage()-1) * $all_category->perpage() + 1; ?>
        @if(count($all_category) > 0)
          @foreach($all_category as $result_val)
            @include('CategoryManage::template.category')
          <?php $i++;?>
          @endforeach
        @else
          <tr><td colspan="7" class="text-center">No data found.</td></tr>
        @endif
        </tbody>
      </table>        
    </div>
    @if(count($all_category) > 0)
    <div class="box-footer">
      Showing {{$all_category->firstItem()}} to {{$all_category->lastItem()}} of {{$all_category->total()}} Category      
      <div style="float: right;">{!! $all_category->appends($_GET)->render() !!}</div>
    </div>
    @endif
  </div>
</section>
@stop

@section('js')
<script type="text/javascript">

//This function is used to view category details in popup model
function viewDetail(category_id) {
  
  $(".box").addClass('panel-refreshing');

  var content = '<div class="row">'+
    '<div class="col-lg-12 form-group">'+
      '<label>Category</label>'+
        '<input type="text" class="form-control input-sm category" name="cat_name" placeholder="Category Display Name" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Description</label>'+
        '<textarea class="form-control input-sm description" rows="3" name="description" placeholder="No Description About This Category" readonly></textarea>'+
    '</div>'+
    '<div class="col-lg-12 form-group media-sec">'+
      '<label>Image</label>'+
      '<div class="media-object text-center"></div>'+
      '<img class="media-object">'+
    '</div>'+
  '</div>';

  $.ajax({
      url: "{{URL::to('category/details')}}",
      method: 'GET',
      data: {'category_id': category_id},
      async: false,
      success: function (data) {
        if(data != ''){
          $.confirm({
            title: 'Category Details',
            theme: 'material',
            content: content,
            // columnClass: 'medium',
            onContentReady: function () {
              var self = this;
              self.$content.find('.category').val(data.details[0].name);
              self.$content.find('.description').html(data.details[0].description);
              if(data.details[0].image_path){
                self.$content.find('.media-object').attr('src','{{url()}}'+'/core/storage/uploads/images/category/'+data.details[0].image_path);
              }else{
                self.$content.find('.media-object').html('No Images To Show');
              }
            },
            buttons: {
              close: function () {}
            }
          });
        }else{}

        $(".box").removeClass('panel-refreshing');
      },
      error: function () {
        alert('error');
      }
  });
}

</script>
@stop

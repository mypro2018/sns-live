<?php namespace App\Modules\CategoryManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\CategoryManage\Models\CategoryManage;
use Illuminate\Support\Facades\DB;
use Exception;



class Logic extends Model {

	/**
     * This function is used to add a category
     * @param array $data Array of data that needed to add category
     * @return object Category object if success, exception otherwise
     */
    public function addCategory($data){

        DB::transaction(function() use($data){
            $category = Categorymanage::create($data);
            if($category){
                return $category;
            }else{
                throw new Exception("Error occured while adding new category");
            }
        });

    }

    /**
     * This function is used to get category name and id
     * @parm -
     * @return category object
     */
    public function getCategoryName(){
        $category_name = Categorymanage::get()->lists('name','id');
        if(count($category_name) > 0){
            return $category_name;
        }
    }

    /**
     * This function is used to get all category details
     * @parm -
     * @return category object
     */
    public function getAllCategories(){
        return $categories = CategoryManage::all();
    }

    /**
     * This function is used to get all category details
     * @parm -
     * @return category object
     */
    public function getAllCategory($paginate = null){
        $paginate = unserialize(SHOW_OPTION)[1];
        return $all_category = CategoryManage::paginate($paginate);
    }

    /**
     * This function is used to get all category details
     * @parm - integer $category_id that need to get category details
     * @return category object
     */
    public function getCategoryDetails($category_id){
        $all_category = CategoryManage::where('id','=',$category_id)->get();
        if(count($all_category) > 0){
            return $all_category;
        }else{
            return false;
        }
    }

    /**
     * This function is used to search category
     * @param integer $category_id
     * @response category object
     */

    public function searchCategory($category){
        $paginate = unserialize(SHOW_OPTION)[1];
        return $search_category = CategoryManage::where('name', 'like', '%' . $category . '%')->paginate($paginate);
    }

    /**
     * This function is used to update item category
     * @param integer $cat_id and array $data
     * @respnse is boolean
     */
    public function updateCategoy($cat_id,$data){
        DB::transaction(function() use($cat_id,$data){
            $update_category = CategoryManage::where('id',$cat_id)->update($data);
            if($update_category){
                return true;
            }else{
                throw new Exception("Error occurred while adding new category");
            }
        });
    }

    /**
     * This function is used to delete category image
     * @param Integer $category_id
     * @respnse is boolean
     */
    public function deleteCategoryImage($cat_id){
        DB::transaction(function() use($cat_id){
            $update_category = CategoryManage::where('id',$cat_id)->update(['image_path' => null]);
            if($update_category){
                return true;
            }else{
                throw new Exception("Error occurred while deleting category image");
            }
        });
    }

}

<?php

Route::group(['middleware' => ['auth']], function(){
    Route::group(array('prefix'=>'auction','module' => 'AuctionLiveManage', 'namespace' => 'App\Modules\AuctionLiveManage\Controllers'), function() {
        /**
         * GET Routes
         */
        Route::get('live/{id}', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@index'
        ]);

        Route::get('select', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@startBid'
        ]);

        Route::any('bid', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@bid'
        ]);

        Route::any('get-bids', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@getBids'
        ]);

        Route::get('live-bid', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@liveBid'
        ]);

        Route::get('call-bid', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@callBid'
        ]);

        Route::get('get-win', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@getWinBid'
        ]);

        Route::get('item-history', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@getItemBidHistory'
        ]);

        Route::get('stop', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@stop'
        ]);
        Route::get('approve', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@approve'
        ]);
        Route::get('reject', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@reAuction'
        ]);

        Route::get('load-item', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@searchItem'
        ]);

        Route::get('live-item', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@auctionLiveProduct'
        ]);

        Route::get('item-details', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@auctionItemDetails'
        ]);

        Route::get('request-offer', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@requestOffer'
        ]);

        Route::get('hold-bidding', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@holdBidding'
        ]);

        Route::get('show-hide-bidder', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@showHideBidder'
        ]);

        Route::any('card-exceed', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@checkOnlineCardExceed'
        ]);

        Route::any('change-image-video', [
            'as' => 'live.auction', 'uses' => 'AuctionLiveManageController@updateImageVideoViewStatus'
        ]);

    });
});
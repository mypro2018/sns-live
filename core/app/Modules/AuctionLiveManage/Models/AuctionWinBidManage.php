<?php namespace App\Modules\AuctionLiveManage\Models;

/**
*
* AuctionWinBidManage
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;


class AuctionWinBidManage extends Model {
    use SoftDeletes;
	/**
     * The sns_auction_bid table associated this model.
     */
    protected $table = 'sns_auction_winner';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutuated to dates
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function winner(){
        return $this->belongsTo('Core\UserManage\Models\User','sns_user_id','id');
    }

    public function bidding_winner(){
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage','sns_user_id','id');
    }


}

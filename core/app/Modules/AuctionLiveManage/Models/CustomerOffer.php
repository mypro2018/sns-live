<?php namespace App\Modules\AuctionLiveManage\Models;

/**
*
* AuctionBidManage
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CustomerOffer extends Model {

    use SoftDeletes;

	/**
     * The sns_auction_bid table associated this model.
     */
    protected $table   = 'customer_offers';
    public $timestamps = true;
    protected $guarded = ['id'];

    protected $dates   = ['deleted_at'];

    public function customer_auction(){
        return $this->hasOne('App\Modules\CustomerManage\Models\CustomerAuctionManage', 'user_id', 'customer_id');
    }

    /** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auction_details(){
		return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionDetailsManage', 'auction_detail_id', 'id');
	}


}

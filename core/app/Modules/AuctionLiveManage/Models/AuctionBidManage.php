<?php namespace App\Modules\AuctionLiveManage\Models;

/**
*
* AuctionBidManage
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AuctionBidManage extends Model {

    use SoftDeletes;

	/**
     * The sns_auction_bid table associated this model.
     */
    protected $table   = 'sns_auction_bid';
    public $timestamps = true;
    protected $guarded = ['id'];

    protected $dates   = ['deleted_at'];



    public function win_bid(){
        return $this->hasOne('App\Modules\AuctionLiveManage\Models\AuctionWinBidManage','sns_auction_bid_id','id');
    }

    public function auction_detail(){
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_auction_detail_id','id');
    }

    public function bidder(){
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage', 'sns_customer_id', 'id');
    }

    public function auc_detail(){
        return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','id','sns_auction_detail_id');
    }

    public function customer_auction(){
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerAuctionManage', 'sns_customer_id', 'user_id');
    }

    public function bid_user(){
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage', 'sns_customer_id', 'id');
    }


    

}

<?php namespace App\Modules\AuctionLiveManage\Controllers;


/**
* AuctionLiveManageController class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Modules\AuctionLiveManage\BusinessLogics\Logic as AuctionLiveLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Pusher;
use App\Modules\ProductManage\BusinessLogics\Logic as Product; 
use App\Modules\CustomerManage\BusinessLogics\Logic as CustomerLogic;
use App\Modules\AuctionLiveManage\Models\AuctionBidManage;
use DB;                                     

class AuctionLiveManageController extends Controller {


	protected $live_auction;
	protected $auction;
	protected $customer;

	public function __construct(AuctionLiveLogic $auctionLive, AuctionLogic $auction,Product $product, CustomerLogic $customer)
	{
        $this->live_auction = $auctionLive;
        $this->auction 		= $auction;
		$this->product      = $product;
		$this->customer     = $customer;       
    }
	/**
	 * This function is used to diplay live auction view
	 * @param $auction_id
	 * @return Response
	 */
	public function index($auction_id){
		
		$auction_live  = $this->live_auction->getAuctionDetail($auction_id);
		$complete_item = $this->live_auction->completeItem($auction_id);
		$auction_item  = $this->live_auction->auctionItem($auction_id);
		
		if($auction_live){
			return view("AuctionLiveManage::auction-bid")->
				with([
					'auction_detail' => $auction_live['all'],
					'select' 		 => $auction_live['select'],
					'img'            => $auction_live['img'],
					'bid_list'       => $auction_live['bidding_list'],
					'bid_count'      => $auction_live['bid_count'],
					'complete_item'  => $complete_item,
					'auction_item'   => $auction_item
				]);
		}else{
			 return response()->view('errors.404');
		}
	}
	/**
	 * This function is used to get item details to bid
	 * @param 
	 * @return  
	 */
	public function startBid(Request $request){
		if($request->ajax()){
				$bid_item = $this->live_auction->getItemDetail($request->get('detail_id'),$request->get('auction_id'));
			return response()->json(['detail' => $bid_item['select_item'],'img' => $bid_item['img']]);
		}else{
			return response()->json([]);
		}	
	}
	/** 
	 * This function is used to calculate bid
	 * @param user | amount | dedail_id
	 * @return
	 */
	public function bid(Request $request){
		$customer = 0;
		if($request->ajax()){
			if (Sentinel::check()){
				$user   = Sentinel::getUser();

				//get customer detail
				$customer_detail = $this->customer->getCustomer($user->id);
				if($customer_detail){
					$customer = $customer_detail->id;
				}

				//validate bidding over for item for front end customers
				$bidding_item_details = $this->live_auction->validateBiddingOver($request);
				if($bidding_item_details['status'] == true){
					// dd($this->live_auction->validateBiddingOver($request));
					$pusher = App::make('pusher');
					$pusher->trigger('auction-channel', 'over-item-rebid-disabled', [$bidding_item_details, $customer]);
					
					return response()->json(['list' => [], 'count' => 0]);
				}else{
					$addbid = DB::transaction(function () use($request, $customer) {
						return $addbid = $this->live_auction->addBid(
							$customer,
							$request->get('amount'),
							$request->get('detail_id'),
							$request->get('bid_count'), 
							$request->get('auction'), 
							$request->get('offer_status'),
							$request->get('bidder_type')
						);
					});

					$bid_count = $this->live_auction->bidCount($request->get('detail_id'));

					return response()->json(['list' => $addbid, 'count' => $bid_count]);
				}	
				
			}else{
				Sentinel::logout();
				return redirect()->route('user.login');
			}
		}else{
			return response()->json([]);
		}
	}
    /**
     * This function is used to live bid
     * @param dedail_id
     * @return
     */
    public function liveBid(Request $request){
		if($request->ajax()){
			if (Sentinel::check()){
				$live_bid = $this->live_auction->liveBid($request->get('detail_id'),$request->get('bid_count'));
				return response()->json(['list' => $live_bid]);
			}else{
				Sentinel::logout();
				return redirect()->route('user.login');
			}
		}else{
			return response()->json([]);
		}
    }
    /**
     * This function is used to call bid
     * @param integer dedail_id integer call
     * @return
     */
    public function callBid(Request $request){
		if($request->ajax()){
			if (Sentinel::check()){ 
				$call_bid = $this->live_auction->callBid($request->get('detail_id'), $request->get('call'));
				$pusher = App::make('pusher');
    			$pusher->trigger( 'auction-channel','call-event',array('call' => $call_bid));
				return response()->json(['call' => $call_bid]);
			}else{
				Sentinel::logout();
				return redirect()->route('user.login');
			}
		}else{
			return response()->json([]);
		}
    }
	/** 
	 * This function is used to get all bids
	 * @param item | auction_id
	 * @return
	 */
	public function getBids(Request $request){
		return $bids = $this->live_auction->getItemBids($request->get('item_id'), $request->get('auction_id'));
	}
    /**
     * This function is used to get wining bid
     * @param auction_detail_id
     * @return
     */
    public function getWinBid(Request $request){
        $win_bid = $this->live_auction->winPrice($request->get('detail_id'));
        return response()->json(['win_bid' => $win_bid]);
    }
    /**
     * This function is used to get item bid history
     * @param
     * @return
     */
    public function getItemBidHistory(Request $request){
        $bid_detail = $this->live_auction->getItemHistory($request->get('detail_id'));
        return response()->json(['detail' => $bid_detail]);
    }

	/** 
	 * This function is used to stop bid
	 * @param 
	 * @return
	 */
	public function stop(Request $request){
		$status = $this->live_auction->stop($request->get('detail_id'));
		return response()->json(['status' => $status]);
	}
	/** 
	 * This Function is used to approve customer highest bid
	 * @param integer auction_id
	 * @param integer customer_card_no
	 * @param integer detail_id
	 * @param string comment
	 * @return
	 */
	public function approve(Request $request){
		//check entered card no is valid or not
		$valid_card = $this->customer->validCardNo($request);
		if($valid_card) {
			$approve = $this->live_auction->approveItem($request);
			return response()->json(['status' => true]);
		}else{
			return response()->json(['status' => false]);
		}
		
	}
	/** 
	 * This Function is used to re-auction the item
	 * @param integer detail_id
	 * @param string comment
	 * @param integer product_id
	 * @return
	 */
	public function reAuction(Request $request){
		try{
			if($request->ajax()){
				$reject = $this->live_auction->reAuction($request);
				$pusher = App::make('pusher');
				$pusher->trigger( 'auction-channel', 'sold-reject-event', $reject);
			}else{
				throw new \Exception("Error occurred, Invalid User Input.");
			}
		}catch(\Exception $e){
			return $e->getMessage(); 
		}
	}

	/** 
	 * This Function is used to search product
	 * @param
	 * @return
	 */
	public function searchItem(Request $request){
		if($request->ajax()){
			try{
				$item = $this->product->getSearchItem($request->get('search'), $request->get('auction_id'));
				if(sizeof($item) > 0){
					return response()->json(['item' => $item]);
				}else{
					return response()->json(['item' => 0]);
				}
				
			}catch(\Exception $e){
				return response()->json([]);
			}
		}else{
			return response()->json([]);
		}
	}

	/** 
	 * This function is used to check item is live. 
	 * @param
	 * @return
	 */
	public function auctionLiveProduct(Request $request){
		if($request->ajax()){
			try{
				$item_live = $this->live_auction->auctionLiveProduct($request->get('auction_id'));
				return response()->json(['item_live' => $item_live]);
			}catch(\Exception $e){
				return response()->json([]);
			}
		}else{
			return response()->json([]);
		}
	}

	/** 
	 * This function is used to get new auction item detail. 
	 * @param
	 * @return
	 */
	public function auctionItemDetails(Request $request){
		if($request->ajax()){
			try{
				$new_item_details = $this->live_auction
					->getAuctionItemDetails($request->get('auction_id'),$request->get('product_id'));
				return response()->json(['new_item_details' => $new_item_details]);
			}catch(\Exception $e){
				return response()->json([]);
			}
		}else{
			return response()->json([]);
		}
	}

	
	/**
     * This function is used to request offer
     * @param integer dedail_id | constant OFFER_REQUEST 
     * @return
     */
    public function requestOffer(Request $request){
		if($request->ajax()){
			$request_offer = $this->live_auction->requestOffer($request->get('detail_id'), OFFER_REQUEST);
			if(count($request_offer) > 0){
				$pusher  = App::make('pusher');
				$pusher->trigger('auction-channel', 'offer-request-event', $request_offer);
			}
		}else{
			return response()->json([]);
		}
    }
	
	/**
	 * This function is used to hold the online bidding from the floor
	 * @param integer hold_status
	 * @param integer auction_id
	 * @return void
	 */

	public function holdBidding(Request $request){
		if($request->ajax()){
			$response = $this->live_auction->holdBidding($request);
			if($response){
				$pusher  = App::make('pusher');
				$pusher->trigger('auction-channel', 'hold-bidding', [
					'auctionId'  => $request->get('auction_id'),
					'holdStatus' => $request->get('status')
				]);
			}
		}else{
			return response()->json([]);
		}
	}

	/**
	 * This function is used to show & hide online bidder name
	 * @param integer show_hide_status
	 * @param integer auction_id
	 * @return void
	 */

	public function showHideBidder(Request $request){
		if($request->ajax()){
			$response = $this->live_auction->showHideBidder($request);
			if($response){
				$pusher  = App::make('pusher');
				$pusher->trigger('auction-channel', 'show-hide-bidder', [
					'auctionId'      => $request->get('auction_id'),
					'showHideStatus' => $request->get('status')
				]);
			}
		}else{
			return response()->json([]);
		}
	}

	
	/**
	 * This function is used issuing card number's status
	 * @param integer live_auction_id
	 * @return integer $response_id
	 */
	public function checkOnlineCardExceed(Request $request){
		if($request->ajax()){
			$card = $this->customer->checkOnlineCardExceed($request);
			$user = Sentinel::getUser();
			if($user){
				$customer_detail = $this->customer->getCustomer($user->id);
				if($customer_detail){
					$request->customer_id = $customer_detail->id;
				}
				$allowRegister = $this->auction->getAllowedAuction($request);
			}
			return response()->json(['response' => $card, 'allowRegister' => $allowRegister]);
		}else{
			return response()->json([]);
		}
	}

	/**
	 * This function is used to change image & video view
	 * @param integer auction_id
	 * @param integer customer_id
	 * @param integer status
	 * @return void
	 */

	public function updateImageVideoViewStatus(Request $request){
		if($request->ajax()){
			$response = $this->auction->updateImageVideoViewStatus($request);
		}else{
			return response()->json([]);
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}




}

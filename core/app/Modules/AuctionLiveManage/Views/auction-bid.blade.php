@extends('layouts.back_master') @section('title','On Live Auction')
@section('links')
<link rel="stylesheet" href="{{asset('assets/dist/boostrap-toggle/css/bootstrap-toggle.min.css')}}" type="text/css"/>
@stop
@section('css')
<style type="text/css">
.thropy{
    color: goldenrod;
    /*font-size: 22px;*/
}

.tag {
  font: 15px/1.5 'PT Sans', serif;
   margin: 25px;
  font-weight:bold;
  background: crimson;
  border-radius: 3px 0 0 3px;
  color: #fff;
  display: inline-block;
  height: 26px;
  line-height: 26px;
  padding: 0 20px 0 23px;
  position: relative;
  margin: 0 0 0 0;
  text-decoration: none;
  -webkit-transition: color 0.2s;
}

.tag-highest {
  font: 18px/1.5 'PT Sans', serif;
  margin: 25px;
  font-weight:bold;
  background: #ddd;
  border-radius: 0 3px 3px 0;
  color: #000;
  /*display: inline-block;*/
  height: 46px;
  line-height: 48px;
  padding: 0 20px 0 23px;
  position: relative;
  margin: 0 0 0 0;
  text-decoration: none;
  -webkit-transition: color 0.2s;
  border :1px solid rgb(198, 198, 198);
}

.tag::before {
  background: #fff;
  border-radius: 10px;
  box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
  content: '';
  height: 6px;
  left: 10px;
  position: absolute;
  width: 6px;
  top: 10px;
}

.tag::after {
  background: #fff;
  border-bottom: 13px solid transparent;
  border-left: 10px solid crimson;
  border-top: 13px solid transparent;
  content: '';
  position: absolute;
  right: 0;
  top: 0;
}
.highest{
  margin-left: auto;
  margin-right: auto;
}
.custom-btn{
  width:100px;
  height:40px;
  background-image: linear-gradient(to right, #f4f4f4, #f4f4f4, #f4f4f4, #f4f4f4);
  box-shadow: 0 0px 0px 0 rgba(45, 54, 65, 0.75);
  font-size: 16px;
  color: #333 !important;
  cursor: pointer;
  text-align:center;
  border-color: #adadad;
  background-size: 300% 100%;
  moz-transition: all .4s ease-in-out;
  -o-transition: all .4s ease-in-out;
  -webkit-transition: all .4s ease-in-out;
  transition: all .4s ease-in-out;
}

.custom-btn:hover {
    background-position: 100% 0;
    moz-transition: all .4s ease-in-out;
    -o-transition: all .4s ease-in-out;
    -webkit-transition: all .4s ease-in-out;
    transition: all .4s ease-in-out;
}

.custom-btn:focus {
    outline: none;
}
.main_button{
  font-weight:bold;
}
.btn-circle.btn-xl {
    height: 70px;
    padding: 10px 16px;
    /* font-size: 24px; */
    /* line-height: 1.33; */
    border-radius: 40px;
}
.custom-gavel{
  font-size: 35px !important;
}
.item-img{
  height: 200px;
}
/*#autocomplete-ajax{
  position: absolute;
  z-index: 2;
  background: transparent;
}*/
#autocomplete-ajax-x{
  color: #CCC;
  position: absolute;
  background: transparent;
  z-index: 1;
}
.card-no{
  background-color: #4a5e63 !important;
}
.bid-list{
  color: #9a9696;
}
.bid-list-panel{
  height: 350px;
}
.panel-bottom-left{
  height: 350px;
}
.panel-bottom-right{
  height: 608px;
}
.top-panel-right{
  height: 300px;
}
.notice{
  font-size: 12px;
  color: #000;
}
.winner-input{
  font-size: 15px !important;
  width: 50% !important;
}
.table-wrapper-scroll-y {
  display: block;
  max-height: 240px;
  overflow-y: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}
.btn-search {
    background: #424242;
    border-radius: 0;
    color: #fff;
    border-width: 1px;
    border-style: solid;
    border-color: #1c1c1c;
  }
  .btn-search:link, .btn-search:visited {
    color: #fff;
  }
  .btn-search:active, .btn-search:hover {
    background: #1c1c1c;
    color: #fff;
  }
  .item_search,.bit_input{
    margin-top: 0px !important;
  }
  #clockdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
  }
  #clockdiv > div{
    padding: 10px;
    border-radius: 3px;
    background: #00BF96;
    display: inline-block;
    margin-right: 10px;
  }

  #clockdiv div > span{
    padding: 15px;
    border-radius: 3px;
    background: #00816A;
    display: inline-block;
  }

  .smalltext{
    padding-top: 5px;
    font-size: 16px;
  }

  #highestdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
  }

   #highestdiv > div{
    padding: 10px;
    border-radius: 3px;
    background: #c2c6c6;
    display: inline-block;
    margin-right: 10px;
  }

  #highestdiv div > span{
    padding: 15px;
    border-radius: 3px;
    background: #36397e;
    display: inline-block;
  }

  .smalltext{
    padding-top: 5px;
    font-size: 16px;
  }
  /*COUNT DOWN TIMER */
  .countdown-rtl {
  direction: rtl;
}
.countdown-holding span {
  color: #888;
}
.countdown-row {
  clear: both;
  width: 100%;
  padding: 0px 2px;
  text-align: center;
}
.countdown-show1 .countdown-section {
  width: 98%;
}
.countdown-show2 .countdown-section {
  width: 48%;
}
.countdown-show3 .countdown-section {
  width: 32.5%;
}
.countdown-show4 .countdown-section {
  width: 24.5%;
}
.countdown-show5 .countdown-section {
  width: 19.5%;
}
.countdown-show6 .countdown-section {
  width: 16.25%;
}
.countdown-show7 .countdown-section {
  width: 14%;
}
.countdown-section {
  display: block;
  float: left;
  font-size: 75%;
  text-align: center;
}
.countdown-amount {
    font-size: 200%;
}
.countdown-period {
    display: block;
}
.countdown-descr {
  display: block;
  width: 100%;
}
.glyphicon{
  line-height: 0;
}

.user-types{
  width:100px;
}
</style>
@stop
<!-- Content Header (Page header) -->
@section('content')
<section class="content">
  <div class="panel panel-default">
    <div class="panel-heading">
      <p class="text-left"><strong>{{ sizeof($complete_item) }} / {{ sizeof($auction_item) }} Items </strong></p>
      @if(isset($auction_detail) && count($auction_detail) > 0)
        <p class="text-center"><strong>{{$auction_detail->auction_no?:''}} - {{$auction_detail->event_name}}</strong> - <span class="glyphicon glyphicon-time"></span> <span class="date-time">{{date('Y-m-d')}} | {{date('h:i:s A')}}</span> <button class="btn btn-default btn-sm space-btn pull-right more_detail" type="button"> <i class="fa fa-cog" aria-hidden="false"></i></button></p>
      @else
        <p class="text-center"><strong>New Auction</strong> - <span class="glyphicon glyphicon-time"></span> <span class="date-time">{{date('Y-m-d')}} | {{date('h:i:s A')}}</span></p>
      @endif
    </div><!-- end panel-heading -->
    <div class="panel-body">
      <div class="container-fluid">
        <div class="row">
          <!-- hide model-content -->
          <div class="model-content hide">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="panel panel-default bid-list-panel">
                  <div class="panel-heading">
                    <strong>Latest Item Bid List</strong>
                    @if(isset($bid_count))
                      <strong class="pull-right bid-count-show">Bid Count : {{$bid_count}}</strong>
                      <input type="hidden" name="bid_count" class="form-control input-sm bid_count" value="{{$bid_count}}">
                    @else
                      <strong class="pull-right bid-count-show"></strong>
                      <input type="hidden" name="bid_count" class="form-control input-sm bid_count" value="0">
                    @endif
                  </div>
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table bid-list-table">
                        <thead>
                          <tr>
                            <th>Customer</th>
                            <th class="text-right">Bid Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(isset($bid_list) && count($bid_list) > 0)
                          @foreach($bid_list as $list)
                            <tr>
                              @if(isset($list->bid_user) && count($list->bid_user) > 0)
                                <td scope="row">{{substr($list->bid_user->fname, 0, 6)}}...
                              @else
                                <td scope="row">Customer</td>
                              @endif
                                @if(count($list->auction_detail) > 0 && count($list->bid_user) > 0 && count($list->bid_user->customer_card) > 0) 
                                  <span class="label label-info card-no"> {{$list->bid_user->customer_card->card_no?:'-'}}</span>                       
                                @endif
                              </td>
                              <td class="text-right">Rs {{number_format($list->bid_amount,2)}}</td>
                            </tr>
                          @endforeach
                        @else
                          <tr>
                            <td colspan="2" class="bid-list"><strong>Latest Bid Not Found</strong></td>
                          </tr>
                        @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div><!-- end panel -->
              </div> <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> -->
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="panel panel-default panel-bottom-left">
                  <div class="panel-heading">
                    <strong>Complete Item List</strong>
                    {{-- <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"> --}}
                  </div>
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table" style="margin-bottom:0px;">
                        <thead>
                          <tr>
                            <th colspan="3">Item Code</th>
                            <th colspan="3">Card No</th>
                            <th class="text-right">Final Amount</th>
                          </tr>
                        </thead>
                      </table>
                      <div class="table-wrapper-scroll-y">
                        <table class="table fixed_header">
                          <tbody class="complete_bid">
                            @if(isset($complete_item) && count($complete_item) > 0)
                              @foreach($complete_item as $value)
                                @if($value->status == ITEM_PAYMENT_APPROVE)
                                  <tr>
                                    <td colspan="3" scope="row">
                                      @if($value->item_approve_sold_status == PAYMENT_SUCCESS)
                                        <a href="javascript:void(0);"  id="item{{$value->item_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Paid" class="btn btn-success btn-xs">
                                          {{ $value->item_code }} 
                                        </a> 
                                      @else
                                        <a href="javascript:void(0);"  id="item{{$value->item_id}}" data-toggle="tooltip" data-placement="top" data-original-title="Payment Pending" class="btn btn-warning btn-xs">
                                          {{ $value->item_code }} 
                                        </a> 
                                      @endif
                                    </td>
                                    <td>
                                      <span class="label label-info" id="card{{$value->item_id}}" style="padding-left: 10px;padding-right: 15px;">{{$value->card_no}}</span>
                                    </td>
                                    <td class="text-right">Rs {{number_format($value->bid_amount, 2)}}</td>
                                  </tr>
                                @elseif($value->status == ITEM_PAYMENT_REJECT)
                                  <tr>
                                    <td colspan="3" scope="row">
                                      <a href="javascript:void(0);"  id="item{{$value->item_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Reauction Item" class="btn btn-danger btn-xs" onclick="itemReauction({{$value->item_id}})">
                                        {{ $value->item_code }}
                                      </a> 
                                    </td>
                                    <td> - </td>
                                    <td class="text-right">Rs {{number_format($value->bid_amount, 2)}}</td>
                                  </tr>
                                @else
                                  <tr>
                                    <td colspan="3" scope="row">
                                      <a href="javascript:void(0);"  id="item{{$value->item_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Payment Pending" class="btn btn-danger btn-xs" onclick="itemReauction({{$value->item_id}})">
                                        {{ $value->item_code }}
                                      </a> 
                                    </td>
                                    <td> - </td>
                                    <td class="text-right">Rs {{number_format($value->bid_amount, 2)}}</td>
                                  </tr>
                                @endif
                              @endforeach
                            @else
                              <tr>
                                <td colspan="2" class="bid-list"><strong>Item Not Found</strong></td>
                              </tr>
                            @endif
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> <!--end model row -->
          </div>
          <!--end hide model-content -->
          <div class="model-content">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="form-group">
                    <div class="input-group input-group-lg">
                      <input type="text" class="form-control bit_input" id="bid_amount" placeholder="Enter Your Bid" min="0" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)">
                      <span class="input-group-btn">
                        <button class="btn btn-default custom-btn bg-purple" type="button" id="bid">Bid Now</button>
                      </span>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table text-center">
                      <tbody>
                        <tr>
                          <td colspan="3" class="text-center">
                            <div class="input-group highest input-group-lg">
                              <span class="input-group-addon">
                                <i class="fa fa-trophy thropy" aria-hidden="false"></i>
                              </span>
                              @if(isset($select) && count($select))
                                <?php $highest_bidder = $select[0]->fname ?>
                                <div class="tag-highest">Rs {{number_format($select[0]->bid_amount,2)}}</div>
                                <input type="hidden" name="highest_bid_amount" id="highest_bid_amount" value="{{$select[0]->bid_amount}}">
                              @else
                                <?php $highest_bidder = '' ?>
                                <div class="tag-highest">Rs 00.00</div>
                                <input type="hidden" name="highest_bid_amount" id="highest_bid_amount" value="0">
                              @endif
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button type="button" @if(isset($bid_count) && $bid_count < 1) disabled @elseif(isset($auction_detail) && $auction_detail->request_offer == OFFER_REQUEST) disabled @endif class="btn btn-sm btn-info bg-purple custom-btn main_button" id="call_bid_btn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing">
                              @if(count($select) > 0)
                                @if($select[0]->call == CALL0)
                                  Call Now
                                @elseif($select[0]->call == CALL1) 
                                  2nd CALL
                                @elseif($select[0]->call == CALL2)
                                  FINAL CALL
                                @else
                                  Wait...  
                                @endif
                              @else
                                Call Now
                              @endif
                            </button>
                          </td>
                          <!-- <td>
                            <button class="btn btn-success btn-sm custom-btn main_button" type="button" id="sold_btn"  @if(isset($bid_count) && $bid_count < 1) disabled @elseif(isset($auction_detail) && $auction_detail->request_offer == OFFER_REQUEST) disabled @endif>
                              Sold
                            </button>
                          </td>-->
                          <td>
                            <button class="btn btn-danger btn-sm custom-btn main_button" type="button" id="unsold_btn"  @if(isset($bid_count) && $bid_count < 1) disabled @elseif(isset($auction_detail) && $auction_detail->request_offer == OFFER_REQUEST) disabled @endif>
                              Not Sold
                            </button>
                          </td>
                          <td>
                            @if($auction_detail && $auction_detail->hold_online_bidding == BIDDING_ACTIVE)
                              <input type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100" data-height="42" data-on="Unhold" data-off="Hold" value="{{BIDDING_ACTIVE}}" class="hold-online-bidding" data-auction="{{ $auction_detail->id }}" checked>
                            @else
                              <input type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-width="100" data-height="42" data-on="Unhold" data-off="Hold" value="{{BIDDING_INACTIVE}}" class="hold-online-bidding" data-auction="{{ $auction_detail->id }}">
                            @endif
                          </td>
                        </tr>
                      {{-- </tbody>
                    </table> <!-- table end-->
                  </div> <!-- end table-responsive -->
                  <div class="table-responsive">
                    <table class="table text-center">
                      <tbody> --}}
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="1" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">1</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="20" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">20</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="50" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">50</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="100" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">100</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="500" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">500</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="1000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">1,000</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="2000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">2,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="5000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">5,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="10000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">10,000</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="25000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">25,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="50000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">50,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="100000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">100,000</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="200000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">200,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-btn bg-purple" type="submit" value="500000" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">500,000</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn amount-clear bg-purple" type="submit">Clear</button>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-center notice">
                          * When you click amount buttons it will increase your bid by default.
                        </td>
                        </tr> 
                        <tr>
                          <td class="text-center notice">
                            <center>
                              <div class="input-group input-group-lg">
                                @if(isset($auction_detail) && sizeof($auction_detail) > 0 && isset($auction_detail->live_auction_product) && sizeof($auction_detail->live_auction_product) > 0 && isset($auction_detail->live_auction_product->bid) && sizeof($auction_detail->live_auction_product->bid) > 0 && isset($auction_detail->live_auction_product->bid->customer_auction) && sizeof($auction_detail->live_auction_product->bid->customer_auction) > 0)
                                  <?php 
                                    $highest_bidder_card_no = $auction_detail->live_auction_product->bid->customer_auction->card_no; 
                                    $highest_bidder_name    = $auction_detail->live_auction_product->bid->customer_auction->customer->fname.' '.$auction_detail->live_auction_product->bid->customer_auction->customer->lname;
                                    $highest_bidder_city    = $auction_detail->live_auction_product->bid->customer_auction->customer->city; 
                                  ?>
                                  <input type="text" class="form-control card-input-no" placeholder="Enter Card No" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" value="{{$auction_detail->live_auction_product->bid->customer_auction->card_no}}">
                                @else
                                  <?php 
                                    $highest_bidder_card_no = '';
                                    $highest_bidder_name    = ''; 
                                    $highest_bidder_city    = '';
                                  ?>
                                  <input type="text" class="form-control card-input-no" placeholder="Enter Card No" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)">
                                @endif
                                <span class="input-group-btn">
                                  <button class="btn btn-default custom-btn bg-purple card-sold-btn" type="button" id="card-sold-btn" @if(isset($bid_count) && $bid_count < 1) disabled @elseif(isset($auction_detail) && $auction_detail->request_offer == OFFER_REQUEST) disabled @endif>Sold</button>
                                </span>
                              </div>
                            </center> 
                          </td>
                          <td class="text-center">
                            <div class="input-group input-group-lg">
                              <input type="text" class="form-control card-holder-name" value="{{$highest_bidder_name}}" aria-describedby="basic-addon1" readonly>
                              <!-- <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span> -->
                            </div>
                          </td>
                          <td class="text-center">
                            <div class="input-group input-group-lg">
                              <input type="text" class="form-control card-holder-city" value="{{$highest_bidder_city}}" aria-describedby="basic-addon1" readonly>
                              <!-- <span class="input-group-addon" id="basic-addon1"><i class="fa fa-map-marker"></i></span> -->
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-center">
                            @if($auction_detail && $auction_detail->show_hide_bidder == BIDDING_ACTIVE)
                              <input type="checkbox" data-toggle="toggle" data-onstyle="default" data-width="150" data-height="42" data-on="Hide Bidder" data-off="Show Bidder" value="{{BIDDING_INACTIVE}}" class="show-hider-bidder" data-auction="{{ $auction_detail->id }}">
                            @else
                              <input type="checkbox" data-toggle="toggle" data-onstyle="default" data-width="150" data-height="42" data-on="Show Bidder" data-off="Hide Bidder" value="{{BIDDING_ACTIVE}}" class="show-hider-bidder" data-auction="{{ $auction_detail->id }}">
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table><!-- table end-->
                  </div><!-- end table-responsive -->
                </div><!-- end panel body-->
              </div><!-- end panel -->
              <div class="panel panel-default">
                <div class="panel-body">
                    <div class="show-image @if(isset($auction_detail) && count($auction_detail) > 0 &&  $auction_detail->request_offer == OFFER_REQUEST) hide @endif">
                      @if(isset($img) && count($img) > 0)
                        <center><img src="{{url('core/storage/uploads/images/item/'.$img->image_path)}}" alt="..." class="img-thumbnail item-img"></center>
                      @else
                        <center><img src="{{asset('assets/images/empty.jpg')}}" alt="..." class="img-thumbnail item-img"></center>
                      @endif
                    </div>
                    <div class="offer-active hide">
                      <h4 class="text-center" id="monitor">Waiting For Customer Offers</h5>
                      <h4 class="textcenter"><div id="defaultCountdown"></div></h4>
                      <br/>
                      @if(sizeof($auction_detail) > 0 && 
                        isset($auction_detail->live_auction_product) &&
                        sizeof($auction_detail->live_auction_product) > 0 &&
                        isset($auction_detail->live_auction_product->customer_offer) &&
                        sizeof($auction_detail->live_auction_product->customer_offer) > 0 && 
                        isset($auction_detail->live_auction_product->customer_offer->offer_amount) && 
                        sizeof($auction_detail->live_auction_product->customer_offer->offer_amount) > 0 && 
                        sizeof($auction_detail->live_auction_product->bid) > 0 && 
                        isset($auction_detail->live_auction_product->customer_offer->customer_auction) &&
                        sizeof($auction_detail->live_auction_product->customer_offer->customer_auction->card_no)
                        )
                        <h3 class="text-center"><span class="offer-text">Customer Offer </span> : Rs <span class="offer-amount">{{$auction_detail->live_auction_product->customer_offer->offer_amount}}</span></h3>
                        <input type="hidden" id="offer_amount" class="offer_amount" value="{{$auction_detail->live_auction_product->customer_offer->offer_amount}}">
                        <div class="smalltext offer_customer_card_no text-center"><span class="card-text">Card No</span>: <span class="card_no">{{$auction_detail->live_auction_product->customer_offer->customer_auction->card_no}}</span></div>
                      <?php 
                        $amount   = $auction_detail->live_auction_product->customer_offer->offer_amount;
                        $card_no  = $auction_detail->live_auction_product->customer_offer->customer_auction->card_no;
                      ?>
                      @else
                      <?php 
                        $amount  = '0.00'; 
                        $card_no = '-'; 
                      ?>
                        <h3 class="text-center"><span class="offer-text">Customer Offer </span> : <span class="offer-amount"></span></h3>
                        <input type="hidden" id="offer_amount" class="offer_amount">
                        <div class="smalltext offer_customer_card_no text-center"><span class="card-text">Card No</span>: <span class="card_no"></span></div>
                      @endif
                      <center><button class="btn btn-default btn-sm amount-clear bg-purple approve-offer" type="button">Approve Customer Offer</button></center>
                    </div>
                </div>
              </div><!-- end panel-->  
            </div><!-- end col-xs-12 col-sm-6 col-md-4 col-lg-4 -->
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
              <div class="panel panel-default top-panel-right">
                <div class="panel-heading">
                  <strong>Item Information</strong>
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <tbody>
                        <tr>
                            <td colspan="2" class="text-center">
                              @if(isset($select) && count($select) > 0)
                                <div class="tag">{{$select[0]->item_code}}</div>
                                <input type="hidden" value="{{$select[0]->sns_auction_detail_id}}" id="live_product">
                                <input type="hidden" value="{{$select[0]->item_id}}" id="product_id">
                              @else
                                <div class="tag">NO ITEM SELECT</div>
                                <input type="hidden" class="form-control" value="0" id="live_product">
                              @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">
                              @if(isset($select) && count($select) > 0)
                                <strong><div class="item_name">{{$select[0]->item_name}}</div></strong>
                              @else
                                <strong><div class="item_name">.......</div></strong>
                              @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">Reserved Price</td>
                            @if(isset($select) && count($select) > 0)
                              <td class="text-center reserved-price">Rs {{number_format($select[0]->start_bid_price,2)}}</td>
                            @else
                              <td class="text-center reserved-price">Rs 00.00</td>
                            @endif
                        </tr> 
                        <tr>
                            <td class="text-center">Withdrawal Price</td>
                            @if(isset($select) && count($select) > 0)
                              <td class="text-center withdrawal-price">Rs {{number_format($select[0]->withdraw_amount, 2)}}</td>
                            @else
                              <td class="text-center withdrawal-price">Rs 00.00</td>
                            @endif
                        </tr>
                        <tr>
                            <td class="text-center">Supplier</td>
                            @if(isset($select) && count($select) > 0)
                              <td class="text-center supplier">{{$select[0]->supplier?$select[0]->supplier:'-'}}</td>
                            @else
                              <td class="text-center supplier">-</td>
                            @endif
                        </tr>
                        <tr>
                          <td colspan="2" class="text-center">
                            @if(isset($select) && count($select) > 0)
                              @if($select[0]->call > 0)
                                @if($select[0]->call == CALL_1)
                                  <strong class="call_view">{{CALL_1}}</strong>
                                @elseif($select[0]->call == CALL_2)
                                  <strong class="call_view">{{CALL_2}}</strong>
                                @else
                                  <strong class="call_view">{{CALL_3}}</strong>
                                @endif
                                <input type="hidden" class="form-control" value="{{$select[0]->call}}" id="call">
                                <input type="hidden" class="form-control offer_status" value="{{OFFER_EXPIRED}}" id="offer_status">
                              @else
                                <strong class="call_view"></strong>
                                <input type="hidden" class="form-control" value="{{CALL0}}" id="call">
                                @if(isset($auction_detail) && count($auction_detail) > 0 &&  $auction_detail->request_offer == OFFER_DEFAULT)
                                  <span class="requested-notice text-center hide"><b>Waiting for Offer...</b></span>
                                  <input type="hidden" class="form-control offer_status" value="{{$auction_detail->request_offer}}" id="offer_status">
                                @elseif(isset($auction_detail) && count($auction_detail) > 0 &&  $auction_detail->request_offer == OFFER_REQUEST)
                                  <span class="requested-notice text-center"><b>Waiting for Offer...</b></span>
                                  <input type="hidden" class="form-control offer_status" value="{{$auction_detail->request_offer}}" id="offer_status">
                                @else
                                  <input type="hidden" class="form-control offer_status" value="{{$auction_detail->request_offer}}" id="offer_status">
                                @endif  
                              @endif
                            @else
                              <center><span class="requested-notice hide"><b>Waiting for Offer...</b></span></center>
                              <input type="hidden" class="form-control offer_status" value="{{OFFER_DEFAULT}}" id="offer_status">
                              <input type="hidden" class="form-control" value="0" id="call">
                              <strong class="call_view"></strong>
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table> <!-- table end-->
                  </div> <!-- end table-responsive -->
                </div><!-- end panel-body-->
              </div><!-- end panel -->
              <div class="panel panel-default panel-bottom-right">
                <div class="panel-body">
                  <!-- <div class="form-group"> -->
                  <!--  <input type="text" class="form-control item_search input-sm" name="item_search" id="autocomplete-ajax" placeholder="Search Item" value="{{Request::get('id')}}" readonly @if(isset($select) && count($select) == 0) autofocus @endif> -->
                  <div class="input-group input-group-lg">
                    <input type="text" class="form-control item_search" placeholder="Search Item Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default search_btn custom-btn bg-purple" type="button">Search</button>
                    </span>
                  </div>
                  <!-- </div>  --> 
                  <!-- ITEM SEARCH PAD -->
                  <div class="table-responsive" style="margin-top:5px;">
                    <table class="table text-center">
                      <tbody>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn space-btn bg-purple" type="button">Space</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn back-btn bg-purple" type="button">Back</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm clear-btn custom-btn bg-purple" type="button">Clear</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">1</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">2</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">3</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">4</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">5</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">6</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">7</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">8</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">9</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">A</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">0</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">B</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">C</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">D</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">E</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">F</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">G</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">H</button>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">I</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">J</button>
                          </td>
                          <td>
                            <button class="btn btn-default btn-sm custom-btn search-btn bg-purple" type="button">K</button>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-center notice">
                          * Select Item by clicking this button.                        
                          </td>
                        </tr>
                      </tbody>
                    </table><!-- table end-->
                  </div><!-- end table-responsive -->
                  <!-- ITEM SEARCH PAD -->
                </div>
              </div><!-- end panel -->
            </div><!-- end col-xs-12 col-sm-6 col-md-4 col-lg-4 -->
          </div><!-- end model-content -->
        </div><!-- end row -->
      </div><!-- end container-fluid -->
    </div><!-- end panel-body -->
  </div><!-- end panel -->
</section><!-- end section-->
<?php 
  $first_bid_time = '0000-00-00 00:00:00'; 
?>
@if(sizeof($auction_detail) > 0 && isset($auction_detail->live_auction_product) && sizeof($auction_detail->live_auction_product) > 0 && isset($auction_detail->live_auction_product->bid) && sizeof($auction_detail->live_auction_product->bid) > 0)
  <?php 
  $first_bid_time = $auction_detail->live_auction_product->bid->bid_time;
  ?>
@endif

@stop
@section('js')
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<!-- <script type="text/javascript" src="{{asset('assets/front/js/plugins.js')}}"></script> -->

<!-- <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.plugin.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/dist/jquery-countdown/jquery.countdown.min.js')}}"></script> -->
<script type="text/javascript" src="{{asset('assets/dist/boostrap-toggle/js/bootstrap-toggle.min.js')}}"></script>
<script type="text/javascript">

var online_bidder_card_no,wining_customer_card_no,wining_customer_card_name,highest_bidder,highest_bidder_name = '';

var date        = '{!! (($auction_detail->live_auction_product)?$auction_detail->live_auction_product->bid?$auction_detail->live_auction_product->bid->bid_time:'':'') !!}';
var bid_history = '';

setInterval(function() {
  var time = new Date();
  $('.date-time').text(new Date().toISOString().slice(0,10)+' | '+new Date().toLocaleTimeString());
}, 1000);

//page load
if($('#call').val() == '{{CALL3}}'){
  auctionWinner('{{ $highest_bidder_name }}', 0); 
}

if($('#offer_status').val() == '{{OFFER_REQUEST}}'){
  toastr.info('Please wait for customer offer..');
  var first_bid_time = new Date('{{$first_bid_time}}');
  customerOffer('{{$amount}}', '{{$card_no}}', first_bid_time);
  var amount = '{{$amount}}';
  var card   = '{{$card_no}}';
  if(amount > 0){
    $('.card-input-no').val(card);
    toastr.info('Customer Highest Offer : Rs '+amount+'<br/>Card No : '+card);
  }
}

/*=============== PUSHER START ==================== */

Pusher.logToConsole = false;
var pusher = new Pusher('55845bb31c3702ef4dd0', {
  cluster: 'ap2',
  forceTLS: true
});

var channel = pusher.subscribe('auction-channel');

//bid event
channel.bind('bid-event', function(data) {
  
  if(data.list.sns_auction_detail_id == $('#live_product').val()){
    //set highest bid amount
    var value = parseInt(data.list.bid_amount);
    var amount   = 'Rs ' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    toastr.info('New bid on this item.'+' '+amount);
    $('.tag-highest').text(amount);
    $('#highest_bid_amount').val(data.list.bid_amount); 
    $('#call').val('{{CALL0}}');
    $("#call_bid_btn").html('Call Now');
    $('.call_view').text('');
    //request offer change
    $('#offer_status').val(data.list.auction_detail.request_offer);
    $('.bid-count-show').html('Bid Count : '+data.count.bid_count);
    $('.bid_count').val(data.count.bid_count);

    var rowCount = $('.bid-list-table tbody tr').length;
   
    if(data.list.auction_detail.request_offer == '{{OFFER_EXPIRED}}'){
      $('.requested-notice').addClass('hide');
      $('#call_bid_btn, #sold_btn, #unsold_btn, #card-sold-btn').removeAttr('disabled');
    }
    if(rowCount >= 6){
      $('.bid-list-table tbody tr:last').remove();
    }

    if(data.list.bid_user.customer_card == null){
      var card_no             = '';
      var highest_bidder_name = '';
      var highest_bidder_city = '';
    }else{
      var card_no               = data.list.bid_user.customer_card.card_no;
      online_bidder_card_no     = data.list.bid_user.customer_card.card_no;
      var highest_bidder_name   = data.list.bid_user.fname+' '+data.list.bid_user.lname;
      var highest_bidder_city   = data.list.bid_user.city;
    }
    //set online customer card no automatically
    $('.card-input-no').val(card_no);
    $('.card-holder-name').val(highest_bidder_name);
    $('.card-holder-city').val(highest_bidder_city);

    //ADDING BID TO LIST OF THE ITEM
    $('.bid-list-table > tbody').prepend('<tr>'+
      '<td>'+data.list.bid_user.fname.substring(0, 6)+'... <span class="label label-info card-no">'+card_no+'</span></td>'+
      '<td class="text-right">'+amount+'</td>'+
      '</tr>');
  }
});
 
// offer bid event
channel.bind('offer-event', function(data) {
  $('.offer-amount').html(data.offer.offer_amount);
  $('#offer_amount').val(data.offer.offer_amount);
  $('.card_no').html(data.offer.customer_auction.card_no);
  $('.card-input-no').val(data.offer.customer_auction.card_no);
  toastr.info('New Customer Offer : '+data.offer.offer_amount+'<br/>Card No : '+data.offer.customer_auction.card_no);
});

//offer confirm event
channel.bind('offer-confirm-event', function(data) {
  if(data.list.sns_auction_detail_id == $('#live_product').val()){
   // toastr.success('Customer Offer is Accepted.');
    //set highest bid amount
    var value  = parseInt(data.list.bid_amount);
    var amount = 'Rs ' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    toastr.info('Current Highest Bid : '+' '+amount);

    $('.tag-highest').text(amount);
    $('#highest_bid_amount').val(data.list.bid_amount); 
    
    $("#call_bid_btn").html('Call Now');
    $('.call_view').text('');
    //request offer change
    $('#offer_status').val(data.list.auction_detail.request_offer);
    $('.bid-count-show').html('Bid Count : '+data.count.bid_count);
    $('.bid_count').val(data.count.bid_count);

    var rowCount = $('.bid-list-table tbody tr').length;
   
    if(data.list.auction_detail.request_offer == '{{OFFER_EXPIRED}}'){
      $('.requested-notice').addClass('hide');
      $('#call_bid_btn, #sold_btn, #unsold_btn, #card-sold-btn').removeAttr('disabled');
    }
    
    if(rowCount >= 6){
      $('.bid-list-table tbody tr:last').remove();
    }

    if(data.list.bid_user.customer_card == null){
      var card_no             = '';
      var highest_bidder_name = '' ;
      var highest_bidder_city = '' ;
    }else{
      var card_no             = data.list.bid_user.customer_card.card_no;
      online_bidder_card_no   = data.list.bid_user.customer_card.card_no;
      var highest_bidder_name = data.list.bid_user.fname+' '+data.list.bid_user.lname;
      var highest_bidder_city = data.list.bid_user.city;
    }

    //set online customer card no automatically
    $('.card-input-no').val(card_no);
    $('.card-holder-name').val(highest_bidder_name);
    $('.card-holder-city').val(highest_bidder_city);

    $('.bid-list-table > tbody').prepend('<tr>'+
      '<td>'+data.list.bid_user.fname.substring(0, 6)+'... <span class="label label-info card-no">'+card_no+'</span></td>'+
      '<td class="text-right">'+amount+'</td>'+
      '</tr>');

  }
});

$(document).ready(function(){

  $('body').addClass('fixed sidebar-mini-expand-feature skin-black-light sidebar-collapse');
  $('.time').text(new Date().toISOString().slice(0,10)+' '+new Date().toLocaleTimeString());
  $('.input-group-lg,.highest').removeClass('input-group-sm');
  /** set enter key press for bid button */
  $(document).bind('keypress', function(e) {
      if(e.keyCode==13){
           $('#bid').trigger('click');
       }
  });

  //model box
  $('.more_detail').on('click',function(){
    var model_content = $('.model-content').html();
    
    bid_history = $.confirm({
      title     : 'Item Bid History',
      content   : model_content,
      boxWidth  : '60%',
      closeIcon : true,
      useBootstrap: false,
      buttons: {
        close: {
          btnClass: 'more_details_box',
          action: function(){

          }
        }
      }
    });
  });

  /** Add Bid for this item */
  $('#bid').on('click',function(){
    var $this = $(this);
    //check product is selected
    if($('#live_product').val() > 0){
      $this.button('loading');
      if($('#offer_status').val() == '{{OFFER_REQUEST}}'){
        //$('.bid-list-table tbody tr:first').remove();
        addBid($('#bid_amount').val(),$('#live_product').val(),$('.bid_count').val(),'{{Request::route('id')}}','{{OFFER_APPROVED}}');
        $('.requested-notice').addClass('hide');
        $('.offer-active').addClass('hide');
        $('.show-image').removeClass('hide');
        $('.card-input-no, .card-holder-name, .card-holder-city').val('');
      }else{
        addBid($('#bid_amount').val(),$('#live_product').val(),$('.bid_count').val(),'{{Request::route('id')}}',$('#offer_status').val());
        $('.card-input-no, .card-holder-name, .card-holder-city').val('');
      }
      setTimeout(function() {
        $this.button('reset');
      }, 100);
    }else{
      toastr.warning('Please Select Item First for Enter Bid');
    }
  });

  /** Call 1st 2nd 3rd for this item*/
  $('#call_bid_btn').on('click',function(){
    callBid($('#live_product').val(), $('#call').val());
  });

  /* Search button **/
  $('.search-btn').on('click',function(){
    $('.item_search').val($('.item_search').val()+$(this).text());
    $('.item_search').change();
  });

  /* clear & back & space button*/
  $('.space-btn').on('click',function(){
    $('.item_search').val($('.item_search').val()+' ');
    $('.item_search').change();
  });

  $('.clear-btn').on('click',function(){
    $('.item_search').val('').change().focus();
  });

  $('.back-btn').on('click',function(){
    var string = $('.item_search').val();
    string = string.substr(0, string.length - 1);
    $('.item_search').val(string).change();
  });

  /*amount button */ 
  $('.amount-btn').on('click',function(){
    var $this = $(this);
    //check product is selected
    if($('#live_product').val() > 0){
      $this.button('loading');
      var highest_bid_amount = parseInt($('#highest_bid_amount').val());

      if($('#offer_status').val() == '{{OFFER_DEFAULT}}' || $('#offer_status').val() == '{{OFFER_EXPIRED}}'){
        var bid_amount = highest_bid_amount + parseFloat($(this).val());
      }else{
        var bid_amount = parseFloat($(this).val());
      }

      if($('#offer_status').val() == '{{OFFER_REQUEST}}'){
        $('.bid-list-table tbody tr:first').remove();
        if($('.bid_count').val() == 0){
           addBid(bid_amount,$('#live_product').val(),$('.bid_count').val(),'{{Request::route('id')}}','{{OFFER_REQUEST}}');
           $('.card-input-no, .card-holder-name, .card-holder-city').val('');
        }else{
          addBid(bid_amount,$('#live_product').val(),$('.bid_count').val(),'{{Request::route('id')}}','{{OFFER_APPROVED}}');
          $('.requested-notice').addClass('hide');
          $('.offer-active').addClass('hide');
          $('.show-image').removeClass('hide');
          $('.card-input-no, .card-holder-name, .card-holder-city').val('');
        }
      }else{
        addBid(bid_amount,$('#live_product').val(),$('.bid_count').val(),'{{Request::route('id')}}',$('#offer_status').val());
        $('.card-input-no, .card-holder-name, .card-hold').val('');
      }
      setTimeout(function() {
        $this.button('reset');
      }, 100);
    }else{
      toastr.warning('Please Select Item First for Enter Bid');
    }
  });

  /*amount clear*/
  $('.amount-clear').on('click',function(){
    $('#bid_amount').val('').focus();
  });

  //sold button
  /*amount clear*/
  // $('#sold_btn').on('click',function(){
  //   auctionWinner();
  // });

  //unsold buttoncard-sold-btn
  $('#unsold_btn').on('click',function(){
    $.confirm({
      icon: '',
      title: 'Not selling confirmation',
      content:'Do you want to re-auction this item ?',
      theme: 'bootstrap',
      closeIcon: true,
      animation: 'scale',
      type: 'blue',
      buttons: {
        'confirm': {
          text: 'Yes, Reauction',
          btnClass: 'btn-green',
          action: function () {
            $.ajax({
              url    : "{{URL::to('auction/reject')}}",
              method : 'GET',
              data   : {
                'detail_id'   : $('#live_product').val(),
                'comment'     : 'ITEM UNSOLD',
                'product_id'  : $('#product_id').val()
              },
              async: false,
              success: function (data) {
                toastr.success('Item reauction');
                location.reload();
              },error: function () {
                toastr.error('Error Occured !..Try Again');
              }
            });
          }
        }
      }
    });
  });

  //card submit
  $('.card-sold-btn').on('click',function(){
      if($('.card-input-no').val() != ''){
        itemSold($('.card-input-no').val());
      }else{
        toastr.error('Please Enter Card No');
      }
  });

  //Search item & bidding started
  $('.search_btn').on('click',function(){
    $.ajax({
      url    :'{{URL('auction/load-item')}}',
      method :'GET',
      data   :{
        'auction_id': "{{Request::route('id')}}", //auction id
        'search'    : $('.item_search').val()
      },
      async  :false,
      success:function(data){
        if(data.item){
          var item_live = liveItem("{{Request::route('id')}}");
          $('.item_search').val('');
          if(item_live > 0){
            var code = $('.tag').text();
            toastr.warning(code+' - Item now in the live');
          }else{
            $('.tag').text(data.item.item_code); //item code
            $('.item_name').text(data.item.name); //item name
            var item_details = getNewProduct("{{Request::route('id')}}", data.item.id);
            $('#live_product').val(item_details.detail_id);//auction product detail id
            $('.reserved-price').text(item_details.start_bid_price);//auction product reserved price
            $('.withdrawal-price').text(item_details.withdraw_amount);//auction product start price
            if(data.item.supplier != null){
              $('.supplier').text(data.item.supplier);
            }else{
              $('.supplier').text('-');
            }
            if(item_details.image_path != null){
              $('.item-img').attr('src','{{url('core/storage/uploads/images/item/')}}/'+item_details.image_path+'');
            }else{
              $('.item-img').attr('src','{{asset('assets/images/empty.jpg')}}');
            }
            addBid(item_details.start_bid_price, item_details.detail_id, $('.bid_count').val(),'{{Request::route('id')}}', '{{OFFER_APPROVED}}');
            toastr.info(data.item.item_code+' - Item is selected.<br/>Bidding started.');
            window.scrollTo(0,0);
          }
        }else{
           toastr.warning('Cannot find item');
           $('.item_search').val('');
        } 
      },error:function(){
        toastr.error('Error occurred....Please try again');
      }
    });
  });

  //offer approve
  $('.approve-offer').on('click', function(){

    if($('#offer_amount').val() > 0){
      
      addBid($('#offer_amount').val(), $('#live_product').val(), $('.bid_count').val(), '{{Request::route('id')}}', '{{OFFER_APPROVED}}');
      $('.requested-notice').addClass('hide');
      $('.offer-active').addClass('hide');
      $('.show-image').removeClass('hide');
    }else{
      toastr.error('No offers from customers');
    }
  });

  //Online Bidding Active & Inactive Status Change
  $('.hold-online-bidding').on('change', function(e) {
    var $this         = $(this);
    var auction_id    = $(this).data('auction');
    if(this.checked) {
      $(this).val(1);
    }else{
      $(this).val(0);
    }
    var status = $(this).val();
    $.ajax({
      url   : "{{URL::to('auction/hold-bidding')}}",
      method: 'GET',
      data  : {
        'auction_id': auction_id,
        'status'    : status
      },
      async : false,
      success : function (data) {
        if(status == '{{BIDDING_ACTIVE}}'){
          toastr.success('{{ONLINE_BID_UNHOLD_MESSAGE}}');
        }else{
          toastr.warning('{{ONLINE_BID_HOLD_MESSAGE}}');
        }
      },error: function () {
        toastr.error('Error Occurred !..Try Again');
      }
    });
  });
  //Show & Hide Online Bidder Name
  $('.show-hider-bidder').on('change', function(e) {
    var $this         = $(this);
    var auction_id    = $(this).data('auction');
    if(this.checked) {
      $(this).val(1);
    }else{
      $(this).val(0);
    }
    var status = $(this).val();
    $.ajax({
      url   : "{{URL::to('auction/show-hide-bidder')}}",
      method: 'GET',
      data  : {
        'auction_id': auction_id,
        'status'    : status
      },
      async : false,
      success : function (data) {
        if(status == '{{BIDDING_ACTIVE}}'){
          toastr.success('Showing online bidder name');
        }else{
          toastr.warning('Hide online bidder name');
        }
      },error: function () {
        toastr.error('Error Occurred !..Try Again');
      }
    });
  });
  //End 
});

//Reauction item for non-paid items
channel.bind('item-reauction-event', function(data){
  //check reauction item in current live auction
  if(data && data.sns_auction_id == "{{Request::route('id')}}"){
    toastr.warning('Item code '+data.item.item_code+' can re-auction.');
    $('#item'+data.sns_item_id+'').removeClass('btn-warning').addClass('btn-danger');
    $('#item'+data.sns_item_id+'').attr('data-original-title', 'Reauction Item');
    $('#item'+data.sns_item_id+'').attr('onClick', 'itemReauction('+data.sns_item_id+');');
    $('#card'+data.sns_item_id+'').removeClass('label label-info');
    $('#card'+data.sns_item_id+'').html('-');
  }
})

//get live product
function liveItem(auction_id){
  var live_item;
  $.ajax({
    url    :'{{URL('auction/live-item')}}',
    method :'GET',
    data   :{'auction_id':auction_id},
    async  :false,
    success:function(data){
       live_item = data.item_live;
    },error:function(){
      live_item = 0;
    }
  });
  return live_item;
}

//get new product details
function getNewProduct(auction_id,product_id){
  var new_item_details = [];
  $.ajax({
    url    :'{{URL('auction/item-details')}}',
    method :'GET',
    data   :{'auction_id':auction_id,
             'product_id':product_id
            },
    async  :false,
    success:function(data){
       new_item_details = data.new_item_details;
    },error:function(){
      new_item_details = [];
    }
  });
  return new_item_details;
}

//add bid
function addBid(amount, detail_id, bid_count, auction, offer_status){
  var bid_time = null;
  $.ajax({
    url   : "{{URL::to('auction/bid')}}",
    method: 'GET',
    data  : {
       'detail_id'    : detail_id,
       'amount'       : amount,
       'auction'      : auction,
       'bid_count'    : bid_count,
       'offer_status' : offer_status,
       'bidder_type'  : '{{FLOOR_BIDDER}}' 
      },
    async: true,
    success: function (data) {
      var html = '';
      if(data.list != 0){
        $('#call').val('{{CALL0}}');
        $("#call_bid_btn").html('Call Now');
        $('.bid-list').addClass('hide');
        $('#bid_amount').val('');
        if(data.list.auction_detail.request_offer == '{{OFFER_REQUEST}}'){
          $('.requested-notice').removeClass('hide');
          toastr.info('Please wait for customer offers...');
          customerOffer('0.00', '-', new Date());
        }
      }else{
        toastr.error('Your bid is not enough.');
        $('#bid_amount').select();
      }
    },error: function () {
      $.alert({
        title: 'Oooops !',
        type: 'red',
        content: 'Error Occured..Try Again',
      });
    }
  });
}

//call bid.
function callBid(detail_id, call){
  //1st call
  if(call == '{{CALL0}}'){
    /*$.confirm({
      title: '1st Call Confirmation',
      content: '',
      theme: 'bootstrap',
      closeIcon: true,
      animation: 'scale',
      type: 'blue',
      buttons: {
        confirm: function () {*/
          var call_status = callToServer(detail_id, call);
          if(call_status.call_status == '{{CALL1}}'){
            $('#call').val(call_status.call_status);
            $(".call_view").text('1st CALL');
            $("#call_bid_btn").html('2nd CALL');
            $('#request_offer').hide();
          }else{
            $.confirm({
              title: 'Important !',
              content: 'No Bids for this item',
              buttons: {
                close: function () {
                  var $this = $('#call_bid');
                  $this.button('reset');
                }
              }
            });
          }
        /*},
        cancel: function () {}
      }
    });*/
  //2nd call
  }else if(call == '{{CALL1}}'){
    // $.confirm({
    //   title: '2nd Call Confirmation',
    //   content: '',
    //   theme: 'bootstrap',
    //   closeIcon: true,
    //   animation: 'scale',
    //   type: 'blue',
    //   buttons: {
    //     confirm: function () {
            var call_status = callToServer(detail_id,call);
            if(call_status.call_status == '{{CALL2}}'){
              $('#call').val(call_status.call_status);
              $(".call_view").text('2nd CALL');
              $("#call_bid_btn").html('Final CALL');
              $('#request_offer').hide();
            }
    //     },
    //     cancel: function () {}
    //   }
    // });
  //3rd call
  }else if(call == '{{CALL2}}'){
    $.confirm({
      title: 'Final Call Confirmation',
      content: '',
      theme: 'bootstrap',
      closeIcon: true,
      animation: 'scale',
      buttons: {
        confirm: function () {
          var call_status = callToServer(detail_id,call);
          if(call_status.call_status == '{{CALL3}}'){
              $('#call').val(call_status.call_status);
              $(".call_view").text('Final Call');
              auctionWinner(call_status.detail.bidder.fname, 0);
         }
        },cancel: function () {}
      }
    });
  }
  return 1;
}

/*call server (1st call,2nd call and final call)*/
function callToServer(detail_id,call){
  var return_status = '';
  $.ajax({
    url: "{{URL::to('auction/call-bid')}}",
    method: 'GET',
    data: {'detail_id': detail_id,
            'call':call
          },
    async: false,
    success: function (data) {
      return_status = data.call;
    },error: function () {
      toastr.error('Error Occured !..Try Again');
    }
  });
  return return_status;
}

//This function is used to get auction winner
function auctionWinner(highest_bidder_name, card_no){
  online_bidder_card_no = '{{$highest_bidder_card_no}}';
  if(online_bidder_card_no == ''){
    online_bidder_card_no = $('.bid-list-table > tbody > tr:first > td:first').find('span').html();
  }
  if(card_no > 0){
    online_bidder_card_no = card_no;
  }
  var content = '<table class="table table-hover" id="myTable">'+
                 '<tbody>'+
                 '<tr><td style="width:50%;">Item Code</td><td><strong>'+$('.tag').text()+'</strong></td></tr>'+
                 '<tr><td>Item Name</td><td><strong>'+$('.item_name').text()+'</strong></td></tr>'+
                 '<tr><td>Reserved Price</td><td><strong>'+$('.reserved-price').text()+'</strong></td></tr>'+
                 '<tr><td>Highest Bid Price</td><td><strong>'+$('.tag-highest').text()+'</strong></td></tr>'+
                 '<tr><td>Highest Bidder</td><td><strong>'+highest_bidder_name+'</strong></td></tr>'+
                 '<tr><td>Card No</td><td><input type="number" min="0" class="form-control" id="customer_card_no" placeholder="Card No" value="'+online_bidder_card_no+'"></td></tr>'+
                 '<tr><td colspan="2"><textarea class="form-control" id="comment" rows="2" placeholder="Write Auctioneer Review"></textarea></td></tr>'+
                 '</tbody>' 
                +'</table>';
  //if(online_bidder_card_no != ''){
    var winner_confirm = $.confirm({
      icon      : '',
      title     : 'Final Confirmation',
      content   : content,
      theme     : 'bootstrap',
      closeIcon : false,
      animation : 'scale',
      type      : 'blue',
      buttons   : {
        'confirm'  : {
          text     : 'Approve',
          btnClass : 'btn-blue final-confirm-btn',
          action   : function () {
            var self = this;
            if(self.$content.find('#customer_card_no').val() > 0){
              $.ajax({
                url    : "{{URL::to('auction/approve')}}",
                method : 'GET',
                data   : {
                  'detail_id'       : $('#live_product').val(),
                  'comment'         : $('#comment').val(),
                  'customer_card_no': $('#customer_card_no').val(),
                  'auction_id'      : "{{ Request::route('id')}}"
                },
                async  : false,
                success: function (data) {
                  if(data.status == true){
                    reloadPage('Item approved for sale.', 'success');
                  }else{
                    toastr.error('Please Enter Valid Card No');
                    winner_confirm.open();
                  }
                },error: function () {
                  toastr.error('Error Occured !..Try Again');
                  winner_confirm.open();
                }
              });
            }else{
              toastr.error('Please Enter Valid Card No');
              return false;
            }
          },
        },
        'no': {
          text:'Reject',
          action:function () {
            var self = this;
            // if(self.$content.find('#customer_card_no').val() > 0){
              $.ajax({
                url   : "{{URL::to('auction/reject')}}",
                method: 'GET',
                data  : {
                  'detail_id': $('#live_product').val(),
                  'comment'  : $('#comment').val(),
                },
                async  : false,
                success: function (data) {
                  reloadPage('Item not approved for sale', 'error');
                },error: function () {
                  toastr.error('Error Occured !..Try Again');
                  winner_confirm.open();
                }
              });
            // }else{
            //   toastr.error('Please Enter Valid Card No');
            //   return false;
            // }
          }
        }
      }
    });
  //}
}

//VALIDATE FINAL CONFIRM
$(document).on('.final-confirm-btn', 'click', function(){
  console.log($('#customer_card_no').val());
});

//This function is used to get item reauction
function itemReauction(product_id){
  var reauction_confirm = $.confirm({
    icon      : '',
    title     : 'Item Reauction',
    content   : 'Do you want to reauction this item ?',
    theme     : 'bootstrap',
    closeIcon : true,
    animation : 'scale',
    type      : 'blue',
    buttons   : {
      'confirm'  : {
        text     : 'Yes',
        btnClass : 'btn-blue',
        action   : function () {
          var item_live    = liveItem("{{Request::route('id')}}");
          if(item_live > 0){
            var code = $('.tag').text();
            bid_history.close();
            toastr.warning(code+' - Item now in the live');
          }else{
            var item_details = getNewProduct("{{Request::route('id')}}", product_id);
            $('.tag').text(item_details.item_code);
            $('.item_name').text(item_details.name);
            $('#live_product').val(item_details.detail_id);
            $('.reserved-price').text(item_details.start_bid_price);
            $('.withdrawal-price').text(item_details.withdraw_amount);
            if(item_details.image_path != null){
              $('.item-img').attr('src','{{url('core/storage/uploads/images/item/')}}/'+item_details.image_path+'');
            }else{
              $('.item-img').attr('src','{{asset('assets/images/empty.jpg')}}');
            }
            addBid(item_details.start_bid_price, item_details.detail_id, $('.bid_count').val(),'{{Request::route('id')}}', '{{OFFER_APPROVED}}');
            toastr.info(item_details.item_code+' - Item is selected.<br/>Bidding started.');
            $('#bid_amount').focus();
            $('.more_details_box').click();
          }
        }
      },
      No: function () {
        $('#bid_amount').focus();
      }
    }
  });
}
//This function is used to get won item details
function winItem(product_id){
  
  $.ajax({
    url    : "{{URL::to('auction/won_item')}}",
    method : 'GET',
    data   : {
      'product_id': product_id
    },
    async   : false,
    success : function (data) {
      location.reload(true);
    },error : function () {
      toastr.error('Error Occured !..Try Again');
    }
  });
  var content = '<table class="table table-hover">'+
                 '<tbody>'+
                 '<tr><td style="width:50%;">Item Code</td><td><strong>'+$('.tag').text()+'</strong></td></tr>'+
                 '<tr><td>Item Name</td><td><strong>'+$('.item_name').text()+'</strong></td></tr>'+
                 '<tr><td>Reserved Price</td><td><strong>'+$('.reserved-price').text()+'</strong></td></tr>'+
                 '<tr><td>Last Bid Price</td><td><strong>'+$('.tag-highest').text()+'</strong></td></tr>'+
                 '<tr><td>Highest Bidder</td><td><strong>'+$('.bid-list-table > tbody > tr:first > td:first').html()+'</strong></td></tr>'+
                 '<tr><td colspan="2"><textarea class="form-control" id="comment" rows="2" placeholder="Write Auctioneer Review"></textarea></td></tr>'+
                 '</tbody>' 
                +'</table>';
  $.confirm({
    icon      : '',
    title     : 'Won Item Information',
    content   :content,
    theme     : 'bootstrap',
    closeIcon : false,
    animation : 'scale',
    type      : 'blue',
    buttons   : {
      'confirm': {
        text     : 'Ok',
        btnClass : 'btn-blue',
        action   : function () {
          
        },
      },
      'no': {
        text  :'Reject',
        action:function () {
          $.ajax({  
            url: "{{URL::to('auction/reject')}}",
            method: 'GET',
            data: {
              'detail_id': $('#live_product').val(),
              'comment'  : $('#comment').val(),
            },
            async: false,
            success: function (data) {
              location.reload(true);
            },error: function () {
              toastr.error('Error Occured !..Try Again');
            }
          });
        }
      }
    }
  });
}

function itemSold(card_no){
  var call_status = callToServer($('#live_product').val(), '{{CALL2}}');
  if(call_status.call_status == '{{CALL3}}'){
      $('#call').val(call_status.call_status);
      $(".call_view").text('Final Call');
      auctionWinner(call_status.detail.bidder.fname, card_no);
  }
}

function customerOffer(offer_amount, card_no, end_time){
  var date1, date2, confirm;
  date1 = new Date(end_time.setDate(end_time.getDate()));
  date1 = date1.setMinutes(date1.getMinutes() + 1);
  var austDay = new Date(date1);
  $('#defaultCountdown').countdown({until: austDay, padZeroes: true, onExpiry: liftOff, onTick: watchCountdown});
  $('.offer-amount').html(offer_amount);
  $('.card_no').html(card_no);
  $('#offer-amount').val(offer_amount);
  $('.show-image').addClass('hide');
  $('.offer-active').removeClass('hide');
}

function liftOff() { 
  //toastr.info('Waiting for auctioneer decesion');
}

function watchCountdown(periods) {
    if(periods[4] == 0){
        if(periods[5] > 0){
            $('#monitor').text('Just ' + periods[5] + ' minutes and ' + periods[6] + ' seconds to go');
        }else{
            $('#monitor').text('Just ' + periods[6] + ' seconds to go');
        }
    }
} 
// reload page function 
//param message & message type
function reloadPage(message, message_type){
  
  if(message_type == 'success'){
    toastr.success(message);
  }else if(message_type == 'error'){
    toastr.error(message);
  }else if(message_type == 'notice'){
    toastr.info(message);
  }else{
    toastr.warning(message);
  }
  location.reload();

}

</script>
@stop

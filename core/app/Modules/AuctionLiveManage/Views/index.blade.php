@extends('layouts.back_master') @section('title','On Live Auction')
@section('css')
<!-- toaster -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" type="text/css" />
<!-- toaster -->
@stop
<!-- Content Header (Page header) -->
@section('content')
<section class="content-header">
	<h1>
	Live Auction
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('auction/list')}}}">Auction List</a></li>
		<li class="active">On Live Auction</li>
	</ol>
</section>
<!-- Main content -->
<section class="content" ng-app="angularApp" ng-controller="AuctionLiveController">
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-body detail-well">
                    <center><iframe width="600" height="300" src="{{$auction_detail[0]->auction_url}}?autoplay=1" frameborder="0"></iframe></center>
                    <div class="jcarousel-wrapper"><!-- Image Slider Begin -->
                        <div class="jcarousel">
                            <div class="loading">Loading...</div>
                        </div>
                        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                    </div><!-- End Slider -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="box-body detail-well">
                        <center><h4>Auction</h4></center>
                        <div class="">
                            <label class="control-label">Auction :</label>
                            <label>{{$auction_detail[0]->event_name}}</label>  
                        </div>
                        <div class="">
                            <label class="control-label">Venue : @if($auction_detail[0]->location != null){{$auction_detail[0]->location->name}}@else - @endif</label>
                        </div>
                        <div class="">
                            <label class="control-label">Date | Start Time : {{$auction_detail[0]->auction_date}} | {{$auction_detail[0]->auction_start_time}}</label>
                        </div>
                        <input type="hidden" id="auction_id" value="{{$auction_detail[0]->id}}">
                        <input type="hidden" id="bid_count" value="{{$bid_count}}">
                        @if(!empty($select))
                            <input type="hidden" id="call" value="{{$select[0]->call}}">
                            <center><h4>Item <button type="submit" data-toggle="tooltip" data-placement="top" title="Pick More Item" class="btn btn-sm btn-default pull-right pick"><i class="fa fa-hand-o-left"></i></button></h4></center>
                            <div>
                                <input type="hidden" id="detail_id" value="{{$select[0]->detail_id}}">
                                <label class="control-label">Item | Code :</label>
                                <label class="item-code">{{$select[0]->item_name}} | {{$select[0]->item_code}}</label>
                            </div>
                            <div>
                                <label class="control-label">Start BID Price(Rs) :</label>
                                <label class="start-bid">{{$select[0]->start_bid_price}}</label>
                            </div>
                            <div>
                                <label class="control-label">Withdrwal BID Price(Rs) :</label>
                                <label class="withdrawal-bid">{{$select[0]->withdraw_amount}}</label>
                            </div>
                            <div class="win_area hide">
                                <center><h4>Winning Bid Price</h4></center>
                                <div class="form-group">
                                    <center><label id="winning_bid"></label></center>
                                </div>
                            </div>
                            <div class="function-area">
                                <center>
                                    <button type="button" class="btn btn-sm bg-purple" id="call_bid" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing"><i class="fa fa-bullhorn"></i>
                                    @if($select[0]->call == 0) 1st CALL @elseif($select[0]->call == 1) 2nd CALL @elseif($select[0]->call == 2) FINAL CALL @endif</button>
                                    <button type="button" class="btn btn-sm bg-purple" id="stop"><i class="fa fa-ban"></i> Stop </button>
                                </center>
                                <center><h4>Our Bidding</h4></center>
                                <div class="form-group">
                                   <!--  <center><input type="text" class="form-control input-sm"  name="bid" id="bid_value" placeholder="Enter BID"></center> -->
                                    <div class="input-group">
                                        <div class="input-group-addon">Rs.</div>
                                        <input type="text" class="form-control" ng-model="fee" name="fee" placeholder="Amount" id="bid_value" price>
                                    </div>
                                </div>
                                <center><button type="submit" class="btn btn-sm bg-purple" id="bid" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Bid Processing"><i class="fa fa-gavel"></i> BID</button></center>
                            </div>
                        @else
                            <div class="pick-item"><center><h4>Select Item Here...  <button type="submit" data-toggle="tooltip" data-placement="top" title="Select Item" class="btn btn-sm btn-default pull-right pick"><i class="fa fa-hand-o-left"></i></button></h4></center></div>
                        @endif
                        <div class="item-details hide">
                            <input type="hidden" id="call">
                            <center><h4>Item<button type="submit" data-toggle="tooltip" data-placement="top" title="Pick More Item" class="btn btn-sm btn-default pull-right pick"><i class="fa fa-hand-o-left"></i></button></h4></center>
                            <input type="hidden" id="detail_id">
                            <div>
                                <label class="control-label">Item | Code :</label>
                                <label class="item-code"></label>
                            </div>
                            <div>
                                <label class="control-label">Start BID Price(Rs) :</label>
                                <label class="start-bid"></label>
                            </div>
                            <div>
                                <label class="control-label">Withdrwal BID Price(Rs) :</label>
                                <label class="withdrawal-bid"></label>
                            </div>
                            <div class="win_area hide">
                                <center><h4>Winning Bid Price(Rs)</h4></center>
                                <div class="form-group">
                                    <center><label id="winning_bid"></label></center>
                                </div>
                            </div>
                            <div class="function-area">
                                <center>
                                    <button type="button" class="btn btn-sm bg-purple" id="call_bid" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing"><i class="fa fa-bullhorn"></i> 1st CALL</button>
                                    <button type="button" class="btn btn-sm bg-purple" id="stop"><i class="fa fa-ban"></i> Stop </button>
                                </center>
                                <center><h4>Our Bidding</h4></center>
                                <div class="form-group">
                                    <!-- <center><input type="text" class="form-control input-sm"  name="bid" id="bid_value" placeholder="Enter BID"></center> -->
                                    <!-- <center>
                                        <div class="input-group-addon">Rs.</div>
                                        <input type="text" class="form-control" name="fee" id="bid_value" placeholder="Amount">
                                    </center> -->
                                    <div class="input-group">
                                        <div class="input-group-addon">Rs.</div>
                                        <input type="text" class="form-control" ng-model="fee" name="fee" placeholder="Amount" id="bid_value" price>
                                    </div>
                                </div>
                                <center><button type="submit" class="btn btn-sm bg-purple" id="bid" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Bid Processing"><i class="fa fa-gavel"></i> BID</button></center>
                            </div>
                        </div>
                        @if(!empty($select))
                            <div class="control-label bid-label">
                               <center><h3><div class="highest_bid">@if($select[0]->id != '') Rs {{number_format($select[0]->bid_amount,2)}} @else No Bids @endif </div></h3></center>
                            </div>
                        @else
                        <div class="control-label bid-label">
                            <center><h3><div class="highest_bid"></div></i></h3></center>
                        </div>
                        @endif
                        @if(!empty($select))
                            @if($select[0]->call == 3)
                                <div class="form-group approve-section">
                                    <center>
                                        <button type="button" class="btn btn-sm pull-left" id="approve" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing"><i class="fa fa-check"></i> Approve</button>
                                        <button type="button" class="btn btn-sm pull-right" id="reject"><i class="fa fa-ban"></i> Reject </button>
                                    </center>
                                </div>
                            @endif
                            <div class="form-group approve-section hide">
                                <center>
                                    <button type="button" class="btn btn-sm pull-left" id="approve" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing"><i class="fa fa-check"></i> Approve</button>
                                    <button type="button" class="btn btn-sm pull-right" id="reject"><i class="fa fa-ban"></i> Reject </button>
                                </center>
                            </div>
                            <div class="form-group approved hide">
                                <center><h3><span style="color:green;border:1px solid beige;padding:2px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Approved</span></h3></center>
                            </div>
                            <div class="form-group rejected hide">
                                <center><h3><span style="color:red;border:1px solid beige;padding:2px;"><i class="fa fa-thumbs-down" aria-hidden="true"></i> Rejected</span></h3></center>
                            </div>
                        @else
                            <div class="form-group approve-section hide">
                                <center>
                                    <button type="button" class="btn btn-sm pull-left" id="approve" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Call Processing"><i class="fa fa-check"></i> Approve</button>
                                    <button type="button" class="btn btn-sm pull-right" id="reject"><i class="fa fa-ban"></i> Reject </button>
                                </center>
                            </div>
                            <div class="form-group approved hide">
                                <center><h3><span style="color:green;border:1px solid beige;padding:2px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Approved</span></h3></center>
                            </div>
                            <div class="form-group rejected hide">
                                <center><h3><span style="color:red;border:1px solid beige;padding:2px;"><i class="fa fa-thumbs-down" aria-hidden="true"></i> Rejected</span></h3></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    <div>
    <div class="form-group">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-striped table-condensed table-bordered table-responsive bid-list-table">
                        <thead>
                            <tr>
                                <th>Item | Item Code</th>
                                <th>Bidding Amount(Rs)</th>
                                <th>Bidder</th>
                                <th>Bid Time</th>
                            </tr>
                        </thead>
                        <tbody id="bid-list">
                            @if(count($bid_list) > 0 && !empty($bid_list))
                                @foreach($bid_list as $bid_list_object)
                                    <tr>
                                        <td>{{$bid_list_object->auction_detail->item->name}} | {{$bid_list_object->auction_detail->item->item_code}}</td>
                                        <td class="text-right">{{number_format($bid_list_object->bid_amount,2)}}</td>
                                        <td>{{$bid_list_object->bidder->first_name}} {{$bid_list_object->bidder->last_name}}</td>
                                        <td>{{$bid_list_object->bid_time}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="no-data"><td colspan="6" class="text-center">No data found.</td></tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($bid_list) > 0)
                        <div class="box-footer">      
                            <div style="float: right;">{!! $bid_list->render() !!}</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<!-- item list - before -->
<div class="item-list hide">
    <table class="table table-striped table-condensed table-bordered table-responsive">
        <thead>
            <tr>
                <th>Item Code</th>
                <th>Item</th>
                <th>Starting BID(Rs)</th>
                <th>Withdrawal BID(Rs)</th>
                <th>Order</th>
                <th>Current BID(Rs)</th>
                <th>Winning BID(Rs)</th>
                <th>Winner</th>
                <th>Status</th>
                <!-- <th width="15%">Starting BID(Rs)</th> -->
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($auction_detail[0]->auction_details as $auction_detail_object)
            <tr>
                <td>{{$auction_detail_object->item->item_code}}</td>
                <td>{{$auction_detail_object->item->name}}</td>
                <td class="text-right">{{number_format($auction_detail_object->start_bid_price,2)}}</td>
                <td class="text-right">{{number_format($auction_detail_object->withdraw_amount,2)}}</td>
                <td>{{$auction_detail_object->order}}</td>
                @if($auction_detail_object->auction_bid != null)
                <td class="text-right">{{number_format($auction_detail_object->auction_bid->bid_amount,2)}}</td>
                @else
                <td></td>
                @endif
                @if(!empty($auction_detail_object->auction_bid->win_bid))
                <td class="text-right">{{number_format($auction_detail_object->auction_bid->win_bid->win_amount,2)}}</td>
                <td>{{$auction_detail_object->auction_bid->win_bid->winner->first_name}} {{$auction_detail_object->auction_bid->win_bid->winner->last_name}}</td>
                @else
                <td></td>
                <td></td>
                @endif
                @if($auction_detail_object->item_approve != null && $auction_detail_object->item_approve->status != 1)
                    <td class="text-center"><span class=""><i class="fa fa-trash" aria-hidden="true"></i> Rejected</span></td>
                @else
                    @if($auction_detail_object->status == 0 || $auction_detail_object->status == 1)
                      <td class="text-center"><span class=""><i class="fa fa-gavel" aria-hidden="true"></i> To BID</span></td>
                    @elseif($auction_detail_object->status == 2)
                      <td class="text-center"><span class=""><i class="fa fa-bullhorn" aria-hidden="true"></i> Bidding</span></td>
                    @else
                       <td class="text-center"><span class=""><i class="fa fa-trophy" aria-hidden="true"></i> Win</span></td>
                    @endif
                @endif    
                <!-- <td>
                    <div class="">
                        <input type="text" placeholder="Start Price" class="form-control input-sm start_amount" width="50%" value="{{number_format($auction_detail_object->start_bid_price,2)}}" />
                    </div>
                </td> -->
                @if($auction_detail_object->status == 0)
                   <td class="text-center"><a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="" onclick="startBid({{ $auction_detail_object->id }},{{$auction_detail[0]->id}})"><i class="fa fa-play" aria-hidden="true"></i></a></td>
                @elseif($auction_detail_object->status == 1)
                   <td class="text-center"><a href="javascript:void(0);" class="btn btn-xs btn-default not-active" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-play" aria-hidden="true"></i></a></td>
                @elseif($auction_detail_object->status == 2)
                  <td class="text-center"><a href="javascript:void(0);" class="btn btn-xs btn-default not-active" data-toggle="tooltip" data-placement="top" title="Bidding" onclick="startBid({{ $auction_detail_object->id}},{{$auction_detail[0]->id}})"><i class="fa fa-pause" aria-hidden="true"></i></a></td>
                @else
                   <td class="text-center"><a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Bid History" onclick="itemHistory({{ $auction_detail_object->id}},{{$auction_detail[0]->id}})"><i class="fa fa-video-camera" aria-hidden="true"></i></a></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop
@section('js')
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<!-- toaster -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!-- toaster -->
<script type="text/javascript">
    /*start pusher*/
   // Pusher.logToConsole = true;
    var pusher = new Pusher('bd44e8ba54aa707e490d', {
      encrypted: true
    });
    var channel = pusher.subscribe('auction-channel');
    channel.bind('bid-event', function(data) {
        toastr.info('New bid on this item.');
        $('.highest_bid').html('Rs '+data[0].bid_amount);
        $('#call').val(0);
        $("#call_bid").html('<i class="fa fa-bullhorn"></i> 1st CALL');
        $('.bid-list-table > tbody > tr:first').before('<tr>'+
                              '<td>'+data[0].auction_detail.item.name+' | '+data[0].auction_detail.item.item_code+'</td>'+
                              '<td class="text-right">'+data[0].bid_amount+'</td>'+
                              '<td>'+data[0].bidder.first_name+' '+data[0].bidder.last_name+'</td>'+
                              '<td>'+data[0].bid_time+'</td>'+
                            '</tr>');
    });
    /*end pusher*/
    var app = angular.module('angularApp',[]);
    app.controller('AuctionLiveController', function() {});
    app.directive('price', [function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                attrs.$set('ngTrim', "false");
                
                var formatter = function(str, isNum) {
                    str = String( Number(str || 0) / (isNum?1:100) );
                    str = (str=='0'?'0.0':str).split('.');
                    str[1] = str[1] || '0';
                    return str[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + '.' + (str[1].length==1?str[1]+'0':str[1]);
                }
                var updateView = function(val) {
                    scope.$applyAsync(function () {
                        ngModel.$setViewValue(val || '');
                        ngModel.$render();
                    });
                }
                var parseNumber = function(val) {
                    var modelString = formatter(ngModel.$modelValue, true);
                    var sign = {
                        pos: /[+]/.test(val),
                        neg: /[-]/.test(val)
                    }
                    sign.has = sign.pos || sign.neg;
                    sign.both = sign.pos && sign.neg;
                    
                    if (!val || sign.has && val.length==1 || ngModel.$modelValue && Number(val)===0) {
                        var newVal = (!val || ngModel.$modelValue && Number()===0?'':val);
                        if (ngModel.$modelValue !== newVal)
                            updateView(newVal);
                        
                        return '';
                    }
                    else {
                        var valString = String(val || '');
                        var newSign = (sign.both && ngModel.$modelValue>=0 || !sign.both && sign.neg?'-':'');
                        var newVal = valString.replace(/[^0-9]/g,'');
                        var viewVal = newSign + formatter(angular.copy(newVal));

                        if (modelString !== valString)
                            updateView(viewVal);

                        return (Number(newSign + newVal) / 100) || 0;
                    }
                }
                var formatNumber = function(val) {
                    if (val) {
                        var str = String(val).split('.');
                        str[1] = str[1] || '0';
                        val = str[0] + '.' + (str[1].length==1?str[1]+'0':str[1]);
                    }
                    return parseNumber(val);
                }
                
                ngModel.$parsers.push(parseNumber);
                ngModel.$formatters.push(formatNumber);
            }
        };
    }]);
    /** set enter key press for bid button */
    $(document).bind('keypress', function(e) {
        if(e.keyCode==13){
             $('#bid').trigger('click');
         }
    });
    /**
    * Auto Run
    * */
    /*setInterval(function() {
       liveUpdate($('#detail_id').val(),$('#bid_count').val());
    },5000);*/

    /** Display auction item list */
    var content = $('.item-list').html();
    var auction_status = '{{$auction_detail[0]->auction_status}}';
    if(auction_status == 1){
        var title = 'Item List - Select item for this Auction';
    }else{
        var title = 'Summary';
    }
    var model = $.confirm({
        title: title,
        theme: 'material',
        content: content,
        columnClass: 'col-md-12',
        closeButtonClass: 'btn-danger',
        onContentReady: function () {},
        buttons: {
            close: function () {
                
            }
        }
    });

    /*** Image Slider Begin */
    var jcarousel = $('.jcarousel').jcarousel();
    @if(!empty($img))
        var json = JSON.parse('<?php echo $img?>');
        var html = '<ul>';
        if(json.length > 0){
            $.each(json, function() {
                html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/'+this.image_path+'" alt="No Images !"></li>';
            });
        }else{
            html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/empty.jpg" alt="No Images !"></li>';
        }
        html += '</ul>';
        // Append items
        jcarousel.html(html);
        // Reload carousel
        jcarousel.jcarousel('reload');
    @endif
    /** Image Slider End */

    //This function is used to get item to start bidding...
    function startBid(detail_id,auction_id){
        model.close();
        $(".box").addClass('panel-refreshing');
        $('.item-details').removeClass('hide');
        $('.pick-item').addClass('hide');
        $('#auction_id').val(auction_id);
        $.ajax({
            url: "{{URL::to('auction/select')}}",
            method: 'GET',
            data: {'detail_id': detail_id,
                   'auction_id':auction_id},
            async: false,
            success: function (data) {
                $(".box").removeClass('panel-refreshing');
                $('.item-code').html(data.detail[0].item.item_code +'|'+ data.detail[0].item.name);
                $('.start-bid').html(data.detail[0].start_bid_price);
                $('.withdrawal-bid').html(data.detail[0].withdraw_amount);
                $('#detail_id').val(data.detail[0].id);
                $('#call').val(data.detail[0].call);
                $('.highest_bid').html('No Bids');
                var html = '<ul>';
                if(data.img.length > 0){
                    $.each(data.img, function() {
                        html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/'+this.image_path+'" alt="No Images !"></li>';
                    });
                }else{
                    html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/empty.jpg" alt="No Images !"></li>';
                }
                html += '</ul>';
                jcarousel.html(html);
                jcarousel.jcarousel('reload');
            },
            error: function () {
                alert('error');
            }
        });
    }//end start bid
    //select next item
    $('.pick').on('click',function(){
        $('#bid_count').val(0);
        location.reload();
    });

    /** Add Bid for this item */
    $('#bid').on('click',function(){
      var $this = $(this);
      $this.button('loading');
      addBid($('#bid_value').val(),$('#detail_id').val(),$('#bid_count').val(),$('#auction_id').val());
      $('#bid_value').val('');
    });
    //This function is used to auto update
    function liveUpdate(detail_id,bid_count){
        $.ajax({
            url: "{{URL::to('auction/live-bid')}}",
            method: 'GET',
            data: {'detail_id': detail_id,
                   'bid_count': bid_count},
            async: false,
            success: function (data) {
                if(data.list.length > bid_count){
                   $('#bid_count').val(data.list.length);
                   $('#call').val(0);
                   $("#call_bid").html('<i class="fa fa-bullhorn"></i> 1st CALL');    
                }else{
                    $('#bid_count').val(bid_count);
                }
                if(data.list.length > bid_count){
                    var html = '';
                    $('#bid-list tr').remove();
                    $('.highest_bid').html('Rs '+data.list[0].bid_amount);
                    for (i=0;i<data.list.length;i++){
                        html += '<tr>'+
                            '<td>'+data.list[i].auction_detail.item.name+' | '+data.list[i].auction_detail.item.item_code+'</td>'+
                            '<td>'+data.list[i].bid_amount+'</td>'+
                            '<td>'+data.list[i].bidder.first_name+' '+data.list[i].bidder.last_name+'</td>'+
                            '<td>'+data.list[i].bid_time+'</td>'+
                            '</tr>';
                    }
                    $('#bid-list').html(html);
                }
            },error: function () {
            }
        });
    }//end auto update
    //This function is used to update bidding list
    function addBid(amount,detail_id,bid_count,auction){
        $.ajax({
            url: "{{URL::to('auction/bid')}}",
            method: 'GET',
            data: {'detail_id': detail_id,
               'amount'   : amount,
               'auction'  : auction,
               'bid_count': bid_count 
              },
            async: false,
            success: function (data) {
                var $this = $('#bid');
                $this.button('reset');
                var html = '';
                if(data.list != 0){
                    if(data.list.length > 0){
                        $('#call').val(0);
                        $("#call_bid").html('<i class="fa fa-bullhorn"></i> 1st CALL');
                        $('.highest_bid').html('Rs '+data.list[0].bid_amount);
                        $('#bid_count').val(data.count[0].bid_count);
                        $('.no-data').addClass('hide');
                        // $('#bid-list tr').remove();
                        // for (i=0;i<data.list.length;i++){
                        //     html += '<tr>'+
                        //               '<td>'+data.list[i].auction_detail.item.name+' | '+data.list[i].auction_detail.item.item_code+'</td>'+
                        //               '<td>'+data.list[i].bid_amount+'</td>'+
                        //               '<td>'+data.list[i].bidder.first_name+' '+data.list[i].bidder.last_name+'</td>'+
                        //               '<td>'+data.list[i].bid_time+'</td>'+
                        //             '</tr>';
                        // }
                        //$('#bid-list').html(html);
                    }
                }else{
                    $.confirm({
                        title: 'Important!',
                        content: 'Your bid is not enough !',
                        typeAnimated: true,
                        closeButton: true,
                        buttons: {
                            tryAgain: {
                                text: 'Try again',
                                btnClass: 'bg-purple',
                                action: function(){

                                }
                            }
                        }
                    });
               }
            }
        });
    }
    $('#call_bid').on('click',function(){
        callBid($('#detail_id').val(),$('#call').val());
    });
    //This function is used to call bid
    function callBid(detail_id,call){
        //1st call
        if(call == 0){
            $.confirm({
                title: 'Confirm - 1st Call !',
                content: 'This is the 1st call for this item.',
                buttons: {
                    confirm: function () {
                        var call_status = callToServer(detail_id,call);
                        if(call_status == 1){
                             $('#call').val(call_status);
                             $("#call_bid").html('<i class="fa fa-bullhorn"></i> 2nd CALL');
                        }else{
                            $.confirm({
                                title: 'Important !',
                                content: 'No Bids for this item',
                                buttons: {
                                    close: function () {
                                        var $this = $('#call_bid');
                                        $this.button('reset');
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {

                    }
                }
            });
        //2nd call
        }else if(call == 1){
            $.confirm({
                title: 'Confirm - 2nd Call !',
                content: 'This is the 2nd call for this item.',
                buttons: {
                    confirm: function () {
                        var call_status = callToServer(detail_id,call);
                        if(call_status == 2){
                             $('#call').val(call_status);
                             $("#call_bid").html('<i class="fa fa-bullhorn"></i> Final CALL');
                        }
                    },
                    cancel: function () {}
                }
            });
        //3rd call
        }else if(call == 2){
            $.confirm({
                title: 'Confirm - Final Call !',
                content: 'This is the Final call for this item.',
                buttons: {
                    confirm: function () {
                        var call_status = callToServer(detail_id,call);
                        console.log(call_status);
                        if(call_status == 3){
                             $('#call').val(call_status);
                             $("#call_bid").html('<i class="fa fa-bullhorn"></i>');
                             $('.function-area').addClass('hide');
                             $('.win_area').removeClass('hide');
                             $('.bid-label').addClass('hide');
                             $('.approve-section').removeClass('hide');
                             $.ajax({
                                url: "{{URL::to('auction/get-win')}}",
                                method: 'GET',
                                data: {'detail_id': detail_id},
                                async: false,
                                success: function (data) {
                                    $('#winning_bid').html('Rs '+data.win_bid[0].win_amount);
                                },error: function () {
                                    $.alert({
                                        title: 'Oooops !',
                                        type: 'purple',
                                        content: 'Error Occured !..Try Again',
                                    });
                                }
                             });
                        }
                    },
                    cancel: function () {}
                }
            });
        }
        return 1;
    }
    /*This function is used to call server (1st call,2nd call and final call)*/
    function callToServer(detail_id,call){
        var return_status = '';
        $.ajax({
           url: "{{URL::to('auction/call-bid')}}",
           method: 'GET',
           data: {'detail_id': detail_id,
                  'call':call},
           async: false,
           success: function (data) {
                console.log(data);
                return_status = data.call;
           },error: function () {
                $.alert({
                    title: 'Oooops !',
                    type: 'purple',
                    content: 'Error Occured !..Try Again',
                });
           }
        });
        return return_status;
    }


    /*This function is used to get item wining history*/
    function itemHistory(detail_id,auction_id){
        model.close();
        $(".box").addClass('panel-refreshing');
        $('.pick-item').addClass('hide');
        $('.function-area').addClass('hide');
        $('.win_area').removeClass('hide');
        $('.bid-label').addClass('hide');
        $('.item-details').removeClass('hide');
        $.ajax({
            url: "{{URL::to('auction/item-history')}}",
            method: 'GET',
            data: {'detail_id': detail_id,
                   'auction_id':auction_id},
            async: false,
            success: function (data) {
                $(".box").removeClass('panel-refreshing');
                $('.item-code').html(data.detail[0].item.item_code +'|'+ data.detail[0].item.name);
                $('.start-bid').html(data.detail[0].start_bid_price);
                $('.withdrawal-bid').html(data.detail[0].withdraw_amount);
                $('#detail_id').val(data.detail[0].id);
                $('#winning_bid').html('Rs '+data.detail[0].bids[0].bid_amount);
                if(data.detail[0].approved == null){
                    $('.approve-section').removeClass('hide');
                }else{
                    if(data.detail[0].approved.status == 1){
                        $('.approved').removeClass('hide');   
                    }else{
                        $('.rejected').removeClass('hide');
                    }
                }
                var html = '<ul>';
                if(data.detail[0].item.images.length > 0){
                    $.each(data.detail[0].item.images, function() {
                        html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/'+this.image_path+'" alt="No Images !"></li>';
                    });
                }else{
                    html += '<li><img src="{{url()}}'+'/core/storage/uploads/images/item/empty.jpg" alt="No Images !"></li>';
                }
                html += '</ul>';
                // Append items
                jcarousel.html(html);
                // Reload carousel
                jcarousel.jcarousel('reload');

                var table = '';
                for (i=0;i<data.detail[0].bids.length;i++){
                    table += '<tr>'+
                              '<td>'+data.detail[0].item.name+' | '+data.detail[0].item.item_code+'</td>'+
                              '<td>'+data.detail[0].bids[i].bid_amount+'</td>'+
                              '<td>'+data.detail[0].bids[i].bidder.first_name+' '+data.detail[0].bids[i].bidder.last_name+'</td>'+
                              '<td>'+data.detail[0].bids[i].bid_time+'</td>'+
                            '</tr>';
                }
                $('#bid-list').html(table);
            },error: function () {
               $.alert({
                    type: 'purple',
                    content: 'Error Occured !..Try Again',
                });
            }
        });
    }//end start bid
    $('#stop').on('click',function(){
        stop($('#detail_id').val());
    });
    /** This function is used to stop bid*/
    function stop(detail_id){
        $.confirm({
            title: 'Notice !',
            content: 'Stop bidding for this item',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            buttons: {
                'confirm': {
                    text: 'Stop',
                    btnClass: 'btn btn-sm bg-purple',
                    action: function () {
                        $.ajax({
                            url: "{{URL::to('auction/stop')}}",
                            method: 'GET',
                            data: {'detail_id': detail_id},
                            async: false,
                            success: function (data) {
                                if(data.status != 1){
                                    $.alert({
                                        title: 'Notice !',
                                        type: 'purple',
                                        content: 'This item has bids...Cannot Stop !',
                                    });   
                                }else{
                                    location.reload();
                                }
                            },error:function(){
                                $.alert({
                                    title: 'Oooops !',
                                    type: 'purple',
                                    content: 'Error Occured !..Try Again',
                                });
                            }
                        });
                    }  
                },cancel: function () {}
            }
        });
    }
    /** 
     * This click event is used to approve item
     */
     $('#approve').on('click',function(){
        $.confirm({
            title: 'Notice !',
            content: 'Approve Item ?',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            content: '' +'<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<label>Comment</label>' +
                        '<input type="text" placeholder="Comment" class="comment form-control"/>' +
                        '</div>' +
                        '</form>',
            buttons: {
                'confirm': {
                    text: 'Approve',
                    btnClass: 'btn btn-sm bg-purple',
                    action: function () {
                        var comment = this.$content.find('.comment').val();
                        $.ajax({
                            url: "{{URL::to('auction/approve')}}",
                            method: 'GET',
                            data: {'detail_id':$('#detail_id').val(),
                                   'comment'  :comment},
                            async: false,
                            success: function (data) {
                                $('.approve-section').addClass('hide');
                                $('.approved').removeClass('hide');
                                $('#bid_count').val(0);
                                location.reload();
                            },error:function(){
                                $.alert({
                                    title: 'Oooops !',
                                    type: 'purple',
                                    content: 'Error Occured !..Try Again',
                                });
                            }
                        });
                    }  
                },cancel: function () {}
            }
        });
    });
    /** 
     * This click event is used to reject item
     */
    $('#reject').on('click',function(){
        $.confirm({
            title: 'Notice !',
            content: 'Reject Item ?',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            content: '' +'<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<input type="text" placeholder="Reason" class="comment form-control"/>' +
                        '</div>' +
                        '<div class="checkbox">'+
                            '<label>'+
                              '<input type="checkbox" class="check" checked> Reauction Now'+
                            '</label>'+
                          '</div>'+
                        '</form>',
            buttons: {
                'confirm': {
                    text: 'Reject',
                    btnClass: 'btn btn-sm bg-purple',
                    action: function () {
                        var comment = this.$content.find('.comment').val();
                        if($('.check').prop('checked') == true){
                            var check = 1;
                        }else{
                            var check = 0;
                        }
                        $.ajax({
                            url: "{{URL::to('auction/reject')}}",
                            method: 'GET',
                            data: {'detail_id': $('#detail_id').val(),
                                   'comment'  : comment,
                                   'check'    : check
                                 },
                            async: false,
                            success: function (data) {
                                $('.approve-section').addClass('hide');
                                $('.rejected').removeClass('hide');
                                $('#bid_count').val(0);
                                location.reload();
                            },error:function(){
                                $.alert({
                                    title: 'Oooops !',
                                    type: 'purple',
                                    content: 'Error Occured !..Try Again',
                                });
                            }
                        });
                    }  
                },cancel: function () {}
            }
        });
    });

</script>
@stop

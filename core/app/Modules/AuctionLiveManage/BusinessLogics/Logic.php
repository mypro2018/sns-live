<?php namespace App\Modules\AuctionLiveManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AuctionManage\Models\AuctionManage;
use App\Modules\AuctionManage\Models\AuctionDetailsManage;
use App\Modules\AuctionLiveManage\Models\AuctionBidManage;
use App\Modules\ProductManage\Models\ProductImageManage;
use App\Modules\AuctionLiveManage\Models\AuctionWinBidManage;
use App\Modules\ProductManage\Models\ItemApproveManage;
use Illuminate\Support\Facades\DB;
use Exception;
use Pusher;
use Illuminate\Support\Facades\App;
use App\Models\ItemComplete;
use App\Modules\AuctionLiveManage\Models\CustomerOffer;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionManageLogic;
use Sentinel;
use App\Modules\CustomerManage\Models\CustomerAuctionManage;

class Logic {
	/** 
	 * This function is used to get auction detail
	 * @param integer $auction_id
	 * @return auction detail object other wise error page.
	 */
	public function getAuctionDetail($auction_id){
        $bid_count = 0;

        $auction_sql = AuctionManage::select(
                'sns_auction.*', 
                'sns_auction_detail.sns_item_id',
                'sns_auction_detail.start_bid_price',
                'sns_auction_detail.withdraw_amount',
                'sns_auction_detail.order',
                'sns_auction_detail.remark',
                'sns_auction_detail.call',
                'sns_auction_detail.request_offer',
                'sns_auction_detail.status'
            )->with(['location', 'live_auction_product.customer_offer.customer_auction' => function($sql) use($auction_id) {
                $sql->where('auction_id', '=', $auction_id);
            }, 'live_auction_product.bid.customer_auction.customer'])
            ->leftJoin('sns_auction_detail', function($join){
                $join->on('sns_auction_detail.sns_auction_id', '=', 'sns_auction.id')
                     ->where('sns_auction_detail.status', '=', PRODUCT_TO_LIVE);   
            })
            ->where('sns_auction.id', $auction_id)
            ->whereNull('sns_auction.deleted_at')
            ->first();    


        $bidding_item = DB::select('SELECT * FROM sns_auction_bid ab RIGHT JOIN (SELECT
										ad.id AS detail_id,
										ad.start_bid_price,
										ad.withdraw_amount,
										i.`name` AS item_name,
										i.item_code AS item_code,
										i.id AS item_id,
										ad.call,
                                        CONCAT(s.supplier_code," - ",s.fname) as supplier
									FROM
										sns_auction a
									INNER JOIN sns_auction_detail ad ON a.id = ad.sns_auction_id
									INNER JOIN sns_item i ON ad.sns_item_id = i.id
                                    LEFT JOIN sns_supplier s ON i.sns_supplier_id = s.id
									WHERE ad.`status` = 2 AND a.id = ?) AS tem ON ab.sns_auction_detail_id = tem.detail_id LEFT JOIN sns_customer_register cr ON ab.sns_customer_id = cr.id WHERE ab.deleted_at IS NULL ORDER BY ab.id DESC LIMIT 1',[$auction_id]);
        
        if (!empty($bidding_item)) {

            $item_image   = ProductImageManage::where('sns_item_id', $bidding_item[0]->item_id)
            ->whereNull('deleted_at')
            ->first();

            $bidding_list = AuctionBidManage::with(['auction_detail.item', 'bid_user.customer_card' => function($sql) use($auction_id) {
                $sql->where('auction_id', '=', $auction_id);
            }])
            ->where('sns_auction_detail_id',$bidding_item[0]->sns_auction_detail_id)
            ->whereNull('deleted_at')
            ->orderBy('bid_time', 'DESC')
            ->limit(5)
            ->get();

            $get_bid = AuctionBidManage::where('sns_auction_detail_id','=',$bidding_item[0]->detail_id)
            ->whereNull('deleted_at')
            ->get();

            if(!empty($get_bid)){
                $bid_count = count($get_bid);
            }
        }else{
            $item_image = [];
            $bidding_list = [];
        }
        if(count($auction_sql) > 0){
			return ['all' => $auction_sql, 'select' => $bidding_item, 'img' => $item_image,'bidding_list' => $bidding_list,'bid_count' => $bid_count];
		}else{
			return [];
		}
	}
	/** 
	 * This function is used to update item bidding status and get selected item details to start a bid
	 * @param integer $detail_id
	 * @response auction detail object
	 */
	public function getItemDetail($detail_id,$auction_id){
		$get_item = AuctionDetailsManage::with('item')->where('id',$detail_id)->get();
        $item_image = ProductImageManage::where('sns_item_id',$get_item[0]->item->id)->get();
		if(count($get_item) > 0){
			return ['select_item' => $get_item,'img' => $item_image];
		}else{
			return [];
		}
	}
	/** 
	 * This function is used to add bid from admin
	 * @param integer $user_id,decimal $amount,integer $detail_id
	 * @return 
	 */
	public function addBid($user_id, $amount, $detail_id, $bid_count, $auction_id, $offer_status, $bidder_type){
        //item detail status update when select the item to start bid
        if($bid_count == 0){
            $update_status = DB::update('UPDATE sns_auction_detail SET status = ? WHERE  id = ?', [PRODUCT_TO_LIVE, $detail_id]);
            if(count($update_status) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while updating auction detail status to live");
            }
            $update_product_status = DB::update('UPDATE sns_item SET status = ? WHERE  id IN(SELECT sns_item_id FROM sns_auction_detail WHERE id = ?)', [PRODUCT_TO_LIVE, $detail_id]);
            if(count($update_product_status) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while updating product status to live");
            }
            $update_auction_status = DB::update('UPDATE sns_auction SET auction_status = ?,hold_online_bidding = ? WHERE  id = ?', [AUCTION_LIVE, BIDDING_INACTIVE, $auction_id]);
            if(count($update_auction_status) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while updating auction status to live");
            }
            //GET ITEM DETAILS
            $detail = AuctionDetailsManage::find($detail_id);
            //GO TO NEXT ITEM EVENT
            if($detail && $offer_status == OFFER_APPROVED){
                $pusher = App::make('pusher');
                $pusher->trigger('auction-channel', 'next-item-event', [
                    'auction_id'    => $auction_id,
                    'detail_id'     => $detail_id,
                    'item_id'       => $detail->sns_item_id,
                    'offer_status'  => $offer_status         
                ]);
            }
        }
        if($offer_status == OFFER_APPROVED){
            // change the offer status to expired
            $remove_first_bid = $this->removeFirstBid($detail_id, OFFER_EXPIRED, $amount);
            //add bid
            $bid = AuctionBidManage::create([
                'sns_auction_detail_id' => $detail_id,
                'sns_customer_id'       => $user_id,
                'bid_amount'            => (float) str_replace(',', '', $amount),
                'bidder_type'           => $bidder_type,
                'bid_time'              => date('Y-m-d H:i:s'),
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s')
            ]);
            if(!$bid){
                DB::rollBack();
                throw new Exception("Error occurred while adding new bid");
            }
            //get last bidding item details
            $bidding_list = AuctionBidManage::with(['auction_detail.item', 'bidder', 'bid_user.customer_card' => function($sql) use($auction_id) {
                $sql->where('auction_id', '=', $auction_id);
            }])
            ->where('sns_auction_detail_id', $detail_id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->first();
            //get bid count
            $bid_count = $this->bidCount($detail_id);
            if($bidding_list){
                DB::commit();
                $pusher = App::make('pusher');
                $pusher->trigger('auction-channel', 'offer-confirm-event', [
                    'list'  => $bidding_list,
                    'count' => $bid_count 
                ]);
                return $bidding_list;
            }else{
                return [];
            }
        }elseif($offer_status == OFFER_EXPIRED){
            $highest_bid = DB::table('sns_auction_detail')
            ->leftJoin('sns_auction_bid', function($join) {
                $join->on('sns_auction_detail.id', '=', 'sns_auction_bid.sns_auction_detail_id')
                    ->whereNull('sns_auction_bid.deleted_at');
            })
            ->select(DB::raw('IFNULL(sns_auction_bid.bid_amount,"0") AS bid_amount'),
                    DB::raw('IFNULL(sns_auction_detail.start_bid_price,"0") AS start_price'))
            ->whereNull('sns_auction_detail.deleted_at')
            ->orderBy('sns_auction_bid.bid_time', 'desc')
            ->where('sns_auction_detail.id', '=', $detail_id)
            ->first();
            if($highest_bid){
                if($bidder_type == ONLINE_BIDDER){
                    if(intval($highest_bid->bid_amount) < (float) str_replace(',', '', $amount)){
                        //add bid
                        $bid = AuctionBidManage::create([
                            'sns_auction_detail_id' => $detail_id,
                            'sns_customer_id'       => $user_id,
                            'bid_amount'            => (float) str_replace(',', '', $amount),
                            'bidder_type'           => $bidder_type,
                            'bid_time'              => date('Y-m-d H:i:s'),
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s')
                        ]);
                        if(!$bid){
                            throw new Exception("Error occurred while adding new bid");
                        }
                        //reset auctioneer call request
                        $update_call = DB::update('UPDATE sns_auction_detail set `call` = ? WHERE id = ?', [CALL0, $detail_id]);
                        if(sizeof($update_call) == 0){
                            DB::rollBack();
                            throw new Exception("Error occurred while resetting call status");
                        }
                        //get last bidding item details
                        $bidding_list = AuctionBidManage::with(['auction_detail.item', 'bidder', 'bid_user.customer_card' => function($sql) use($auction_id){
                            $sql->where('auction_id', '=', $auction_id);
                        }])
                        ->where('sns_auction_detail_id', $detail_id)
                        ->whereNull('deleted_at')
                        ->orderBy('bid_time', 'DESC')
                        ->first();
                        //get bid count
                        $bid_count = $this->bidCount($detail_id);
                        if($bidding_list){
                            DB::commit();
                            $pusher = App::make('pusher');
                            $pusher->trigger('auction-channel', 'bid-event', [
                                'list'  => $bidding_list,
                                'count' => $bid_count 
                            ]);
                            return $bidding_list;
                        }else{
                            return [];
                        }
                    }else{
                        return [];
                    }
                }else{ //Floor Bidding
                    //add bid
                    $bid = AuctionBidManage::create([
                        'sns_auction_detail_id' => $detail_id,
                        'sns_customer_id'       => $user_id,
                        'bid_amount'            => (float) str_replace(',', '', $amount),
                        'bidder_type'           => $bidder_type,
                        'bid_time'              => date('Y-m-d H:i:s'),
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s')
                    ]);
                    if(!$bid){
                        throw new Exception("Error occurred while adding new bid");
                    }
                    //reset auctioneer call request
                    $update_call = DB::update('UPDATE sns_auction_detail set `call` = ? WHERE id = ?', [CALL0, $detail_id]);
                    if(sizeof($update_call) == 0){
                        DB::rollBack();
                        throw new Exception("Error occurred while resetting call status");
                    }
                    //get last bidding item details
                    $bidding_list = AuctionBidManage::with(['auction_detail.item', 'bidder', 'bid_user.customer_card' => function($sql) use($auction_id){
                        $sql->where('auction_id', '=', $auction_id);
                    }])
                    ->where('sns_auction_detail_id', $detail_id)
                    ->whereNull('deleted_at')
                    ->orderBy('bid_time', 'DESC')
                    ->first();
                    //get bid count
                    $bid_count = $this->bidCount($detail_id);
                    if($bidding_list){
                        DB::commit();
                        $pusher = App::make('pusher');
                        $pusher->trigger('auction-channel', 'bid-event', [
                            'list'  => $bidding_list,
                            'count' => $bid_count 
                        ]);
                        return $bidding_list;
                    }else{
                        return [];
                    }
                }
            }else{
                return [];
            } //end if highest bid have -->
        }
	}
	/**
     * This function is used to get all item bids
     * @parm  integer $item_id, integer $auction_id that need to get item bids
     * @return if item details have then item object else return false
     */
    public function getItemBids($item_id, $auction_id){
    	return $itemBids = AuctionDetailsManage::with(['bids.bidder','bids' => function($sql){
            $sql->whereNull('deleted_at');
        }])
        ->where('sns_auction_id',$auction_id)
        ->where('sns_item_id',$item_id)
        ->where('status', PRODUCT_TO_LIVE)
        ->get();
    }
    /**
     * This function is used to get live update bids
     * @param integer $detail_id integer $bid_count
     * @return $bidding_list object
     */
    public function liveBid($detail_id,$bid_count){
        $get_bid = DB::table('sns_auction_bid')
            ->rightJoin('sns_auction_detail','sns_auction_bid.sns_auction_detail_id', '=', 'sns_auction_detail.id')
            ->select('sns_auction_bid.*')
            ->where('sns_auction_detail_id','=',$detail_id)
            ->where('sns_auction_detail.status','=',2)
            ->get();
        if(count($get_bid) > $bid_count) {
            $update_call = DB::update('UPDATE sns_auction_detail set `call` = 0 WHERE id = ?', [$detail_id]);
            $bidding_list = AuctionBidManage::with('auction_detail.item','bidder')->where('sns_auction_detail_id',$detail_id)->orderBy('created_at','DESC')->get();           
        }else{
            $bidding_list = AuctionBidManage::with('auction_detail.item','bidder')->where('sns_auction_detail_id',$detail_id)->orderBy('created_at','DESC')->get();
        }
        return $bidding_list;
    }
    /**
     * This function is used to call bid
     * @param integer $detail_id integer $call
     * @response integer $call_status
     */
    public function callBid($detail_id, $call){
        //check items has bid
        $get_bid = AuctionBidManage::select(
            'bid_amount', 
            'sns_customer_id', 
            'sns_auction_detail_id'
        )
        ->with(['bidder','customer_auction' => function($query) {
            $query->select('auction_id');
        }])
        ->where('sns_auction_detail_id', '=', $detail_id)
        ->orderBy('bid_time', 'DESC')
        ->first();

        if(sizeof($get_bid) > 0) {

            if ($call == CALL0) {

                $update_call = DB::update('UPDATE sns_auction_detail set `call` = ? WHERE id = ?', [CALL1, $detail_id]);
                if (count($update_call) == 0) {
                    throw new Exception("Error occurred while updating 1st call");
                }else{
                    return ['call_status' => CALL1, 'detail' => $get_bid];
                }
            } elseif ($call == CALL1) {

                $update_call = DB::update('UPDATE sns_auction_detail set `call` = ? WHERE id = ?', [CALL2, $detail_id]);
                if (count($update_call) == 0) {
                    throw new Exception("Error occurred while updating 2nd call");
                }else{
                    return ['call_status' => CALL2, 'detail' => $get_bid];
                }
            } elseif ($call == CALL2) {

                $update_call = DB::update('UPDATE sns_auction_detail set `call` = ? WHERE id = ?', [CALL3, $detail_id]);
                if (count($update_call) == 0) {
                    throw new Exception("Error occurred while updating 3rd call");
                }else{
                    return ['call_status' => CALL3, 'detail' => $get_bid];
                }

            }else{
                
                return ['call_status' => CALL3, 'detail' => $get_bid];
            }
        }else{
            return 0;
        }
    }
    /*This function is used to get winning price*/
    public function winPrice($detail_id){
        $win_price = DB::select('SELECT cast((win_amount) as DECIMAL (10,2)) as win_amount FROM sns_auction_winner WHERE sns_auction_bid_id IN(SELECT id FROM sns_auction_bid WHERE sns_auction_detail_id = ?)',[$detail_id]);
        if(!$win_price){
            throw new Exception("Error occurred while getting win item price");
        }else{
            return $win_price;
        }
    }
    /*This function is used get winning bid history*/
    public function getItemHistory($detail_id){
        return $auction_sql = AuctionDetailsManage::with('item.images','bids.bidder','auction.location','approved')->where('id',$detail_id)->get();
    }
    /* This function is used to stop bidding for this item
    * @param integer $detail_id
    * @return status
    */
    public function stop($detail_id){
        $get_bid = AuctionBidManage::where('sns_auction_detail_id','=',$detail_id)->get();
        if(count($get_bid) > 1) {
            return 0;
        }else{
            $update_item = DB::update('UPDATE sns_auction_detail SET `status` = 0 WHERE id  = ?', [$detail_id]);
            return 1;
        }
    }

    /**
     * This function is used to update auction winner table
     * @param integer $id, array $data
     * @respnse is boolean
     */
    public function updateAuctionWinner($id, $data) {
        DB::transaction(function() use($id, $data) {

            $winner = AuctionWinBidManage::where('id','=',$id)->update($data);

            if(count($winner) > 0) {
                return true;
                
            }else {
                throw new Exception("Error occurred while updating winner");
            }
        });

        return true;
    }
    /**
    * This function is used to approve item 
    * @param integer auction_id
	* @param integer customer_card_no
	* @param integer detail_id
	* @param string comment
    * @return
    */
    public function approveItem($request){
        DB::beginTransaction();
        //Get last bid details
        $get_highest_bid = DB::table('sns_auction_bid')
        ->where('sns_auction_detail_id', $request->detail_id)
        ->select('id', 'sns_customer_id', 'bid_amount')
        ->orderBy('bid_time', 'DESC')
        ->whereNull('deleted_at')
        ->first();
        /** Find Card No assigned customer */
        $findCustomer = CustomerAuctionManage::where('auction_id', $request->auction_id)
        ->where('card_no', $request->customer_card_no)
        ->first();
        if($get_highest_bid){
            //approve item
            $item_approve = ItemApproveManage::create([
                'auction_id'  => $request->auction_id,
                'detail_id'   => $request->detail_id,
                'customer_id' => $findCustomer->user_id,
                'status'      => ITEM_PAYMENT_APPROVE,
                'comment'     => $request->comment,
                'card_no'     => $request->customer_card_no
            ]);
            if($item_approve){
                //add highest bidder to winner table
                $add_winner = AuctionWinBidManage::create([
                    'sns_auction_bid_id' => $get_highest_bid->id,
                    'sns_user_id'        => $findCustomer->user_id,
                    'card_no'            => $request->customer_card_no,
                    'status'             => DEFAULT_STATUS,
                    'win_amount'         => $get_highest_bid->bid_amount,
                    'item_approve_id'    => $item_approve->id
                ]);
                if(!$add_winner){
                    DB::rollBack();
                    throw new Exception("Error occurred while adding winner");
                }
            }else{
                DB::rollBack();
                throw new Exception("Error occurred while adding item approvement");
            }
            //get auction item details
            $get_item_details = AuctionDetailsManage::with('highest_bid.win_bid')
            ->where('id', $request->detail_id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->first();
            if($get_item_details){
                //update call in auction detail table 
                $update_item_detail = DB::update('UPDATE sns_auction_detail SET `status` = ?,`call` = ? WHERE id  = ?', [PRODUCT_TO_OVER, CALL0, $request->detail_id]);
                if(count($update_item_detail) == 0){
                    DB::rollBack();
                    throw new \Exception("Error occurred while updating product detail");
                }
                //update status in item table to sold
                $update_item = DB::update('UPDATE sns_item SET `status` = ? WHERE id  = ?', [PRODUCT_TO_OVER, $get_item_details->sns_item_id]);   
                if(count($update_item) == 0){
                    DB::rollBack();
                    throw new \Exception("Error occurred while updating product");
                }
                DB::commit();
                //auction complete
                $this->auctionComplete($get_item_details->sns_auction_id);
                /* start pusher */ 
                $pusher = App::make('pusher');
                $pusher->trigger('auction-channel', 'payment_event', [
                    'auction_id' => $get_item_details->sns_auction_id,
                    'item_id'    => $get_item_details->sns_item_id,
                    'customer_id'=> $get_item_details->highest_bid->sns_customer_id
                ]);
                /* end pusher */
                return $request->detail_id;  
            }
        }else{
            throw new \Exception("Error occurred while getting last bid");
        }
    }
    /**
    * This function is used to re-auction item 
    * @param integer $detail_id
    * @param string comment
    * @param integer product_id
    * @return
    */
    public function reAuction($request){
        $get_item_details = AuctionDetailsManage::with('highest_bid.win_bid', 'item')
        ->where('id', $request->detail_id)
        ->whereNull('deleted_at')
        ->first();
        if($get_item_details && $get_item_details->highest_bid){
            $approve_validate = ItemApproveManage::where('detail_id', $request->detail_id)
            ->where('status', ITEM_PAYMENT_APPROVE)
            ->get();
            //check item approve table has approved item
            if(sizeof($approve_validate) > 0){
                //change the item approve status to reject
                $update_item_approve = ItemApproveManage::where('detail_id', $request->detail_id)
                ->update([
                    'status'    => PAYMENT_PENDING,
                    'comment'   => $request->comment
                ]);
                //check win bid link with item approve
                $winner = AuctionWinBidManage::where('item_approve_id', $approve_validate[0]->id)->first();
                if($winner){
                    //win item remove from the winner table with approve relation
                    $delete_win_item = AuctionWinBidManage::where('item_approve_id', $approve_validate[0]->id)
                    ->delete();
                }else{
                    //win item remove from the winner table with highest bid relation
                    $delete_win_item = AuctionWinBidManage::where('sns_auction_bid_id', $get_item_details->highest_bid->id)
                    ->delete();
                }
                 //update auction allocated detail item deleted_at
                $update_item_detail = DB::update('UPDATE sns_auction_detail SET `deleted_at` = ? WHERE id  = ?', [date('Y-m-d H:i:s'), $request->detail_id]);   
                if(count($update_item_detail) == 0){
                    throw new Exception("Error occurred while updating auction allocated product detail");
                }
                //update item status to available to auction
                $update_item_detail = DB::update('UPDATE sns_item SET `status` = ? WHERE id  = ?', [PRODUCT_TO_BEGIN, $get_item_details->sns_item_id]);   
                if(count($update_item_detail) == 0){
                    throw new Exception("Error occurred while updating product detail");
                }
                //add clone item as new to auction detail
                $add_detail = AuctionDetailsManage::create([
                    'sns_auction_id' => $get_item_details->sns_auction_id,
                    'sns_item_id'    => $get_item_details->sns_item_id,
                    'start_bid_price'=> $get_item_details->start_bid_price,
                    'withdraw_amount'=> $get_item_details->withdraw_amount,
                    'order'          => $get_item_details->order,
                    'remark'         => $get_item_details->remark
                ]); 
                if(!$add_detail){
                    DB::rollBack();
                    throw new Exception("Error occurred while updating product detail");
                }
                return $get_item_details;
            }else{
                DB::transaction(function() use($request, $get_item_details){
                    $item_approve = ItemApproveManage::create([
                       'auction_id'  => $get_item_details->sns_auction_id,
                       'detail_id'   => $request->detail_id,
                       'customer_id' => $get_item_details->highest_bid->sns_customer_id,
                       'status'      => ITEM_PAYMENT_REJECT,
                       'comment'     => $request->comment
                    ]);
                    if($item_approve){
                        $update_item_detail = DB::update('UPDATE sns_auction_detail SET `status` = ?,`deleted_at` = ? WHERE id  = ?', [PRODUCT_TO_OVER, date('Y-m-d H:i:s'), $request->detail_id]);   
                        if(count($update_item_detail) == 0){
                            throw new Exception("Error occurred while updating product detail");
                        }
                        $update_item_detail = DB::update('UPDATE sns_item SET `status` = ? WHERE id  = ?', [PRODUCT_TO_BEGIN, $get_item_details->sns_item_id]);   
                        if(count($update_item_detail) == 0){
                            throw new Exception("Error occurred while updating product detail");
                        }
                        $add_detail = AuctionDetailsManage::create([
                            'sns_auction_id' => $get_item_details->sns_auction_id,
                            'sns_item_id'    => $get_item_details->sns_item_id,
                            'start_bid_price'=> $get_item_details->start_bid_price,
                            'withdraw_amount'=> $get_item_details->withdraw_amount,
                            'order'          => $get_item_details->order,
                            'remark'         => $get_item_details->remark
                        ]); 
                        if(!$add_detail){
                            DB::rollBack();
                            throw new Exception("Error occurred while updating product detail");
                        }
                    }else{
                        throw new Exception("Error occurred while approving item");
                    }
                });
                return $get_item_details;
            }
        }else{
            throw new Exception("Error occurred while re-auction item");
        }
    }
    /**
     * This function is used to  get bid count for item 
     * @param integere $detail_id
     * @return bid_count
     */
    public function bidCount($detail_id){
        return $bid_count = AuctionBidManage::select(DB::raw('count(id) AS bid_count'))
            ->where('sns_auction_detail_id', $detail_id)
            ->whereNull('deleted_at')
            ->first();
    }


    /**
     * This function is used to check item is live. 
     */

    public function auctionLiveProduct($auction_id){

        $live_product = AuctionDetailsManage::join('sns_auction_bid',function($join){
            $join->on('sns_auction_detail.id', '=', 'sns_auction_bid.sns_auction_detail_id');  
        })
        ->where('sns_auction_detail.sns_auction_id', '=', $auction_id)
        ->where('sns_auction_detail.status', '=', PRODUCT_TO_LIVE)
        ->whereNull('sns_auction_detail.deleted_at')
        ->whereNull('sns_auction_bid.deleted_at')
        ->first();
        if($live_product){
            return count($live_product);
        }else{
            return [];
        }
    }

    /**
     * This function is used to get auction item details
     */

    public function getAuctionItemDetails($auction_id, $item_id){
        $auction_new_item = AuctionDetailsManage::leftJoin('sns_item_image',function($join){
            $join->on('sns_auction_detail.sns_item_id', '=', 'sns_item_image.sns_item_id');
        })
        ->join('sns_item',function($join){
            $join->on('sns_auction_detail.sns_item_id', '=', 'sns_item.id');
        })
        ->select('sns_auction_detail.start_bid_price','sns_auction_detail.withdraw_amount','sns_item_image.image_path', 'sns_auction_detail.id as detail_id','sns_item.item_code','sns_item.name')
        ->where('sns_auction_detail.sns_auction_id', '=', $auction_id)
        ->where('sns_auction_detail.sns_item_id', '=', $item_id)
        ->whereNull('sns_auction_detail.deleted_at')
        ->whereNull('sns_item_image.deleted_at')
        ->orderBy('sns_auction_detail.id', 'DESC')
        ->first();
        if(count($auction_new_item) > 0){
            return $auction_new_item;
        }else{
            return [];
        }
    }

    /**
     * This function is used to REQUEST OFFER
     */

    public function requestOffer($detail_id, $offer_status){
        //change offer status
        $request_offer = AuctionDetailsManage::find($detail_id);
        $request_offer->request_offer = $offer_status;
        $request_offer->save();

        if(sizeof($request_offer) > 0){
            $first_bid = AuctionBidManage::select('bid_amount')
                ->where('sns_auction_detail_id', '=', $detail_id)
                ->whereNull('deleted_at')
                ->first();
            if(count($first_bid) > 0){
                DB::commit();
                return $first_bid;
            }else{
                DB::rollBack();
                return [];
            }
        }else{
            DB::rollBack();
            return [];
        }
    }


    /**
     * This function is used to get bidding complete item.
     * @param Integer $auction_id
     * @return Object 
     */

    public function completeItem($auction_id = null){
       
        $complete_item = DB::select('SELECT 
            si.id AS item_id,
            si.item_code,
            sa.card_no,
            si.name AS item_name,
            sa.status,
            sa.item_approve_sold_status,
            (SELECT bid_amount FROM sns_auction_bid WHERE deleted_at IS NULL AND sns_auction_detail_id = temp.latest ORDER BY bid_time DESC LIMIT 1) AS bid_amount
            FROM
            item_approve sa
                INNER JOIN
            (SELECT 
                ad.sns_item_id AS sns_item, 
                MAX(ad.id) AS latest
            FROM
                sns_auction_detail ad
            WHERE
                ad.sns_auction_id = ? AND ad.status = ?
            GROUP BY ad.sns_item_id) AS temp 
            ON sa.detail_id = temp.latest
                INNER JOIN
            sns_item si ON temp.sns_item = si.id
            ORDER BY sa.created_at DESC',[$auction_id, PRODUCT_TO_OVER]); 
        if(count($complete_item) > 0){
                return $complete_item; 
        }else{
            return [];
        }  
    }

    //add customer offers
    public function addCustomerOffers($detail_id, $offer_amount, $customer_id, $auction_id){
  
        $offerCheck = CustomerOffer::where('auction_detail_id', $detail_id)
                       ->whereNull('deleted_at')
                       ->orderBy('offer_amount', 'DESC')
                       ->first();

        if(isset($offerCheck) && sizeof($offerCheck) > 0){

            if(isset($offerCheck->offer_amount) && $offerCheck->offer_amount < $offer_amount){

                $addOffer = CustomerOffer::create([
                    'auction_detail_id' => $detail_id,
                    'customer_id'       => $customer_id,
                    'offer_amount'      => $offer_amount,
                    'status'            => DEFAULT_STATUS,
                    'approve_status'    => 0,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')      
                ]);

                if(sizeof($addOffer) == 0){
                    DB::rollBack();
                    throw new Exception("Error occurred while adding auctioers offer"); 
                }
            }

        }else{

            $addOffer = CustomerOffer::create([
                'auction_detail_id' => $detail_id,
                'customer_id'       => $customer_id,
                'offer_amount'      => $offer_amount,
                'status'            => DEFAULT_STATUS,
                'approve_status'    => 0,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')      
            ]);

            if(sizeof($addOffer) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while adding auctioers offer"); 
            }
        }

        DB::commit();
        //GET LATEST OFFER
        $offer = CustomerOffer::with(['customer_auction' => function($sql) use($auction_id){
            $sql->where('auction_id', '=', $auction_id);
        }])
        ->where('auction_detail_id', $detail_id)
        ->orderBy('offer_amount', 'DESC')
        ->first();

        if(isset($addOffer) && sizeof($addOffer) > 0){
            return $offer;
        }else{
            return $offer;
        }
        
    }


    //remove first bid
    public function removeFirstBid($detail_id, $offer_status, $amount){
        $request_offer                = AuctionDetailsManage::find($detail_id);
        $request_offer->request_offer = $offer_status;
        $request_offer->save();
        if(sizeof($request_offer) == 0){
            DB::rollBack();
            throw new Exception("Error occurred while updating offer expired");
        }
        DB::commit();
    }

    /**
     * This function is used to finish the auction
     * @param integer auction_id
     * @return void
     */
    public function auctionComplete($auction_id){
        DB::beginTransaction();
        $available_item = AuctionDetailsManage::where('sns_auction_id', $auction_id)
        ->where('request_offer', '!=', OFFER_EXPIRED)
        ->where('status', '!=', PRODUCT_TO_OVER)
        ->get();
        if(sizeof($available_item) == 0){
            //update auction complete
            $update = AuctionManage::find($auction_id);
            $update->auction_status = AUCTION_OVER;
            $update->save();
            if(count($update) == 0){
                DB::rollBack();
                throw new Exception("Error occurred while updating auction over");
            }
            DB::commit();
        }
    } 
    
    //validate bid over item for rebidding disabled
    public function validateBiddingOver($request){
        $auction_item_details = AuctionDetailsManage::find($request->get('detail_id'));
        if($auction_item_details && sizeof($auction_item_details) > 0){
            if($auction_item_details->status == PRODUCT_TO_OVER){
                return ['status' => true, 'auction_id' => $auction_item_details->sns_auction_id, 'item_id' => $auction_item_details->sns_item_id];
            }else{
                return ['status' => false, 'auction_id' => $auction_item_details->sns_auction_id, 'item_id' => $auction_item_details->sns_item_id];
            }
        }else{
            throw new Exception("Error occurred, cannot find auction item details.");
        }
    }

    //update approved sold item status as paid
    public function updateSoldStatus($auction_id, $customer_id, $detail_id, $paid_status){
        if(isset($auction_id) && isset($customer_id) && isset($detail_id) && isset($paid_status)){
            $update_status = DB::transaction(function () use($auction_id, $customer_id, $detail_id, $paid_status) {
                return $update_sold_status = ItemApproveManage::where('auction_id', $auction_id)
                ->where('customer_id', $customer_id)
                ->where('detail_id', $detail_id)
                ->where('status', DEFAULT_STATUS)
                ->update([
                    'item_approve_sold_status' => PAYMENT_SUCCESS,
                    'advance_status'           => $paid_status
                ]);
            });
            if($update_status){
                return true;
            }else{
                return false;
            }
        }else{
            throw new \Exception('Missing Parameters');
        }
    }

    /**
     * This function is used find the auction
     * @param integer auction_id
     * @return array
     */
    public function auctionItem($auction_id){
        return $auctionItem = AuctionDetailsManage::where('sns_auction_id', $auction_id)
        ->whereNull('deleted_at')
        ->get();
    }

    /**
	 * This function is used to hold the online bidding from the floor
	 * @param integer hold_status
	 * @param integer auction_id
	 * @return array auction
	 */
    public function holdBidding($request){
        if($request){
            return DB::transaction(function () use($request){
                $auction                      = AuctionManage::find($request->auction_id);
                $auction->hold_online_bidding = $request->status;
                $auction = $auction->save();
                return $auction;
            });
        }else{
            return [];
        }
    }

    /**
	 * This function is used to show & hide online bidder
	 * @param integer show_hider_status
	 * @param integer auction_id
	 * @return array auction
	 */

    public function showHideBidder($request){
        if($request){
            return DB::transaction(function () use($request){
                $auction                    = AuctionManage::find($request->auction_id);
                $auction->show_hide_bidder  = $request->status;
                $auction                    = $auction->save();
                return $auction;
            });
        }else{
            return [];
        }
    }


}

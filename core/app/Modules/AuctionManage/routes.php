<?php

Route::group(array('prefix'=>'auction', 'namespace' => 'App\Modules\AuctionManage\Controllers'), function() {
    Route::get('get-auction-status', [
        'as' => 'auction.get-auction-status', 'uses' => 'AuctionManageController@getAuctionStatus'
    ]);
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(array('prefix'=>'auction','namespace' => 'App\Modules\AuctionManage\Controllers'), function() {
        /**
         * GET Routes
         */
        Route::get('add',[
            'as' => 'auction.add','uses' => 'AuctionManageController@add'
        ]);
        Route::get('list',[
            'as' => 'auction.list','uses' => 'AuctionManageController@listAuction'
        ]);
        Route::get('search', [
            'as' => 'auction.search', 'uses' => 'AuctionManageController@searchAuction'
        ]);
        Route::get('details', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@getAuctionDetails'
        ]);
        Route::get('edit/{id}', [
            'as' => 'auction.edit', 'uses' => 'AuctionManageController@edit'
        ]);
        Route::get('history/{id}', [
            'as' => 'auction.edit', 'uses' => 'AuctionManageController@getItemDetails'
        ]);
        Route::get('visible', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@changeVisible'
        ]);
        Route::get('register/{id}', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@registerCustomer'
        ]);
        Route::get('search/customer', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@searchCustomer'
        ]);
        Route::get('save/range', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@saveCardRange'
        ]);
        
        Route::get('band/aution', [
            'as' => 'customer.list', 'uses' => 'AuctionManageController@bandAuction'
        ]);

        Route::get('bidding-action', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@biddingAction'
        ]);
        Route::get('print/{id}/auction/{auction}', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@printCustomer'
        ]);

        /**
        * POST Routes
        */
        Route::post('add', [
            'as' => 'auction.add', 'uses' => 'AuctionManageController@addAuction'
        ]);
        Route::post('update', [
            'as' => 'auction.update', 'uses' => 'AuctionManageController@update'
        ]);
        Route::post('delete_detail', [
            'as' => 'auction.delete_detail', 'uses' => 'AuctionManageController@deleteDetail'
        ]);
        Route::post('register/{id}', [
            'as' => 'auction.details', 'uses' => 'AuctionManageController@updateCustomer'
        ]);
    });
});
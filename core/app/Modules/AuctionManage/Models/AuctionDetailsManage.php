<?php namespace App\Modules\AuctionManage\Models;

/**
*
* AuctionDetailsManage Model
* @author Nishan Randika <nishanran@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class AuctionDetailsManage extends Model {
    use SoftDeletes;
	/**
     * The sns_location table associated this model.
     */
    protected $table = 'sns_auction_detail';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutuated to dates
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * This function is used to get sns item details
     */

    public function item(){
	    return $this->belongsTo('App\Modules\ProductManage\Models\ProductManage','sns_item_id','id');
    }

    public function bid(){
        return $this->hasOne('App\Modules\AuctionLiveManage\Models\AuctionBidManage','sns_auction_detail_id','id')->orderBy('bid_time', 'desc');
    }

    public function bids(){
        return $this->hasMany('App\Modules\AuctionLiveManage\Models\AuctionBidManage','sns_auction_detail_id','id')->orderBy('id', 'desc');
    }

    public function auction_bid(){
        return $this->belongsTo('App\Modules\AuctionLiveManage\Models\AuctionBidManage','id','sns_auction_detail_id');
    }

    public function watchList(){
        return $this->hasMany('App\Models\WatchList','sns_item_id','id');
    }
    /*This function is used to get highest bid*/
    public function highest_bid(){
        return $this->hasOne('App\Modules\AuctionLiveManage\Models\AuctionBidManage','sns_auction_detail_id','id')->orderBy('bid_time', 'desc');
    }
    
    /**
     * auction belongs to user
     * @return object User
     */
    public function auction(){
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionManage','sns_auction_id','id');
    }
    
    public function item_approve(){
        return $this->hasOne('App\Modules\ProductManage\Models\ItemApproveManage','detail_id','id');
    }

    /** 
    * This function is used to get approved item
    */
    public function approved(){
        return $this->hasOne('App\Modules\ProductManage\Models\ItemApproveManage','detail_id','id');
    }

    public function item_with_code(){
        return $this->belongsTo('App\Modules\ProductManage\Models\ProductManage','sns_item_id','id')
                    ->select(DB::raw("CONCAT(item_code,' - ',name) AS sns_item"),'id');
    }

    public function customer_auction(){
        return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerAuctionManage', 'sns_auction_id', 'auction_id');
    }

    public function customer_offer(){
        return $this->hasOne('App\Modules\AuctionLiveManage\Models\CustomerOffer','auction_detail_id', 'id')
                    ->orderBy('offer_amount', 'DESC');
    }



}

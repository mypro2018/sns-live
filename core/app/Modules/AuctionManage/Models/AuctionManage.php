<?php namespace App\Modules\AuctionManage\Models;

/**
*
* AuctionManage Model
* @author Nishan Randika <nishanran@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class AuctionManage extends Model {

	/**
     * The sns_location table associated this model.
     */
    protected $table = 'sns_auction';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
	 * Location
	 * @return object Location
	 */
	public function location() {
		return $this->belongsTo('App\Modules\LocationManage\Models\LocationManage', 'sns_location_id', 'id');
	}

	/**
	 * auction belongs to user
	 * @return object User
	 */
	public function user(){
        return $this->belongsTo('App\Models\User','created_by','id');
    }

    /** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auction_details(){
		return $this->hasMany('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_auction_id','id')
		->where('status', PRODUCT_TO_BEGIN)
		->orWhere('status', PRODUCT_TO_LIVE);
	}

	public function live_auction_product(){
	    return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_auction_id','id')->where('status', PRODUCT_TO_LIVE);
	}


	/** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auction_details_available(){
		return $this->hasMany('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_auction_id','id');
	}

	public function card_range(){
	    return $this->hasMany('App\Models\CardRange','auction_id','id');
	}

	/** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auction_details_summary(){
		return $this->hasMany('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_auction_id','id');
	}

	public function auction_allowed_customer_type(){
	    return $this->hasMany('App\Models\AuctionAllowedCustomerType','auction_id','id')
		->where('status', DEFAULT_STATUS)
		->whereNull('deleted_at');
	}



}

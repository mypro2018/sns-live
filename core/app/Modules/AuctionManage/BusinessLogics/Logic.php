<?php namespace App\Modules\AuctionManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Nishan Randika <nishanran@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AuctionManage\Models\AuctionManage;
use App\Modules\AuctionManage\Models\AuctionDetailsManage;
use App\Modules\CustomerManage\Models\CustomerAuctionManage;
use Core\UserManage\Models\User;
use Illuminate\Support\Facades\DB;
use App\Modules\CustomerManage\Models\CustomerManage;
use Exception;
use App\Models\CardRange;
use App\Modules\AuctionLiveManage\Models\AuctionBidManage;
use App\Modules\PaymentManage\Models\PaymentManage;
use App\Modules\PaymentManage\Models\PaymentTransactionManage;
use League\Flysystem\Exception as FlysystemException;
use App\Models\AuctionAllowedCustomerType;
use App\Models\CustomerAllowedType;
use App\Models\ButtonPad;

class Logic{

	/**
     * This function is used to add a auction
     * @param array $data, array $items
     * @return object Auction object if success, exception otherwise
     */
    public function add($data, $items, $types) {
        
        DB::transaction(function() use($data, &$items, $types){
            $auction = AuctionManage::create($data);
            if($auction) {
                //save auction allower customer types here
                if(sizeof($types) > 0){
                    foreach ($types as $value) {
                        $auction_allowed_customer_types = AuctionAllowedCustomerType::create([
                            'auction_id'        => $auction->id,
                            'customer_type_id'  => $value,
                            'status'            => DEFAULT_STATUS    
                        ]);
                        if(!$auction_allowed_customer_types) {
                            throw new Exception("Error occured while allocating customer types");
                        }
                    }
                }
                if(count($items) > 0){
                    foreach ($items as $item) {
                        $details = AuctionDetailsManage::create([
                            'sns_auction_id'    => $auction->id,
                            'sns_item_id'       => $item->item->id,
                            'start_bid_price'   => $item->min_value,
                            'withdraw_amount'   => $item->with_value,
                            'order'             => $item->order,
                            'status'            => PRODUCT_TO_BEGIN
                        ]);
                        if(!$details) {
                            throw new Exception("Error occured while adding new auction details");
                        }
                    }
                }
            }
            else {
                throw new Exception("Error occured while adding new auction");
            }

            // admin user assign to all auctions
            // $mass_arr = [];
            // $user = User::all();
            // foreach($user as $u){
            //     $customer_auction = CustomerAuctionManage::where('user_id','=',$u->id)->where('auction_id','=',$auction->id)->get();
            //     if(count($customer_auction) > 0){}else{
            //         array_push($mass_arr,['user_id' => $u->id,'auction_id' => $auction->id]);
            //     }
            // }
            // DB::transaction(function() use($mass_arr)
            // {
            //     $customer_auction = CustomerAuctionManage::insert($mass_arr);
            //     if(!$customer_auction){
            //         return redirect('user/add')->with([ 'error' => true,
            //         'error.message'=> 'Error Occueered !',
            //         'error.title' => 'Ooops!']);
            //     }
            // });
        });
        return true;
    }

    /**
     * This function is used to get paginate auction details
     * @return auction object
     */
    public function getPaginateAuctions() {
        return $auction = AuctionManage::with(['location','user','card_range'])->paginate(unserialize(SHOW_OPTION)[1]);
    }

    /**
     * This function is used to get all available auction details
     * @return auction array
     */
    public function getAvailableAuctions() {
        return $auction = AuctionManage::with('live_auction_product')
                ->where('auction_status', '!=', AUCTION_OVER)
                ->where('auction_view_status', '=', AUCTION_SHOW)
                ->orderBy('auction_date', 'desc')
                ->get();
        //dd($auction);
    }

    /**
     * This function is used to get all auction details
     * @return auction object
     */
    public function getAllAuctions() {
        return $auction = DB::table('sns_auction')
                           ->leftjoin('sns_auction_location', 'sns_auction.sns_location_id', '=', 'sns_auction_location.id')
                           ->select('sns_auction.id','sns_auction.event_name','sns_auction.auction_date','sns_auction_location.name')
                           ->orderBy('sns_auction.created_at', 'DESC')
                           ->get();
    }

    /**
     * This function is used to get auction details
     * @parm - integer $id that need to get auction details
     * @return auction object
     */
    public function getAuctionDetails($id, $category = null) {
        return $auction_detail = AuctionDetailsManage::with(['watchList'])->select('items.*','images.image_path','sns_auction_detail.start_bid_price','sns_auction_detail.sns_auction_id')
            ->leftJoin('sns_item as items', function($item_join){
                $item_join->on('sns_auction_detail.sns_item_id', '=', 'items.id');
            })
            ->leftJoin('sns_auction as auctions', function($auction_detail_join) {
                $auction_detail_join->on('sns_auction_detail.sns_auction_id', '=', 'auctions.id');
            })
            ->leftJoin('sns_item_image as images', function($image_join){
                $image_join->on('sns_auction_detail.sns_item_id', '=', 'images.sns_item_id');
            })
            ->where('sns_auction_detail.call', '<>', 3)
            ->where('sns_auction_detail.sns_auction_id', $id)
            ->where('auctions.auction_status', '!=', 3)
            ->where('sns_auction_detail.status', '<>', 3)
            ->groupBy('items.id')
            ->get();

    }

    /**
     * This function is used to get auction details
     * @parm - integer $id that need to get auction details
     * @return auction object
     */
    public function getAuctionCategoryFilterDetails($id, $category) {
        $auction_detail = AuctionDetailsManage::with(['watchList'])->select('items.*','images.image_path','sns_auction_detail.start_bid_price','sns_auction_detail.sns_auction_id')
            ->leftJoin('sns_item as items', function($item_join){
                $item_join->on('sns_auction_detail.sns_item_id', '=', 'items.id');
            })
            ->leftJoin('sns_auction as auctions', function($auction_detail_join) {
                $auction_detail_join->on('sns_auction_detail.sns_auction_id', '=', 'auctions.id');
            })
            ->leftJoin('sns_item_image as images', function($image_join){
                $image_join->on('sns_auction_detail.sns_item_id', '=', 'images.sns_item_id');
            })
            ->where('sns_auction_detail.call', '<>', 3)
            ->where('sns_auction_detail.sns_auction_id', $id)
            ->where('auctions.auction_status', '!=', 3)
            ->where('sns_auction_detail.status', '<>', 3);
            
        if($category > 0){
            $auction_detail = $auction_detail->where('sns_item_category_id', '=', $category);
        }

        $auction_detail->groupBy('items.id');
        return $auction_detail->get();

    }

    /**
     * This function is used to get auction item details
     * @parm - integer $id that need to get auction item details
     * @return auction object
     */
    public function getAuctionItemDetails($id) {
        $auctionItem = AuctionDetailsManage::find($id);
        if($auctionItem) {
            return $auctionItem;
        }
        else {
            return false;
        }
    }

    /**
     * This function is used to search auction
     * @param integer $id, datetime $date,integer $status,integer $location
     * @response auction object
     */
    public function searchAuction($id,$date,$status,$location) {
        $search_auction = AuctionManage::with(['location' => function($sql){
            $sql->select('id','name');
        },'user']);
        if($id > 0){
            $search_auction = $search_auction->where('id',$id);
        }
        if($date > 0){
            $search_auction = $search_auction->where('auction_date',$date);
        }
        if($status != ''){
            $search_auction = $search_auction->where('auction_status',$status);
        }
        if($location > 0){
            $search_auction = $search_auction->where('sns_location_id',$location);
        }        
        $search_auction->orderBy('created_at','desc');
        return $search_auction->paginate(10);
    }

    /**
     * This function is used to update auction & auction details
     * @param integer $id, array $data, array $products, array $newProducts
     * @respnse is boolean
     */
    public function updateAuction($id, $data, $products, $newProducts, $customerTypes) {
        DB::transaction(function() use($id, $data, $products, $newProducts, $customerTypes) {
            //update auction
            $auction = AuctionManage::find($id)->update($data);
            if($auction){
                //if updated then check customer types allocated
                $delete_all_auction_allowed_customer_types = AuctionAllowedCustomerType::where('auction_id', $id)->delete();
                
                if(sizeof($customerTypes) > 0){
                    foreach ($customerTypes as $value) {
                        $auction_allowed_customer_types = AuctionAllowedCustomerType::create([
                            'auction_id'        => $id,
                            'customer_type_id'  => $value,
                            'status'            => DEFAULT_STATUS    
                        ]);
                        if(!$auction_allowed_customer_types) {
                            throw new Exception("Error occurred while allocating customer types");
                        }
                    }
                }
                if(count($products) > 0) {
                    // update item details
                    foreach ($products as $product) {
                        // get auction detail
                        $details = AuctionDetailsManage::find($product->id)->update([
                            'start_bid_price'   => $product->start_bid_price,
                            'withdraw_amount'   => $product->withdraw_amount,
                            'order'             => $product->order
                        ]);

                        if(!$details) {
                            throw new Exception("Error occured while updating auction details");
                        }
                    }
                }

                if(count($newProducts) > 0) {
                    // add new item details
                    foreach ($newProducts as $product) {
                        if(isset($product->item) && $product->min_value && $product->with_value && $product->order) {
                            $details = AuctionDetailsManage::create([
                                'sns_auction_id'    => $id,
                                'sns_item_id'       => $product->item->id,
                                'start_bid_price'   => $product->min_value,
                                'withdraw_amount'   => $product->with_value,
                                'order'             => $product->order
                            ]);

                            if(!$details) {
                                throw new Exception("Error occured while adding new auction details");
                            }
                        }
                    }

                    return true;
                }
                
            }else {
                throw new Exception("Error occurred while updating auction");
            }
        });
    }
    /**
     * This function is used to auction details
     * @param integet $auction_id integer $customer_id
     * @return auction detail object
     */

    public function getItemDetails($auction_id,$customer_id){
        return $item_details = AuctionDetailsManage::with(array(
            'item' => function($query){},
            'highest_bid' => function($query) use ($customer_id)
            {
                $query->where('sns_customer_id',$customer_id);
            },
            'bid' => function($query){}))->where('sns_auction_id',$auction_id)->paginate(10);
    }

    /**
     * This function is used to get auction
     * @param integet $auction_id
     * @return auction object
     */
    public function getAuction($auction_id){
        return $auction = AuctionManage::with(['auction_details' => function($query) {
            $query->orderBy('created_at', 'DESC');
        },'location','auction_details.item_with_code', 'auction_allowed_customer_type', 'card_range' => function($query){
            $query->where('card_type', ONLINE_CARD);
        }])
        ->where('id', $auction_id)
        ->get();
    }
    

     /**
     * This function is used to get auction only
     * @param integet $auction_id
     * @return auction object
     */
    public function getAuctionOnly($auction_id){
        return $auction = AuctionManage::find($auction_id);
    }


    /**
     * This function is used to get auction and its details
     * @param integet $auction_id
     * @return auction object
     */
    public function getAuctionDetail($auction_id){
        return $auction = AuctionManage::with(['location','auction_details_available' => function($sql){
            $sql->select('id','sns_item_id','sns_auction_id','start_bid_price', 'withdraw_amount', 'order', 'call', 'request_offer', 'status')->whereNull('deleted_at');
        },'auction_details_available.item', 'auction_allowed_customer_type'])
            ->where('id',$auction_id)
            ->get();
    }

    /**
     * This function is used to auction item details
     * @param integet $auction_id
     * @return auction detail object
     */

    public function getAuctionHistory($auction_id){
        return $item_details = AuctionDetailsManage::with([
            'item' => function($query){},
            'highest_bid.bidder' => function($query){},
            'bid' => function($query){},
            'item_approve' => function($query){} 
            ])->where('sns_auction_id',$auction_id)->paginate(unserialize(SHOW_OPTION)[3]);
    }

    /**
     * This function is used to get auction_details
     * @param integet $auction_id
     * @return auction_details object
     */
    /*public function getRelatedItemsByAuction($auction_id){
        return $auction = AuctionDetailsManage::select('items.*', 'images.image_path')
            ->leftJoin('sns_item as items', function($item_join){
                $item_join->on('sns_auction_detail.sns_item_id', '=', 'items.id');
            })
            ->leftJoin('sns_item_image as images', function($image_join){
                $image_join->on('items.id', '=', 'images.sns_item_id');
            })
            ->where('sns_auction_detail.sns_auction_id',$auction_id)
            // ->where('items.status', '<>', 3)
            ->groupBy('sns_auction_detail.sns_item_id')
            ->get();
    }*/


    /**
     * This function is used to get auction count
     * @param 
     * @return auction_count 
     */
    
    public function auctionCount(){
        $auction = AuctionManage::all();
        return count($auction);
    }

    /**
     * This function is used to get auction item count
     * @param 
     * @return auction item count 
     */
    public function itemCount(){
        $item = AuctionDetailsManage::groupBy('sns_item_id')->get();
        return count($item);
    }
    /**
     * This function is used to get auction with item
     * @param 
     * @return auction list with item 
     */
    public function auction_details(){
        return $auction_details = AuctionManage::with('auction_details.item','auction_details.highest_bid','auction_details.item_approve')->orderBy('auction_date','DESC')->get();
    }
    /**
     * This function is used to get auction with item
     * @param integer $auction_id
     * @return auction list with item 
     */
    public function search_auction_details($auction_id){
        return $auction_details = AuctionManage::with('auction_details.highest_bid','auction_details.item','auction_details.item_approve')->where('id',$auction_id)->get();
    }

     /**
     * This function is used to update auction show 
     * @param integer $auction_id
     * @param integer $visible_status
     * @return 
     */

    public function auctionVisible($auction_id, $visible_status){
        DB::beginTransaction();
        $visible = AuctionManage::find($auction_id);
        $visible->auction_view_status = $visible_status;
        $visible->save();
        if(count($visible) == 0){
            DB::rollBack();
            throw new Exception("Error occured while changing auction view status");
        }
        DB::commit();
    }

    /**
     * This function is used to get all auction
     * @param 
     * @return auction object
     */
    public function auctionList(){
        return $auction = AuctionManage::select('id', 'event_name')
            ->where('auction_status', '!=', AUCTION_OVER)
            ->whereNull('deleted_at')
            ->get();
    }

    //search customer
    public function searchCustomer($auction_id, $search){

        //get customer details
        $customer = CustomerManage::with('customer_allowed_type', 'country')->leftJoin('sns_customer_auction',function($join){
            $join->on('sns_customer_register.id', '=', 'sns_customer_auction.user_id');
        })
        ->select(
            'sns_customer_register.*', 
            'sns_customer_auction.id as customer_auction_id',
            'sns_customer_auction.auction_id'
        )
        ->where(function ($query) use($search) {
           $query->where('sns_customer_register.id', '=', trim($search))
                  ->orWhere('sns_customer_register.nic', '=', trim($search))
                  ->orWhere('sns_customer_register.contact_no', '=', trim($search));
        })
        ->whereNull('sns_customer_register.deleted_at')
        ->where('sns_customer_register.status', '=', DEFAULT_STATUS)
        ->first();

        //get customer auction details
        $customerAuction = CustomerAuctionManage::join('sns_customer_register', function($join){
            $join->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id');
        })
        ->select('sns_customer_auction.card_no')
        ->where('sns_customer_auction.auction_id', '=', $auction_id)
        ->where(function ($query) use($search) {
            $query->where('sns_customer_auction.card_no', '=', trim($search))
                  ->orWhere('sns_customer_register.id', '=', trim($search)) 
                  ->orWhere('sns_customer_register.nic', '=', trim($search))
                  ->orWhere('sns_customer_register.contact_no', '=', trim($search));
        })
        ->where('sns_customer_register.status', '=', DEFAULT_STATUS)
        ->whereNull('sns_customer_auction.deleted_at')
        ->whereNull('sns_customer_register.deleted_at')
        ->first();

        return [
            'customer'                  => count($customer),
            'customer_auction'          => count($customerAuction),
            'customer_details'          => $customer,
            'customer_auction_details'  => $customerAuction
        ];
    }

    //save customer card range

    public function saveCardRange($request){
        DB::beginTransaction();
        //check card range exist for this auction
        $searchCardRange = CardRange::where('auction_id', '=', $request->auction_id)
        ->where('status', '=', DEFAULT_STATUS)
        ->whereNull('deleted_at')
        ->get();
        
        //check card range no used by customer
        $online_from = explode('-', $request->range_plan['online_range_from']);
        $online_to   = explode('-', $request->range_plan['online_range_end']);
        $floor_from  = explode('-',$request->range_plan['floor_range_from']);
        $floor_to    = explode('-',$request->range_plan['floor_range_end']);

        if(count($searchCardRange) > 0){
            //update online range
            if($online_from[1] == ONLINE_CARD && $online_to[1] == ONLINE_CARD && !is_null($online_from[0]) && !is_null($online_to[0])){
                $updateOnlineRange = CardRange::where('auction_id', $request->auction_id)
                    ->where('card_type', ONLINE_CARD)
                    ->update([
                        'card_range_from' => $online_from[0],
                        'card_range_to'   => $online_to[0],
                        'status'          => DEFAULT_STATUS
                    ]);
                if(count($updateOnlineRange) == 0){
                    DB::rollBack(); 
                    throw new Exception("Error occured while updating online card range");
                }
            }
            //update floor range
            if($floor_from[1] == FLOOR_CARD && $floor_to[1] == FLOOR_CARD && !is_null($floor_from[0]) && !is_null($floor_to[0])){
                $updateFloorRange = CardRange::where('auction_id', $request->auction_id)
                ->where('card_type', FLOOR_CARD)
                ->update([
                    'card_range_from' => $floor_from[0],
                    'card_range_to'   => $floor_to[0],
                    'status'          => DEFAULT_STATUS
                ]);
                if(count($updateFloorRange) == 0){
                    DB::rollBack(); 
                    throw new Exception("Error occured while updating floor card range");
                }
            }
        }else{
            if($online_from[1] == ONLINE_CARD && $online_to[1] == ONLINE_CARD && !is_null($online_from[0]) && !is_null($online_to[0])){
                $createFloorRange = CardRange::create([
                    'auction_id'        => $request->auction_id,
                    'card_type'         => ONLINE_CARD,
                    'card_range_from'   => $online_from[0],
                    'card_range_to'     => $online_to[0],
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d h:i:s'),
                    'updated_at'        => date('Y-m-d h:i:s')
                ]);
                if(count($createFloorRange) == 0){
                    DB::rollBack(); 
                    throw new Exception("Error occured while adding floor card range");
                }
            }
            if($floor_from[1] == FLOOR_CARD && $floor_to[1] == FLOOR_CARD && !is_null($floor_from[0]) && !is_null($floor_to[0])){
                $createFloorRange = CardRange::create([
                    'auction_id'        => $request->auction_id,
                    'card_type'         => FLOOR_CARD,
                    'card_range_from'   => $floor_from[0],
                    'card_range_to'     => $floor_to[0],
                    'status'            => DEFAULT_STATUS,
                    'created_at'        => date('Y-m-d h:i:s'),
                    'updated_at'        => date('Y-m-d h:i:s')
                ]);
                if(count($createFloorRange) == 0){
                    DB::rollBack(); 
                    throw new Exception("Error occured while adding floor card range");
                }
            }
            
        }
        DB::commit();
        $search = $this->getCardRange($request->auction_id);
        
        if(count($search) > 0){
            return $search;
        }else{
            return [];
        }
    }

    //get card range
    public function getCardRange($auction_id){

        $search = CardRange::where('auction_id', '=', $auction_id)
        ->where('status', '=', DEFAULT_STATUS)
        ->whereNull('deleted_at')
        ->get();
        if(count($search) > 0){
            return $search;
        }else{
            return [];
        }
    }


    //get card range
    public function getLastRegisterCard($auction_id, $card_type){

        $search = CardRange::with('last_customer_card')
        ->where('auction_id', '=', $auction_id)
        ->where('card_type', $card_type)
        ->where('status', '=', DEFAULT_STATUS)
        ->whereNull('deleted_at')
        ->first();
        if(count($search) > 0){
            return $search;
        }else{
            return [];
        }
    }

    //band customer for auction
    public function bandAuction($auction_id, $customer_id, $status){
        DB::beginTransaction();
        // $check_bid_have = AuctionBidManage::join('sns_auction_detail',function($join){
        //     $join->on('sns_auction_bid.sns_auction_detail_id', '=', 'sns_auction_detail.id');
        // })
        // ->where('sns_auction_detail.sns_auction_id', $auction_id)
        // ->where('sns_auction_bid.sns_customer_id', $customer_id)
        // ->whereNUll('sns_auction_detail.deleted_at')
        // ->whereNUll('sns_auction_bid.deleted_at')
        // ->first();
        // if(count($check_bid_have) > 0){
        //     return $check_bid_have;
        // }else{
            $band_auction = CustomerAuctionManage::where('auction_id', $auction_id)
            ->where('user_id', $customer_id)
            ->update([
                'band_status'=> $status,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            if(count($band_auction) == 0){
                DB::rollBack();
                throw new Exception("Error occured while updating customer auction band");
            }
 
            DB::commit();   
            return $band_auction;

        //}
    }


    //get customer auction registerd card
    public function customerAuctionCard($customer_id = null, $auction_id = null){

        return $card = CustomerAuctionManage::where('user_id', $customer_id)
                                            ->where('auction_id', $auction_id)
                                            ->whereNull('deleted_at')
                                            ->first();
    }


    //find auction item details
    public function findAuctionItemDetails($item_id){
        if(isset($item_id)){
            return $auction_item_details = AuctionDetailsManage::where('sns_item_id', $item_id)
            ->orderBy('id', 'DESC')
            ->first();
        }else{
            throw new Exception('Argument missing.');
        }
    }

     /**
     * This function is used to active & inactive bid now option
     * @param integer $auction_id
     * @param integer $visible_status
     * @return 
     */

    public function bidnowActiveInactive($request){
        DB::beginTransaction();
        $visible = AuctionManage::find($request->get('auction_id'));
        if($request->get('section') == 1){
            $visible->bid_now_active_status_1 = $request->get('status');
        }elseif($request->get('section') == 2){
            $visible->bid_now_active_status_2 = $request->get('status');
        }else{
            $visible->live_section_display_status = $request->get('status');
        }
        $visible->save();
        if(count($visible) == 0){
            DB::rollBack();
            throw new Exception("Error occured while changing auction bidding status");
        }
        DB::commit();
    }


    public function registerCustomerDetails($customer_id, $auction_id){

        return $customer_auction = DB::table('sns_customer_auction')->join('sns_customer_register', function($join){
            $join->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id');
        })
        ->join('sns_auction', function($sql){
            $sql->on('sns_customer_auction.auction_id', '=', 'sns_auction.id');
        })
        ->leftJoin('sns_auction_location', function($sql){
            $sql->on('sns_auction.sns_location_id', '=', 'sns_auction_location.id');
        })
        ->select('sns_auction.auction_date', 'sns_auction.event_name', 'sns_auction_location.name as location', 'sns_auction.auction_start_time', 'sns_customer_register.id as register_id', 'sns_customer_auction.card_no', DB::raw("CONCAT(sns_customer_register.fname, ' ',sns_customer_register.lname) AS customer_name"), 'sns_customer_register.contact_no', 'sns_customer_register.nic')
        ->where('sns_customer_auction.user_id', '=', $customer_id)
        ->where('sns_customer_auction.auction_id', '=', $auction_id)
        ->get();
    } 

    /**
     * This function is used to validate with auction allowed 
     * customer types with customer allocated customer types
     * @param integer auction_id
     * @param integer customer_id
     * @return 
     */
    public function getAllowedAuction($request){
        $in_array = 0;
        $auction_allowed_types = AuctionAllowedCustomerType::join('customer_type', function($join){
            $join->on('auction_allowed_customer_type.customer_type_id', '=', 'customer_type.id');
        })->where('auction_allowed_customer_type.auction_id', $request->live_auction_id)
        ->whereNull('auction_allowed_customer_type.deleted_at')
        ->lists('customer_type.name')
        ->toArray();
        if(sizeof($auction_allowed_types) > 0){
            foreach($auction_allowed_types as $key => $value){
                $customer_allowed_types = CustomerAllowedType::join('customer_type', function($join){
                    $join->on('customer_allowed_type.customer_type_id', '=', 'customer_type.id');
                })->where('customer_allowed_type.customer_id', $request->customer_id)
                ->whereNull('customer_allowed_type.deleted_at')
                ->lists('customer_type.name')
                ->toArray();
                if(sizeof($customer_allowed_types) > 0){
                    if (in_array($value, $customer_allowed_types)){
                        $in_array = 1;
                    }
                }
            }
            return $in_array;
        }else{
            return $in_array; //cannot login
        }
    }

    /**
     * Update bidding panel image & video view
     * @param integer customer_id
     * @param integer auction_id
     * @param integer status
     */
    public function updateImageVideoViewStatus($request){
        DB::beginTransaction();
        $update = CustomerAuctionManage::where('user_id', $request->customer_id)
        ->where('auction_id', $request->auction_id)
        ->update([
            'video_image_view_status' => $request->status
        ]);
        if(count($update) == 0){
            DB::rollBack();
            throw new Exception("Error occured while updating customer auction");
        }
        DB::commit();
    }


    /**
     * Check customer already registered with same auction 
     * @param integer auction_id
     * @param integer customer_id
     * @return 
     * */

    public function isRegister($auction_id, $customer_id){
        $customerAuction = CustomerAuctionManage::with(['card_range' => function($sql){
            $sql->where('card_type', FLOOR_CARD);
        }])
        ->where('auction_id', $auction_id)
        ->where('user_id', $customer_id)
        ->first();
        return $customerAuction;
    }


}

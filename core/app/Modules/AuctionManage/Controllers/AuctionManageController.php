<?php 
namespace App\Modules\AuctionManage\Controllers;


/**
* AuctionManageController class
* @author Nishan Randika <nishanran@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use App\Modules\LocationManage\BusinessLogics\Logic as LocationLogic;
use App\Modules\ProductManage\BusinessLogics\Logic as ProductLogic;
use App\Modules\CustomerManage\BusinessLogics\Logic as CustomerLogic;
use App\Modules\FrontEnd\BusinessLogics\Logic AS FrontLogic;

use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Exception;
use Response;
use Sentinel;
// use App\Classes\PdfTemplate;

class AuctionManageController extends Controller {

	protected $auction;
	protected $location;
	protected $product;
	protected $customer;
	protected $front;

	public function __construct(AuctionLogic $auction, LocationLogic $location, ProductLogic $product,CustomerLogic $customer, FrontLogic $front)
	{
        $this->auction  = $auction;
        $this->location = $location;
		$this->product 	= $product;
		$this->customer = $customer;
		$this->front    = $front;
    }

	/**
	 * This function use to display form to add new auction
	 *
	 * @return Response
	 */
	public function add()
	{
		return view("AuctionManage::add")->with([
			'locations'			=> $this->location->getLocationList(),
			'products'			=> $this->product->getAvailableItems(),
			'customer_types'	=> $this->customer->getCustomerTypes()
		]);
	}

    /**
     * Display a listing of the auction.
     *
     * @return Response
     */
    public function listAuction()
    {
        $paginateList = $this->auction->getPaginateAuctions();
        $auctions     = $this->auction->getAllAuctions();
        $locations    = $this->location->getLocationList();

        return view("AuctionManage::list")->with([
        	'paginateList' 		=> $paginateList,
        	'auctions' 			=> $auctions,
        	'locations' 		=> $locations,
        	'auction_id' 		=> '',
        	'auction_date' 		=> '',
        	'auction_status' 	=> '',
        	'auction_location' 	=> '',
        ]);
    }

	/**
     * This function used to add auction
     */
    public function addAuction(Request $request)
    {
		try {
			// decode json object
			$form = json_decode($request->auction);

			if(isset($form->items[0]->item)){

				$item = $form->items;

			}else{

				$item = [];

			}

            //check image is browse
            if($request->hasFile('file')) {
            	$file = $request->file('file'); //get file
            	$extn = $file->getClientOriginalExtension(); //get extension
            	$fileName = 'auction-' . date('YmdHis') . '.' . $extn; //set file name	
            	$destinationPath = storage_path('uploads/images/auction'); // destination	
            	$file->move($destinationPath, $fileName);//move to file
                //resize image
                $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
                    $c->upsize();
                },"top");
                //save
                $img->save($destinationPath . '/' . $fileName);
            }
            else {
            	$fileName = '';
            }
			$user_id = Sentinel::getUser()->id;
			
            $auction = $this->auction->add([
				'auction_no'          	=> $form->no,
                'event_name'          	=> $form->name,
                'auction_date'  		=> $form->date,
                'auction_start_time'  	=> $form->time,
                'registration_fee'    	=> (isset($form->fee))?$form->fee:'',
                'auction_address'    	=> (isset($form->address))?$form->address:'',
                'auction_url'    		=> (isset($form->url))?$this->converUrl($form->url):'',
                'note'    				=> (isset($form->note))?$form->note:'',
                'sns_location_id'    	=> (isset($form->location->id))?$form->location->id:'',
                'image'    				=> $fileName,
                'auction_status'        => DEFAULT_STATUS,
                'created_by'            => $user_id,
				'advance_rate'          => $form->advance
            ], $item, $form->types);

            return Response::json([
				'success' => true,
				'message' => 'Auction added successfully.',
				'title' => 'Job well done!'
			]);

        }catch(Exception $e){

        	return Response::json([
				'error' => true,
				'message' => $e->getMessage(),
				'title' => 'Ops!'
			]);
        }
	}


    public function converUrl($url){
        $string     = $url;
        $search     = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
        $replace    = "youtube.com/embed/$1";    
        $url = preg_replace($search,$replace,$string);
        return $url;
    }

	/**
     * This function is used to display auction image and other details
     * @return Response
     */
    public function getAuctionDetails(Request $request) {
        if($request->ajax()) {
			$customer_types	= $this->customer->getCustomerTypes();
            $auction 		= $this->auction->getAuctionDetail($request->get('auction_id'));
            return Response::json(['details' => $auction[0], 'customer_types' => $customer_types]);
        }
        else {
            return Response::json([]);
        }
    }

	/**
     * This function is used to search auction
     * @return Response
     */
    public function searchAuction(Request $request){
    	$paginateList = $this->auction->searchAuction($request->get('auction_id'),$request->get('auction_date'),$request->get('auction_status'),$request->get('auction_location'));
        
        $auctions = $this->auction->getAllAuctions();
        $locations = $this->location->getLocationList();
        
        return view("AuctionManage::list")->with([
        	'paginateList' 		=> $paginateList,
        	'auctions' 			=> $auctions,
        	'locations' 		=> $locations,
        	'auction_id' 		=> $request->get('auction_id'),
        	'auction_date' 		=> $request->get('auction_date'),
        	'auction_status' 	=> $request->get('auction_status'),
        	'auction_location' 	=> $request->get('auction_location'),
        ]);
    }

	/**
     * This function is used to auction item details
     * @param integer $auction_id
     * @return
     */
    public function getItemDetails($auction_id){
        $auction = $this->auction->getAuction($auction_id);
        $item_details = $this->auction->getAuctionHistory($auction_id);
        return view("AuctionManage::auction")->with(['auction' => $auction,'details' => $item_details]);
    }

    /**
     * This function is used to auction item details
     * @param integer $auction_id
     * @return
     */
    public function getAuctionStatus(Request $request){
        $product = $this->product->getItemDetails($request->item_id);
        if($product[0] && $product[0]->auction_detail){
            $status = $product[0]->auction_detail->auction->auction_status;
        }
        else{
            $status = 0;
        }
        return Response::json([
            'status' => $status
        ]);
	}
	


	public function changeVisible(Request $request){
		try{
			$visible = $this->auction->auctionVisible($request->get('auction_id'), $request->get('status'));
		}catch(\Exception $e){
			return $e;
		}
	}

	public function biddingAction(Request $request){
		try{
			$visible = $this->auction->bidnowActiveInactive($request);
		}catch(\Exception $e){
			return $e;
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Register Customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function registerCustomer($id)
	{
		$auction   		= $this->auction->getAuctionOnly($id);
		$cardRange 		= $this->auction->getCardRange($id);
		$lastFloorCard  = $this->auction->getLastRegisterCard($id, FLOOR_CARD);
		$customer_types	= $this->customer->getCustomerTypes();
		$countries      = $this->front->allCountries();
		$cities         = $this->front->allCities();


		if($auction){
			return view('AuctionManage::customer_register')->with([
                'auction'    		=> $auction,
				'card_range' 		=> $cardRange,
				'last_floor_card'	=> $lastFloorCard,
				'customer_types'	=> $customer_types,
				'countries'			=> $countries,
				'cities'			=> $cities
            ]);
		}else{
			return redirect( 'auction/list' )->with([
                'error' => true,
                'error.message' => 'Auction Cannot Found !',
                'error.title'   => 'Error..!'
            ]);
		}
		
	}

	/**
	 * Register Customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function searchCustomer(Request $request)
	{
		if($request->ajax()){
			$search = $this->auction->searchCustomer($request->get('auction_id'), $request->get('search'));
			return Response::json(['search' => $search]);
		}else{
			return Response::json([
				'status' => $status
			]);
		}
	}
	

	//save customer card range
	public function saveCardRange(Request $request){
		if($request->ajax()){
			$saveRange = $this->auction->saveCardRange($request);
			if(count($saveRange) > 0){
				return Response::json(['details' => $saveRange]);
			}else{
                return Response::json([]);
			}
		}else{
			return Response::json([]);
		}
	}

	//save customer
	public function updateCustomer(Request $request, $id){

		$last_card_no  		 = null;
		$from    	   		 = trim($request->get('floor_range_from'));
		$to      	   	     = trim($request->get('floor_range_to'));
		$card_no 	   		 = trim($request->get('floor_range_to'));
		$user_id       		 = $request->get('customer_id');
		$customer_auction_id = trim($request->get('customer_auction_id'));
		
		//validate user input
        $this->validate($request, [
			// 'card_no'    	=> 'required|integer|between:'.$from.','.$to.'|unique:sns_customer_auction,card_no,'.$customer_auction_id.',id,auction_id,'.$id,
			'first_name' 	=> 'required',
			'nic'		 	=> 'unique:sns_customer_register,nic,'.$user_id,
			'contact'    	=> 'required|numeric|digits:10|unique:sns_customer_register,contact_no,'.$user_id,
			'mail'       	=> 'unique:sns_customer_register,email,'.$user_id.'|email',
			'telephone_no'  => 'numeric|unique:sns_customer_register,telephone_no,'.$user_id
        ]);
		
		try{
			$result = $this->customer->registerCustomer($request);
			if(sizeof($result) > 0){
				//upload customer nic or licence image
				$fileName1 = $result->nic_image_path;
				$fileName2 = $result->proof_image_path;
				if($request->hasFile('file1')){
					$file 	         = $request->file('file1'); //get file
					$extension 		 = $file->getClientOriginalExtension(); //get extension
					$fileName1       = 'customer-'.$result->id.'.'.$extension; //set file name
					$destinationPath = storage_path('uploads/images/customer/nic-licence'); // destination
					$file->move($destinationPath, $fileName1);//move to file
				}
				//upload customer proof file image
				if($request->hasFile('file2')){
					$file 	         = $request->file('file2'); //get file
					$extension 		 = $file->getClientOriginalExtension(); //get extension
					$fileName2  	 = 'customer-'.$result->id.'.'.$extension; //set file name
					$destinationPath = storage_path('uploads/images/customer/proof'); // destination
					$file->move($destinationPath, $fileName2);//move to file
				}
				$updateFIlePath = $this->customer->updateCustomerUploadImages($result->id, $fileName1, $fileName2);
				if(!$updateFIlePath){
					throw new Exception("Error occurred while updating uploaded files");
				}

				//floor card range id
				$floor_card_range = $this->auction->getLastRegisterCard($id, FLOOR_CARD);

				//assign last card no
				if(sizeof($floor_card_range) > 0 && sizeof($floor_card_range->last_customer_card) > 0){
					//check this customer already registered with this auction as floor customer
					if($this->auction->isRegister($id, $user_id)){
						$last_card_no = $floor_card_range->last_customer_card->card_no;
					}else{
						$last_card_no = $floor_card_range->last_customer_card->card_no;
						$last_card_no = $last_card_no + 1;
					}
				}else{
					$last_card_no =  $floor_card_range->card_range_from;
				} 
				//generate reference no
				$referenceNo = $result->id.$id.$last_card_no;

				$customer_auction = [
					'user_id' 	  	=> $result->id,
					'auction_id'  	=> $id,
					'card_range_id' => $floor_card_range->id,
					'card_no'	  	=> $last_card_no,
					'invoice_no'    => $referenceNo
				];
				

				$add_auction = $this->customer->addCustomerAuction($id, $last_card_no, $result->id, $customer_auction);
				

				if(count($add_auction) > 0){
					
					return back()->with([
						'print' 		=> true,
						'print.content' => 'Customer Successfully Registered.',
						'print.title'   => $add_auction['card_no'],
						'print.customer'=> $add_auction['user_id'],
						'auction.id'    =>  $id
					]);

				}else{
					return back()->with([
						'error' => true,
						'error.message' => 'This Card No already registered.',
						'error.title'   => 'Notice'
					]);
				}
			}
		}catch(\Exception $e){
			return redirect( 'auction/list' )->with([
                'error' 		=> true,
                'error.message' => $e->getMessage(),
                'error.title'   => 'Error..!'
            ]);
		}
	}

	/**
	 * This function is used to display the edit fileds.
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
    	$details = $this->auction->getAuction($id);
        if(sizeof($details) > 0) {
            return view("AuctionManage::edit")->with([
            	'id' 				=> $id,
            	'details' 			=> $details[0],
				'locations'			=> $this->location->getLocationList(),
				'products'			=> $this->product->getAvailableItems(),
				'customer_types'	=> $this->customer->getCustomerTypes()
			]);
        }else{
            return response()->view('errors.404');
        }
    }

	/**
	 * Update the specified resource in storage.
	 * @param  array  $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		try {
			
			// decode json object
			$form = json_decode($request->auction);
			if(isset($form->items[0])){
				$item = $form->items;
			}else{
				$item = [];
			}
			
            //check image is browse
            if($request->hasFile('file') && $request->isRemote == 'false') {
            	$file = $request->file('file'); //get file
            	$extn = $file->getClientOriginalExtension(); //get extension
            	$fileName = 'auction-' . date('YmdHis') . '.' . $extn; //set file name	
            	$destinationPath = storage_path('uploads/images/auction'); // destination	
            	$file->move($destinationPath, $fileName);//move to file
                //resize image
                $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
                    $c->upsize();
                },"top");
                //save
                $img->save($destinationPath . '/' . $fileName);

                
            	//get this category records
        		$auction_details = $this->auction->getAuctionDetails($form->id);
            	//remove previous uploaded image file
            	if(isset($auction_details[0]->image) && $auction_details[0]->image) {
	                $remove_path = storage_path('uploads/images/auction/'.$auction_details[0]->image_path);
	                if (!unlink($remove_path)) {
	                    return Response::json([
							'error' => true,
							'message' => 'Error occurred when removing image!',
							'title' => 'Ops!'
						]);
	                }
	            }
            }else {
            	if($request->isRemote && $request->isRemote == 'true') {
            		$fileName = $request->lfFileName;
            	}else {
            		//get this category records
	        		$auction_details = $this->auction->getAuctionDetails($form->id);
	            	if(count($auction_details) > 0){
                    //remove previous uploaded image file
    	            	if($auction_details[0]->image) {
    		                $remove_path = storage_path('uploads/images/auction/'.$auction_details[0]->image_path);
    		                if (!unlink($remove_path)) {
    		                    return Response::json([
    								'error' => true,
    								'message' => 'Error occurred when removing image!',
    								'title' => 'Ops!'
    							]);
    		                }
    		            }
                    }


            		$fileName = '';
            	}            	
            }
            $auction = $this->auction->updateAuction($form->id, [
				'auction_no'          	=> $form->no,
                'event_name'          	=> $form->name,
                'auction_date'  		=> $form->date,
                'auction_start_time'  	=> $form->time,
                'registration_fee'    	=> (isset($form->fee))?$form->fee:'',
                'auction_address'    	=> (isset($form->address))?$form->address:'',
                'auction_url'    		=> (isset($form->url))?$form->url:'',
                'note'    				=> (isset($form->note))?$form->note:'',
                'sns_location_id'    	=> (isset($form->location->id))?$form->location->id:'',
                'image'    				=> $fileName,
				'advance_rate'			=> $form->advance
            ],$item, $form->newProducts, $form->types);

            return Response::json([
				'success' => true,
				'message' => 'Auction updated successfully.',
				'title'   => 'Job well done!',
				'details' 	=> $this->auction->getAuctionDetails($form->id),
				'locations'	=> $this->location->getLocationList(),
				'products'	=> $this->product->getAvailableItems()
			]);

        }catch(Exception $e){

        	return Response::json([
				'error' => true,
				'message' => $e->getMessage(),
				'title' => 'Ops!'
			]);
        }
	}

	/**
	 * This function is used to remove auction items from storage.
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteDetail(Request $request) {
        $detail = $this->auction->getAuctionItemDetails($request->id);
        if($detail) {
        	$detail->delete();
            return response()->json(['products'=>$this->product->getAvailableItems(), 'status' => 'success']);
        }
        else {
            return response()->json(['status' => 'invalid_id']);
        }
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	//band customer from the auction
	public function bandAuction(Request $request){
		if($request->ajax()){
			$band_auction = $this->auction->bandAuction($request->get('auction_id'), $request->get('customer_id'), $request->get('status'));
			return response()->json(['status' => 'success', 'result' => $band_auction]);
		}else{
			return response()->json(['status' => 'erro']);
		}
	}

	//print customer receipt
	//print sold item
	public function printCustomer($customer_id, $auction_id)
	{
		$registered_customer = $this->auction->registerCustomerDetails($customer_id, $auction_id);
		
		if(!$registered_customer){
			return back()->with([
                'error' 		=> true,
                'error.message' => 'Cannot found registered customer',
                'error.title'   => 'Error..!'
            ]);
		}	

		// $cartDetails = $this->common->getCartDetails($records['order']->user_id, 'with_order', $id);

		return $page1 =  view('AuctionManage::template.print')->with(['registered_customer'=> $registered_customer])->render();

		// $pdf   = new PdfTemplate(["no"=>date('Y-m-d')]);
        // $pdf->SetMargins(5, 0, 5);
        // $pdf->SetTitle("CUSTOMER_".$registered_customer[0]->register_id.'_REGISTERED');
        // $pdf->SetFont('helvetica', 5);
        // $pdf->SetAutoPageBreak(TRUE, 30);
        // $pdf->AddPage();
        // $pdf->writeHtml($page1);
        // $pdf->output("CUSTOMER_".$registered_customer[0]->register_id."_".date('Y_m_d').".pdf", 'I');
	}

}

@extends('layouts.back_master') @section('title','Auction Summary')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Auction
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('auction/list')}}}">Auction List</a></li>
		<li class="active">View Auction Summary</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
   <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">View Auction Summary</h3>
        </div>
        <div class="box-body">
            <div class="form-group form-horizontal">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Auction</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->event_name}}" readonly>
                        </div>
                        <label class="col-sm-2 control-label">Location</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="@if($auction[0]->location != null){{$auction[0]->location->name}}@endif" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Auction Date</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->auction_date}}" readonly>
                        </div>
                        <label class="col-sm-2 control-label">Start Time</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->auction_start_time}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">End Time</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{$auction[0]->auction_end_time}}" readonly>
                        </div>
                        <label class="col-sm-2 control-label">Registration Fee(Rs)</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="{{number_format($auction[0]->registration_fee,2)}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Auction Item List</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <label class="control-label"><b>Auction Item Detail</b></label>
                    <table class="table table-bordered bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th><label class="control-label">#</label></th>
                            <th><label class="control-label">Item Code</label></th>
                            <th><label class="control-label">Item</label></th>
                            <th><label class="control-label">Minimum BID Amount(Rs)</label></th>
                            <th><label class="control-label">Withdraw BID Amount(Rs)</label></th>
                            <th><label class="control-label">Highest BID Amount(Rs)</label></th>
                            <th><label class="control-label">Highest Bidder</label></th>
                            <th><label class="control-label">Item Status</label></th>
                            <th><label class="control-label">Approve Status</label></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                    @if(count($details) > 0)
                        @foreach($details as $result_val)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$result_val->item->item_code}}</td>
                                <td >{{$result_val->item->name}}</td>
                                <td class="text-right">{{number_format($result_val->start_bid_price,2)}}</td>
                                <td class="text-right">{{number_format($result_val->withdraw_amount,2)}}</td>
                                @if($result_val->highest_bid)
                                <td class="text-right">{{number_format($result_val->highest_bid->bid_amount,2)}}</td>
                                <td>(C{{$result_val->highest_bid->bidder->id}}) {{$result_val->highest_bid->bidder->first_name}} {{$result_val->highest_bid->bidder->last_name}}</td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                                <td>
                                @if($result_val->status == 0)
                                    <i  aria-hidden="true"></i> Bid Not Start
                                @elseif($result_val->status == 1 || $result_val->status == 0)
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i> Pending
                                @elseif($result_val->status == 2)
                                    <i class="fa fa-gavel" aria-hidden="true"></i> Bidding
                                @elseif($result_val->status == 3)
                                    @if($result_val->bid['bid_amount'] == $result_val->highest_bid['bid_amount'])
                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Won
                                    @else
                                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Lost
                                    @endif
                                @endif
                                </td>
                                @if($result_val->status == 3)
                                    <td class="text-center">
                                        @if(!empty($result_val->item_approve && $result_val->item_approve->status == 1))
                                            <span class="approved">Approved</span>
                                        @else
                                            <span class="rejected">Rejected</span>
                                        @endif
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                            <?php $i++;?>
                        @endforeach
                    @else
                        <tr><td colspan="9" class="text-center">No data found.</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @if(count($details) > 0 )
            <div class="box-footer">
                    <div style="float: right;">{!! $details->render() !!}</div>
            </div>
             @endif
        </div>
    </div>
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="checkbox"]').change(function(e) {
        var detail_id = $(this).data('id'); 
        if (this.checked) {
            $.confirm({
                title: 'Confirmation !',
                content: 'Approve for the payment.',
                icon: 'fa fa-exclamation-circle',
                animation: 'scale',
                closeAnimation: 'scale',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Approve',
                        btnClass: 'btn bg-purple',
                        action: function () {
                            $.ajax({
                                url: "{{URL::to('item/approve')}}",
                                method: 'GET',
                                data: {'detail_id': detail_id},
                                async: false,
                                success: function (data) { 
                                    $('#'+detail_id).attr("disabled", true);
                                      $.confirm({
                                        title: 'Success !',
                                        content: 'Payment Approved Success.',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'Ok',
                                                btnClass: 'btn bg-purple',
                                                action: function(){
                                                }
                                            }
                                        }   
                                    });
                                },error: function () {
                                    $.confirm({
                                        title: 'Encountered an error!',
                                        content: 'Please try again.',
                                        type: 'red',
                                        typeAnimated: true,
                                        buttons: {
                                            tryAgain: {
                                                text: 'Try again',
                                                btnClass: 'btn-red',
                                                action: function(){
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    },cancel: function () {
                        $('#'+detail_id).attr('checked', false);
                    }
                }
            });
        }
    });
});
</script>
@stop
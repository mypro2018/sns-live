@extends('layouts.back_master') @section('title','Register Customer')
@section('css')
<!-- toogle -->
<link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" />
<!-- toogle -->
<style>
.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 50px; }
.toggle.ios .toggle-handle { border-radius: 50px; }
.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
.img-rounded{
    width:100px;
    height:100px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Customer
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('auction/list')}}}">Auction List</a></li>
		<li class="active">New Customer</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" class="form-horizontal form-validation" method="post" id="submit_form" enctype="multipart/form-data">
    		<div class="box-header with-border">
    			<h3 class="box-title">New Customer</h3>
                <a href="{{{url('customer/list')}}}" target="_blank" class="btn btn-sm bg-purple pull-right">Customer List</a>
    		</div>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction </label>
                    <div class="col-sm-9">
                        <b><input type="text" class="form-control input-sm" name="auction" placeholder="Auction" value="{{$auction->event_name}} - {{$auction->auction_date}}" readonly></b>
                    </div>
                </div>
                @if(isset($card_range) && count($card_range) > 0)
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Card Range <span class="require">*</span></label>
                        <div class="col-sm-9">
                            <table class="table table-striped">
                                <tbody>
                                    @foreach($card_range as $value)
                                        @if($value->card_type == ONLINE_CARD)
                                            <tr>
                                                <td>
                                                    <label class="control-label">Online Customer Card </label>
                                                </td>
                                                <td>
                                                    <input type="number" min="1" class="form-control input-sm" name="online_range_from" id="online_range_from" placeholder="Online Card Start" value="{{$value->card_range_from}}" readonly>
                                                </td>
                                                <td>
                                                    <label class="control-label"> to </label>
                                                </td>
                                                <td>
                                                    <input type="number" min="1" class="form-control input-sm" name="onlie_range_to" id="online_range_to" placeholder="Online Card End" value="{{$value->card_range_to}}" readonly>
                                                </td>
                                                <td>
                                                    <div class="checkbox @if(count($last_floor_card) > 0 && count($last_floor_card->last_customer_card) > 0) disabled @endif">
                                                        <label>
                                                            <input type="checkbox" data-toggle="toggle" data-on="Edit" data-off="Read" id="edit_online" data-size="mini" data-onstyle="success" data-offstyle="default" data-style="ios" checked  @if(count($last_floor_card) > 0 && count($last_floor_card->last_customer_card) > 0) disabled @endif>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        @elseif($value->card_type == FLOOR_CARD)
                                            <tr>
                                                <td>
                                                    <label class="control-label">Floor Customer Card </label>
                                                </td>
                                                <td>
                                                    <input type="number" min="1" class="form-control input-sm" name="floor_range_from" id="floor_range_from" placeholder="Floor Card Start" value="{{$value->card_range_from}}" readonly>
                                                </td>
                                                <td>
                                                    <label class="control-label"> to </label>
                                                </td>
                                                <td>
                                                    <input type="number" min="1" class="form-control input-sm" name="floor_range_to" id="floor_range_to" placeholder="Floor Card End" value="{{$value->card_range_to}}" readonly>
                                                </td>
                                                <td>
                                                    <div class="checkbox @if(count($last_floor_card) > 0 && count($last_floor_card->last_customer_card) > 0) disabled @endif">
                                                        <label>
                                                        <input type="checkbox" data-toggle="toggle" data-on="Edit" data-off="Read" id="edit_floor" data-size="mini" data-onstyle="success" data-offstyle="default" data-style="ios" checked @if(count($last_floor_card) > 0 && count($last_floor_card->last_customer_card) > 0) disabled @endif>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Card Range <span class="require">*</span></label>
                        <div class="col-sm-9">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="control-label">Online Customer Card </label>
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control input-sm" name="online_range_from" id="online_range_from" placeholder="Online Card Start">
                                        </td>
                                        <td>
                                            <label class="control-label"> to </label>
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control input-sm" name="onlie_range_to" id="online_range_to" placeholder="Online Card End">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label">Floor Customer Card </label>
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control input-sm" name="floor_range_from" id="floor_range_from" placeholder="Floor Card Start">
                                        </td>
                                        <td>
                                            <label class="control-label"> to </label>
                                        </td>
                                        <td>
                                            <input type="number" min="1" class="form-control input-sm" name="floor_range_to" id="floor_range_to" placeholder="Floor Card End">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-8">
                    <button type="button" class="btn btn-sm bg-purple pull-right" id="range_save" disabled><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
            <hr/>
    		<div class="box-body">
	            {!!Form::token()!!}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-1 notice" style="padding-top:7px;"></div>
                </div>
                <div class="form-group">
                    <div class="col-xm-6 col-sm-6 col-md-5 col-lg-5 col-xm-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Customer NIC , Contact No , Card No" name="search_customer" id="search_customer" class="search_customer" autocomplete="false">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-search" type="button" id="search" class="search"><i class="fa fa-search fa-fw"></i> Search</button>
                            </span>
                        </div>
                    </div>
                    <!-- <div class="col-sm-1">
                        <button class="btn btn-default btn-search btn-sm" type="button" id="clear" class="clear"><i class="fa fa-eraser"></i> Clear</button>
                    </div> -->
                    <div class="col-sm-1">
                        <a href="{{Request::get('id')}}" class="btn btn-default btn-search btn-sm" id="refresh" class="refresh"><i class="fa fa-refresh"></i> Refresh</a>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">S&S Reg No</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="id" placeholder="S&S Reg No" value="{{Input::old('id')}}" readonly>
                    </div>
                </div>
                <div class="form-group @if($errors->has('card_no')) has-error @endif">
                    <input type="hidden" name="auction" value="{{Request::route('id')}}" readonly>
                    <input type="hidden" name="customer_id" value="{{Input::old('customer_id')}}" readonly>
                    <input type="hidden" name="customer_auction_id" value="{{Input::old('customer_auction_id')}}" readonly>
                    <label class="col-sm-2 control-label">Card No <span class="require">*</span></label>
                    <div class="col-sm-9">
                        @if(!empty(Input::old('card_no')))
                            <b><input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" value="{{Input::old('card_no')}}" readonly></b>
                        @else
                            @if(count($last_floor_card) > 0 && count($last_floor_card->last_customer_card) > 0)
                                @if(!empty($last_floor_card->last_customer_card->card_no))
                                    <b><input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" value="{{$last_floor_card->last_customer_card->card_no + 1}}" readonly></b>
                                @else
                                    <b><input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" value="{{$last_floor_card->card_range_from}}" readonly></b>
                                @endif
                            @else
                                @if(count($last_floor_card) > 0)
                                    <b><input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" value="{{$last_floor_card->card_range_from}}" readonly></b>
                                @else
                                    <b><input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" readonly></b>
                                @endif
                            @endif
                        @endif
                        @if($errors->has('card_no'))
                            <label id="label-error" class="error" for="label">{{$errors->first('card_no')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('first_name')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Customer Name <span class="require">*</span></label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{Input::old('first_name')}}">
                        @if($errors->has('first_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('first_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Address</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <textarea class="form-control input-sm" rows="3" name="address" placeholder="Address">{{Input::old('address')}}</textarea>
                    </div>
                </div>
                <div class="form-group @if($errors->has('contact')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Contact No <span class="require">*</span></label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="contact" placeholder="0712345678" value="{{Input::old('contact')}}" min="0" onkeypress="return event.charCode >= 48">
                        @if($errors->has('contact'))
                            <label id="label-error" class="error" for="label">{{$errors->first('contact')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('telephone_no')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Telephone No </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="number" class="form-control input-sm" name="telephone_no" placeholder="0112345678" value="{{Input::old('telephone_no')}}" min="0" onkeypress="return event.charCode >= 48">
                        @if($errors->has('telephone_no'))
                            <label id="label-error" class="error" for="label">{{$errors->first('telephone_no')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('nic')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">NIC </label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="nic" placeholder="914578945V" value="{{Input::old('nic')}}">
                        @if($errors->has('nic'))
                            <label id="label-error" class="error" for="label">{{$errors->first('nic')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('mail')) has-error @endif">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">E-mail</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="mail" placeholder="E-mail" value="{{Input::old('mail')}}" readonly>
                        @if($errors->has('mail'))
                            <label id="label-error" class="error" for="label">{{$errors->first('mail')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Contact Person</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{Input::old('last_name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">City</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <select name="city" class="form-control chosen iput-sm">
                            <option value="">Select City</option>
                            @if($cities)
                                @foreach ($cities as $city)
                                    <option value="{{$city->name}}"> {{$city->name}} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Country</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <select name="country" class="form-control chosen iput-sm">
                            <option value="">Select Country</option>
                            @if($countries)
                                @foreach ($countries as $country)
                                    <option value="{{$country->id}}"> {{$country->country_name}} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Note</label>
                    <div class="col-xm-9 col-sm-9 col-md-9 col-lg-9">
                        <textarea class="form-control input-sm" rows="3" name="note" placeholder="Note">{{Input::old('note')}}</textarea>
                    </div>
                </div>
                @if(sizeof($customer_types) > 0)
                    <div class="form-group">
                        <label class="col-xm-2 col-sm-2 col-md-2 col-lg-2 control-label">Customer Types</label>
                        @foreach($customer_types as $key => $value)
                            <div class="col-xm-2 col-sm-2 col-md-2 col-lg-2">
                                <div class="btn-group btn-group-sm">
                                    <input type="radio" name="customer_type" id="{{ $value->id }}" autocomplete="off" value="{{ $value->id }}"/>
                                    {{-- <div class="btn-group btn-group-sm">
                                        <label for="{{ $value->id }}" class="btn btn-default">
                                            <span class="glyphicon glyphicon-ok"></span>
                                            <span></span>
                                        </label>
                                        <label for="{{ $value->id }}" class="btn btn-default"><!-- active-->
                                            {{ $value->name }}
                                        </label>
                                    </div> --}}
                                    <label for="{{ $value->id }}" class=""><!-- active-->
                                        {{ $value->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Uploaded Files</label>
                    <div class="col-sm-2">
                        <img class="img-rounded nic-file" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
                        <br/>NIC/Licence
                    </div>
                    <div class="col-sm-2">
                        <img class="img-rounded proof-file" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
                        <br/>Proof of Address Document
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Customer NIC/Drving Licence Image</label>
                    <div class="col-sm-9">
                        <input id="file1" type="file" name="file1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Proof of Address Document</label>
                    <div class="col-sm-9">
                        <input id="file2" type="file" name="file2">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="com-xm-12 col-sm-12 col-md-6 col-lg-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="com-xm-12 col-sm-12 col-md-6 col-lg-6 text-right">
                        @if(count($card_range) > 0 && count($card_range[1]->last_customer_card) > 0 && $card_range[1]->last_customer_card->card_no >= $card_range[1]->card_range_to)
                            <button type="button" class="btn btn-sm bg-purple pull-right submit" disabled><i class="fa fa-floppy-o"></i> Save</button>
                        @else
                            <button type="button" class="btn btn-sm bg-purple pull-right submit"><i class="fa fa-floppy-o"></i> Save</button>
                        @endif
                    </div>
                </div>
            </div>
		</form>
    </div>
    <a class="btn btn-default btn-sm float-right print-register-chit" style="display:none;" target="popup" 
    href="{{url('auction/print')}}/{{session('print.customer')}}/auction/{{session('auction.id')}}" target="popup" 
    onclick="window.open('{{url('auction/print')}}/{{session('print.customer')}}/auction/{{session('auction.id')}}','popup','width=500,height=400'); return false;"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
</section>
@stop
@section('js')
<!-- toogle -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- toogle -->
<script type="text/javascript">
$("#file1, #file2").fileinput({
    uploadUrl: "",
    maxFileSize: 2000,
    allowedFileExtensions: ["jpg", "JPG", "png", "PNG", "Jpeg"],
    allowedFileTypes: ["image"],
    showRemove: false,
    showUpload: false,
    initialPreview: []
});
$(document).ready(function(){
    $(document).keypress(function(e){
        if (e.which == 13){
            $('#search').click();
        }
    });
    
    //search customer
    $('#search').on('click', function(){
        $('#search_customer').focus();
        if($('#search_customer').val() != ''){
            $(".box").addClass('panel-refreshing');
            $.ajax({
                url: "{{URL::to('auction/search/customer')}}",
                method: 'GET',
                data: {
                    'auction_id': "{{Request::route('id')}}",
                    'search'    : $('#search_customer').val()
                },
                async: false,
            success: function (data) {
                $(".box").removeClass('panel-refreshing');
                if(data.search.customer > 0){
                    toastr.success('Customer Information Found');
                    $('.form-group').removeClass('has-error');
                    $('.error').html('');
                    $('input[name="id"]').val(data.search.customer_details.id);
                    $('input[name="first_name"]').val(data.search.customer_details.fname);
                    $('input[name="last_name"]').val(data.search.customer_details.lname);
                    $('input[name="nic"]').val(data.search.customer_details.nic);
                    $('input[name="contact"]').val(data.search.customer_details.contact_no);
                    $('input[name="telephone_no"]').val(data.search.customer_details.telephone_no);
                    $('input[name="mail"]').val(data.search.customer_details.email);
                    $('input[name="customer_id"]').val(data.search.customer_details.id);
                    $('input[name="customer_auction_id"]').val(data.search.customer_details.customer_auction_id);
                    if(data.search.customer_details.city != 'null'){
                        $('select[name="city"]').val(data.search.customer_details.city).trigger("chosen:updated");
                    }
                    if(data.search.customer_details.country_id != null){
                        $('select[name="country"]').val(data.search.customer_details.country_id).trigger("chosen:updated");
                    }
                    if(data.search.customer_details.address_1 != 'null'){
                        $('textarea[name="address"]').text(data.search.customer_details.address_1);
                    }
                    if(data.search.customer_details.note != 'null'){
                        $('textarea[name="note"]').text(data.search.customer_details.note);
                    }
                    if(data.search.customer_auction > 0){
                        $('input[name="card_no"]').val(data.search.customer_auction_details.card_no);
                        $('input[name="card_no"]').attr('readonly', true);
                        toastr.success('Customer Registered with this Auction.');
                        $('.notice').html('<span class="label label-success" style="font-size:14px;">Registered</span>');
                    }else{
                        $('.notice').html('<span class="label label-danger" style="font-size:14px;">Not Registered</span>'); 
                        toastr.error('Customer not registered with this auction.');  
                    }
                    if(data.search.customer_details.customer_allowed_type.length > 0){
                        for(var j = 0; j < data.search.customer_details.customer_allowed_type.length; j++){
                            $(":radio[value="+data.search.customer_details.customer_allowed_type[j].customer_type_id+"]").prop("checked","true");
                        }
                    }
                    if(data.search.customer_details.nic_image_path != null){
                        $('.nic-file').attr('src','{{url('core/storage/uploads/images/customer/nic-licence/')}}/'+data.search.customer_details.nic_image_path+'');
                    }else{
                        $('.nic-file').attr('src','{{asset('assets/images/empty.jpg')}}');
                    }
                    if(data.search.customer_details.proof_image_path != null){
                        $('.proof-file').attr('src','{{url('core/storage/uploads/images/customer/proof/')}}/'+data.search.customer_details.proof_image_path+'');
                    }else{
                        $('.proof-file').attr('src','{{asset('assets/images/empty.jpg')}}');
                    }
                }else{
                    toastr.error('Customer Information not found.');    
                    $('input[name="first_name"]').val('');
                    $('input[name="last_name"]').val('');
                    $('input[name="nic"]').val('');
                    $('input[name="contact"]').val('');
                    $('input[name="mail"]').val('');
                    $('textarea[name="address"]').val('');
                    $('input[name="city"]').val('');
                    $('.notice').html('<span class="label label-danger" style="font-size:14px;">Not Registered</span>'); 
                    $('input[name="customer_auction_id"]').val('');
                    $('input[name="telephone_no"]').val('');
                    $('input[name="cutomer_type"]').removeAttr('checked');
                    $('.nic-file, .proof-file').attr('src','{{asset('assets/images/empty.jpg')}}');
                }
            },error: function () {
                $(".box").removeClass('panel-refreshing');   
                toastr.error('Error Occured !..Try Again');
                }
            });
        }
    });

    //clear information field
    $('#clear').on('click', function(){
        $('input[name="id"]').val('');
        $('input[name="first_name"]').val('');
        $('input[name="last_name"]').val('');
        $('input[name="nic"]').val('');
        $('input[name="contact"]').val('');
        $('input[name="mail"]').val('');
        $('input[name="address"]').val('');
        $('input[name="city"]').val('');
        $('#search_customer').val('').focus();
        $('textarea[name="address"]').val('');
        $('.notice').html('');
        $('.form-group').removeClass('has-error');
        $('.error').html('');
        $('input[name="customer_auction_id"]').val('');
        $('input[name="telephone_no"]').val('');
        $('input[name="cutomer_type"]').removeAttr('checked');
    });

    //range save
    $('#range_save').on('click', function(){
        var obj = {
            'online_range_from' : $('#online_range_from').val()+'-'+'{{ONLINE_CARD}}',
            'online_range_end'  : $('#online_range_to').val()+'-'+'{{ONLINE_CARD}}',
            'floor_range_from'  : $('#floor_range_from').val()+'-'+'{{FLOOR_CARD}}',
            'floor_range_end'   : $('#floor_range_to').val()+'-'+'{{FLOOR_CARD}}',
        };
        $.ajax({
            url: "{{URL::to('auction/save/range')}}",
            method: 'GET',
            data: {
                'auction_id' : "{{Request::route('id')}}",
                'range_plan' : obj
            },
            async: false,
            success: function (data) {
                $('#range_save').attr('disabled', false);
                $('#online_range_from').parent().parent().append('<td>'+
                                            '<div class="checkbox">'+
                                                '<label>'+
                                                '<input type="checkbox" data-toggle="toggle" data-on="Edit" data-off="Read" id="edit_online" data-size="mini" data-onstyle="success" data-offstyle="default" data-style="ios" checked>'+
                                                '</label>'+
                                            '</div>'+
                                        '</td>');
                $('#floor_range_from').parent().parent().append('<td>'+
                    '<div class="checkbox">'+
                        '<label>'+
                        '<input type="checkbox" data-toggle="toggle" data-on="Edit" data-off="Read" id="edit_floor" data-size="mini" data-onstyle="success" data-offstyle="default" data-style="ios" checked>'+
                        '</label>'+
                    '</div>'+
                '</td>');
                toastr.success('Card Range Sucessfully added.');
                setTimeout(function(){ location.reload(); }, 1000);
            },error: function () {
                
            }
        });
    });

    //card range option
    $('#edit_floor').change(function() {
        if(this.checked) {
            $('#floor_range_from,#floor_range_to').attr('readonly',true);
        }else{
            $('#floor_range_from,#floor_range_to').removeAttr('readonly');
        }   
    });

    $('#edit_online').change(function() {
        if(this.checked) {
            $('#online_range_from,#online_range_to').attr('readonly',true);
        }else{
            $('#online_range_from,#online_range_to').removeAttr('readonly');
        }   
    });

    //save button
    $('#online_range_from,#online_range_to,#floor_range_from,#floor_range_to').on('change keyup', function(evt){
        if($('#online_range_from').val() != '' && $('#online_range_to').val() != '' && $('#floor_range_from').val() != '' && $('#floor_range_to').val() != ''){
            $('#range_save').attr('disabled', false);
        }else{
            $('#range_save').attr('disabled', true);
        }
    });

    //submit user
    $('.submit').on('click', function(){
        $('#submit_form').submit();
    });
});
</script>
@stop

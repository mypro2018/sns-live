<tr id="{{ $i }}">
    <td class="text-center" data-id="{{$auction->id}}">{{ $i }}</td>
    <td class="text-uppercase">{{ $auction->auction_no?:'-' }}</td>
    <td class="text-uppercase">{{ $auction->event_name }}</td>
    <td class="text-center">{{ $auction->auction_date}}</td>
    <td class="text-center">{{ $auction->auction_start_time}}</td>
    <td class="text-center">
        @if($auction->auction_view_status == AUCTION_SHOW)
            <input type="checkbox" data-onstyle="info" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="request_view" value="{{AUCTION_SHOW}}" id="{{$auction->id}}" data-style="android" checked>
        @else
            <input type="checkbox" data-onstyle="info" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="request_view" value="{{AUCTION_NOT_SHOW}}" id="{{$auction->id}}" data-style="android">
        @endif
    </td>
    <td class="text-center">
        @if($auction->bid_now_active_status_1 == BIDDING_ACTIVE)
            <input type="checkbox" data-onstyle="info" data-section="1"  data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="bidding_active" value="{{BIDDING_ACTIVE}}" id="{{$auction->id}}" data-style="android" checked>
        @else
            <input type="checkbox" data-onstyle="info" data-section="1" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="bidding_active" value="{{BIDDING_INACTIVE}}" id="{{$auction->id}}" data-style="android">
        @endif
    </td>
    <td class="text-center">
        @if($auction->bid_now_active_status_2 == BIDDING_ACTIVE)
            <input type="checkbox" data-onstyle="info" data-section="2" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="bidding_active_panel" value="{{BIDDING_ACTIVE}}" id="{{$auction->id}}" data-style="android" checked>
        @else
            <input type="checkbox" data-onstyle="info" data-section="2" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="bidding_active_panel" value="{{BIDDING_INACTIVE}}" id="{{$auction->id}}" data-style="android">
        @endif
    </td>
    <td class="text-center">
        @if($auction->live_section_display_status == BIDDING_ACTIVE)
            <input type="checkbox" data-onstyle="info" data-section="3" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="live_section" value="{{BIDDING_ACTIVE}}" id="{{$auction->id}}" data-style="android" checked>
        @else
            <input type="checkbox" data-onstyle="info" data-section="3" data-toggle="toggle" data-size="small" data-on="Show" data-off="Hide" class="live_section" value="{{BIDDING_INACTIVE}}" id="{{$auction->id}}" data-style="android">
        @endif
    </td>
    @if(sizeof($auction->card_range) > 0)
        @if($auction->auction_status == AUCTION_TO_BEGIN)
            <td class="text-center">
                <a href="{{ 'live/'.$auction->id }}" class="btn btn-sm btn-info go-event" data-toggle="tooltip" data-placement="top" title="Auction To Start"> Begin </a>
            </td>
        @elseif($auction->auction_status == AUCTION_LIVE)
            <td class="text-center">
                <a href="{{ 'live/'.$auction->id }}" class="btn btn-sm btn-success go-event" data-toggle="tooltip" data-placement="top" title="Auction To Start"> Live </i></a>
            </td>
        @else
            <td class="text-center">
                <button class="btn btn-sm btn-danger go-event">Over</button>
            </td>
        @endif
    @else
        <td class="text-center">
            <button class="btn btn-sm btn-warning go-event" data-toggle="tooltip" data-placement="top" title="Please Add Card Range" onclick="myFunction()">Begin</button>
        </td>
    @endif
    <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View Detail" onclick="viewDetail({{ $auction->id }})"><i class="fa fa-eye view"></i></a>
            @if($auction->auction_status != AUCTION_OVER)
            <a href="{{ 'register/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Customer Register"><i class="fa fa-user-plus"></i></a>
                <a href="{{ 'edit/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Edit Detail"><i class="fa fa-pencil"></i></a>
            @endif    
                @if($auction->auction_status == AUCTION_TO_BEGIN)
                    <!-- <a href="{{ 'live/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Auction To Start"><i class="fa fa-gavel"></i></a> -->
                @elseif($auction->auction_status == AUCTION_LIVE)
                    <!--  <a href="{{ 'live/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Auction Live"><i class="fa fa-gavel"></i></a> -->
                @else
                    <!--  <a href="{{ 'live/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Auction Over"><i class="fa fa-gavel"></i></a> -->
                @endif
            <a href="{{ 'history/'.$auction->id }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Auction Summary"><i class="fa fa-history"></i></a>
            <a href="{{ 'history/'.$auction->id }}" class="btn btn-sm btn-default disabled" data-toggle="tooltip" data-placement="top" title="Delete Auction"><i class="fa fa-trash"></i></a>
        </div>
    </td>
</tr>
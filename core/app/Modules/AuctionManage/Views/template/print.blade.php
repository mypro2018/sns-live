<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
    .price{
        text-align: center;
    }
</style>
<body>
<img src="{{url('assets/images/logo/logo.png')}}">
<!--  -->
<div>
    <table>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Date</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td style="width: 30%;">
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->auction_date }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auction Name</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td style="width: 50%;">
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->event_name }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auction Location</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->location }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auction Time</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->auction_start_time }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer Register No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->register_id }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer Auction No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->card_no }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer Name</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->customer_name }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer NIC</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->nic }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer Contact No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if(sizeof($registered_customer) > 0)
                    {{ $registered_customer[0]->contact_no }}
                @else
                    {{ '-' }}
                @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Refundable Deposit</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
               
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Entrance Fee</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
               
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Signature</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auctioneer</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Company</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>    
</table>
</body>
</html>
<!-- <img style="bottom:0;" src="{{url('assets/images/invoice-footer.jpg')}}"> -->
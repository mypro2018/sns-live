@extends('layouts.back_master') @section('title','Create Auction')


@section('links')
<!-- Angular Material File Input -->
<link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/angular-material.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.css')}}">
<!-- Angular Material File Input -->
@stop


@section('css')
<style type="text/css">
.btn-group >.btn {
    padding: 5px 8px;
}
.user-types{
    width:100px;
}
</style>
@stop


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Auction
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('auction/list')}}}">Auction List</a></li>
		<li class="active">Edit Auction</li>
	</ol>
</section>
<!-- Content Header (Page header) -->

<!-- Main content -->
<section class="content" ng-app="angularApp" ng-controller="AuctionController" ng-cloak>
	<!-- Default box -->
	<div class="box">
        <form role="form" name="auctionForm" class="form-horizontal" method="post" enctype="multipart/form-data">
    		<div class="box-header with-border">
    			<h3 class="box-title">Edit Auction</h3>
    		</div>		
    	    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction No <span class="require">*</span></label>
                    <div class="col-sm-4" id="auction_no_error">
                        <input type="text" class="form-control input-sm validate" name="auction_no" ng-model="auction_no" placeholder="Auction No" value="{{Input::old('auction_no')}}" autocomplete="off">
                        <label class="error" for="label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction Name <span class="require">*</span></label>
                    <div class="col-sm-10" id="name_error">
                        <input type="text" class="form-control input-sm validate" name="name" ng-model="name" placeholder="Auction Name" value="{{Input::old('first_name')}}">
                        <label class="error" for="label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction Date <span class="require">*</span></label>
                    <div class="col-sm-4" id="date_error">
                        <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="YYYY-MM-DD" name="date" ng-model="date" placeholder="Auction Start Date" dt>
                        <label class="error" for="label"></label>
                    </div>
                    <label class="col-sm-2 control-label">Auction Time <span class="require">*</span></label>
                    <div class="col-sm-4" id="time_error">
                        <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="HH:mm" ng-model="time" name="time" placeholder="Auction Start Time" dt>
                        <label class="error" for="label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Registration Fee</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                          <div class="input-group-addon">Rs.</div>
                          <input type="text" class="form-control" ng-model="fee" name="fee" placeholder="Amount" price>
                        </div>
                    </div>
                    <label class="col-sm-2 control-label">Auction Location</label>
                    <div class="col-sm-4">
                        <select class="form-control input-sm"
                            chosen
                            name="location"
                            option="locations"
                            ng-model="location"
                            ng-options="location.name for location in locations track by location.id">
                            <option value="">- Select Location -</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction Live URL <span class="require">*</span></label>
                    <div class="col-sm-10" id="url_error">
                        <input type="text" class="form-control input-sm validate" ng-model="url" name="url" placeholder="Auction Live URL" validate-web-address>
                        <label class="error" for="label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Auction Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control input-sm" ng-model="address" name="address" placeholder="Auction Address"></textarea>
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Note</label>
                    <div class="col-sm-10">
                        <textarea class="form-control input-sm" ng-model="note" name="note" placeholder="Write Something About Auction"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Advance Percentage(%)</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <input type="text" class="form-control" ng-model="advance" name="advance" placeholder="Advance Percentage">
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Image(Banner)</label>
                    <div class="col-sm-10">
                        <lf-ng-md-file-input name="files00" lf-files="file" lf-filesize="5MB" lf-mimetype="image/*" lf-browse-label="Browse..." lf-api="lfApi" preview></lf-ng-md-file-input>
                        <div ng-messages="auctionForm.files00.$error" style="color:red;">
                            <div ng-message="filesize">File size too large.</div>
                            <div ng-message="mimetype">Mimetype error.</div>
                        </div>
                        <label class="control-label pull-right" style="margin-right:15px;"><small>Image Size should be 400px * 300px - Use this <a href="https://resizeimage.net/" target="_blank">https://resizeimage.net</a> to resize image</small></label>
                    </div>
                </div>
                <div class="form-group" ng-if="customer_types.length > 0">
                    <label class="col-sm-2 control-label">Customer Types</label>
                    <div class="col-sm-10">
                        <label ng-repeat="type in customer_types" class="checkbox-inline user-types">
                            <input type="checkbox" ng-checked="isChecked(type.id)" ng-click="change(type)" ng-model="type.isChecked">@{{type.name}}
                        </label> 
                    </div>
                </div>
            </div>

            <div class="box-header with-border">
                <h5 class="box-title">Add Product</h5>
            </div>

            <div class="box-body">
                <div class="form-group" style="margin-bottom: 0;">
                  <label class="col-sm-4 required">Items</label>
                  <label class="col-sm-3 required">Minimum Bid Value</label>
                  <label class="col-sm-3 required">Withdrawal Bid Value</label>
                  <label class="col-sm-1 required">Order</label>
                  <label class="col-sm-1 required">Action</label>
                </div>

                <!-- Old item array -->
                <div ng-repeat="product in products" class="form-group">                  
                    <div class="col-sm-4" id="item_@{{$index}}_error">
                        <input type="text" class="form-control input-sm" readonly="readonly" ng-model="product.item_with_code.sns_item">
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-3" id="min_value_@{{$index}}_error">
                        <div class="input-group input-group-sm">
                            <div class="input-group-addon">Rs.</div>
                            <input type="text" class="form-control input-sm validate" ng-model="product.start_bid_price" name="min_value_@{{$index}}" placeholder="Amount" format="number" price>
                        </div>
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-3" id="with_value_@{{$index}}_error">
                        <div class="input-group input-group-sm">
                            <div class="input-group-addon">Rs.</div>
                            <input type="text" class="form-control input-sm validate" ng-model="product.withdraw_amount" name="with_value_@{{$index}}" placeholder="Amount" format="number" price>
                        </div>
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-1" id="order_@{{$index}}_error">
                        <input type="text" class="form-control input-sm validate" ng-model="product.order" name="order_@{{$index}}" placeholder="Order" numbers-only="numbers-only">
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-1">
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn bg-purple" ng-click="deleteRow($index,product.id)"><i class="fa fa-minus-circle"></i></button>
                        </div>
                    </div>
                </div>

                <!-- New Item Array -->
                <div ng-repeat="product in newProducts" class="form-group">      <!-- item-validate -->            
                    <div class="col-sm-4" id="new_product_@{{$index}}_error">
                        <select class="form-control input-sm"  
                            chosen
                            name="new_product_@{{$index}}"
                            option="item"
                            ng-model="product.item"
                            ng-change="validateItems(product,$index,@{{ (product.item)?product.item:0 }})"
                            ng-options="item.sns_item for item in items track by item.id">
                            <option value="">- Select Item -</option>
                        </select>
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-3" id="new_min_value_@{{$index}}_error">
                        <div class="input-group input-group-sm">
                            <div class="input-group-addon">Rs.</div>
                            <input type="text" class="form-control input-sm" ng-model="product.min_value" name="new_min_value_@{{$index}}" placeholder="Amount" price>
                        </div>
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-3" id="new_with_value_@{{$index}}_error">
                        <div class="input-group input-group-sm">
                            <div class="input-group-addon">Rs.</div>
                            <input type="text" class="form-control input-sm" ng-model="product.with_value" name="new_with_value_@{{$index}}" placeholder="Amount" price>
                        </div>
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-1" id="new_order_@{{$index}}_error">
                        <input type="text" class="form-control input-sm order_no" ng-model="product.order" name="new_order_@{{$index}}" placeholder="Order" numbers-only="numbers-only">
                        <label class="error" for="label"></label>
                    </div>
                    <div class="col-sm-1">
                        <div class="btn-group btn-group-sm">                      
                            <button type="button" class="btn btn-default" ng-click="addRow()"><i class="fa fa-plus-circle"></i></button>
                            <button type="button" class="btn btn-default" ng-hide="$index == 0" ng-click="removeRow($index)"><i class="fa fa-minus-circle"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="margin-top: 150px !important;">
                <div class="row">
                    <div class="col-sm-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" ng-disabled="auctionForm.$invalid" class="btn btn-sm bg-purple pull-right" ng-click="save()"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
	</div>
</section>
<!-- Main content -->
@stop


@section('js')
<!-- Angular Material File Input -->
<script src="{{asset('assets/dist/angular/angular-material/angular-animate.min.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular-material/angular-aria.min.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular-material/angular-material.min.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular-material/angular-messages.min.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular-material/lf-ng-md-file-input.js')}}"></script>
<!-- Angular Material File Input -->

<!-- Auction script -->
<script type="text/javascript">

    $(".input-group").addClass('input-group-sm');
        
    'use strict';

    var app = angular.module('angularApp',['localytics.directives','cp.ngConfirm','ngAnimate','ngMaterial','ngMessages','lfNgMdFileInput']);

    app.controller('AuctionController', function ($timeout,$scope,$ngConfirm,$http,BASE_URL) {

        $scope.newProducts   = [{}];
        $scope.id            = {!! json_encode($id) !!};
        $scope.checkedTypes  = [];
        

        // edit view
        $scope.editView = function(locations, products, details, customer_types) {
            $scope.locations        = locations;
            $scope.items            = products;
            $scope.customer_types   = customer_types;

            if(details){
                $scope.auction_no = details.auction_no;
                $scope.name     = details.event_name;
                $scope.date     = details.auction_date;
                $scope.time     = details.auction_start_time;
                $scope.fee      = details.registration_fee;
                $scope.location = details.location;
                $scope.url      = details.auction_url;
                $scope.address  = details.auction_address;
                $scope.note     = details.note;
                $scope.selected = details.auction_allowed_customer_type;
                $scope.advance  = details.advance_rate;
                //add to current selected types to array
                if(details.auction_allowed_customer_type){
                    for(var i=0 ; i < details.auction_allowed_customer_type.length; i++) {
                        $scope.checkedTypes.push(details.auction_allowed_customer_type[i].customer_type_id);
                    }
                }
                $timeout(
                    function(){
                        $scope.lfApi.removeAll();
                        if(details.image) {
                            $scope.lfApi.addRemoteFile('{{url()}}'+'/core/storage/uploads/images/auction/'+details.image,details.image,'image');
                        }
                    }
                );

                $scope.products    = details.auction_details;
            }
        }
        
        //if checked values in customer types then checked customer types when form load
        $scope.isChecked = function(id){
            for(var i=0 ; i < $scope.selected.length; i++) {
                if($scope.selected[i].customer_type_id == id){
                    return true
                }
            }
            return false;
        };

        //get checked values
        $scope.change = function(type){
            if (type.isChecked){
                $scope.checkedTypes.push(type.id);
            }else{
                $scope.checkedTypes.splice($scope.checkedTypes.indexOf(type.id), 1);
            }
        };

        // load view
        $scope.editView({!! json_encode($locations) !!}, {!! json_encode($products) !!}, {!! json_encode($details) !!}, {!! json_encode($customer_types) !!});

        // add row
        $scope.addRow = function() {
          $scope.newProducts.push({});
        }

        //remove row
        $scope.removeRow = function(index){
            $scope.newProducts.splice( index, 1);
        }

        // delete a detail
        $scope.deleteRow = function(index,id){          
          $ngConfirm({
            theme: 'material',
            title: 'Are you sure?',
            content: 'You will not be able to recover this!',
            buttons: {
              'confirm': {
                text: 'Yes',
                btnClass: 'bg-purple',
                action: function () {
                    $(".box").addClass('panel-refreshing');
                    $http.post("{{url('auction/delete_detail')}}",{id : id})
                    .success(function (response) {
                        if(response.status=='success') {
                            $scope.products.splice( index, 1);
                            $scope.items = response.products;
                            $ngConfirm({title: 'Delete Success!',theme: 'material',content: 'Auction detail deleted successfully',buttons: {ok: function () {}}})
                        }else if(response.status=='invalid_id') {
                            $ngConfirm({title: 'Error!',theme: 'material',content: 'Auction detail ID doesn\'t exists.',buttons: {ok: function () {}}})
                        }else {
                            $ngConfirm({title: 'Error!',theme: 'material',content: 'Please try again.',buttons: {ok: function () {}}})
                        }
                        $(".box").removeClass('panel-refreshing');
                    })
                    .error(function (response, status, header) {
                        console.log("An error occurred during the AJAX request");
                        $(".box").removeClass('panel-refreshing');
                    });
                }
              },
              cancel: function () {}
            }
          });
        }

        // check duplicate items
        $scope.validateItems = function(row,index,old_item) {
          var old = (old_item !== 0)?old_item:'';
          var i = 0;
          var pass = 'true';
          angular.forEach($scope.newProducts, function(p) {
            if (i != index && p.item && p.item.id == row.item.id) {
              pass = 'false';
              p.item = row.item;
              return false;
            }
            i++;
          });
          if(pass == 'false'){
            $ngConfirm({theme: 'material', content:'<strong>'+row.item.sns_item+'</strong> already exists.', title:'Warning!', closeIcon: false, buttons: {ok: function () {}}});
            row.item = old;
          }
        }

        //add auction details
        $scope.save = function (){

            var $i = 0;

            // auction details validation
            $("form .validate").each(function() {

                var $this = $(this);

                var $name = String($this.attr('name')).split('_');
                if($this.val() == "" || $this.val() == null) {
                    $('#' + $this.attr('name') + '_error').addClass('chosen-error');
                    $('#' + $this.attr('name') + '_error').find('input').addClass('error');
                    $('#' + $this.attr('name') + '_error').find('label').show().text('The '+ $name[0] + (($name[1] && !$.isNumeric($name[1]))?' '+$name[1]:'') +' field is required.'); 
                    $i++;
                }
                else{
                    $('#' + $this.attr('name') + '_error').removeClass('chosen-error');
                    $('#' + $this.attr('name') + '_error').find('input').removeClass('error');
                    $('#' + $this.attr('name') + '_error').find('label').hide();
                }
            });

            //validate order no duplicate
            var idx = {};
            $('.order_no').each(function(){
                var val = $(this).val();
                if(val.length)
                {
                    if(idx[val]){
                        idx[val]++;
                    }else{
                        idx[val] = 1;   
                    }
                }
                var gt_one = $.map(idx,function(e,i){
                            return e>1 ? e: null
                        });
                if(gt_one > 1){
                    $(this).val('');
                }
            });

            // auction product details validation
            if($scope.products.length == 0) {
                $("form .item-validate").each(function() {

                    var $this = $(this);

                    var $name = String($this.attr('name')).split('_');
                    if($this.val() == "" || $this.val() == null) {
                        $('#' + $this.attr('name') + '_error').addClass('chosen-error');
                        $('#' + $this.attr('name') + '_error').find('input').addClass('error');
                        $('#' + $this.attr('name') + '_error').find('label').show().text('The '+ $name[0] + (($name[1] && !$.isNumeric($name[1]))?' '+$name[1]:'') +' field is required.'); 
                        $i++;
                    }
                    else{
                        $('#' + $this.attr('name') + '_error').removeClass('chosen-error');
                        $('#' + $this.attr('name') + '_error').find('input').removeClass('error');
                        $('#' + $this.attr('name') + '_error').find('label').hide();
                    }
                });
            }

            if($i > 0) {
                return false;
            }

            $(".box").addClass('panel-refreshing');
            //get selected checkbox
            var auction = {
                id          : $scope.id,
                no          : $scope.auction_no,
                name        : $scope.name,
                date        : moment($scope.date).format('YYYY-MM-DD'),
                time        : ($scope.time._i)?moment($scope.time).format('HH:mm'):$scope.time,
                fee         : $scope.fee,
                location    : $scope.location,
                url         : $scope.url,
                address     : $scope.address,
                note        : $scope.note,
                items       : $scope.products,
                newProducts : $scope.newProducts,
                types       : $scope.checkedTypes,
                advance     : $scope.advance
            };

            var formData = new FormData();

            formData.append('file', ($scope.file.length > 0)?$scope.file[0].lfFile:'');
            
            formData.append('isRemote', ($scope.file.length > 0)?$scope.file[0].isRemote:'');

            formData.append('lfFileName', ($scope.file.length > 0)?$scope.file[0].lfFileName:'');

            formData.append('auction', JSON.stringify(auction));

            $http.post(BASE_URL+"auction/update", formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
                if(response.data.success == true) {

                    $scope.newProducts = [{}];
                    
                    $scope.editView(response.data.locations, response.data.products, response.data.details);

                    $('.error').hide();
                    $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'green', closeIcon: false, buttons: {
                        ok: function () {
                            window.location = BASE_URL+"auction/list";
                        }
                    }});

                
                } else if(response.data.error == true) {
                    $ngConfirm({theme: 'material', content:response.data.message, title:response.data.title, type:'red', closeIcon: false, buttons: {ok: function () {}}});
                }

                $(".box").removeClass('panel-refreshing');

            },function(error){
                console.log(error);
            });
        }
    });

    app.constant('BASE_URL', "{{asset('/')}}");

    app.directive('dt', function(){
        return {
          require: '?ngModel',
          restrict: 'A',
          link: function ($scope, element, attrs, controller) {
                var updateModel, onblur;

                if (controller !== null) {

                    updateModel = function () {
                        if (element.data("DateTimePicker").minViewMode === element.data("DateTimePicker").viewMode) {
                            element.data("DateTimePicker").hide();
                            element.blur();
                        }
                    };

                    onblur = function () {
                        var date = element.datetimepicker().data("DateTimePicker").date();
                        return $scope.$apply(function () {
                            //console.log(date);
                            return controller.$setViewValue(date);
                        });
                    };
                }
                return attrs.$observe('dt', function (value) {
                    var options;
                    options = {format:element.data("format")}; //<--- insert your own defaults here!
                    if (angular.isObject(value)) {
                        options = value;
                    }
                    if (typeof (value) === "string" && value.length > 0) {
                        options = angular.fromJson(value);
                    }
                    return element.datetimepicker(options).on('change.dp', updateModel).on('blur', onblur);
                });
                return element;
            }
        };
    });

    app.directive('price', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    return;
                }

                ctrl.$formatters.unshift(function () {
                    return $filter('number')(ctrl.$modelValue);
                });

                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[\,\.]/g, ''),
                        b = $filter('number')(plainNumber);

                    elem.val(b);

                    return plainNumber;
                });
            }
        };
    });

    /*app.directive('price', [function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                attrs.$set('ngTrim', "false");
                
                var formatter = function(str, isNum) {
                    str = String( Number(str || 0) / (isNum?1:100) );
                    str = (str=='0'?'0.0':str).split('.');
                    str[1] = str[1] || '0';
                    return str[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + '.' + (str[1].length==1?str[1]+'0':str[1]);
                }
                var updateView = function(val) {
                    scope.$applyAsync(function () {
                        ngModel.$setViewValue(val || '');
                        ngModel.$render();
                    });
                }
                var parseNumber = function(val) {
                    var modelString = formatter(ngModel.$modelValue, true);
                    var sign = {
                        pos: /[+]/.test(val),
                        neg: /[-]/.test(val)
                    }
                    sign.has = sign.pos || sign.neg;
                    sign.both = sign.pos && sign.neg;
                    
                    if (!val || sign.has && val.length==1 || ngModel.$modelValue && Number(val)===0) {
                        var newVal = (!val || ngModel.$modelValue && Number()===0?'':val);
                        if (ngModel.$modelValue !== newVal)
                            updateView(newVal);
                        
                        return '';
                    }else {
                        var valString = String(val || '');
                        var newSign = (sign.both && ngModel.$modelValue>=0 || !sign.both && sign.neg?'-':'');
                        var newVal  = valString.replace(/[^0-9]/g,'');
                        var viewVal = newSign + formatter(angular.copy(newVal));

                        if (modelString !== valString)
                            updateView(viewVal);

                        return (Number(newSign + newVal) / 100) || 0;
                    }
                }
                // var formatNumber = function(val) {
                //     if (val) {
                //         var str = String(val).split('.');
                //         str[1]  = str[1] || '0';
                //         val     = str[0] + '.' + (str[1].length==1?str[1]+'0':str[1]);
                //     }
                //     return parseNumber(val);
                // }
                ngModel.$parsers.push(parseNumber);
                //ngModel.$formatters.push(formatNumber);
            }
        };
    }]);*/

    app.directive('numbersOnly', function(){
        return {
            require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                   // this next if is necessary for when using ng-required on your input. 
                   // In such cases, when a letter is typed first, this parser will be called
                   // again, and the 2nd time, the value will be undefined
                   if (inputValue == undefined) return '' 
                   var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
                   if (transformedInput!=inputValue) {
                      modelCtrl.$setViewValue(transformedInput);
                      modelCtrl.$render();
                   }         

                   return transformedInput;         
                });
            }
        };
    });

    app.directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                var parts = attrs.format.split(':');
                attrs.foramtType = parts[0];
                attrs.pass = parts[1];

                ctrl.$formatters.unshift(function (a) {
                    return $filter(attrs.foramtType)(ctrl.$modelValue, attrs.pass)
                });


                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.foramtType)(plainNumber, attrs.pass));
                    return plainNumber;
                });
            }
        };
    }]);

    app.directive('validateWebAddress', function () {
        var URL_REGEXP = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                element.on("keyup", function () {
                    var isValidUrl = URL_REGEXP.test(element.val());
                    if (isValidUrl && element.hasClass('alert-danger') || element.val() == '') {
                        element.removeClass('alert-danger');
                        $('.bg-purple').removeClass('disabled');
                    } else if (isValidUrl == false && !element.hasClass('alert-danger')) {
                        element.addClass('alert-danger');
                        $('.bg-purple').addClass('disabled');
                    }
                });
            }
        }
    });

</script>
<!-- Auction script -->
@stop

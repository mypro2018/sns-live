@extends('layouts.back_master') @section('title','Auction List')
@section('css')
<!-- toogle -->
<link rel="stylesheet" href="{{asset('assets/dist/boostrap-toggle/css/bootstrap-toggle.min.css')}}" type="text/css"/>
<style type="text/css">
  .table-sec table{
    margin-bottom: 0;
  }
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
  .toggle.ios .toggle-handle { border-radius: 20px; }
  .go-event{
    width: 80px;
    border-radius:0px !important;
  }
  .toggle.android { border-radius: 0px;}
  .toggle.android .toggle-handle { border-radius: 0px; }
  .user-types{
    width:100px;
  }
</style>
@stop


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Auction
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Auction List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
    <form role="form" method="get" action="{{url('auction/search')}}">	    
      <div class="box-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="auction_id">
                <option value="">-- Search All Auctions --</option>
                  @if($auctions)
                    @foreach($auctions as $auction)
                      <option value="{{ $auction->id }}" @if($auction->id == $auction_id) selected @endif>{{ $auction->event_name }}</option>
                    @endforeach
                  @endif
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <div class="input-group date" id="datetimepicker1">
                  <input type="text" data-date-format="YYYY-MM-DD" name="auction_date" class="form-control input-sm" value="{{ ($auction_date)?$auction_date:'' }}" placeholder="Auction Date" />
                  <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                  </span>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <div class="form-group">
                <select class=" form-control input-sm chosen" name="auction_status">
                  <option value="">-- Search Auctions Status --</option>
                  <?php
                    $status = [1=>'Begin',2=>'Live',3=>'Over'];
                  ?>
                  @if($status)
                    @foreach($status as $key => $value)
                      <option value="{{ $key }}" @if($key == $auction_status) selected @endif>{{ $value }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="auction_location">
                <option value="">-- Search All Auction Location --</option>
                @if($locations)
                    @foreach($locations as $location)
                      <option value="{{ $location->id }}" @if($location->id == $auction_location) selected @endif>{{ $location->name }}</option>
                    @endforeach
                  @endif
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
          <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
        </div>
      </div>
    </form>
  </div>

  <div class="box box-refresh">
    <div class="box-header with-border">
      <h3 class="box-title">Auction List</h3>
      <a href="{{url('auction/add')}}" class="btn btn-sm bg-purple pull-right">New Auction</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
            <thead>
              <tr class="text-uppercase">
                <th class="text-center" width="5%">#</th>
                <th class="text-center">Auction No</td>
                <th class="text-center">Name</td>
                <th class="text-center">Date</th>
                <th class="text-center">Time</th>
                <th class="text-center">Auction</th>
                <th class="text-center">BidNow in Live Section</th>
                <th class="text-center">BidNow in Bidding Panel</th>
                <th class="text-center">Live Section</th>
                <th class="text-center">Status</th>
                <th class="text-center" width="20%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $i = 1;
              ?>
              @if(count($paginateList) > 0)
                @foreach($paginateList as $auction)
                  @include('AuctionManage::template.auction')
                <?php $i++;?>
                @endforeach
              @else
                <tr><td colspan="8" class="text-center">No data found.</td></tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @if(count($paginateList) > 0)
      <div class="box-footer">     
        <div class="pull-right">{!! $paginateList->appends($_GET)->render() !!}</div>
      </div>
    @endif
  </div>
</section>
@stop


@section('js')
<!-- toogle -->
<script src="{{asset('assets/dist/boostrap-toggle/js/bootstrap-toggle.min.js')}}"></script>
<!-- toogle -->
<script type="text/javascript">

function myFunction(){
   toastr.error('Please Add Customer Card Range Before Start Auction !');
}

$(document).ready(function(){
  //auction display & non display
  $('.request_view').on('change', function(e) {

    var $this      = $('.request_view');
    var text       = '';
    var toast_text = '';
    var auction_id = $(this).closest('tr').find('td:first').data('id');
    
    if(this.checked) {
      $(this).val(1);
      text       = 'Do you want to show auction ?';
      toast_text = 'Auction now visble';
    }else{
      $(this).val(0);
      text       = 'Do you want to hide auction ?';
      toast_text = 'Auction now invisble';
    }
    var status     = $(this).val();
    //send to server
    var a = $.confirm({
      icon      : '',
      title     : 'Confirmation',
      content   :text,
      theme     : 'bootstrap',
      closeIcon : false,
      animation : 'scale',
      type      : 'blue',
      buttons : {
        'confirm' : {
          text    : 'Yes',
          btnClass: 'btn-blue',
          action  : function () {
            $.ajax({
              url   : "{{URL::to('auction/visible')}}",
              method: 'GET',
              data  : {
                'auction_id': auction_id,
                'status'    : status
              },
              async : false,
              success : function (data) {
                toastr.success('Auction Display status changed.<br/><b>'+toast_text+'</b>');  
              },error: function () {
                toastr.error('Error Occured !..Try Again');
              }
            });
          },
        },
        'no': {
          text:'No',
          action:function () {
            location.reload();
          }
        }
      }
    });   
  });

  //auction bidding active or inactive in live section 
  $('.bidding_active, .bidding_active_panel, .live_section').on('change', function(e) {

    var $this      = $(this);
    var text       = '';
    var toast_text = '';
    var auction_id = $(this).closest('tr').find('td:first').data('id');
    var section    = $(this).data('section');

    if(this.checked) {
      $(this).val(1);
      if(section < 3){
        text       = 'Do you want to show bidding ?';
        toast_text = 'Bid Now Visible Now';
      }else{
        text       = 'Do you want to show live section ?';
        toast_text = 'Live Section Visible Now';
      }
    }else{
      $(this).val(0);
      if(section < 3){
        text       = 'Do you want to hide bid now ?';
        toast_text = 'Bid Now Successfully hidden now';
      }else{
        text       = 'Do you want to hide live section ?';
        toast_text = 'Live Section Successfully hidden now';
      }
    }
    var status     = $(this).val();
    //send to server
    var a = $.confirm({
      icon      : '',
      title     : 'Confirmation',
      content   : text,
      theme     : 'bootstrap',
      closeIcon : false,
      animation : 'scale',
      type      : 'blue',
      buttons   : {
        'confirm' : {
          text    : 'Yes',
          btnClass: 'btn-blue',
          action  : function () {
            $.ajax({
              url: "{{URL::to('auction/bidding-action')}}",
              method: 'GET',
              data: {
                'auction_id': auction_id,
                'status'    : status,
                'section'   : section
              },
              async: false,
              success: function (data) {
                toastr.success('Auction Bid Now Visible Status changed.<br/><b>'+toast_text+'</b>');  
              },error: function () {
                toastr.error('Error Occurred !..Try Again');
              }
            });
          },
        },
        'no': {
          text   :'No',
          action :function () {
            location.reload();
          }
        }
      } 
    });   
  });
});

$(function () {
  $('#datetimepicker1').datetimepicker();
});

function viewDetail(auction_id) {
  
  $(".box-refresh").addClass('panel-refreshing');

  var content = '<div class="row">'+
    '<div class="col-lg-6 form-group">'+
      '<label>Auction No</label>'+
        '<input type="text" class="form-control input-sm" id="auction_no" placeholder="Auction No" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Auction Name</label>'+
        '<input type="text" class="form-control input-sm" id="name" placeholder="Auction Display Name" readonly>'+
    '</div>'+
    '<div class="col-lg-6 form-group">'+
      '<label>Auction Date</label>'+
        '<input type="text" class="form-control input-sm" id="date" placeholder="Auction Date" readonly>'+
    '</div>'+
    '<div class="col-lg-6 form-group">'+
      '<label>Auction Time</label>'+
        '<input type="text" class="form-control input-sm" id="time" placeholder="Auction Time" readonly>'+
    '</div>'+
    '<div class="col-lg-6 form-group">'+
      '<label>Registration Fee</label>'+
        '<div class="input-group">'+
          '<div class="input-group-addon">Rs.</div>'+
          '<input type="text" class="form-control input-sm" id="fee" placeholder="No Registration Fee" readonly>'+
        '</div>'+
    '</div>'+
    '<div class="col-lg-6 form-group">'+
      '<label>Auction Location</label>'+
        '<input type="text" class="form-control input-sm" id="location" placeholder="No Location" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Auction Live URL</label>'+
        '<input type="text" class="form-control input-sm" id="url" placeholder="No Auction Live URL" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Auction Address</label>'+
        '<textarea class="form-control input-sm" rows="4" id="address" placeholder="No Auction Address" readonly></textarea>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Note</label>'+
        '<textarea class="form-control input-sm" rows="3" id="note" placeholder="No Note" readonly></textarea>'+
    '</div>'+
    '<div class="col-lg-6 form-group">'+
      '<label>Advance Rate</label>'+
      '<div class="input-group">'+
          '<input type="text" class="form-control input-sm" id="advance" placeholder="Advance Presentage" readonly>'+
          '<div class="input-group-addon">%</div>'+
      '</div>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Allowed Customer Types</label>'+
      '<div class="customer-types"></div>'+  
    '</div>'+
    '<div class="col-lg-4 form-group media-sec">'+
      '<label>Banner</label>'+
      '<img class="media-object">'+
    '</div>'+

    '<div class="col-lg-12 form-group table-sec">'+
      '<h4>Item List</h4>'+
      '<table class="table table-striped table-condensed table-bordered table-responsive">'+
        '<thead>'+
          '<tr>'+
            '<th>#</th>'+
            '<th>Item Code</th>'+
            '<th>Item Name</th>'+
            '<th>Minimum Bid Value</th>'+
            '<th>Withdrawal Bid Value</th>'+
            '<th>Status</th>'+
            '<th>Order</th>'+
          '</tr>'+
        '</thead>'+
        '<tbody>'+
        '</tbody>'+
      '</table>'+
    '</div>'+

  '</div>';

  $.ajax({
    url: "{{URL::to('auction/details')}}",
    method: 'GET',
    data: {'auction_id': auction_id},
    async: false,
      success: function (data) {
        if(data != ''){
          $.confirm({
            title: 'Auction Details',
            theme: 'bootstrap',
            closeIcon: true,
            content: content,
            columnClass: 'large',
            onContentReady: function () {
              var self = this;
              self.$content.find('#auction_no').val(data.details.auction_no);
              self.$content.find('#name').val(data.details.event_name);
              self.$content.find('#date').val(data.details.auction_date);
              self.$content.find('#time').val(data.details.auction_start_time);
              self.$content.find('#fee').val(data.details.registration_fee);
              self.$content.find('#location').val((data.details.location)?data.details.location.name:'');
              self.$content.find('#url').val(data.details.auction_url);
              self.$content.find('#address').val(data.details.auction_address);
              self.$content.find('#note').val(data.details.note);
              self.$content.find('#advance').val(data.details.advance_rate);
              if(data.details.image){
                self.$content.find('.media-object').attr('src','{{url()}}'+'/core/storage/uploads/images/auction/'+data.details.image);
              }else{
                self.$content.find('.media-sec').hide();
              }
              var customer_type = '';
              if(data.customer_types.length > 0){
                for(var i = 0; i < data.customer_types.length; i++){
                    customer_type += '<label class="checkbox-inline user-types">'+
                              '<input type="checkbox" id="'+data.customer_types[i].id+'"  value="'+data.customer_types[i].id+'" onclick="return false;">'+data.customer_types[i].name+'</label>';
                }
                self.$content.find('.customer-types').html(customer_type);
                for(var j = 0; j < data.details.auction_allowed_customer_type.length; j++){
                  $(":checkbox[value="+data.details.auction_allowed_customer_type[j].customer_type_id+"]").prop("checked","true");
                }
              }

              var str    = '';
              var index  = 1;
              var status = '';
              $.each( data.details.auction_details_available, function( key, value ) { 
                if(value.status == '{{PRODUCT_TO_BEGIN}}'){
                  status = '<span class="label label-default">Available</span>';
                }else if(value.status == '{{PRODUCT_TO_LIVE}}'){
                  status = '<span class="label label-default">Live</span>';
                }else{
                  status = '<span class="label label-default">Over</span>';
                }
                 
                str+='<tr><td>'+index+'</td><td>'+value.item.item_code+'</td><td>'+value.item.name+'</td><td class="text-right">'+value.start_bid_price+'</td><td class="text-right">'+value.withdraw_amount+'</td><td class="text-center">'+status+'</td><td class="text-center">'+value.order+'</td></tr>';
                index++;
              });
              self.$content.find('.table-sec tbody').html(str);
            },
            buttons: {
              close: function () {}
            }
          });
        } else {}

        $(".box").removeClass('panel-refreshing');
      },
      error: function () {
        alert('error');
      }
  });
}
</script>
@stop

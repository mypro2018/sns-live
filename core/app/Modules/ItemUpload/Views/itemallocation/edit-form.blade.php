<br>
<div class="form-group row {{ $errors->has('pricebook_name') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="name">Pricebook name</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        @if(isset($pricebook->pricebook_id))
            <input type="hidden" name="pricebook_id" value="{{ $pricebook->pricebook_id }}">
        @endif
        <input type="text" name="pricebook_name" id="pricebook_name" class="form-control" placeholder="Enter pricebook name..." value="{{ isset($pricebook->name)? $pricebook->name : old('pricebook_name') }}">
        <input type="hidden" name="old_pricebook_name" value="{{ $pricebook->name?:'' }}">
        @if($errors->has('pricebook_name'))
            <span class="help-block">
                {{ $errors->first('pricebook_name') }}
            </span>
        @endif
    </div>  
</div>
<div class="form-group row {{ $errors->has('channel') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="channel">Channel</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        {!! Form::select('channel_id', $channels, isset($pricebook->channel_id)? $pricebook->channel_id:old('channel_id'), [
                'class' => 'form-control chosen',
                'id'    => 'channel_id'
            ]) 
        !!}
        <input type="hidden" name="old_channel_id" value="{{ $pricebook->channel_id?:'' }}">
        @if($errors->has('channel_id'))
            <span class="help-block">
                {{ $errors->first('channel_id') }}
            </span>
        @endif
    </div>  
</div>
<div class="form-group row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="Description">Description</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <textarea name="description" rows="5" class="form-control" placeholder="Enter description.">{{ isset($pricebook->description)?$pricebook->description:old('description') }}</textarea>
        <input type="hidden" name="old_desciption" value="{{ $pricebook->description?:'' }}">
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-lg-offset-1">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2 col-lg-offset-8">
                <a href="{{ asset('assets/files/example.xls') }}" class="btn btn-default btn-block"><span class="fa fa-download"></span> Example Excel</a>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-lg-offset-1">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label class="control-label pull-right" title="Description">Status</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                <label class="switch">
                    <input type="hidden" name="old_status" value="{{ $pricebook->status }}">
                    <input type="checkbox" name="status" {{ $pricebook->status == 1? 'checked':'' }}>
                  <span class="slider"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label class="control-label pull-right" title="Description">Excel File</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                <span class="btn bg-purple btn-file btn-block">
                    Browse <input type="file" name="excel_file" id="excel_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                </span>
                @if($errors->has('excel_file'))
                    <p class="padding-top-10 text-red">
                      {{ $errors->first('excel_file') }}  
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-lg-9 col-lg-offset-2">
        <h4 class="line-bottom padding-bottom-10">Products Price</h4>
        <br>
        @if(isset($products) && count($products) > 0)
            <table class="table table-striped table-bordered">
                <tr>
                    <th class="verticle-center text-center">
                        <input type="checkbox" name="select_all" id="select_all">
                    </th>
                    <th class="verticle-center">#</th>
                    <th class="verticle-center">Code</th>
                    <th class="text-center verticle-center">Name</th>
                    <th class="text-center verticle-center">Channel</th>
                    <th class="text-right verticle-center">Price</th>
                    <th class="text-center verticle-center">Status</th>
                    <th class="text-center verticle-center">Date</th>
                </tr>
                @foreach($products as $key => $product)
                    <tr>
                        <td class="verticle-center text-center">
                            <input type="checkbox" name="selected_product[]" class="selected_product" value="{{ $product->code?:'' }}">
                        </td>
                        <td class="verticle-center">{{ ++$key }}</td>
                        <td class="verticle-center">{{ $product->code?:'-' }}</td>
                        <td class="text-center verticle-center">{{ $product->product_name?:'-' }}</td>
                        <td class="text-center verticle-center">{{ $product->channel?:'-' }}</td>
                        <td class="text-right verticle-center" width="10%">
                            <div class="input-group">
                              <div class="input-group-addon">Rs</div>
                              <input type="text" class="form-control new_price" name="new_price[]" placeholder="Enter price." value="{{ $product->price?:'-' }}" style="width: 150px;" disabled>
                              <input type="hidden" class="old_price" name="old_price[]" value="{{ $product->price?:'' }}" disabled>
                            </div>
                        </td>
                        <td class="text-center verticle-center">
                            @if($product->price_status == 1)
                                <span class="fa fa-check-circle text-green"></span>
                            @else
                                <span class="fa fa-check-circle text-grey"></span>
                            @endif
                        </td>
                        <td class="text-center verticle-center">{{ $product->product_pricebook_date?:'-' }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <h3 align="center" class="text-grey">Products not found!.</h3>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-2 col-md-2 col-ms-2 col-xs-3 col-lg-offset-9 col-md-offset-9  col-ms-offset-9  col-xs-offset-8">
        <button type="submit" name="btnSubmit" id="btnSubmit" class="btn bg-purple btn-block" disabled><span class="fa fa-save"></span> Update</button>
    </div>      
</div>
@section('common_js')
<script>
    $(document).ready(function() {
        $('#pricebook_form').on('change keyup', 'input, select, textarea', function(evt){
            if($('#pricebook_name').val().trim().length > 0 && $('#channel_id').val() != ''){
                $('#btnSubmit').attr('disabled', false);
            }else{
                $('#btnSubmit').attr('disabled', true);
            }
        });  

        $('#select_all').click(function(){
            $('.selected_product').prop('checked', $(this).prop('checked'));
            $('.selected_product').parents('tr').find('.new_price').attr('disabled', !$(this).prop('checked'));
            $('.selected_product').parents('tr').find('.old_price').attr('disabled', !$(this).prop('checked'));
        });

        $('.selected_product').on('change', function(e){
            $(this).parents('tr').find('.new_price').attr('disabled', !$(this).prop('checked'));
            $(this).parents('tr').find('.old_price').attr('disabled', !$(this).prop('checked'));
        });

        //currency format
        var lates_cur_value = null;
        var format = function(num){
            var pattern = /^[+]?([1-9][0-9]*(?:[\.][0-9]*)?|0*\.0*[1-9][0-9]*)(?:[eE][+-][0-9]+)?$/;             

            if(pattern.test(num)){
                var str = num.toString(), parts = false, output = [], i = 1, formatted = null;
                if(str.indexOf(".") > 0) {
                    parts = str.split(".");
                    str = parts[0];
                }
                str = str.split("").reverse();
                for(var j = 0, len = str.length; j < len; j++) {
                    if(str[j] != ",") {
                        output.push(str[j]);
                        i++;
                    }
                }

                formatted = output.reverse().join("");
                lates_cur_value = num;
                return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
            }else{
                return  lates_cur_value;
            }
        };

        $('.new_price').on('keyup', function(e){
            console.log($(this).val());
            $(this).val(format($(this).val()));
        });
    });
</script>
@stop
<div class="form-group row {{ $errors->has('channel') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="channel">Auction</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <select name="auction_id" id="auction_id" class = "form-control chosen">
        <option value="0">Select Auction</option>
        @foreach($auction_list as $value)
            <option value="{{$value->id}}">{{$value->event_name}}</option>
        @endforeach
        </select>
        @if($errors->has('auction_id'))
            <span class="help-block">
                {{ $errors->first('auction_id') }}
            </span>
        @endif
    </div>  
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <label class="control-label pull-right" title="Description">Excel File</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-12 col-xs-12">
                <span class="btn bg-purple btn-file btn-block">
                    Browse <input type="file" name="excel_file" id="excel_file">
                </span>
                @if($errors->has('excel_file'))
                    <p class="padding-top-10 text-red">
                      {{ $errors->first('excel_file') }}  
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                <a href="{{ asset('assets/files/upload_product_allocation.xlsx') }}" class="btn btn-default btn-block"><span class="fa fa-download"></span> Example Excel</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-lg-offset-9 col-md-offset-9">
                <button type="submit" name="btnSubmit" id="btnSubmit" class="btn bg-purple btn-block" disabled><span class="fa fa-save"></span> Allocate Item</button>
            </div>      
        </div>
    </div>
</div>
@section('common_js')
<script>
$(document).ready(function() {
    $('#itemallocation_form').on('change keyup', 'input, select, textarea', function(evt){
        if($("#excel_file").get(0).files.length > 0 && $('#auction_id').val() > 0){
            $('#btnSubmit').attr('disabled', false);
        }else{
            $('#btnSubmit').attr('disabled', true);
        }
    });
    
});
</script>
@stop
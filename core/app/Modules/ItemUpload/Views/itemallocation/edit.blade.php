@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
  
</style>
<script>
    function diasbledSubmitButton(form){
        form.btnSubmit.disabled = true;
        form.btnSubmit.text = 'Waiting...';
        return true;
    }
</script>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Pricebook 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Pricebook</li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Pricebook</h3>
            <div class="box-title pull-right">
                <button class="btn btn-warning btn-xs" type="button" onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([
                'route'  => ['pricebook.update', isset($pricebook->pricebook_channel_id)? $pricebook->pricebook_channel_id:''], 
                'class'    => 'form-horizontal', 
                'id'       => 'pricebook_form', 
                'method'   => 'post',
                'files'    => true,
                'onsubmit' => 'return diasbledSubmitButton(this)']) 
            !!}
                <input type="hidden" name="pricebook_channel_id" value="{{ isset($pricebook->pricebook_channel_id)? $pricebook->pricebook_channel_id :'' }}">
                @include ('Pricebook::pricebook.edit-form')
            {!! Form::close() !!}
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
@yield('common_js')
@stop





































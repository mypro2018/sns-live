@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Pricebook 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Pricebook</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
            {!! Form::open(['method' => 'GET', 'route' => 'pricebook.index', 'role' => 'search'])  !!}
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-ms-3 col-xs-3">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            {!! Form::select('status', ['all' => 'All', '1' => 'active', '0' => 'Inactive'], Input::get('status'), [
                                    'class' => 'form-control chosen',
                                    'name'  => 'status'
                                ]) 
                            !!}
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-ms-7 col-xs-7">
                        <div class="form-group">
                            <label class="control-label">&nbsp;&nbsp;</label>
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ Input::get('search')?:old('search') }}">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                        <label class="control-label">&nbsp;&nbsp;</label>
                        <button class="btn btn-default pull-right btn-block" type="submit"><span class="fa fa-search"></span> Search</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div> 
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Pricebook</h3>
            <div class="box-title pull-right">
                <a href="{{ route('pricebook.create') }}" class="btn btn-success btn-sm" title="Add New Pricebook">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Channel</th>
                            <th>Pricebook name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($pricebooks) > 0)
                        @foreach($pricebooks as $key => $pricebook)
                            <tr>
                                <td>{{ (++$key) }}</td>
                                <td>{{ $pricebook->channel }}</td>
                                <td>{{ $pricebook->name }}</td>
                                <td>{{ $pricebook->description }}</td>
                                <td>
                                    @if($pricebook->status == 1)
                                        <span class="fa fa-check-circle text-green"></span>
                                    @else
                                        <span class="fa fa-check-circle text-grey"></span>
                                    @endif
                                </td>
                                <td>{{ $pricebook->created_at->format('Y-m-d') }}</td> 
                                <td class="text-center">
                                    <a href="{{ route('pricebook.show', $pricebook->pricebook_channel_id) }}" title="View Pricebook"><button class="btn btn-default btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                    <a href="{{ route('pricebook.edit', $pricebook->pricebook_channel_id) }}" title="Edit Pricebook"><button class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                    <button type="button" data-url="{{ route('pricebook.destroy', ['pricebook_id' => $pricebook->pricebook_id, 'pricebook_channel_id' => $pricebook->pricebook_channel_id]) }}" title="Delete Pricebook" class="btn btn-default btn-xs" name="btn-delete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>  
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="7">
                            <h3 align="center" class="text-grey">Data not found!.</h3>
                        </td>
                    @endif
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $pricebooks->appends(['search' => Request::get('search'), 'status' => Request::get('status')])->render() !!} </div>
            </div>  
            @if(Session::get('products') !== null)
                <br>
                <h3 class="text-grey line-bottom">Upload status</h3>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered">
                            <tr class="color-dark-blue">
                                <th>Product code</th>
                                <th>Message</th>
                                <th>Upload status</th>
                            </tr>
                            @if(isset(Session::get('products')->info))
                                @foreach(Session::get('products')->info as $key => $info)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $info->msg }}</td>
                                        <td>{!! $info->status == 'success'? '<span class="fa fa-check text-green"></span> success': '<span class="fa fa-times text-red"></span> error'!!}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">
                                        <h3 align="center" class="text-grey">Product not found!.</h3>
                                    </td>
                                </tr>
                            @endif
                        </table>
                        <div class="pull-right">
                            <table class="padding-10">
                                <tr>
                                    <td><strong><p class="text-green">Success</p></strong></td>
                                    <td class="padding-5"><p>{{ Session::get('products')->success_count !== null? Session::get('products')->success_count :'-' }}</p></td>
                                </tr>
                                <tr>
                                    <td><strong><p class="text-red">Error</p></strong></td>
                                    <td class="padding-5"><p>{{ Session::get('products')->error_count !== null? Session::get('products')->error_count :'-' }}</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endif    
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('button[name=btn-delete]').click(function(){
        var link = $(this).data('url');
        //params [link, title, message]
        confirm_delete(link, 'Are you sure?', 'You wanna delete this pricebook?.');
    });
});
</script>
@stop



































<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <label class="control-label pull-right" title="Description">Excel File</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-12 col-xs-12">
                <span class="btn bg-purple btn-file btn-block">
                    Browse <input type="file" name="excel_file" id="excel_file">
                </span>
                @if($errors->has('excel_file'))
                    <p class="padding-top-10 text-red">
                      {{ $errors->first('excel_file') }}  
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                <a href="{{ asset('assets/files/upload_product.xlsx') }}" class="btn btn-default btn-block"><span class="fa fa-download"></span> Example Excel</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-lg-offset-9 col-md-offset-9">
                <button type="submit" name="btnSubmit" id="btnSubmit" class="btn bg-purple btn-block" disabled><span class="fa fa-save"></span>Upload Item</button>
            </div>      
        </div>
    </div>
</div>
@section('common_js')
<script>
$(document).ready(function() {
    $('#itemupload_form').on('change keyup', 'input, select, textarea', function(evt){
    if($("#excel_file").get(0).files.length > 0){
        $('#btnSubmit').attr('disabled', false);
    }else{
        $('#btnSubmit').attr('disabled', true);
    }
    });
});
</script>
@stop
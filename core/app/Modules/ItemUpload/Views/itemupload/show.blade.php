@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Pricebook 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Pricebook</li>
        <li class="active">List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title pull-right">
                <button class="btn btn-warning btn-xs" type="button" onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                <a href="{{ route('pricebook.edit', $pricebook->pricebook_channel_id) }}" title="Edit Pricebook"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                <button type="button" data-url="{{ route('pricebook.destroy', ['pricebook_id' => $pricebook->pricebook_id, 'pricebook_channel_id' => $pricebook->pricebook_channel_id]) }}" name="btn-delete" id="btn-delete" class="btn btn-danger btn-xs">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <form accept="{{ route('pricebook.show', $pricebook->id) }}" method="get">
                    <div class="col-lg-6 col-lg-offset-4">
                        <div class="form-group">
                            <input type="text" name="search" id="search" placeholder="Enter Product name or code." class="form-control" value="{{ Input::get('search') }}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-default btn-block">Search <span class="fa fa-search"></span></button>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <h4 class="line-bottom padding-bottom-10">Pricebook</h4>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>Channel</th>
                                    <td>{{ $pricebook->channel?:'-' }}</td>
                                </tr>
                                <tr>
                                    <th>Pricebook name</th>
                                    <td>{{ $pricebook->name?:'-' }}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{{ $pricebook->description?:'-' }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        @if($pricebook->status == 1)
                                            <span class="fa fa-check-circle text-green"></span>
                                        @else
                                            <span class="fa fa-check-circle text-grey"></span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <td>{{ $pricebook->created_at->format('Y-m-d')?:'-' }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-8">
                    <h4 class="line-bottom padding-bottom-10">Products Price</h4>
                    <br>
                    @if(isset($products) && count($products) > 0)
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Channel</th>
                                <th class="text-right">Price</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Date</th>
                            </tr>
                            @foreach($products as $key => $product)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $product->code?:'-' }}</td>
                                    <td class="text-center">{{ $product->product_name?:'-' }}</td>
                                    <td class="text-center">{{ $product->channel?:'-' }}</td>
                                    <td class="text-right">{{ 'Rs '.$product->price?:'-' }}</td>
                                    <td class="text-center">
                                        @if($product->price_status == 1)
                                            <span class="fa fa-check-circle text-green"></span>
                                        @else
                                            <span class="fa fa-check-circle text-grey"></span>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $product->product_pricebook_date?:'-' }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <h3 align="center" class="text-grey">Products not found!.</h3>
                    @endif
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('button[name=btn-delete]').click(function(){
        var link = $(this).data('url');
        //params [link, title, message]
        confirm_delete(link, 'Are you sure?', 'You wanna delete this pricebook?.');
    });
});
</script>
@stop





























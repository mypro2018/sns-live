<?php
/**
 * ITEM UPLOAD MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
namespace App\Modules\ItemUpload\Requests;

use App\Http\Requests\Request;

class ItemUploadRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;
		$rules = [
			'excel_file' => 'required|mimes:xls,xlsx',
		];
		
		return $rules;
	}

}

<?php
/**
 * ITEM UPLOAD MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-19
 */
 
Route::group(['middleware' => ['auth']], function(){
    Route::group(['prefix' => 'itemupload', 'namespace' => 'App\Modules\ItemUpload\Controllers'],function(){
    	/*===================================
					 ITEM UPLOAD GET request
    	===================================*/
		Route::get('create', [
			'uses' => 'ItemUploadController@create',
			'as'   => 'itemupload.create'
        ]);

        Route::get('allocate', [
			'uses' => 'ItemUploadController@allocateCreate',
			'as'   => 'itemallocation.create'
        ]);
        

        // Route::get('/', [
		// 	'uses' => 'ItemUploadController@index',
		// 	'as'   => 'itemupload.index'
        // ]);

        // Route::get('delete/pricebook_id/{pricebook_id}/pricebook_channel_id/{pricebook_channel_id}', [
		// 	'uses' => 'ItemUploadController@destroy',
		// 	'as'   => 'itemupload.destroy'
        // ]);

        // Route::get('show/{id}', [
        //     'uses' => 'ItemUploadController@show',
        //     'as'   => 'itemupload.show'
        // ]);

        // Route::get('edit/{id}', [
        //     'uses' => 'ItemUploadController@edit',
        //     'as'   => 'itemupload.edit'
        // ]);


        /*===================================
					POST request
    	===================================*/
    	Route::post('store', [
    		'uses' => 'ItemUploadController@store',
    		'as'   => 'itemupload.store'
        ]);
        
        Route::post('allocatestore', [
    		'uses' => 'ItemUploadController@storeAllocate',
    		'as'   => 'itemallocation.store'
    	]);

        // Route::post('update/{id}', [
        //     'uses' => 'ItemUploadController@update',
        //     'as'   => 'itemupload.update'
        // ]);
	});  	
}); 


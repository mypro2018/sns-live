<?php
/**
 * PRICEBOOK MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-31
 */

namespace App\Modules\ItemUpload\ItemUploadRepository;

use App\Modules\ProductManage\Models\ProductManage;
use App\Modules\ItemUpload\Requests\ItemUploadRequest;
use App\Modules\AuctionManage\Models\AuctionDetailsManage;
use App\Modules\SupplierManage\Models\SupplierManage;
use App\Modules\CategoryManage\Models\CategoryManage;
use Excel;
use DB;

class ItemUploadRepository{
    public function __construct(){
        
    }

    //get all channel
    public function getAllChannel($type){
    	try {
    		if($type == "get")
    		{
    			$channels = Channel::all();

	    		if(count($channels) > 0){
	    			return $channels;
	    		}else{
	    			return [];
	    		}
    		}
    		elseif($type == "list")
    		{
    			$channels = Channel::lists('name', 'id')->prepend('-- channel --', '');

	    		if(count($channels) > 0){
	    			return $channels;
	    		}else{
	    			return [];
	    		}
    		}
    		else
    		{
    			throw new \Exception('type parameter was not matched.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

    //get product by product code
    public function getProductByCode($product_code){
    	try {
    		if(!empty($product_code)){
    			$hasProduct = ProductManage::where('item_code', '=', $product_code)->first();
    			if(count($hasProduct) > 0){
    				return $hasProduct;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception(' Product Code cannot be empty!.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
	}

	//get supplier by supplier code
    public function getSupplierByCode($supplier_code){
    	try {
			$hasSupplier = SupplierManage::where('supplier_code', '=', $supplier_code)->first();
			if(count($hasSupplier) > 0){
				return $hasSupplier;
			}else{
				return [];
			}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
	}


	//get category by category code
    public function getCategoryByCode($category_code){
    	try {
    		if(!empty($category_code)){
    			$hasCategory = CategoryManage::where('code', '=', $category_code)->first();
    			if(count($hasCategory) > 0){
    				return $hasCategory;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception('Category Code cannot be empty!.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
	}
	
	//get product by product code
    public function productHasCategory($product_code){
    	try {
    		if(!empty($product_code)){
				$hasProduct = ProductManage::where('item_code', '=', $product_code)
				->whereNotNull('sns_item_category_id')
				->whereNull('deleted_at')
				->first();
    			if(count($hasProduct) > 0){
    				return $hasProduct;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception(' Product Code cannot be empty!.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
	}
	
	//get product by product code
    public function checkProductAllocate($auction_id, $product_id){
    	try {
			$hasProduct = AuctionDetailsManage::where('sns_auction_id', '=', $auction_id)
			->where('sns_item_id', '=', $product_id)
			->whereNull('deleted_at')
			->first();
			if(count($hasProduct) > 0){
				return $hasProduct;
			}else{
				return [];
			}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    //check is exists channel by id
    public function isChannelExist($pricebook_channel_id = null, $channel_id){
    	try {
    		if(!empty($channel_id)){
    			$channel = PricebookChannel::where('channel_id', $channel_id)
    				->where('status', 1);

    			if($pricebook_channel_id !== null){
    				$channel = $channel->where('id', '!=', $pricebook_channel_id);
    			}

    			$channel = $channel->first();

    			if(count($channel) > 0){
    				return $channel;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception('Channel not found for this ID :'.$channel_id);	
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    //save pricebook
    public function uploadItem($request){
    	try {
    		DB::beginTransaction();
		
			$excel_file       = $request->file('excel_file');
			$uploadProduct    = null;
			$success_count    = 0;            
			$error_count      = 0; 
			$msg              = [];           
			if(count($excel_file) > 0){
		    	Excel::load($excel_file, function($reader) use($uploadProduct, &$success_count, &$error_count, &$msg){
					$reader->each(function($row)use($uploadProduct, &$success_count, &$error_count, &$msg){
						if(isset($row->code)){
							$product   = $this->getProductByCode($row->code);
							$category  = $this->getCategoryByCode($row->category_code);
							$status    = null;
							if(count($category) > 0){
								$category = $category->id;
								$status   = PRODUCT_TO_BEGIN; 
							}else{
								$category = 0;
								$status   = PRODUCT_NOT_AVAILABLE;
							}
							$supplier  = $this->getSupplierByCode($row->supplier_code);
							if(count($supplier) > 0){
								$supplier = $supplier->id;
							}else{
								$supplier = 0;
							}

							if(count($product) == 0){
								$uploadProduct  = new ProductManage();
								$uploadProduct->item_code    		 = $row->code;
								$uploadProduct->name 	     		 = $row->name;
								$uploadProduct->display_name 		 = $row->name;
								$uploadProduct->description  		 = $row->description;
								$uploadProduct->sns_supplier_id 	 = $supplier;
								$uploadProduct->sns_item_category_id = $category;
								$uploadProduct->status    	 		 = $status;
								$uploadProduct->created_at 	 		 = date('Y-m-d h:i:s');
								$uploadProduct->updated_at 	 		 = date('Y-m-d h:i:s');
								$uploadProduct->save();
								
								if(count($uploadProduct) > 0){
									$success_count = ++$success_count;
									$msg['info'][$uploadProduct->item_code]['status'] = 'success';
									$msg['info'][$uploadProduct->item_code]['msg']    = 'Uploaded.';
								}else{
									$error_count = ++$error_count;
									$msg['info'][$row->code]['status'] = 'error';
									$msg['info'][$row->code]['msg']    = 'Data cannot saved in product.';
								}
								 
							}else{
								$error_count = ++$error_count;
								$msg['info'][$product->item_code]['status'] = 'error';
								$msg['info'][$product->item_code]['msg']    = 'Product already exist "'.$product->item_code.'".';
							}
						}else{
							DB::rollback();
							throw new \Exception("Invalid Excel file, columns doesn't matched");
						}
					});
					DB::commit();
				});
				$msg['success_count'] = $success_count;
				$msg['error_count']   = $error_count;
		        return json_decode(json_encode($msg));
			}else{
				DB::rollback();
				return [];
			}
    	} catch (\Exception $e) {
    		DB::rollback();
    		throw new \Exception('message: '.$e->getMessage());
    	}
	}
	
	//save pricebook
    public function allocateProduct($request){
    	try {
    		DB::beginTransaction();
		
			$excel_file       = $request->file('excel_file');
			$allocateProduct  = null;
			$success_count    = 0;            
			$error_count      = 0; 
			$msg              = [];
			$order            = 1;
			$auction_id       = $request->auction_id;
			if(count($excel_file) > 0){
		    	Excel::load($excel_file, function($reader) use($allocateProduct, &$success_count, &$error_count, &$msg, $auction_id, $order){
					$reader->each(function($row)use($allocateProduct, &$success_count, &$error_count, &$msg, $auction_id, $order){
						if(isset($row->code)){
							$product = $this->getProductByCode($row->code);
							if(count($product) > 0){
								$categoryHas = $this->productHasCategory($product->item_code);
								if(count($categoryHas) > 0){
									$allocate = $this->checkProductAllocate($auction_id, $product->id);
									if(count($allocate) == 0){
										$allocateProduct = new AuctionDetailsManage();
										$allocateProduct->sns_auction_id 	= $auction_id;
										$allocateProduct->sns_item_id    	= $product->id;
										$allocateProduct->start_bid_price   = $row->grn_price;
										$allocateProduct->withdraw_amount 	= $row->withdrawal_price;
										$allocateProduct->order       		= $order;
										$allocateProduct->status       		= PRODUCT_TO_BEGIN;
										$allocateProduct->created_at   		= date('Y-m-d h:i:s');
										$allocateProduct->updated_at  	 	= date('Y-m-d h:i:s');
										$allocateProduct->save();
										if(count($allocateProduct) > 0){
											$success_count = ++$success_count;
											$msg['info'][$product->item_code]['status'] = 'success';
											$msg['info'][$product->item_code]['msg']    = 'Uploaded.';
										}else{
											$error_count = ++$error_count;
											$msg['info'][$product->item_code]['status'] = 'error';
											$msg['info'][$product->item_code]['msg']    = 'Data cannot saved in product.';
										}
									}else{
										$error_count = ++$error_count;
										$msg['info'][$product->item_code]['status'] = 'error';
										$msg['info'][$product->item_code]['msg']    = 'Product already allocated "'.$product->item_code.'".';
									}
								}else{
									$error_count = ++$error_count;
									$msg['info'][$product->item_code]['status'] = 'error';
									$msg['info'][$product->item_code]['msg']    = 'No Category Assigned to this product "'.$product->item_code.'".';
								}
							}else{
								$error_count = ++$error_count;
								$msg['info'][$row->code]['status'] = 'error';
								$msg['info'][$row->code]['msg']    = 'Product cannot found "'.$row->code.'".';
							}
						}else{
							DB::rollback();
							throw new \Exception("Invalid Excel file, columns doesn't matched");
						}
					});
					DB::commit();
				});
				$msg['success_count'] = $success_count;
				$msg['error_count']   = $error_count;
		        return json_decode(json_encode($msg));
			}else{
				DB::rollback();
				return [];
			}
    	} catch (\Exception $e) {
    		DB::rollback();
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

}

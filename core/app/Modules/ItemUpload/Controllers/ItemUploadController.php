<?php
/**
 * ITEM UPLOAD MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-24
 */

namespace App\Modules\ItemUpload\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\ProductManage\Models\ProductManage;
use App\Modules\ItemUpload\ItemUploadRepository\ItemUploadRepository;
use App\Modules\ItemUpload\ItemUploadLogic\ItemUploadLogic;
use App\Classes\Functions;
use App\Modules\ItemUpload\Requests\ItemUploadRequest;
use App\Modules\ItemUpload\Requests\ItemUploadRequestOnUpdate;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
// use App\Models\Channel;
use Illuminate\Http\Request;
use Session;

class ItemUploadController extends Controller
{
    //variable declaration
    private $itemUploadLogic;
    private $itemUploadRepo;
    private $common;
    private $auction;

    //assigned dependancy injection
    public function __construct(ItemUploadLogic $itemUploadLogic, ItemUploadRepository $itemUploadRepo,Functions $common, AuctionLogic $auction){
        $this->itemUploadRepo  = $itemUploadRepo;
        $this->itemUploadLogic = $itemUploadLogic;
        $this->common          = $common;
        $this->auction         = $auction;
    }

    /**
     * Display a listing of the pricebooks.
     *
     * @return \Illuminate\View\View
     */
    // public function index(Request $request)
    // {
    //     try {
    //         $perPage    = 25;
    //         //params: request, page count for pagination
    //         $pricebooks = $this->pricebookRepo->getPricebookByFilter($request, $perPage);
    //         return view('Pricebook::pricebook.index', compact('pricebooks'));       
    //     } catch (\Exception $e) {
    //         return $this->common->redirectWithAlert(
    //             'pricebook.index', 
    //             'error', 
    //             'Error!.', 
    //             $e->getMessage()
    //         ); 
    //     }
    // }

    /**
     * load itemupload form
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        try {
            return view('ItemUpload::itemupload.create');
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'itemupload.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Store a newly uploaded item excell in db.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ItemUploadRequest $request)
    {
        try {
            $product = $this->itemUploadRepo->uploadItem($request);
            if(count($product) > 0){
               return redirect()->route('itemupload.create')->with(['product' => $product]);  
            }else{
                throw new \Exception("Something went wrong, item couldn't be saved.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'itemupload.create',
                'error',
                'Error!.',
                $e->getMessage()
            );
        }
    }

    /**
     * load item allocation form
     *
     * @return \Illuminate\View\View
     */
    public function allocateCreate()
    {   
        try {
            $auction_list = $this->auction->auctionList();
            return view('ItemUpload::itemallocation.create')->with(['auction_list' => $auction_list]);
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'itemallocation.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }


    /**
     * Store a newly allocated item excell in db.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeAllocate(ItemUploadRequest $request)
    {
        try {
            $product = $this->itemUploadRepo->allocateProduct($request);
            if(count($product) > 0){
               return redirect()->route('itemallocation.create')->with(['product' => $product]);  
            }else{
                throw new \Exception("Something went wrong, item allocation couldn't be saved.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'itemallocation.create',
                'error',
                'Error!.',
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified pricebook.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    // public function show($id, Request $request)
    // {   
    //     try {
    //         //params: id
    //         $pricebook = $this->pricebookRepo->getPricebookById($id);
    //         $products   = $this->pricebookRepo->getProductByPricebookChannelId($pricebook->pricebook_channel_id, $request->search);

    //         return view('Pricebook::pricebook.show', compact('pricebook', 'products'));
    //     } catch (\Exception $e) {
    //         return $this->common->redirectWithAlert(
    //             'pricebook.index', 
    //             'error', 
    //             'Error!.', 
    //             $e->getMessage()
    //         ); 
    //     }
    // }

    /**
     * Show the form for editing the specified pricebook.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    // public function edit($id, Request $request)
    // {
    //     try {
    //         //params: id
    //         $pricebook = $this->pricebookRepo->getPricebookById($id);
    //         $channels  = $this->pricebookRepo->getAllChannel('list');
    //         $products  = $this->pricebookRepo->getProductByPricebookChannelId($pricebook->pricebook_channel_id, $request->search);
    //         $page      = 'edit';
            
    //         return view('Pricebook::pricebook.edit', compact('pricebook', 'channels', 'page', 'products'));
    //     } catch (\Exception $e) {
    //         return $this->common->redirectWithAlert(
    //             'pricebook.index', 
    //             'error', 
    //             'Error!.', 
    //             $e->getMessage()
    //         ); 
    //     }
    // }

    /**
     * Update the specified pricebook in db.
     *
     * @param  int  $id,
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    // public function update($id, PricebookRequestOnUpdate $request)
    // {
    //     try {
    //         $products = $this->pricebookRepo->updatePricebook($id, $request);

    //         $perPage = 25;

    //         if(count($products) > 0){
    //             if(isset($products->is_updated) && $products->is_updated == true){
    //                 if(isset($products->error)){
    //                     return $this->common->redirectWithAlert(
    //                         'pricebook.index', 
    //                         'error', 
    //                         'Error!.', 
    //                         $products->error
    //                     );        
    //                 }else{
    //                     return $this->common->redirectWithAlert(
    //                         'pricebook.index', 
    //                         'success', 
    //                         'Done!.', 
    //                         'Pricebook has been successfully updated!.'
    //                     );        
    //                 }                    
    //             }elseif(isset($products->error)){
    //                 return $this->common->redirectWithAlert(
    //                     'pricebook.index', 
    //                     'error', 
    //                     'Error!.', 
    //                     $products->error
    //                 );        
    //             }

    //             $pricebooks = $this->pricebookRepo->getPricebookByFilter($request, $perPage);
    //             return redirect()->route('pricebook.index')->with([
    //                 'products'   => $products,
    //                 'pricebooks' => $pricebooks
    //             ]);
    //         }else{
    //             throw new \Exception("message: pricebook couldn't be updated.");
    //         }
    //     } catch (\Exception $e) {
    //         return $this->common->redirectWithAlert(
    //             'pricebook.index', 
    //             'error', 
    //             'Error!.', 
    //             $e->getMessage()
    //         );
    //     }
    // }

    /**
     * Remove the specified pricebook from db.
     *
     * @param  int  $pricebook_id, $pricebook_channel_id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    // public function destroy($pricebook_id, $pricebook_channel_id)
    // {
    //     try {
    //         $pricebook = $this->pricebookRepo->deletePricebook($pricebook_id, $pricebook_channel_id);

    //         if(count($pricebook) > 0){
    //             return $this->common->redirectWithAlert(
    //                 'pricebook.index', 
    //                 'success', 
    //                 'Done!.', 
    //                 'Pricebook has been successfully deleted!.'
    //             );
    //         }else{
    //             throw new \Exception("Pricebook couldn't be deleted.");
    //         }
    //     } catch (\Exception $e) {
    //         return $this->common->redirectWithAlert(
    //             'pricebook.index', 
    //             'error', 
    //             'Error!.', 
    //             $e->getMessage()
    //         );
    //     }
    // }
}

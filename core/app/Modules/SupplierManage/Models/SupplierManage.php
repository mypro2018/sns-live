<?php namespace App\Modules\SupplierManage\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class SupplierManage extends Model {

    /**
     * The sns_supplier table associated this model.
     */
    protected $table = 'sns_supplier';
    public $timestamps = true;
    protected $guarded = ['id'];

}

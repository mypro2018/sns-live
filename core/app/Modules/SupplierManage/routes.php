<?php

Route::group(['middleware' => 'auth'], function () {
    Route::group(array('prefix' => 'supplier', 'module' => 'SupplierManage', 'namespace' => 'App\Modules\SupplierManage\Controllers'), function () {
        /**
         * GET Routes
         */

        Route::get('add',[
            'as' => 'supplier.add','uses' => 'SupplierManageController@index'
        ]);
        Route::get('list',[
            'as' => 'supplier.add','uses' => 'SupplierManageController@listView'
        ]);
        Route::get('edit/{id}',[
            'as' => 'supplier.add','uses' => 'SupplierManageController@edit'
        ]);
        Route::get('search',[
            'as' => 'supplier.list','uses' => 'SupplierManageController@searchSupplier'
        ]);
        Route::get('details',[
            'as' => 'supplier.list','uses' => 'SupplierManageController@getSupplierDetails'
        ]);


        /**
         * POST Routes
         */

        Route::post('add',[
            'as' => 'supplier.add','uses' => 'SupplierManageController@addSupplier'
        ]);

        Route::post('edit/{id}', [
            'as' => 'supplier.list','uses' => 'SupplierManageController@update'
        ]);

    });
});
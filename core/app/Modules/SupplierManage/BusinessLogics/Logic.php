<?php namespace App\Modules\SupplierManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\SupplierManage\Models\SupplierManage;
use Illuminate\Support\Facades\DB;
use Exception;


class Logic extends Model {

    /**
     * This function is used to add a supplier
     * @param array $data Array of data that needed to add supplier
     * @return object Supplier object if success, exception otherwise
     */

    public function addSupplier($data){

        DB::transaction(function() use($data){
            $supplier = SupplierManage::create($data);
            if($supplier){
                return $supplier;
            }else{
                throw new Exception("Error occured while adding new supplier");
            }
        });

    }

    /**
     * This function is used to get supplier list
     * @parm -
     * @return category object
     */
    public function getSupplierList(){
        $supplier_list = SupplierManage::select('id','fname','supplier_code')->get();
        if(count($supplier_list) > 0){
            return $supplier_list;
        }else{
            return [];
        }
    }


    /**
     * This function is used to get all supplier details
     * @parm -
     * @return supplier object
     */
    public function getAllSupplier(){
        return $all_supplier = SupplierManage::paginate(10);
    }


    /**
     * This function is used to search supplier
     * @param integer $supplier_id
     * @response supplier object
     */

    public function searchSupplier($supplier){
        return $search_supplier = SupplierManage::where('fname', 'like', '%' . $supplier . '%')->orWhere('lname', 'like', '%' . $supplier . '%')->paginate(10);
    }


    /**
     * This function is used to get all supplier details
     * @parm  integer $supplier_id that need to get supplier details
     * @return supplier object
     */
    public function getSupplierDetails($supplier_id){
        $supplier_details = SupplierManage::where('id','=',$supplier_id)->get();
        if(count($supplier_details) > 0){
            return $supplier_details;
        }else{
            return false;
        }
    }

    /**
     * This function is used to update supplier
     * @param integer $supplier_id and array $data
     * @response is boolean
     */

    public function updateSupplier($supplier_id,$data){
        DB::transaction(function() use($supplier_id,$data){
            $update_supplier = SupplierManage::where('id',$supplier_id)->update($data);
            if($update_supplier){
                return true;
            }else{
                throw new Exception("Error occurred while updating supplier");
            }
        });
    }

}

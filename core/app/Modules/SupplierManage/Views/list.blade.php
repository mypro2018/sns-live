@extends('layouts.back_master') @section('title','Supplier List')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Supplier
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Supplier List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('supplier/search')}}">
    	    <div class="box-body">
                <div class="row">
                   <div class="row">
                       <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <!-- <label for="exampleInputEmail1" id="filed-label">Supplier</label> -->
                                <input type="text" class="form-control input-sm" name="supplier" placeholder="Search Supplier" value="{{$supplier}}">
                            </div>
                       </div>
                   </div>
               </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search"></i> Search</button>
                             <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>

    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Supplier List</h3>
          <a href="{{url('supplier/add')}}" class="btn btn-sm bg-purple pull-right">New Supplier</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Supplier Code</th>
                                <th>Supplier Name</td>
                                <th>Contact No</th>
                                <th>Created At</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;?>
                        @if(count($supplier_details) > 0)
                            @foreach($supplier_details as $result_val)
                                @include('SupplierManage::template.supplier')
                            <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="6" class="text-center">No data found.</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @if(count($supplier_details) > 0 && count($supplier_details) == 10)
            <div class="box-footer">      
                <div class="pull-right">{!! $supplier_details->render() !!}</div>
            </div>
        @endif
    </div>
</section>
@stop


@section('js')
<script type="text/javascript">
/*This function is used to view supplier details in popup model*/
function viewDetail(supplier_id) {
    $('.refresh').addClass('panel-refreshing');
    var content='<div class="row">'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Supplier Code</label>'+
                        '<input type="text" class="form-control input-sm code" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Supplier Name</label>'+
                        '<input type="text" class="form-control input-sm name" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Contact No</label>'+
                        '<input type="text" class="form-control input-sm contact" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Address</label>'+
                        '<textarea class="form-control input-sm address" rows="3" readonly></textarea>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>City</label>'+
                        '<input type="text" class="form-control input-sm city" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>E-mail</label>'+
                        '<input type="text" class="form-control input-sm mail" readonly>'+
                    '</div>'+
                    '<div class="col-lg-12 form-group">'+
                        '<label>Remarks</label>'+
                        '<textarea class="form-control input-sm remark" rows="3" readonly></textarea>'+
                    '</div>'+
                '</div>';
    $.ajax({
        url: "{{URL::to('supplier/details')}}",
        method: 'GET',
        data: {'supplier_id': supplier_id},
        async: false,
        success: function (data) {
            console.log(data);
            $.confirm({
                title: 'Supplier Details',
                theme: 'material',
                content: content,
                // columnClass: 'medium',
                onContentReady: function () {
                    var self = this;
                    self.$content.find('.code').val(data.details[0].supplier_code);
                    self.$content.find('.name').val(data.details[0].fname+' '+data.details[0].lname);
                    self.$content.find('.contact').val(data.details[0].contact_no);
                    self.$content.find('.address').html(data.details[0].address);
                    self.$content.find('.city').val(data.details[0].city);
                    self.$content.find('.mail').val(data.details[0].email);
                    self.$content.find('.remark').html(data.details[0].remark);
                },buttons: {
                    close: function () {
                        $('.refresh').removeClass('panel-refreshing');
                    }
            }
        });
        },error: function () {
            alert('error');
        }
    });
}
</script>
@stop

@extends('layouts.back_master') @section('title','Add Supplier')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Supplier
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('supplier/list')}}}">Supplier List</a></li>
		<li class="active">Add Supplier</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" class="form-horizontal form-validation" method="post">
    		<div class="box-header with-border">
    			<h3 class="box-title">Add Suppiler</h3>
    		</div>
    		<div class="box-body">		    
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('supplier_code')) has-error @endif">
                    <label class="col-sm-2 control-label">Supplier Code <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="supplier_code" placeholder="Supplier Code" value="{{Input::old('supplier_code')}}">
                        @if($errors->has('supplier_code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('supplier_code')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('first_name')) has-error @endif">
                    <label class="col-sm-2 control-label">First Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{Input::old('first_name')}}">
                        @if($errors->has('first_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('first_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{Input::old('last_name')}}">
                    </div>
                </div>
                <div class="form-group @if($errors->has('contact')) has-error @endif">
                    <label class="col-sm-2 control-label">Contact No</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="contact" placeholder="94712345678" value="{{Input::old('contact')}}">
                        @if($errors->has('contact'))
                            <label id="label-error" class="error" for="label">{{$errors->first('contact')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('mail')) has-error @endif">
                    <label class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="mail" placeholder="E-mail" value="{{Input::old('mail')}}">
                        @if($errors->has('mail'))
                            <label id="label-error" class="error" for="label">{{$errors->first('mail')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="3" name="address" placeholder="Address">{{Input::old('address')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="city" placeholder="City" value="{{Input::old('city')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Note</label>
                    <div class="col-sm-10">
                         <textarea class="form-control input-sm" rows="3" name="note" placeholder="Write Something About Suppier">{{Input::old('note')}}</textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="submit" class="btn btn-sm bg-purple pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
		</form>
	</div>
</section>
@stop

<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val['supplier_code']}}</td>
     <td>{{$result_val['fname'].' '.$result_val['lname']}}</td>
     <td>{{$result_val['contact_no']}}</td>
     <td>{{$result_val['created_at']}}</td>
     <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Supplier" onclick="viewDetail({{$result_val['id']}})"><i class="fa fa-eye view"></i></a>
            <a href="{{'edit/'.$result_val['id']}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Supplier"><i class="fa fa-pencil"></i></a>
        </div>
     </td>
</tr>


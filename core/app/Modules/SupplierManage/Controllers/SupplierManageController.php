<?php namespace App\Modules\SupplierManage\Controllers;


/**
* Controller class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\SupplierManage\BusinessLogics\Logic;
use Exception;
use Response;


class SupplierManageController extends Controller {



    protected $supplier;

    public function __construct(Logic $logic){
        $this->supplier = $logic;
    }

	/**
	 * This function used to display supplier add form
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("SupplierManage::add");
	}


    /**
     * This function used to add supplier
     *
     */
    public function addSupplier(Request $request){
        //validate user input
        $this->validate($request,[
            'supplier_code' => 'required|unique:sns_supplier,supplier_code,NULL,id,deleted_at,NULL',
            'first_name'    => 'required',
            'mail'          => 'Email',
            'contact'       => 'regex:/(94)[0-9]{9}/'
        ]);
        try {
            $supplier = $this->supplier->addSupplier([
                'supplier_code' => $request->supplier_code,
                'fname'         => $request->first_name,
                'lname'         => $request->last_name,
                'address'       => $request->address,
                'city'          => $request->city,
                'contact_no'    => $request->contact,
                'email'         => $request->mail,
                'remark'        => $request->note
            ]);

            return redirect( 'supplier/add' )->with([
                'success' => true,
                'success.message' => 'Supplier added successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return redirect('supplier/add')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }

    }



    /**
     * This function is used to display all suppliers
     * @return Response
     */
    public function listView(){
        $supplier_details = $this->supplier->getAllSupplier();
        return view("SupplierManage::list")->with(['supplier_details' => $supplier_details,'supplier' => '']);
    }


    /**
     * This function is used to search suppliers
     * @return Response
     */

    public function searchSupplier(Request $request){
        $supplier_details = $this->supplier->searchSupplier($request->get('supplier'));
        return view("SupplierManage::list")->with(['supplier' => $request->get('supplier'),'supplier_details' => $supplier_details]);
    }


    /**
     * This function is used to display supplier more details
     * @return Response
     */
    public function getSupplierDetails(Request $request){
        if($request->ajax()) {
            $supplier_details = $this->supplier->getSupplierDetails($request->get('supplier_id'));
            return Response::json(['details' => $supplier_details]);
        }else{
            return Response::json([]);
        }
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * This function is used to display the edit fileds.
     *
     * @param  int  $supplier_id
     * @return Response
     */
    public function edit($supplier_id){
        $supplier_details = $this->supplier->getSupplierDetails($supplier_id);
        if($supplier_details) {
            return view("SupplierManage::edit")->with(['supplier_details' => $supplier_details]);
        }else{
            return response()->view('errors.404');
        }
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        //validate user input
        $this->validate($request,[
            'supplier_code' => 'required|unique:sns_supplier,supplier_code,'.$id.',id,deleted_at,NULL',
            'first_name'    => 'required',
            'email'         => 'Email',
            'contact'       => 'regex:/(94)[0-9]{9}/'
        ]);
        try {
            $supplier = $this->supplier->updateSupplier($id,[
                'supplier_code' => $request->supplier_code,
                'fname'         => $request->first_name,
                'lname'         => $request->last_name,
                'address'       => $request->address,
                'city'          => $request->city,
                'contact_no'    => $request->contact,
                'email'         => $request->email,
                'remark'        => $request->note
            ]);

            return redirect('supplier/list')->with([
                'success' => true,
                'success.message' => 'Supplier Updated successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return back()->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

<?php namespace App\Modules\ReportManagement\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\ProductManage\Models\ItemApproveManage;
use App\Modules\AuctionLiveManage\Models\AuctionWinBidManage;
use App\Modules\CustomerManage\Models\CustomerManage;
use App\Modules\CustomerManage\Models\CustomerAuctionManage;
use DB;

class Logic extends Model {

	/**
	* This function is used to get report detail
	* @param
	* @return
	*/
	function getReportDetail(){
        $report = AuctionWinBidManage::select(
            'sns_auction_winner.id as sold_order_id', 
            'item.id',
            'detail.id as detail_id',
            'item.name',
            'item.item_code',
            'sns_auction_winner.win_amount',
            'sns_auction_winner.created_at',
            'sns_auction_winner.card_no',
            'item_approve.item_approve_sold_status',
            'sns_auction_winner.sns_user_id as register_id',
             DB::raw("CONCAT(sns_customer_register.fname, ' ',sns_customer_register.lname) AS customer_name"),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'card_range.card_type',
            DB::raw('SUM(sns_auction_winner.win_amount) AS total_sale'),
            'sns_auction.event_name',
            'item_approve.advance_status'
        )
        ->leftJoin('sns_auction_bid as bid', function($sql_bid){
            $sql_bid->on('sns_auction_winner.sns_auction_bid_id', '=', 'bid.id');
        })
        ->leftJoin('sns_auction_detail as detail', function($sql_detail){
            $sql_detail->on('bid.sns_auction_detail_id', '=', 'detail.id')
            ->whereNull('detail.deleted_at');
        })
        ->join('sns_auction', function($sql_detail){
            $sql_detail->on('sns_auction.id', '=', 'detail.sns_auction_id');
        })
        ->leftJoin('sns_item as item', function($sql_item){
            $sql_item->on('item.id', '=', 'detail.sns_item_id');
        })
        ->leftJoin('item_approve', function($query){
            $query->on('detail.id', '=', 'item_approve.detail_id');
        })
        ->leftJoin('sns_customer_auction', function($query){
            $query->on('sns_auction_winner.card_no', '=', 'sns_customer_auction.card_no');
        })
        ->leftJoin('sns_customer_register', function($query){
            $query->on('sns_auction_winner.sns_user_id', '=', 'sns_customer_register.id');
        })
        ->leftJoin('card_range', function($query){
            $query->on('sns_customer_auction.card_range_id', '=', 'card_range.id');
        })
        ->orderBy('sns_auction_winner.created_at', 'DESC')
        ->groupBY('detail.sns_item_id')
        ->paginate(unserialize(SHOW_OPTION)[1]);

        return $report;
    }
    
    /**
	 * This function is used to filter report data
	 * @param date date
	 * @param integer auction
	 * @param integer card_no
	 */
    public function searchReport($request){
        $report = AuctionWinBidManage::select(
            'sns_auction_winner.id as sold_order_id', 
            'item.id',
            'detail.id as detail_id',
            'item.name',
            'item.item_code',
            'sns_auction_winner.win_amount',
            'sns_auction_winner.created_at',
            'sns_auction_winner.card_no',
            'item_approve.item_approve_sold_status',
            'sns_auction_winner.sns_user_id as register_id',
            DB::raw("CONCAT(sns_customer_register.fname, ' ',sns_customer_register.lname) AS customer_name"),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'card_range.card_type',
            DB::raw('SUM(sns_auction_winner.win_amount) AS total_sale'),
            'sns_auction.event_name',
            'item_approve.advance_status'
        )
        ->leftJoin('sns_auction_bid as bid', function($sql_bid){
            $sql_bid->on('sns_auction_winner.sns_auction_bid_id', '=', 'bid.id')
            ->whereNull('bid.deleted_at');
        })
        ->leftJoin('sns_auction_detail as detail', function($sql_detail){
            $sql_detail->on('bid.sns_auction_detail_id', '=', 'detail.id')
            ->whereNull('detail.deleted_at');
        })
        ->join('sns_auction', function($sql_detail){
            $sql_detail->on('sns_auction.id', '=', 'detail.sns_auction_id')
            ->whereNull('sns_auction.deleted_at');
        })
        ->leftJoin('sns_item as item', function($sql_item){
            $sql_item->on('item.id', '=', 'detail.sns_item_id')
            ->whereNull('item.deleted_at');
        })
        ->leftJoin('item_approve', function($query){
            $query->on('detail.id', '=', 'item_approve.detail_id')
            ->whereNull('item_approve.deleted_at');
        })
        ->leftJoin('sns_customer_auction', function($query){
            $query->on('sns_auction_winner.card_no', '=', 'sns_customer_auction.card_no')
            ->whereNull('sns_auction_winner.deleted_at');
        })
        ->leftJoin('sns_customer_register', function($query){
            $query->on('sns_auction_winner.sns_user_id', '=', 'sns_customer_register.id')
            ->whereNull('sns_customer_register.deleted_at');
        })
        ->leftJoin('card_range', function($query){
            $query->on('sns_customer_auction.card_range_id', '=', 'card_range.id')
            ->whereNull('card_range.deleted_at');
        });

        if(!empty($request->date)){
            $report = $report->whereDate('sns_auction_winner.created_at', '=', date('Y-m-d',strtotime($request->date)));
        }

        if(!empty($request->auction)){
            $report = $report->where('detail.sns_auction_id', $request->auction);
        }

        if(!empty($request->card_no)){
            $report = $report->where('sns_auction_winner.card_no', $request->card_no);
        }

        if(!empty($request->registration_no)){
            $report = $report->where('sns_auction_winner.sns_user_id', $request->registration_no);
        }

        $report = $report->orderBy('sns_auction_winner.created_at', 'DESC')
        ->groupBY('detail.sns_item_id')
        ->paginate(unserialize(SHOW_OPTION)[1]);
        return $report;

    }
    
    //calculate total sale
    public function totalSale($auction_id = null){
        if($auction_id == null){
            $total_sale = AuctionWinBidManage::select(DB::raw('SUM(win_amount) as total_sale'))
            ->whereNull('deleted_at')
            ->first();
            return $total_sale->total_sale;
        }else{
            $auction_sale = AuctionWinBidManage::select(DB::raw('SUM(win_amount) as total_sale'))
            ->whereIn('sns_auction_bid_id', function($sql) use($auction_id){
                $sql->select('id')
                    ->from('sns_auction_bid')
                    ->whereIn('sns_auction_detail_id', function($sql) use($auction_id){
                        $sql->select('id')
                            ->from('sns_auction_detail')
                            ->where('sns_auction_id', $auction_id);
                    });
            })
            ->whereNull('deleted_at')
            ->first();
            return $auction_sale->total_sale;
        }
    }

    //get sold item details
    /**
	* This functin is used to get report detail
	* @param
	* @return
	*/
	public function getSoldItemDetail($id){
        $sold_item_detail = AuctionWinBidManage::select('item.id',
            'item.id',
            'item.name',
            'item.item_code',
            'sns_auction_winner.win_amount',
            'sns_auction_winner.created_at',
            'sns_auction_winner.card_no',
            'item_approve.item_approve_sold_status',
             DB::raw("CONCAT(sns_customer_register.fname, ' ',sns_customer_register.lname) AS customer_name"),
            'sns_customer_register.contact_no',
            'card_range.card_type',
            'sns_auction.event_name',
            'sns_auction.id as auction_id',
            'sns_auction.auction_date',
            'sns_customer_register.id as register_id',
            'detail.id as detail_id',
            'item_approve.advance_status',
            'sns_auction.advance_rate',
            'sns_payment.amount'
        )
        ->leftJoin('sns_auction_bid as bid', function($sql_bid){
            $sql_bid->on('sns_auction_winner.sns_auction_bid_id', '=', 'bid.id');
        })
        ->leftJoin('sns_auction_detail as detail', function($sql_detail){
            $sql_detail->on('bid.sns_auction_detail_id', '=', 'detail.id');
        })
        ->join('sns_auction', function($query){
            $query->on('sns_auction.id', '=', 'detail.sns_auction_id');
        })
        ->leftJoin('sns_item as item', function($sql_item){
            $sql_item->on('item.id', '=', 'detail.sns_item_id');
        })
        ->leftJoin('item_approve', function($query){
            $query->on('detail.id', '=', 'item_approve.detail_id');
        })
        ->leftJoin('sns_customer_auction', function($query){
            $query->on('sns_auction_winner.sns_user_id', '=', 'sns_customer_auction.user_id');
        })
        ->leftJoin('sns_customer_register', function($query){
            $query->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id');
        })
        ->leftJoin('card_range', function($query){
            $query->on('sns_customer_auction.card_range_id', '=', 'card_range.id');
        })
        ->leftJoin('sns_payment', function($query){
            $query->on('item.id', '=', 'sns_payment.sns_item_id');
        })
        ->find($id);

        return $sold_item_detail;
    }

    /**
     * Pocket Balance Transaction Report
     * @param integer customer
     * @param date date
     * @param integer auction
     * @param integer auction_register_type
     */
    public function getPocketBalanceTransactionReportDetail($request){
        $report = DB::table('sns_customer_register')
        ->leftJoin(DB::raw('(SELECT * FROM sns_customer_auction WHERE id in (SELECT max(id) FROM sns_customer_auction group by sns_customer_auction.user_id)) AS customer_auction'), function($join) {
            $join->on('sns_customer_register.id', '=', 'customer_auction.user_id')
            ->whereNull('customer_auction.deleted_at');
        })
        ->join('pocket_balance', function($join){
            $join->on('sns_customer_register.id', '=', 'pocket_balance.customer_id')
            ->whereNull('pocket_balance.deleted_at');
        })
        ->join('pocket_balance_transaction', function($join){
            $join->on('pocket_balance.id', '=', 'pocket_balance_transaction.pocket_balance_id');
        })
        ->leftJoin('card_range', function($join){
            $join->on('card_range.id', '=', 'customer_auction.card_range_id');
        })
        ->select(
            'sns_customer_register.id as registration_no',
            'customer_auction.card_no',
            DB::raw('CONCAT(sns_customer_register.fname," ",sns_customer_register.lname) AS name'),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'sns_customer_register.email',
            DB::raw('CONCAT(sns_customer_register.address_1," ", sns_customer_register.address_2) AS address'),
            'sns_customer_register.nic',
            'pocket_balance_transaction.created_at as transaction_date',
            'pocket_balance_transaction.transaction_amount',
            'pocket_balance_transaction.transaction_type'
        );
        if(!empty($request->customer)){
            $report = $report->where(function($sql) use($request){
                $sql->where('sns_customer_register.id', $request->customer)
                    ->Orwhere('sns_customer_register.fname','LIKE','%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.lname', 'LIKE', '%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.contact_no', $request->customer)
                    ->Orwhere('sns_customer_register.nic', $request->customer);
                });
        }
        if(!empty($request->auction)){
            $report = $report->where('customer_auction.auction_id', $request->auction);
        }
        if(!empty($request->date)){
            $report = $report->whereDate('pocket_balance_transaction.created_at', '=', $request->date);
        }

        if(!empty($request->register_type)){
            $report = $report->where('card_range.card_type', $request->register_type);
        }
        $report = $report->orderBy('pocket_balance_transaction.created_at', 'DESC');
        $report = $report->paginate(unserialize(SHOW_OPTION)[1]);
        return $report;
    }

    /**
     * Current Pocket Balance Report
     * @param integer customer
     * @param integer auction_id
     * @param integer auction_register_type
     */
    public function getCurrentPocketBalanceReportDetail($request){
        $report = DB::table('sns_customer_register')
        ->leftJoin(DB::raw('(SELECT * FROM sns_customer_auction WHERE id in (SELECT max(id) FROM sns_customer_auction group by sns_customer_auction.user_id)) AS customer_auction'), function($join) {
            $join->on('sns_customer_register.id', '=', 'customer_auction.user_id')
            ->whereNull('customer_auction.deleted_at');
        })
        ->join('pocket_balance', function($join){
            $join->on('sns_customer_register.id', '=', 'pocket_balance.customer_id')
            ->whereNull('pocket_balance.deleted_at');
        })
        ->leftJoin('card_range', function($join){
            $join->on('card_range.id', '=', 'customer_auction.card_range_id')
            ->whereNull('card_range.deleted_at');
        })
        ->leftJoin('sns_auction', function($join){
            $join->on('customer_auction.auction_id', '=', 'sns_auction.id')
            ->whereNull('sns_auction.deleted_at');
        })
        ->select(
            'sns_customer_register.id as registration_no',
            'customer_auction.card_no',
            DB::raw('CONCAT(sns_customer_register.fname," ",sns_customer_register.lname) AS name'),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'sns_customer_register.email',
            DB::raw('CONCAT(sns_customer_register.address_1," ", sns_customer_register.address_2) AS address'),
            'sns_customer_register.nic',
            'sns_auction.event_name',
            'sns_customer_register.created_at as register_date',
            'pocket_balance.balance as pocket_balance'
        )->where('pocket_balance.balance', '>', 0);
        if(!empty($request->customer)){
            $report = $report->where(function($sql) use($request){
                $sql->where('sns_customer_register.id', $request->customer)
                    ->Orwhere('sns_customer_register.fname','LIKE','%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.lname', 'LIKE', '%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.contact_no', $request->customer)
                    ->Orwhere('sns_customer_register.nic', $request->customer);
                });
        }
        if(!empty($request->auction)){
            $report = $report->where('customer_auction.auction_id', $request->auction);
        }
        // if(!empty($request->date)){
        //     $report = $report->whereDate('pocket_balance_transaction.created_at', '=', $request->date);
        // }

        if(!empty($request->register_type)){
            $report = $report->where('card_range.card_type', $request->register_type);
        }
        $report = $report->orderBy('sns_customer_register.created_at', 'DESC');
        $report = $report->paginate(unserialize(SHOW_OPTION)[1]);
        return $report;
    }

    /**
     * Customer Level Management Report
     * @param integer customer
     * @param integer auction_id
     * @param integer auction_register_type
     * @param integer customer_type
     * @param date date
     */
    public function getCustomerLevelReportDetail($request){
        $report = DB::table('sns_customer_register')
        ->leftJoin(DB::raw('(SELECT * FROM sns_customer_auction WHERE id in (SELECT max(id) FROM sns_customer_auction group by sns_customer_auction.user_id)) AS customer_auction'), function($join) {
            $join->on('sns_customer_register.id', '=', 'customer_auction.user_id')
            ->whereNull('customer_auction.deleted_at');
        })
        ->leftJoin('users', function($join) {
            $join->on('sns_customer_register.user_id', '=', 'users.id');
        })
        ->leftJoin('card_range', function($join){
            $join->on('card_range.id', '=', 'customer_auction.card_range_id')
            ->whereNull('card_range.deleted_at');
        })
        ->leftJoin('sns_auction', function($join){
            $join->on('customer_auction.auction_id', '=', 'sns_auction.id')
            ->whereNull('sns_auction.deleted_at');
        })
        ->leftJoin('customer_allowed_type', function($join){
            $join->on('sns_customer_register.id', '=', 'customer_allowed_type.customer_id')
            ->whereNull('customer_allowed_type.deleted_at');
        })
        ->select(
            'sns_customer_register.id as registration_no',
            'users.last_login',
            'customer_auction.card_no',
            DB::raw('CONCAT(sns_customer_register.fname," ",sns_customer_register.lname) AS name'),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            'sns_customer_register.nic_image_path',
            'sns_customer_register.proof_image_path',
            'sns_customer_register.email',
            DB::raw('CONCAT(sns_customer_register.address_1," ", sns_customer_register.address_2) AS address'),
            'sns_customer_register.nic',
            'sns_customer_register.note',
            'sns_auction.event_name',
            'sns_customer_register.created_at as register_date',
            DB::raw('(select COUNT(id) from sns_customer_auction where user_id = sns_customer_register.id) as auction_count'),
            'customer_allowed_type.customer_type_id'
        );
        if(!empty($request->customer)){
            $report = $report->where(function($sql) use($request){
                $sql->where('sns_customer_register.id', $request->customer)
                    ->Orwhere('sns_customer_register.fname','LIKE','%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.lname', 'LIKE', '%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.contact_no', $request->customer)
                    ->Orwhere('sns_customer_register.telephone_no', $request->customer)
                    ->Orwhere('sns_customer_register.nic', $request->customer);
                });
        }
        if(!empty($request->auction)){
            $report = $report->where('customer_auction.auction_id', $request->auction);
        }
        if(!empty($request->register_type)){
            $report = $report->where('card_range.card_type', $request->register_type);
        }
        if(!empty($request->customer_types)){
            $report = $report->where('customer_allowed_type.customer_type_id', $request->customer_types);
        }
        if(!empty($request->date)){
            $report = $report->whereDate('sns_customer_register.created_at', '=', $request->date);
        }

        $report = $report->orderBy('sns_customer_register.created_at', 'DESC');
        $report = $report->paginate(unserialize(SHOW_OPTION)[1]);
        return $report;
    }

    /**
     * Invoice Summery Report
     * @param integer customer
     * @param integer auction_id
     * @param integer auction_register_type
     * @param integer customer_type
     * @param date date
     */

    public function getInvoiceSummeryReportDetail($request){
        $report = DB::table('sns_customer_auction')
        ->leftJoin('sns_auction', function($join){
            $join->on('sns_customer_auction.auction_id', '=', 'sns_auction.id')
            ->whereNull('sns_auction.deleted_at');
        })
        ->leftJoin('card_range', function($join){
            $join->on('sns_customer_auction.card_range_id', '=', 'card_range.id')
            ->whereNull('card_range.deleted_at');
        })
        ->leftJoin('sns_customer_register', function($join){
            $join->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id')
            ->whereNull('sns_customer_register.deleted_at');
        })
        ->leftJoin('customer_allowed_type', function($join){
            $join->on('sns_customer_register.id', '=', 'customer_allowed_type.customer_id')
            ->whereNull('customer_allowed_type.deleted_at');
        })
        ->leftJoin('customer_type', function($join){
            $join->on('customer_allowed_type.customer_type_id', '=', 'customer_type.id')
            ->whereNull('customer_type.deleted_at');
        })
        ->select(
            'sns_customer_auction.invoice_no',
            'sns_auction.event_name',
            'sns_customer_auction.card_no',
            'card_range.card_type',
            'customer_type.name as customer_level',
            'sns_customer_register.id as register_id',
            DB::raw('CONCAT(sns_customer_register.fname," ",sns_customer_register.lname) AS customer_name'),
            'sns_customer_register.contact_no',
            'sns_customer_register.telephone_no',
            DB::raw('(select SUM(win_amount) from sns_auction_winner where item_approve_id IN(select id FROM item_approve WHERE auction_id = sns_auction.id AND customer_id = sns_customer_register.id AND status = 1)) as invoice_amount'),
            DB::raw('(select SUM(amount) from sns_payment where sns_user_id = sns_customer_register.id AND sns_auction_id = sns_auction.id AND payment_status = 1 AND sns_item_id > 0) as paid_amount'),
            'sns_auction.advance_rate',
            'sns_auction.id as auction_id'
        )
        ->whereNull('sns_customer_auction.deleted_at')
        ->where(DB::raw('(select SUM(amount) from sns_payment where sns_user_id = sns_customer_register.id AND sns_auction_id = sns_auction.id AND payment_status = 1 AND sns_item_id > 0)'), '>', 0);
        
        if(!empty($request->customer)){
            $report = $report->where(function($sql) use($request){
                $sql->where('sns_customer_register.id', $request->customer)
                    ->Orwhere('sns_customer_register.fname','LIKE','%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.lname', 'LIKE', '%'.$request->customer.'%')
                    ->Orwhere('sns_customer_register.contact_no', $request->customer)
                    ->Orwhere('sns_customer_register.nic', $request->customer);
                });
        }
        if(!empty($request->auction)){
            $report = $report->where('sns_customer_auction.auction_id', $request->auction);
        }
        if(!empty($request->register_type)){
            $report = $report->where('card_range.card_type', $request->register_type);
        }
        if(!empty($request->date)){
            $report = $report->whereDate('sns_customer_register.created_at', '=', $request->date);
        }
        $report = $report->paginate(unserialize(SHOW_OPTION)[1]);
        return $report;
    }

    /**
     * Invoice Summery Detail
     * @param integer customer_id
     * @param integer auction_id
     */

    public function getInvoiceDetail($request){
        if($request){
            $invoiceReportDetail = CustomerAuctionManage::with(['approve_payment' => function($sql) use($request) {
                $sql->where('auction_id', $request->auction_id)
                    ->where('customer_id', $request->customer_id);
            }, 'approve_payment.detail.item.payment', 'approve_payment.winItem', 'customer', 'get_auction'])
            ->where('auction_id', $request->auction_id)
            ->where('user_id', $request->customer_id)
            ->first();
            return $invoiceReportDetail;
        }else{
            return [];
        }
    }
}

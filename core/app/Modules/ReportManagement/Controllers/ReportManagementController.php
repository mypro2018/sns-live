<?php namespace App\Modules\ReportManagement\Controllers;


/**
* ReportManagementController class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\ReportManagement\BusinessLogics\Logic as ReportLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic AS Auction;
use App\Modules\AuctionLiveManage\BusinessLogics\Logic AS AuctionLive;
use Illuminate\Support\Facades\App;
use Pusher;
use App\Modules\PaymentManage\BusinessLogics\Logic as Payment;
use App\Modules\CustomerManage\BusinessLogics\Logic as Customer;
use App\Classes\PdfTemplate;
use Illuminate\Http\Request;

class ReportManagementController extends Controller {

	protected $report;
	protected $payment;
	protected $live_auction;
	protected $auction;
	protected $customer;

    public function __construct(ReportLogic $report,Auction $auction, AuctionLive $live_auction, Payment $payment, Customer $customer){
        $this->report 		= $report;
        $this->auction 		= $auction;
		$this->live_auction = $live_auction;
		$this->payment 		= $payment;
		$this->customer 	= $customer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$auction_list   = $this->auction->getAllAuctions();
		$report_details = $this->report->getReportDetail();
		$total_sale     = $this->report->totalSale();
		return view("ReportManagement::index")->with([
			'report_detail' 	=> $report_details,
			'auction_list'  	=> $auction_list,
			'auction'       	=> '',
			'date'          	=> '',
			'card_no'			=> '',
			'total_sale'    	=> $total_sale,
			'registration_no'	=> '',
		]);
	}

	/**
	 * This function is used to filter report data
	 * @param date date
	 * @param integer auction
	 * @param integer card_no
	 */
	public function search(Request $request)
	{
		$auction_list   = $this->auction->getAllAuctions();
		$report_details = $this->report->searchReport($request);
		$total_sale     = $this->report->totalSale($request->get('auction'));
		return view("ReportManagement::index")->with([
			'report_detail' 	=> $report_details,
			'auction_list'  	=> $auction_list,
			'auction' 			=> $request->get('auction'),
			'date' 				=> $request->get('date'),
			'card_no'			=> $request->get('card_no'),
			'registration_no'	=> $request->get('registration_no'),
			'total_sale'   		=> $total_sale
		]);
	}


	//print sold item
	public function printSoldItem($id)
	{
		$sold_item = $this->report->getSoldItemDetail($id);
		
		if(!$sold_item){
			return redirect( 'report/list' )->with([
                'error' => true,
                'error.message' => 'Item not found',
                'error.title'   => 'Error..!'
            ]);
		}	

		// $cartDetails = $this->common->getCartDetails($records['order']->user_id, 'with_order', $id);

		return $page1 =  view('ReportManagement::template.print')->with(['records'=> $sold_item])->render();

		// $pdf   = new PdfTemplate(["no"=>date('Y-m-d')]);
        // $pdf->SetMargins(5, 0, 5);
        // $pdf->SetTitle("ITEM_".$sold_item->item_code.'_SOLD');
        // $pdf->SetFont('helvetica', 5);
        // $pdf->SetAutoPageBreak(TRUE, 30);
        // $pdf->AddPage();
        // $pdf->writeHtml($page1);
        // $pdf->output("ITEM_".$sold_item->item_code."_SOLD".".pdf", 'I');
	}

	/**
	 * This function is used to re-auction this item
	 * @param integer detail_id
	 * @param string comment
	 * @param integer product_id
	 * @return pusher notification
	 */
	public function reauctionItem(Request $request){
		try{
			if($request->ajax()){
				$reauction = $this->live_auction->reAuction($request);
				$pusher    = App::make('pusher');
				$pusher->trigger('auction-channel', 'item-reauction-event', $reauction);
			}else{
				throw new \Exception("Error occurred, Invalid User Input.");
			}
		}catch(\Exception $e){
			return $e->getMessage(); 
		}
	}

	/**
	 * This function is used to get sold item details
	 * @param integer sold_order_id
	 * @return array
	 */
	public function getSoldItemDetails(Request $request){
		if($request->ajax()){
			$sold_item = $this->report->getSoldItemDetail($request->get('sold_order_id'));
			return response()->json(['response' => $sold_item]);
		}else{
			return response()->json([]);
		}
	}


	/**
	 * This function is used to make payment for the item
	 * @param integer auction_id
	 * @param integer item_id
	 * @param integer customer_id
	 * @param float amount
	 * @param integer detail_id
	 * @param integer paid_status
	 */
	public function makePayment(Request $request){
		if($request->ajax()){
			try{
				//add payment
				$payment = $this->payment->add([
					'sns_auction_id'    => $request->auction_id,
					'amount'  			=> floatval(str_replace(',', '', $request->actual_pay)),
					'sns_user_id'    	=> $request->customer_id,
					'sns_item_id'    	=> $request->item_id,
					'payment_method'	=> CASH_PAYMENT
				]);
				if($payment){
					//adding balance amount to currently paid amount & update total amount as paid
					$paidAmount = $payment->amount;
					if($request->paid_status == ADVANCE_PAID){
						$balanceAmount = floatval(str_replace(',', '', $request->actual_pay));
						$totalAmount   = ($paidAmount + $balanceAmount);
					}
					$totalAmount = $paidAmount;
					//add payment transaction
					$payment_transaction = $this->payment->update($payment->id, [
						'payment_status'	=> PAYMENT_SUCCESS,
						'reference_no'		=> '0000'.$payment->id,
						'amount'			=> $totalAmount	
					],[
						'sns_payment_id'	=> $payment->id,
						'transaction_id'	=> TRANSACTION_ID,
						'transaction_time'	=> date('Y-m-d H:i:s'),
						'paid_amount'		=> floatval(str_replace(',', '', $request->actual_pay)),
						'error_code'		=> TRANSACTION_ERROR_CODE,
						'error_message'		=> TRANSACTION_ERROR_MESSAGE,
						'status'			=> TRANSACTION_STATUS
					]);
					if($payment_transaction){
						//update approved item as paid
						/**
						 * Function updateSoldStatus()
						 * @param integer auction_id
						 * @param integer customer_id
						 * @param integer detail_id
						 * @param integer paid_status
						 */
						$approved = $this->live_auction->updateSoldStatus($request->auction_id, $request->customer_id, $request->detail_id, $request->paid_status);
						return response()->json(['response' => $payment, 'response_message' => 'success']);
					}
				}
			}catch(\Exception $e){
				return response()->json(['response' => [], 'response_message' => $e]);
			}
		}else{
			return response()->json(['response' => [], 'response_message' => 'Request should be ajax.']);
		}
	}

	/**
	 * Pocket Balance Transaction Report
	 * @param string customer
	 * @param date date
	 * @param integer auction
	 * @param integer register_type
	 */
	public function getPocketBalanceTransactionReport(Request $request){
		try{
			$auction_list   = $this->auction->getAllAuctions();
			$report = $this->report->getPocketBalanceTransactionReportDetail($request);
			return view("ReportManagement::pocket-balance-transaction-report")
			->with(compact('report', 'auction_list'));
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * Current Pocket Balance Transaction Report
	 * @param string customer
	 * @param date date
	 * @param integer auction
	 * @param integer register_type
	 */
	public function getCurrentPocketBalanceReport(Request $request){
		try{
			$auction_list   = $this->auction->getAllAuctions();
			$report 		= $this->report->getCurrentPocketBalanceReportDetail($request);
			return view("ReportManagement::current-pocket-balance-report")
			->with(compact('report', 'auction_list'));
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * Customer Level Management Report
	 * @param string customer
	 * @param date date
	 * @param integer auction
	 * @param integer register_type
	 */
	public function getCustomerLevelReport(Request $request){
		try{
			$customer_type  = $this->customer->getCustomerTypes();
			$auction_list   = $this->auction->getAllAuctions();
			$report 		= $this->report->getCustomerLevelReportDetail($request);
			return view("ReportManagement::customer-level-management-report")
			->with(compact('report', 'auction_list', 'customer_type'));
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * Update customer allocated type in customer level management report
	 * @param integer customer_id
	 * @param integer customer_type
	 */
	public function updateCustomerAllocatedType(Request $request){
		try{
			if($request->ajax()){
				$customer_type  = $this->customer->updateCustomerAllocatedType($request);
				return response()->json(['response' => $customer_type]);
			}else{
				return response()->json(['response' => []]);
			}
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * Customer Invoice Summery Report
	 * @param string customer
	 * @param date date
	 * @param integer auction
	 * @param integer register_type
	 */
	public function getInvoiceSummeryReport(Request $request){
		try{
			$auction_list   = $this->auction->getAllAuctions();
			$report 		= $this->report->getInvoiceSummeryReportDetail($request);
			return view("ReportManagement::invoice-summery-report")
			->with(compact('report', 'auction_list'));
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}


	/**
	 * This function is used to get invoice details
	 * @param integer sold_order_id
	 * @return array
	 */
	public function getInvoiceDetails(Request $request, $customer_id, $auction_id){
		if(isset($customer_id) && isset($auction_id)){
			$request->customer_id = $customer_id;
			$request->auction_id  = $auction_id;
			$invoice_detail       = $this->report->getInvoiceDetail($request);
			return view("ReportManagement::invoice-detail")->with(compact('invoice_detail'));
		}else{
			return redirect('invoice-summery-report')->with([
                'error' 		=> true,
                'error.message' => 'Invoice details not found',
                'error.title'   => 'Error..!'
            ]);
		}
	}

	/**
	 * Print Invoice
	 */
	public function printInvoice(Request $request, $customer_id, $auction_id){
		if(isset($customer_id) && isset($auction_id)){
			$request->customer_id = $customer_id;
			$request->auction_id  = $auction_id;
			$records = $this->report->getInvoiceDetail($request);
			$page1   = view('ReportManagement::print')->with(compact('records'));
			$pdf     = new PdfTemplate(["no"=>date('Y-m-d')]);
			$pdf->SetMargins(5, 0, 5);
			$pdf->SetTitle("INVOICE_NO_".date('Y_m_d').'_'.$customer_id.'_'.$auction_id);
			$pdf->SetFont('helvetica', 5);
			$pdf->SetAutoPageBreak(TRUE, 30);
			$pdf->AddPage();
			$pdf->writeHtml($page1);
			$pdf->output("INVOICE_NO_".$customer_id.'_'.date('Y_m_d').".pdf", 'I');
		}else{
			return redirect('invoice-summery-report')->with([
                'error' 		=> true,
                'error.message' => 'Parameters not found',
                'error.title'   => 'Error..!'
            ]);
		}
	}




	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

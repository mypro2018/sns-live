<?php
Route::group(['middleware' => ['auth']], function(){
	Route::group(array('prefix'=>'report','namespace' => 'App\Modules\ReportManagement\Controllers'), function() {
		/**
	     * GET Routes
	     */
	    Route::get('list', [
	        'as' => 'report.list', 'uses' => 'ReportManagementController@index'
		]);
		
		Route::get('search', [
	        'as' => 'report.search', 'uses' => 'ReportManagementController@search'
		]);

		Route::get('print/{id}', [
	        'as' => 'report.print', 'uses' => 'ReportManagementController@printSoldItem'
		]);
		
		Route::get('auction-item/reauction', [
	        'as' => 'report.item.reauction', 'uses' => 'ReportManagementController@reauctionItem'
		]);

		Route::get('auction-item/payment', [
	        'as' => 'report.item.payment', 'uses' => 'ReportManagementController@getSoldItemDetails'
		]);

		Route::get('make-payment', [
	        'as' => 'report.item.payment', 'uses' => 'ReportManagementController@makePayment'
		]);

		Route::get('pocket-balance-transaction-report', [
	        'as' => 'report.pocket-balance-transaction-report', 'uses' => 'ReportManagementController@getPocketBalanceTransactionReport'
		]);

		Route::get('current-pocket-balance-report', [
	        'as' => 'report.current-pocket-balance-report', 'uses' => 'ReportManagementController@getCurrentPocketBalanceReport'
		]);

		Route::get('customer-level-report', [
	        'as' => 'customer-level-report', 'uses' => 'ReportManagementController@getCustomerLevelReport'
		]);

		Route::get('update-customer-type', [
	        'as' => 'customer-level-report', 'uses' => 'ReportManagementController@updateCustomerAllocatedType'
		]);

		Route::get('invoice-summery-report', [
	        'as' => 'invoice-summery-report', 'uses' => 'ReportManagementController@getInvoiceSummeryReport'
		]);

		Route::get('invoice-detail/customer/{customer_id}/auction/{auction_id}', [
	        'as' => 'invoice-detail', 'uses' => 'ReportManagementController@getInvoiceDetails'
		]);

		Route::get('print-invoice/customer/{customer_id}/auction/{auction_id}', [
	        'as' => 'print-invoice', 'uses' => 'ReportManagementController@printInvoice'
		]);

		
	});
}); 
@extends('layouts.back_master') @section('title','Report Management')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.pay-button{
    width:100px;
}
.pay-input{
    width: 100px;
}
.action-btn{
    width: 120px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
    Invoice Details
	<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{{url('report/invoice-summery-report')}}}">Invoice Summery Report</a></li>
		<li class="active">Invoice Details</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                @if($invoice_detail && $invoice_detail->get_auction)
                    {{$invoice_detail->get_auction->event_name}} - <span class="label label-info">{{$invoice_detail->card_no}}</span>
                @endif
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center">Item Code</th>
                                    <th width="10%" class="text-center">Sold Amount</th>
                                    <th width="10%" class="text-center">Paid Amount</th>
                                    <th width="10%" class="text-center">Balance Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($invoice_detail && sizeof($invoice_detail->approve_payment) > 0)
                                    @foreach($invoice_detail->approve_payment as $result_val)
                                        <?php $balance = $result_val->winItem?$result_val->winItem->win_amount:0 ?>
                                        @if($result_val->detail && $result_val->detail->item && sizeof($result_val->detail->item->payment) > 0 && $balance > 0)
                                            <?php $balance -= $result_val->detail->item->payment[0]?$result_val->detail->item->payment[0]->amount:0 ?>
                                        @endif
                                        @include('ReportManagement::template.detail')
                                    @endforeach
                                @else
                                    <tr><td colspan="7" class="text-center">No data found.</td></tr>
                                @endif
                            </tbody>
                        </table> 
                    </div>       
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
<script type="text/javascript">
</script>
@stop

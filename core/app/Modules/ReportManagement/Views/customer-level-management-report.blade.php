@extends('layouts.back_master') @section('title','Report Management')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.pay-button{
    width:100px;
}
.img-circle{
    width:100px;
    height:100px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
    Customer Level Management
	<small> Report</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Customer Level Management Report</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('report/customer-level-report')}}">
            <div class="box-body">
                <div class="form-group" style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Search Customer</label>
                                <input type="text" class="form-control input-sm" name="customer" placeholder="Search Customer" value="{{Request::get('customer')}}">        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Auction</label>
                                <select class=" form-control chosen input-sm" name="auction">
                                    <option value="">-- Search All Auctions --</option>
                                    @if($auction_list)
                                        @foreach($auction_list as $key => $val)
                                        <option value="{{$val->id}}" @if($val->id == Request::get('auction')) selected @endif>{{$val->event_name}} - {{$val->auction_date}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Register Type</label>
                                <select class=" form-control chosen input-sm" name="register_type">
                                    <option value="">-- Search Register Type --</option>
                                    <option value="{{ONLINE_CARD}}" @if(Request::get('register_type') == ONLINE_CARD) selected @endif>Online</option>
                                    <option value="{{FLOOR_CARD}}"  @if(Request::get('register_type') == FLOOR_CARD) selected @endif>Floor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Auction</label>
                                <select class=" form-control chosen input-sm" name="customer_types">
                                    <option value="">-- Search Customer Types --</option>
                                    @if($customer_type)
                                        @foreach($customer_type as $key => $val)
                                        <option value="{{$val->id}}" @if($val->id == Request::get('customer_types')) selected @endif>{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="YYYY-MM-DD" name="date" placeholder="Date" value="{{Request::get('date')}}" dt>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
                    <a href="customer-level-report" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th width="5%" class="text-center">Registration No</th>
                                    <th width="8%" class="text-center">Session Login Date/Time</th>
                                    <th width="5%" class="text-center">Card No</th>
                                    <th width="15%" class="text-center">Customer Name</th>
                                    <th width="8%" class="text-center">Contact No</th>
                                    <th width="8%" class="text-center">Telephone No</th>
                                    <th width="8%" class="text-center">Customer ID Picture</th>
                                    <th width="8%" class="text-center">Customer Proof of Address Picture</th>
                                    <th width="5%" class="text-center">E-mail</th>
                                    <th width="10%" class="text-center">Address</th>
                                    <th width="8%" class="text-center">NIC</th>
                                    <th width="10%" class="text-center">Note</th>
                                    <th width="8%" class="text-center">Latest Auction</th>
                                    <th width="8%" class="text-center">Register Date</th>
                                    <th width="8%" class="text-center">All Auction Count</th>
                                    @if($customer_type)
                                        @foreach($customer_type as $key => $val)
                                            <th width="8%" class="text-center">{{ $val->name }} Customer</th>
                                        @endforeach
                                    @endif
                                    <th width="8%" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $i = ($report->currentpage()-1) * $report->perpage() + 1; 
                                $userArray = [];
                            ?>
                            @if(sizeof($report) > 0)
                                @foreach($report as $result_val)
                                    @include('ReportManagement::template.customer-level')
                                <?php $i++;?>
                                @endforeach
                            @else
                                <tr><td colspan="20" class="text-center">No data found.</td></tr>
                            @endif
                            </tbody>
                        </table> 
                    </div>       
                    @if(count($report) > 0)
                    <div class="box-footer">      
                        <div style="float: right;">{!! $report->appends($_GET)->render() !!}</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $(function () {
        $('.datetimepicker').datetimepicker();
    });
    $(document).on('click', '.update-types', function(){
       var checked_type = $(this).closest('tr').find('input[type="radio"]:checked').val();
       var customer_id  = $(this).closest('tr').attr('id');
       $.ajax({
          url   : "{{URL::to('report/update-customer-type')}}",
          method: 'GET',
          data  : {
              'customer_id'   : customer_id,
              'customer_type' : checked_type
          },
          async : false,
          success: function (data) {
            if(data && data.response){
                toastr.success('Customer types allocated sucessfully');
            }else{
                toastr.error('Error..Occurred');
            }
          }
          ,error: function () {
            toastr.error('Error..Occurred');
          }
        });
    });
});

</script>
@stop
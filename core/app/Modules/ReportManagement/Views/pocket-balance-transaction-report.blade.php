@extends('layouts.back_master') @section('title','Report Management')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.pay-button{
    width:100px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
    Pocket Balance Transaction 
	<small> Report</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Pocket Balance Transaction Report</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('report/pocket-balance-transaction-report')}}">
            <div class="box-body">
                <div class="form-group" style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Search Customer</label>
                                <input type="text" class="form-control input-sm" name="customer" placeholder="Search Customer" value="{{Request::get('customer')}}">        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Auction</label>
                                <select class=" form-control chosen input-sm" name="auction">
                                    <option value="">-- Search All Auctions --</option>
                                    @if($auction_list)
                                        @foreach($auction_list as $key => $val)
                                        <option value="{{$val->id}}" @if($val->id == Request::get('auction')) selected @endif>{{$val->event_name}} - {{$val->auction_date}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="YYYY-MM-DD" name="date" placeholder="Date" value="{{Request::get('date')}}" dt>        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Register Type</label>
                                <select class=" form-control chosen input-sm" name="register_type">
                                    <option value="">-- Search Register Type --</option>
                                    <option value="{{ONLINE_CARD}}" @if(Request::get('register_type') == ONLINE_CARD) selected @endif>Online</option>
                                    <option value="{{FLOOR_CARD}}"  @if(Request::get('register_type') == FLOOR_CARD) selected @endif>Floor</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
                    <a href="pocket-balance-transaction-report" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th width="5%" class="text-center">Registration No</th>
                                    <th width="5%" class="text-center">Card No</th>
                                    <th width="15%" class="text-center">Customer Name</th>
                                    <th width="8%" class="text-center">Contact No</th>
                                    <th width="8%" class="text-center">Telephone No</th>
                                    <th width="5%" class="text-center">E-mail</th>
                                    <th width="10%" class="text-center">Address</th>
                                    <th width="8%" class="text-center">NIC</th>
                                    <th width="10%" class="text-center">Trancection Date</th>
                                    <th width="8%" class="text-center">Deposit</th>
                                    <th width="8%" class="text-center">Withdrawal</th>
                                    <th width="8%" class="text-center">Available Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $i = ($report->currentpage()-1) * $report->perpage() + 1; 
                                $userArray = [];
                                $amount    = 0;
                            ?>
                            @if(sizeof($report) > 0)
                                @foreach($report as $result_val)
                                    <?php 
                                        if (array_key_exists($result_val->registration_no, $userArray)){
                                            if($result_val->transaction_type > 0){
                                                $userArray[$result_val->registration_no] = ($userArray[$result_val->registration_no] + $result_val->transaction_amount);  
                                            }else{
                                                $userArray[$result_val->registration_no] = ($userArray[$result_val->registration_no] - $result_val->transaction_amount);
                                            }
                                        }else{
                                            $userArray[$result_val->registration_no] = $result_val->transaction_amount;
                                        }
                                        $amount += $result_val->transaction_amount;
                                    ?>
                                    @include('ReportManagement::template.pocket-transaction')
                                <?php $i++;?>
                                @endforeach
                            @else
                                <tr><td colspan="12" class="text-center">No data found.</td></tr>
                            @endif
                            </tbody>
                        </table> 
                    </div>       
                    @if(count($report) > 0)
                    <div class="box-footer">
                        <p class="pull-right"><strong> Pocket Balance Amount : <span style="border-bottom: 4px double #333;">Rs {{number_format($amount, 2)}}</span></strong></p>     
                        <div style="float: right;">{!! $report->appends($_GET)->render() !!}</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
<script type="text/javascript">
$(function () {
    $('.datetimepicker').datetimepicker();
});
</script>
@stop

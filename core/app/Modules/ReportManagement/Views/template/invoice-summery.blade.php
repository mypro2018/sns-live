<tr>
     <td class="text-center">{{$i}}</td>
     <td class="text-center">{{$result_val->invoice_no?:'INV000'}}</td>
     <td class="text-center">{{$result_val->event_name?:'-'}}</td>
     <td class="text-center">{{$result_val->card_no?:'-'}}</td>
     @if($result_val->card_type == ONLINE_CARD)
          <td class="text-center">Online</td>
     @else
          <td class="text-center">Floor</td>
     @endif
     <td class="text-center">{{$result_val->customer_level?:'-'}} Customer</td>
     <td class="text-center" >{{$result_val->register_id?:'-'}}</td>
     <td class="text-center" >{{$result_val->customer_name?:'-'}}</td>
     <td class="text-center" >{{$result_val->contact_no?:''}}</td>
     <td class="text-center" >{{$result_val->telephone_no?:''}}</td>
     @if($result_val->invoice_amount != null)
          <td class="text-center" >{{number_format($result_val->invoice_amount, 2)}}</td>
     @else
          <td class="text-center" >{{number_format(0, 2)}}</td>
     @endif
     @if($result_val->card_type == ONLINE_CARD)
          <td class="text-center" >{{number_format($result_val->paid_amount, 2)}}</td>
     @else
          <td class="text-center" >-</td>
     @endif
     <td class="text-center" >{{number_format($balance, 2)}}</td>
     <td class="text-center">
          <div class="btn-group action-btn" role="group" aria-label="report-action">
               <a class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Print" href="{{url('report/print-invoice')}}/customer/{{$result_val->register_id}}/auction/{{$result_val->auction_id}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
               <a class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View Details" href="{{url('report/invoice-detail')}}/customer/{{$result_val->register_id}}/auction/{{$result_val->auction_id}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
          </div>
     </td>
</tr>
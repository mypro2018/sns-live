<tr>
     <td class="text-center">{{$result_val->detail->item->item_code}}</td>
     <td class="text-center">{{number_format($result_val->winItem?$result_val->winItem->win_amount:0, 2)}}</td>
     @if($result_val->detail && $result_val->detail->item && sizeof($result_val->detail->item->payment) > 0)
          <td class="text-center">{{number_format($result_val->detail->item->payment[0]->amount, 2)}}
     @else
          <td class="text-center">{{number_format(0, 2)}}</td>
     @endif
     <td class="text-center">{{number_format($balance, 2)}}</td>
     </td>
</tr>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
    .price{
        text-align: center;
    }
</style>
<body>
<img src="{{url('assets/images/logo/logo.png')}}">
<!--  -->
<div>
    <table>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Date</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td style="width: 30%;">
                @if($records)
                    {{$records->auction_date}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auction Name</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td style="width: 50%;">
            @if($records)
                {{$records->event_name}}
            @else
                {{ '-' }} 
            @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Item Lot No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->item_code}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Item Description</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->name}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Purchaser Bidding No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->card_no}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Purchaser Name</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->customer_name}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Purchaser Id No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->register_id}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Purchaser Contact No</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    {{$records->contact_no}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Sold Price</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    Rs {{ number_format($records->win_amount, 2) }}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Advanced Price (25%)</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
                @if($records)
                    <?php
                        $value  = (($records->win_amount * ADVANCE_RATE) / 100);
                    ?>
                    Rs {{ number_format($value, 2) }} 
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>ADV Paid</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr><td colspan="3"></td></tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Signature</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Auctioneer</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Company</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:20%;">
                <strong>Customer</strong>
            </td>
            <td style="width: 2%"> : </td>
            <td>
            </td>
        </tr>    
</table>
</body>
</html>
<!-- <img style="bottom:0;" src="{{url('assets/images/invoice-footer.jpg')}}"> -->
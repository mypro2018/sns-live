<tr id="{{$result_val->registration_no}}">
     <td class="text-center">{{$i}}</td>
     <td class="text-center">{{$result_val->registration_no?:'-'}}</td>
     <td class="text-center">{{$result_val->last_login?:'-'}}</td>
     <td class="text-center">{{$result_val->card_no?:'-'}}</td>
     <td class="text-center">{{$result_val->name?:'-'}}</td>
     <td class="text-center">{{$result_val->contact_no?:'-'}}</td>
     <td class="text-center">{{$result_val->telephone_no?:'-'}}</td>
     <td class="text-center">
     @if(!empty($result_val->nic_image_path))
          <img src="{{ url('core/storage/uploads/images/customer/nic-licence/'.$result_val->nic_image_path) }}" class="img-circle" alt="Showing Image"/>
     @else
          <img class="img-circle" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
     @endif
     </td>
     <td class="text-center">
     @if(!empty($result_val->proof_image_path))
          <img src="{{ url('core/storage/uploads/images/customer/proof/'.$result_val->proof_image_path) }}" class="img-circle" alt="Showing Image"/>
     @else
          <img class="img-circle" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
     @endif
     </td>
     <td class="text-center" >{{$result_val->email?:'-'}}</td>
     <td class="text-center" >{{$result_val->address?:'-'}}</td>
     <td class="text-center" >{{$result_val->nic?:'-'}}</td>
     <td class="text-center" >{{$result_val->note?:'-'}}</td>
     <td class="text-center" >{{$result_val->event_name?:'-'}}</td>
     <td class="text-center" >{{$result_val->register_date?:'-'}}</td>
     <td class="text-center" >{{$result_val->auction_count?:'-'}}</td>
     @foreach($customer_type as $key => $val)
          <td width="8%" class="text-center"><input type="radio" name="customer_type_{{$result_val->registration_no}}" value="{{$val->id}}" @if($val->id == $result_val->customer_type_id) checked @endif></td>
     @endforeach
     <td class="text-center">
          <a class="btn btn-sm btn-default update-types" data-toggle="tooltip" data-placement="top" title="Edit Customer Types" href="javascript:void(0);"><i class="fa fa-pencil" aria-hidden="true"></i></a>
     </td>
</tr>
<tr>
     <td class="text-center">{{$i}}</td>
     <td class="text-center">{{$result_val->registration_no?:'-'}}</td>
     <td class="text-center">{{$result_val->card_no?:'-'}}</td>
     <td class="text-center">{{$result_val->name?:'-'}}</td>
     <td class="text-center" >{{$result_val->contact_no?:'-'}}</td>
     <td class="text-center" >{{$result_val->telephone_no?:'-'}}</td>
     <td class="text-center" >{{$result_val->email?:'-'}}</td>
     <td class="text-center" >{{$result_val->address?:'-'}}</td>
     <td class="text-center" >{{$result_val->nic?:'-'}}</td>
     <td class="text-center" >{{$result_val->transaction_date?:'-'}}</td>
     @if($result_val->transaction_type == ONLINE_CARD)
          <td class="text-center">{{number_format($result_val->transaction_amount, 2)?:'-'}}</td>
     @else
          <td class="text-center"></td>
     @endif
     @if($result_val->transaction_type == 0)
          <td class="text-center">{{number_format($result_val->transaction_amount, 2)?:'-'}}</td>
     @else
          <td class="text-center"></td>
     @endif
     <td class="text-center" >{{number_format($userArray[$result_val->registration_no], 2)}}</td>
</tr>


<?php $amount = 0;?>
<tr id="{{$result_val->id}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val->event_name?:'-'}}</td>
     <td>{{$result_val->item_code?:'-'}}</td>
     <td>{{$result_val->name?:'-'}}</td>
     <td class="text-center">
     @if($result_val->card_type == ONLINE_CARD)
          <span class="label label-info">{{$result_val->card_no?:'-'}}</span></td>
     @else
          <span class="label label-success">{{$result_val->card_no?:'-'}}</span></td>
     @endif
     <td class="text-center">{{$result_val->register_id?:'-'}}</td>
     <td class="text-center">{{$result_val->customer_name?:'-'}}</td>
     <td class="text-center">{{$result_val->contact_no?:'-'}}</td>
     <td class="text-center">{{$result_val->telephone_no?:'-'}}</td>
     <td>Rs {{number_format($result_val->win_amount,2)}}</td>
     <td>{{$result_val->created_at}}</td>
     <td class="text-center">
          @if($result_val->item_approve_sold_status == PAYMENT_SUCCESS && $result_val->advance_status == FULL_PAID)
               <span class="label label-success">Paid</span>
          @elseif($result_val->advance_status == ADVANCE_PAID)
               <span class="label label-info">Advance Paid</span>
          @else
               <span class="label label-warning">Pending</span>
          @endif
     </td>
     <td class="text-center">
          <div class="btn-group" role="group" aria-label="report-action">
               <a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="popup" href="{{url('report/print')}}/{{$result_val->sold_order_id}}" target="popup" onclick="window.open('{{url('report/print')}}/{{$result_val->sold_order_id}}','popup','width=500,height=400'); return false;"><i class="fa fa-print" aria-hidden="true"></i></a>
               @if($result_val->item_approve_sold_status == PAYMENT_SUCCESS && $result_val->advance_status == FULL_PAID)
                    <a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Paid" href="javascript:void(0);" disabled><i class="fa fa-usd" aria-hidden="true"></i></a>
               @else
                    <a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Pay & Reauction" href="javascript:void(0);" onclick="makePayment({{$result_val->detail_id}}, {{$result_val->id}}, {{$result_val->sold_order_id}})"><i class="fa fa-usd" aria-hidden="true"></i></a>
               @endif
               {{-- <a class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View Invoice" href="javascript:void(0);" disabled><i class="fa fa-eye" aria-hidden="true"></i></a> --}}
          </div>
     </td>
</tr>


<tr>
     <td class="text-center">{{$i}}</td>
     <td class="text-center">{{$result_val->registration_no?:'-'}}</td>
     <td class="text-center">{{$result_val->card_no?:'-'}}</td>
     <td class="text-center">{{$result_val->name?:'-'}}</td>
     <td class="text-center" >{{$result_val->contact_no?:'-'}}</td>
     <td class="text-center" >{{$result_val->telephone_no?:'-'}}</td>
     <td class="text-center" >{{$result_val->email?:'-'}}</td>
     <td class="text-center" >{{$result_val->address?:'-'}}</td>
     <td class="text-center" >{{$result_val->nic?:'-'}}</td>
     <td class="text-center" >{{$result_val->event_name?:'-'}}</td>
     <td class="text-center" >{{$result_val->register_date?:'-'}}</td>
     <td class="text-center" >{{number_format($result_val->pocket_balance, 2)}}</td>
</tr>
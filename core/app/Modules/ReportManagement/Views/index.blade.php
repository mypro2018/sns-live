@extends('layouts.back_master') @section('title','Report Management')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.pay-button{
    width:100px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Report
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Report Management</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('report/search')}}">
            <div class="box-body">
                <div class="form-group" style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Auction Date</label>
                                <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="YYYY-MM-DD" name="date" placeholder="Auction Date" value="{{$date}}" dt>        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Auction</label>
                                <select class=" form-control chosen input-sm" name="auction">
                                    <option value="">-- Search All Auctions --</option>
                                    @if($auction_list)
                                        @foreach($auction_list as $key => $val)
                                        <option value="{{$val->id}}" @if($val->id == $auction) selected @endif>{{$val->event_name}} - {{$val->auction_date}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Card No</label>
                                <input type="text" class="form-control input-sm" name="card_no" placeholder="Card No" value="{{$card_no}}">        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Registration No</label>
                                <input type="text" class="form-control input-sm" name="registration_no" placeholder="Registration No" value="{{$registration_no}}">        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
                    <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Total Sale : Rs {{ number_format($total_sale, 2) }}</h4>
                    <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="12%">Auction</th>
                                <th width="12%">Item Code</th>
                                <th width="10%">Description</th>
                                <th width="5%">Card No</th>
                                <th width="5%">Register ID</th>
                                <th width="10%">Customer Name</th>
                                <th width="5%">Contact No</th>
                                <th width="5%">Telephone No</th>
                                <th width="12%">Amount</th>
                                <th width="10%">Date</th>
                                <th width="5%">Status</th>
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = ($report_detail->currentpage()-1) * $report_detail->perpage() + 1; ?>
                        @if(count($report_detail) > 0)
                            @foreach($report_detail as $result_val)
                                @include('ReportManagement::template.report')
                            <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="11" class="text-center">No data found.</td></tr>
                        @endif
                        </tbody>
                    </table>        
                    </div>
                    @if(count($report_detail) > 0 && count($report_detail) >= 10)
                    <div class="box-footer">      
                        <div style="float: right;">{!! $report_detail->appends($_GET)->render() !!}</div>
                    </div>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('js')
<!-- accounting js -->
<script src="{{asset('assets/dist/accounting/accounting.min.js')}}"></script>
<!-- accounting js -->
<script type="text/javascript">
    $(function () {
        $('.datetimepicker').datetimepicker();
    });
    $(document).on('keyup','.actual-pay', function(event){
        if (event.which >= 37 && event.which <= 40) return;
        $(this).val((i,v) => formatCurrency(v));
    });
    
    /*
    * input filed with thousand seperator
    */
    
    /** make payment for approved item in the auction
        param : integer detail_id
        param : integer product_id
        param : string comment default null
    */
    function makePayment(detail_id, product_id, sold_id){
        // confirmation
        $.confirm({
            title   : 'Payment & Reauction Confirmation',
            content : 'Please confirm make payment for this item or reauction this item.',
            icon: 'fa fa-question-circle',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            columnClass: 'medium',
            buttons: {
                'confirm': {
                    text: 'Pay',
                    btnClass: 'btn-green pay-button',
                    action: function(){
                       paymentPanel(sold_id);
                    }
                },
                moreButtons: {
                    text: 'Reauction',
                    btnClass: 'btn-red',
                    action: function(){
                        $.ajax({
                            url    : "{{URL::to('report/auction-item/reauction')}}",
                            method : 'GET',
                            data   : {
                                'detail_id'   : detail_id,
                                'comment'     : 'Reauction Item',
                                'product_id'  : product_id
                            },
                            async: false,
                            success: function (data) {
                                document.getElementById(product_id).remove();
                                toastr.success('Item re-auctioned successfully');
                            },error: function () {
                                toastr.error('Error Occured !..Try Again');
                            }
                        });
                    }
                },
                close: function(){
                    
                }
            }
        });
    }

    /** create payment panel
     * param : integer sold_id
     */
    function paymentPanel(sold_id){
        var item_code      = null;
        var auction        = null;
        var win_amount     = null;
        var advance_rate   = 0
        var item           = null;
        var advance_amount = null;
        var balance_amount = null;
        var auction_id     = null;
        var item_id        = null;
        var customer_id    = null;
        var detail_id      = null;
        var paid_status    = null;
        var pay_amount     = null;
        var paid_amount    = 0;
        var advance_have   = null;
        $.ajax({
            url    : "{{URL::to('report/auction-item/payment')}}",
            method : 'GET',
            data   : {
                'sold_order_id'   : sold_id
            },
            async: false,
            success: function (data) {
                console.log(data.response);
                if(data && data.response){
                    auction        = data.response.event_name;
                    item_code      = data.response.item_code;
                    item           = data.response.name;
                    win_amount     = data.response.win_amount;
                    advance_rate   = data.response.advance_rate;
                    advance_amount = ((win_amount * advance_rate) / 100);
                    auction_id     = data.response.auction_id;
                    item_id        = data.response.id;
                    customer_id    = data.response.register_id;
                    detail_id      = data.response.detail_id;
                    paid_status    = data.response.advance_status;
                    paid_amount    = data.response.amount;
                    if(data.response.amount > 0){
                        balance_amount = (win_amount - paid_amount);
                    }else{
                        balance_amount = (win_amount - advance_amount);
                    }
                }
            },error: function () {
                toastr.error('Error Occured !..Try Again');
            }
        });

        //check is there any pending payment
        if(advance_rate == 0){
            advance_have = 'disabled';
        }
        if(paid_status == '{{FULL_PAID}}'){
            pay_amount     = win_amount;
            balance_amount = 0;
            var payment_section = `<tr>
                <td colspan="3" class="text-center">
                    <label class="radio-inline">
                        <input type="radio" name="payemnt_option" id="inlineRadio1" value="{{ FULL_PAID }}" checked> Full Payment (`+accounting.formatMoney(win_amount, "Rs")+`)
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="payemnt_option" id="inlineRadio2" value="{{ ADVANCE_PAID }}" `+advance_have+`> Advance Payment (`+accounting.formatMoney(advance_amount, "Rs")+`)
                    </label>
                </td>
            </tr>`;
        }else{
            var payment_section = `<tr>
                <td colspan="3" class="text-center">Paid 
                    <span class="balance-payment">`+accounting.formatMoney(paid_amount, "Rs")+`</span>
                </td>
            </tr>`;
            paid_status = '{{ FULL_PAID }}';
            pay_amount  = balance_amount;
        }

        var payment_panel = `<table class="table">
            <tr>
                <td colspan="3" class="text-center auction-name">`+auction+`</td>
            </tr>
            <tr>
                <td colspan="3" class="text-center"><h3 class="item-code">`+item_code+`</h3></td>
            </tr>
            <tr>
                <td colspan="3" class="text-center item-name">`+item+`</td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
                    <h3>`+accounting.formatMoney(win_amount, "Rs")+`</h3>
                </td>
            </tr>
            `+payment_section+`
            <tr>
                <td colspan="3" class="text-center">Balance Payment 
                    <span class="balance-payment"><h4 class="balance-payment-tag">`+accounting.formatMoney(balance_amount, "Rs")+`<h4></span>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">Actual Pay 
                    <center><input type="text" name="actual_pay" id="actual_pay" class="form-control input-sm actual-pay" value="`+formatCurrency(pay_amount)+`" width="20" style="width:50%"/></center>
                </td>
            </tr>
        </table>`;
        var payAmount = win_amount;
        $.confirm({
            title   : 'Payment Portal',
            content : payment_panel,
            icon    : '',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            columnClass: 'medium',
            onContentReady: function () {
                var self = this;
                self.$content.find('input[type=radio][name=payemnt_option]').click(function() {
                    if($(this).is(':checked')){
                        if($(this).val() == '{{ FULL_PAID }}'){
                            paid_status     = '{{ FULL_PAID }}';
                            pay_amount      = win_amount;
                            self.$content.find('.balance-payment-tag').html('Rs.0.00');
                            self.$content.find('.actual-pay').val(formatCurrency(win_amount));
                            payAmount       = self.$content.find('.actual-pay').val();
                        }else{
                            paid_status    = '{{ ADVANCE_PAID }}';
                            pay_amount     = advance_amount;
                            balance_amount = (win_amount - advance_amount);
                            self.$content.find('.balance-payment-tag').html(accounting.formatMoney(balance_amount, "Rs"));
                            self.$content.find('.actual-pay').val(formatCurrency(advance_amount));
                            payAmount      = self.$content.find('.actual-pay').val();
                        }
                    }
                });
            },
            buttons: {
                'confirm': {
                    text: 'Pay',
                    btnClass: 'btn-green pay-button',
                    action: function(){
                        payAmount      = $(document).find('.actual-pay').val();
                        $.ajax({
                            url    : "{{URL::to('report/make-payment')}}",
                            method : 'GET',
                            data   : {
                                'auction_id'  :auction_id,
                                'item_id'     :item_id,
                                'amount'      :pay_amount,
                                'customer_id' :customer_id,
                                'detail_id'   :detail_id,
                                'paid_status' :paid_status,
                                'actual_pay'  :payAmount
                            },
                            async: false,
                            success: function (data) {
                                toastr.success('Payment Success!');
                                location.reload();
                            },error: function () {
                                toastr.error('Error Occured !..Try Again');
                            }
                        });
                    }
                },
                close: function(){
                    
                }
            }
        });
    }

    const formatCurrency = (str) => (""+str).replace(/[^\d.]/g, "").replace(/^(\d*\.)(.*)\.(.*)$/, '$1$2$3').replace(/\.(\d{2})\d+/, '.$1').replace(/\B(?=(\d{3})+(?!\d))/g, ",");

</script>
@stop

<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
    .price{
        text-align: center;
    }
</style>
<body>
<!-- <img src="{{url('assets/images/invoice-header.jpg')}}"> -->
<div style="font-size: 8px;text-align: left;"></div>
<br/><br/><br/><br/>
<div>
    <table>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;">
                @if($records && $records->customer)
                    {{$records->customer->fname}} {{$records->customer->lname}}
                @else
                    {{'-'}}
                @endif
            </td>
            <td style="width: 20%;">
            </td>
            <td style="text-align:right;width:40%;line-height:2;">
                {{date('d-M-Y')}}
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;">
                @if($records && $records->customer)
                    {{$records->customer->address_1}}
                @else
                    {{'-'}}
                @endif
            </td>
            <td style="width: 20%"></td>
            <td style="text-align:right;width:40%;line-height:2;">
                @if($records)
                    {{$records->invoice_no}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;">
                @if($records && $records->customer)
                   {{$records->customer->address_2}}
                @else
                    {{'-'}}
                @endif
            </td>
            <td style="width: 20%;line-height:2;font-family:ui-sans-serif;font-size:12px;">INVOICE</td>
            <td style="text-align:right;width:40%;line-height:2;"></td>
        </tr>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;">
                @if($records && $records->customer)
                    {{$records->customer->city}}
                @else
                    {{'-'}}
                @endif
            </td>
            <td style="width:20%;text-align:center;line-height:2;"></td>
            <td style="text-align:right;width:40%;line-height:2;">
                @if($records)
                    {{$records->auction_id}} (Auct-No)
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;">
                @if($records && $records->customer)
                    {{$records->customer->contact_no}} / {{$records->customer->telephone_no}}
                @else
                    {{'-'}}
                @endif
            </td>
            <td style="width: 20%"></td>
            <td style="text-align:right;width:40%;">
                PAYMENT TERM : {{'CASH'}}
            </td>
        </tr>
        <tr>
            <td style="text-align:left;width:40%;line-height:2;"></td>
            <td style="width: 20%"></td>
            <td style="text-align:right;width:40%;line-height:2;">
                @if($records)
                    CARD NO {{$records->card_no}}
                @else
                    {{ '-' }} 
                @endif
            </td>
        </tr>
    </table>
</div>
<br/>
<div>
    <table>
        <tr style="line-height: 3">
            <th style="text-align:left;width:20%;font-weight:bold;font-family:'FontAwesome';"><h4>Item Code</h4></th>
            <th style="text-align:left;width:60%;font-family:'FontAwesome';"><h4>Item Description</h4></th>
            <th style="text-align:right;width:20%;font-family:'FontAwesome';"><h4>Amount</h4></th>
        </tr>
        <?php $total_amount = 0 ?>
        @foreach ($records->approve_payment as $key => $value)
            <tr style="line-height: 2">
                <td style="text-align:left;">{{$value->detail->item['item_code']}}</td>
                <td style="text-align:left;">{{$value->detail->item['name']}}</td>
                <td style="text-align:right;" class="price">{{number_format($value->winItem['win_amount'], 2)}}</td>
            </tr>   
            <?php $total_amount +=  $value->winItem['win_amount'] ?>
        @endforeach
    </table>
</div>
<br/>
<table>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Invoice Amount</strong></td>
        <td style="text-align:right"><strong>{{number_format($total_amount, 2)}}</strong></td>
    </tr>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Advance Received</strong></td>
        <td style="text-align:right"><strong>0.00</strong></td>
    </tr>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Balance Amount</strong></td>
        <td style="text-align:right"><strong>{{number_format($total_amount, 2)}}</strong></td>
    </tr>
</table>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<table>
    <tr style="line-height: 2">
        <td><b><u>Terms & Condition</u></b></td>
    </tr>
    <tr style="line-height: 2">
        <td>1.Goods once Sold will not be accepted as returns.</td>
    </tr>
    <tr style="line-height: 2">
        <td>2.All payments by cheques subject to realisation.</td>
    </tr>
    <tr style="line-height: 2">
        <td>3.All moneys collected on behalf of the owner.</td>
    </tr>
</table>
<br/><br/>
<table>
    <tr>
        <td style="text-align:center;width:33%;">
            ________________________________
        </td>
        <td style="text-align:center;width:33%;">
            ________________________________
        </td>
        <td style="text-align:center;width:33%;">
            ________________________________
        </td>
    </tr>
    <tr>
        <td style="text-align:center;width:33%;">
            Issuing officer's signature
        </td>
        <td style="text-align:center;width:33%;">
            Approved by
        </td>
        <td style="text-align:center;width:33%;">
            Goods received in good order & condition
        </td>
    </tr>
</table>
</body>
</html>
<!-- <img style="bottom:0;" src="{{url('assets/images/invoice-footer.jpg')}}"> -->
<?php


Route::group(array('prefix'=>'test','module' => 'AuctionHistoryManage', 'namespace' => 'App\Modules\AuctionHistoryManage\Controllers'), function() {

    Route::resource('AuctionHistoryManage', 'AuctionHistoryManageController');
    
}); 
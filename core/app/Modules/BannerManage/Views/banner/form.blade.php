    <div class="row">
    <div class="col-lg-3 col-md-3 col-ms-3 col-xs-3">
        <div class="form-group">
            <label class="control-label">Banner Page</label>
            {!! 
                Form::select('banner_page', $banner_pages, '', [
                    'class'       => 'form-control chosen', 
                    'placeholder' => '-- banner page --',
                    'id'          => 'banner_page'
                ])  
            !!}
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-ms-6 col-xs-6">
        <div class="form-group">
            <label class="control-label">Banner Position</label>
            {!! 
                Form::select('banner_position', [], '', [
                    'class'       => 'form-control chosen', 
                    'placeholder' => '-- banner position --', 
                    'id'          => 'banner_position'
                ]) 
            !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-ms-3 col-xs-3">
        <div class="form-group">
            <label class="control-label">Banner Type</label>
            {!! 
                Form::select('banner_type', $banner_types, '', [
                    'class'       => 'form-control chosen', 
                    'placeholder' => '-- banner type --',
                    'id'          => 'banner_type'
                ]) 
            !!}
        </div>
    </div>
</div>
<div class="row" id="link_block" style="display: block;">
    <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <div class="form-group">
            <label class="control-label">Link</label>
            <input type="text" name="link" id="link" class="form-control" placeholder="Enter link...">
        </div>
    </div>
</div>
<div class="form-group">
    <input id="upload_image" name="upload_image[]" type="file" multiple>
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Upload', ['class' => 'btn btn-primary bg-purple pull-right', 'id' => 'btn_upload' , 'style' => 'border:0px;', 'disabled' => true]) !!}
</div>


@extends('layouts.back_master') @section('title','Create banner')
@section('css')
<style type="text/css">
    .imageuploadify-container{
        margin-left: 15px !important;
        margin-bottom: 15px !important;
      }
  </style>
<style type="text/css">
      @import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css");
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Banner 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{ route('banner.index') }}">Banner List</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add Banner</h3>
            <div class="box-title pull-right">
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('banner.store') }}" method="post" id="banner_form" enctype="multipart/form-data" accept-charset="UTF-8">
                {{ csrf_field() }}
                @include ('BannerManage::banner.form')
            </form>
        </div>
    </div>  
</section>

@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    /*$('input[type="file"]').imageuploadify();*/

    $('#banner_page').on('change', function(e){

        var banner_page_id = $(this).val();

        if(banner_page_id !== '')
        {
            var data = new FormData();
            data.append('banner_page_id', banner_page_id);

            $.ajax({
                url: '{{ route('banner.load.data') }}',
                method: 'post',
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function(response){
                    if(response.success == true)
                    {
                        $('#banner_position').html('<option value="">-- banner position --</option>');

                        $.each(response.data, function(key, value){
                            $('#banner_position').append($('<option></option>').attr('value', value.id).text(value.name));
                        });

                        updated_chosen();
                    }
                    else
                    {
                        $('#banner_position').html('<option value="">-- banner position --</option>');
                        
                        updated_chosen();
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        }
        else
        {
            $('#banner_position').html('<option value="">-- banner position --</option>');
            $('.chosen').trigger('chosen:updated');
            
           updated_chosen();
        }
    }); 

    $('#banner_form').on('change keyup', 'input, select', function(e){

        if($('#banner_page').val() == "" || $('#banner_position').val() == "" || $('#banner_type').val() == "" ||  $('#upload_image').val() == "")
        {
            $('#btn_upload').attr('disabled', true);
        }
        else
        {
            $('#btn_upload').attr('disabled', false);
        }
    });

    $("#upload_image").fileinput({
        uploadUrl: "{}",
        autoReplace: true,
        maxFileCount: false,
        showUpload:false,
        showRemove:false,
        maxFileCount: 50,
        allowedFileExtensions: ["jpg", "png", "gif", "PNG", "JPG", "JPGE", "gif", "GIF"]
    });

    //show link block when choose banner type as advertisement
    $('#banner_type').on('change', function(e){
        if($('#banner_type option:selected').text().toLowerCase() == 'advertisement' || $('#banner_type option:selected').text().toLowerCase() == 'ads')
        {
            //$('#link_block').css('display', 'block');
            $('#upload_image').attr('multiple', false);
        }
        else
        {
            //$('#link_block').css('display', 'none');
            $('#upload_image').attr('multiple', true);
        }
    });
});
function updated_chosen()
{
    $('.chosen').trigger('chosen:updated');
}
</script>
@stop
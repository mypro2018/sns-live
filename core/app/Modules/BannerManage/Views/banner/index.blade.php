@extends('layouts.back_master') @section('title','List Banner')
@section('css')
<style type="text/css">
  
    
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Banner 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Banner List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Banner List</h3>
            <div class="box-title pull-right">
                <div class="pull-right">
                    <a href="{{ route('banner.create') }}" class="btn bg-purple btn-sm btn-block" style="padding: 6px 13px;" title="Add New banner">
                        New Banner
                    </a>
                </div>
            </div>
        </div>
        <form action="{{ route('banner.filter') }}" method="get" id="banner_form">
            <div class="box-body">
              <div class="row">
                <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                    <div class="form-group">
                        <label class="control-label">Page</label>
                        {!! 
                            Form::select('banner_page', $banner_pages, Input::get('banner_page')?:old('banner_page')?:'', [
                                'class'       => 'form-control chosen', 
                                'placeholder' => '-- page --',
                                'id'          => 'banner_page'
                            ])  
                        !!}
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-ms-3 col-xs-3">
                    <div class="form-group">
                        <label class="control-label">Banner Position</label>
                        {!! 
                            Form::select('banner_position', $banner_position, Input::get('banner_position')?:old('banner_position')?:'', [
                                'class'       => 'form-control chosen', 
                                'placeholder' => '-- banner position --', 
                                'id'          => 'banner_position'
                            ]) 
                        !!}
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-ms-3 col-xs-3">
                    <div class="form-group">
                        <label class="control-label">Banner Type</label>
                        {!! 
                            Form::select('banner_type', $banner_types, Input::get('banner_type')?:old('banner_type')?:'', [
                                'class'       => 'form-control chosen', 
                                'placeholder' => '-- banner type --',
                                'id'          => 'banner_type'
                            ]) 
                        !!}
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        {!! 
                            Form::select('status', ['1' => 'Active', '0' => 'Inactive'], Input::get('status'), [
                                'class'       => 'form-control chosen', 
                                'placeholder' => '-- status --',
                                'id'          => 'status'
                            ]) 
                        !!}
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button type="submit" id="btn_search" class="btn bg-purple btn-block btn-sm" disabled><span class="fa fa-search"></span> Search</button>
                    </div>
                </div>
            </div>
        </form>
        <form method="post" id="bannr_delete_form">
            {{ csrf_field() }}
            <input type="hidden" id="url" value="{{ route('banner.destroy') }}">
            <div align="center" style="color: #999" id="preview-block">
                @if(isset($display) && $display == 'block')
                    @if(count($banners) > 0)
                        <br>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="input-group-btn">
                                    <select  name="select_option" id="select_option" class="form-control chosen check">
                                        <option value="check">Select all</option>
                                        <option value="uncheck">Deselect all</option>
                                    </select>
                                    <button type="button" id="btn-delete-img" class="btn btn-default btn-sm" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><span class="fa fa-trash-o"></span></button>
                                    @if(Input::get('status') == '')
                                        <button type="button" id="btn-active-img" class="btn btn-default btn-sm" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><span class="fa fa-check"></span></button>
                                        <button type="button" id="btn-inactive-img" class="btn btn-default btn-sm" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><span class="fa fa-times"></span></button>
                                    @elseif(Input::get('status') == 1)
                                        <button type="button" id="btn-inactive-img" class="btn btn-default btn-sm" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><span class="fa fa-times"></span></button>
                                    @elseif(Input::get('status') == 0)
                                        <button type="button" id="btn-active-img" class="btn btn-default btn-sm" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;"><span class="fa fa-check"></span></button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <!-- <div align="left" style="border-bottom: 1px solid rgba(189, 195, 199,0.3);padding-bottom: 4px;margin-bottom: 29px;">
                            <h4 style="line-height: 0.6;">Image of Slider</h4>
                                <div id="filter-breadcrumb">Loading...</div>
                        </div> -->
                        <div class="row">
                            @if(array_key_exists('ads', $banners) && count($banners['ads']) > 0)
                                <div class="col-lg-12">
                                    <div align="left"  style="margin-bottom: -10px;">
                                        <div id="filter-breadcrumb">Loading...</div>
                                        <div style="border-bottom: 1px solid rgba(189, 195, 199,0.3);padding-bottom: 4px;margin-bottom: 29px;font-size: 15px;">Advertisement of <span id="ads_text"></span></div>
                                    </div>
                                </div>
                                @foreach($banners['ads'] as $banner)
                                    @if(count($banner) > 0)
                                        <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                                            <span class="fa fa-check-circle fa-3x {{ $banner->status == 1? 'text-green': ($banner->status == 0? 'text-danger':'') }} custom-badge"></span>
                                            <div class="thumbnail" style="height: 138px;">
                                                <img src="{{ url(UPLOAD_PATH.UPLOADS_DIR.$banner->img_path) }}" alt="{{ $banner->banner_page.' page '.$banner->banner_type }}" style="width: 100%;height: 90px;">
                                                <div class="caption">
                                                    <div class="pull-left">
                                                        <label class="checkbox-inline" style="padding-top: 3px;">
                                                           <input type="checkbox" name="check[]" class="checkbox" checked value="{{ $banner->id }}"> {{ strtolower($banner->banner_type) == 'advertisement'? 'Ads': ucfirst($banner->banner_type) }}
                                                        </label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-default btn-xs btn-delete-each">
                                                            <span class="fa fa-trash-o text-red"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ol class='breadcrumb text-right' style='padding: 1px;background-color: white;border-top: 1px solid rgba(189, 195, 199,0.3);margin-top: 3px;'>
                                            <li>{{ $getAdsImgCountText }}</li>
                                        </ol>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            @if(array_key_exists('sliders', $banners) && count($banners['sliders']) > 0)
                                <div class="col-lg-12">
                                    <div align="left" style="margin-bottom: -10px;">
                                        <div style="border-bottom: 1px solid rgba(189, 195, 199,0.3);padding-bottom: 4px;margin-bottom: 29px;font-size: 15px;">Slider of <span id="slider_text"></span></div>
                                    </div>
                                </div>
                                @foreach($banners['sliders'] as $banner)
                                    @if(count($banner) > 0)
                                        <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2">
                                            <div class="thumbnail" style="height: 138px;">
                                                <img src="{{ url(UPLOAD_PATH.UPLOADS_DIR.$banner->img_path) }}" alt="{{ $banner->banner_page.' page '.$banner->banner_type }}" style="width: 100%;height: 90px;">
                                                <div class="caption">
                                                    <div class="pull-left">
                                                        <label class="checkbox-inline" style="padding-top: 3px;">
                                                           <input type="checkbox" name="check[]" class="checkbox" checked value="{{ $banner->id }}"> {{ $banner->banner_type == 'advertisement'? 'Ads': ucfirst($banner->banner_type) }}
                                                        </label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-default btn-xs btn-delete-each">
                                                            <span class="fa fa-trash-o text-red"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ol class='breadcrumb text-right' style='padding: 1px;background-color: white;border-top: 1px solid rgba(189, 195, 199,0.3);margin-top: 3px;'>
                                            <li>{{ $getSliderImgCountText }}</li>
                                        </ol>
                                        <div class="pull-right">
                                            <span class="fa fa-check-circle text-green"></span> Active
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <span class="fa fa-check-circle text-danger"></span> Inactive
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @else
                        <h2 align="center">Data not found!.</h2>
                        <p align="center">Use filters for get Result</p>
                    @endif
                @else
                    <h2 align="center">Use filters for get Result</h2>
                @endif
            </div>
        </div> <!--/. form -->
        <!-- {{ csrf_field() }} -->
    </form>
</section>

@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  $('#banner_page').on('change', function(e){
        var banner_page_id = $(this).val();

        if(banner_page_id !== '')
        {
            var data = new FormData();
            data.append('banner_page_id', banner_page_id);

            $.ajax({
                url: '{{ route('banner.load.data') }}',
                method: 'post',
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function(response){
                    if(response.success == true)
                    {
                        $('#banner_position').html('<option value="">-- banner position --</option>');

                        $.each(response.data, function(key, value){
                            $('#banner_position').append($('<option></option>').attr('value', value.id).text(value.name));
                        });

                        updated_chosen();
                    }
                    else
                    {
                        $('#banner_position').html('<option value="">-- banner position --</option>');
                        $('.chosen').trigger('chosen:updated');
                        
                        updated_chosen();
                    }
                    console.log(response);
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        }
        else
        {
            $('#banner_position').html('<option value="">-- banner position --</option>');
            $('.chosen').trigger('chosen:updated');
            
            updated_chosen();
        }
    }); 

    $('#banner_form').on('change keyup', 'input, select', function(e){
        if($('#banner_page').val() == "" && $('#banner_position').val() == "" && $('#banner_type').val() == "")
        {
            $('#btn_search').attr('disabled', true);
        }
        else
        {
            $('#btn_search').attr('disabled', false);
        }
    });

    //select deselect all
    $('#select_option').on('change', function() {
        if($('#select_option').val() == 'check')
        {
            $('input[type=checkbox]').each(function(){
               $(this).prop('checked', true); 
            });
        }
        else
        {
            $('input[type=checkbox]').each(function(){
               $(this).prop('checked', false); 
            });
        }
    });

    $('#btn-delete-img').click(function(){
        var param = 'delete';
        var _url  = "{{ route('banner.destroy') }}";
        _url      = _url+"?action="+param;

        $.confirm({
            theme: 'material',
            title: 'Are you sure?',
            content: 'You wanna delete this image?.',
            buttons: {
                confirm: {
                    btnClass: 'btn-red',
                    keys: ['enter'],
                    action: function(){
                        $('#bannr_delete_form').prop('action', _url);
                        $('#bannr_delete_form').submit();
                    }
                },
                cancel: function () {
                    $('#banner_delete_form').prop('action', $('#banner_delete_form').data('url')+'?action=');
                }
            }
        });
    });

    $('#btn-active-img').click(function(){
        var param = 'active';
        var _url  = "{{ route('banner.destroy') }}";
        _url      = _url+"?action="+param;

        $.confirm({
            theme: 'material',
            title: 'Are you sure?',
            content: 'You wanna Active this image?.',
            buttons: {
                confirm: {
                    btnClass: 'btn-red',
                    keys: ['enter'],
                    action: function(){
                        $('#bannr_delete_form').prop('action', _url);
                        $('#bannr_delete_form').submit();
                    }
                },
                cancel: function () {
                    $('#banner_delete_form').prop('action', $('#banner_delete_form').data('url')+'?action=');
                }
            }
        });
    });

    $('#btn-inactive-img').click(function(){
        var param = 'inactive';
        var _url  = "{{ route('banner.destroy') }}";
        _url      = _url+"?action="+param;

        $.confirm({
            theme: 'material',
            title: 'Are you sure?',
            content: 'You wanna Inactive this image?.',
            buttons: {
                confirm: {
                    btnClass: 'btn-red',
                    keys: ['enter'],
                    action: function(){
                        $('#bannr_delete_form').prop('action', _url);
                        $('#bannr_delete_form').submit();
                    }
                },
                cancel: function () {
                    $('#banner_delete_form').prop('action', $('#banner_delete_form').data('url')+'?action=');
                }
            }
        });
    });

    $('.btn-delete-each').click(function(){
        var param = $(this).parents('.thumbnail').find('.checkbox').val();
        var _url  = "{{ route('banner.destroy') }}";
        _url      = _url+"?action=delete&img_id="+param;
        
        $.confirm({
            theme: 'material',
            title: 'Are you sure?',
            content: 'You wanna delete this image?.',
            buttons: {
                confirm: {
                    btnClass: 'btn-red',
                    keys: ['enter'],
                    action: function(){
                        window.open(_url, "_self");
                    }
                },
                cancel: function () {

                }
            }
        });
    });

    var page      = $('#banner_page option:selected');
    var position  = $('#banner_position option:selected');
    var type      = $('#banner_type option:selected');
    var html_code = "<div class='pull-right' style='margin-top:7px;'>";

    if(page.val() !== '')
    {
        html_code += page.text();
        $('#ads_text').html(page.text()+" page");
        $('#slider_text').html(page.text()+" page");
    }

    if(position.val() !== '')
    {
         html_code += "/"+position.text();
    }

    if(type.val() !== '')
    {
         html_code += "/"+type.text();
    }

    html_code += "</div>";

    $('#filter-breadcrumb').html(html_code);
});
function updated_chosen()
{
    $('.chosen').trigger('chosen:updated');
}
</script>
@stop
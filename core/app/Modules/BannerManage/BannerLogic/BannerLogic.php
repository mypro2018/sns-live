<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */
 
namespace App\Modules\BannerManage\BannerLogic;
use App\Modules\BannerManage\BannerRepository\BannerRepository;
use Image;
use File;

class BannerLogic {
	//created a directory
	public function makeDirNotExist($path, $permission = 0777, $status = true){
		try {
			return File::exists(storage_path($path)) or File::makeDirectory(storage_path($path), $permission, $status);	
		} catch (\Exception $e) {
			throw new \Exception("message : ".$e->getMessage());
		}
	}

	//save image in database and move to storage
	public function saveImage($image, $full_dir_path, $width, $expectWidth, $img_quality = null){

		try {
			if(isset($width) && $width > $expectWidth)
	        {
	            $img_real_path = Image::make($image->getRealPath());
	            $img_real_name = $image->getClientOriginalName();
	            $gen_img_name  = date('Ymdhis')."-".$img_real_name;

	            $img_real_path->resize($expectWidth, null, function ($constraint) {
	                $constraint->aspectRatio();
	            });

	            $path          = $full_dir_path.'/'.$gen_img_name;
	            $saved         = $img_real_path->save(storage_path($path), 90);
	            return ['path' => $path, 'count' => 1];
	        }
	        else
	        {
	            $img_real_path = Image::make($image->getRealPath());
	            $img_real_name = $image->getClientOriginalName();
	            $gen_img_name  = date('Ymdhis')."-".$img_real_name;
	            $path          = $full_dir_path.'/'.$gen_img_name;
	            $saved         = $img_real_path->save(storage_path($path), 90);
	            return ['path' => $path, 'count' => 1];
	        }
		} catch (\Exception $e) {
			throw new \Exception("message : ".$e->getMessage());
		}
	}

	//get image count text
	public function getImageCountText($key, $banners){
		try {
			if(array_key_exists($key, $banners))
            {
                if(count($banners[$key]) > 1)
                {
                    return rtrim($key, 's')." count : ".count($banners[$key]);
                }
                else
                {
                    if(count($banners[$key]) == 0)
                    {
                        return '';
                    }
                    else
                    {
                        return rtrim($key, 's')." count : ".count($banners[$key]);
                    }
                }
            }
            else
            {
            	return $key." key not exists in array.";
            }
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}
}
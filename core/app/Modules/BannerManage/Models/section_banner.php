<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\BannerManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class section_banner extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'section_banner';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $timestamp = true;

    protected $fillable = [
        'banner_id', 
        'banner_section_id', 
        'banner_page_id', 
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    
}

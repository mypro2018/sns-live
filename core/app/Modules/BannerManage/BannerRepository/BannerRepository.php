<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\BannerManage\BannerRepository;

use App\Modules\BannerManage\Models\banner_type;
use App\Modules\BannerManage\Models\banners as banner;
use App\Modules\BannerManage\Models\section_banner_image as section_image;
use App\Modules\BannerManage\Models\section_banner;
use App\Modules\BannerManage\Models\banner_page;
use DB;
use Response;

class BannerRepository {
	//get banner type by id
	public function getBannerTypeById($id){
		try {
			if(!empty($id))
			{
				$banner = banner_type::select(
							"id",
							"name as banner_type",
							"status",
							"created_at",
							"updated_at",
							"deleted_at"
						)
						->where("id", $id)
						->first();

				if(count($banner) > 0)
				{
					return $banner;
				}
				else
				{
					return [];
				}
			}
			else
			{
				throw new \Exception('Invalid ID');
			}
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//save banner data in database
	public function saveBanner($data){
		try {
			DB::beginTransaction();

			if(count($data) > 0)
			{
				$banner             = new banner();
	        	$banner->img_path   = $data['img_path']; 
	        	$banner->link       = $data['link']; 
	        	$banner->status     = $data['status'];
	        	$banner->created_at = $data['created_at']; 
	        	$banner->save(); 

	        	DB::commit();
	        	return $banner;
			}
			else
			{
				DB::rollback();
				throw new \Exception('data not found!.');
			}
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//save data to section_banner_image tabe
	public function saveBannerImage($data){
		try {
			DB::beginTransaction();

			if(count($data) > 0)
			{
				$section_image                    = new section_image();
	            $section_image->section_banner_id = $data['section_banner_id'];
	            $section_image->banner_type_id    = $data['banner_type_id'];
	            $section_image->banner_id         = $data['banner_id'];
	            $section_image->status            = $data['status'];
	            $section_image->created_at        = $data['created_at'];
	            $section_image->save();

	            DB::commit();
	            return $section_image;
			}
			else
			{
				DB::rollback();
				throw new \Exception('data not found!.');
			}	
		} catch (Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//get banner sectio rule 
	public function getBannerSectionRules($banner_position_id, $banner_page_id){
		try {
			$banner_section_rules = section_banner::select(
                'section_banner.id',
                'section_banner.width', 
                'section_banner.height'
            )
            ->join('banner_section as bs', 'bs.id', '=', 'section_banner.banner_section_id')
            ->where('section_banner.banner_section_id', $banner_position_id)
            ->where('section_banner.banner_page_id', $banner_page_id)
            ->where('section_banner.status', 1)
            ->where('section_banner.deleted_at', NULL)
            ->orderBy('section_banner.id', 'DESC')
            ->first();

            if(count($banner_section_rules) > 0)
            {
            	return $banner_section_rules;
            }
            else
            {
            	return $banner_section_rules = [];
            }
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}	
	}

	//filter banners
	public function filterBanner($banner_page_id, $banner_position_id, $banner_type_id, $status = ''){
		try {
			$banner_info = section_image::select(
				'bt.name as banner_type', 
				'bp.name as banner_page', 
				'bs.name as banner_position', 
				'b.*'
			)
			->join('banner_type as bt', 'bt.id', '=', 'section_banner_image.banner_type_id')
			->join('section_banner as sb', 'sb.id', '=', 'section_banner_image.section_banner_id')
			->join('banner_page as bp', 'bp.id', '=', 'sb.banner_page_id')
			->join('banner_section as bs', 'bs.id', '=', 'sb.banner_section_id')
			->join('banner as b', 'b.id', '=', 'section_banner_image.banner_id')
			->where('b.deleted_at', null);

			if($status !== ''){
				$banner_info = $banner_info->where('b.status', $status);
			}

			if(!empty($banner_page_id))
			{
				$banner_info->where('bp.id', $banner_page_id);
			}

			if(!empty($banner_position_id))
			{
				$banner_info->where('bs.id', $banner_position_id);
			}

			if(!empty($banner_type_id))
			{
				$banner_info->where('bt.id', $banner_type_id);
			}

			$banner_info = $banner_info->orderBy('bt.id', 'DESC')->get();	

			$sliders      = [];
			$ads          = [];

			if(count($banner_info) > 0)
			{
				foreach($banner_info as $key => $banner)
				{
					if(strtolower($banner->banner_type) == "slider")
					{
						$sliders['sliders'][$key] = $banner;
					}
					else
					{
						$ads['ads'][$key] = $banner;
					}
				}

				return array_merge($sliders, $ads);
			}
			else
			{
				//return array_push($sliders, $ads);
				return [];
			}

		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//delete selected images
	public function deleteImage($selected_img, $action = null){
		try {
			DB::beginTransaction();

			$action_count  = 0;
			$selected_count = count($selected_img);			

			if(count($selected_img) > 0)
			{
				foreach ($selected_img as $img_id) 
	            {
					if(isset($action) && $action !== "")
					{	
						if($action == 'delete')
						{
							$deleted = banner::where('id', $img_id)->delete();
							$deleted = section_image::where('banner_id', $img_id)->delete();
							DB::commit();
							$action_count++;
						}
						elseif($action == 'active')
						{
							$updated = banner::where('id', $img_id)->update([
								'status'     => 1,
								'updated_at' => date('Y-m-d h:i:s')
							]);
	
							$updated = section_image::where('banner_id', $img_id)->update([
								'status'     => 1,
								'updated_at' => date('Y-m-d h:i:s')
							]);
	
							DB::commit();
							$action_count++;
						}
						elseif($action == 'inactive')
						{
							$updated = banner::where('id', $img_id)->update([
								'status'     => 0,
								'updated_at' => date('Y-m-d h:i:s')
							]);
	
							$updated = section_image::where('banner_id', $img_id)->update([
								'status'     => 0,
								'updated_at' => date('Y-m-d h:i:s')
							]);
	
							DB::commit();
							$action_count++;
						}
						else
						{
							DB::rollback();
							throw new \Exception('Invalid Action performed!.');
						}
					}
					else
					{
						DB::rollback();
						throw new \Exception('Action cannot be empty!.');
					}
	            }

	            return ['action_count' => $action_count, 'selected_count' => $selected_count];
			}
			else
			{
				DB::rollback();
				throw new \Exception('message : '.$e->getMessage());
			}

		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//load data to selectbox
	public function loadData($banner_page_id){
		try {
			if(!empty($banner_page_id))
	        {
	            $banner_section = section_banner::select(DB::raw("CONCAT(bs.name, '  |  ', width, 'x', height) as `name`, bs.id"))
	                    ->join('banner_section as bs', 'bs.id', '=', 'banner_section_id')
						->where('banner_page_id', $banner_page_id)
						->whereNull('bs.deleted_at')
	                    ->get();

	            if(count($banner_section) > 0)
	            {
	                return Response::json([
	                    'success' => true, 
	                    'message' => 'successfully get banner postions',
	                    'data'    => $banner_section
	                ]);
	            }
	            else
	            {
	                return Response::json([
	                    'success' => false, 
	                    'message' => 'banner page not found!.',
	                    'data'    => []
	                ]);
	            }
	        }
	        else
	        {
	            return Response::json([
	                    'success' => false, 
	                    'message' => 'banner page invalid or not found!.',
	                    'data'    => []
	                ]);
	        }
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	public function loadPageRules($banner_page_id){
		try {
			if(!empty($banner_page_id))
	        {
	            $banner_position = section_banner::select(DB::raw("CONCAT(bs.name, '  |  ', width, 'x', height) as `name`, bs.id"))
                        ->join('banner_section as bs', 'bs.id', '=', 'banner_section_id')
                        ->where('banner_page_id', $banner_page_id)
                        ->lists('bs.name','bs.id');

	            if(count($banner_position) > 0)
	            {
	                return $banner_position;
	            }
	            else
	            {
	                return [];
	            }
	        }
	        else
	        {
	            return [];
	        }
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	//get all page in banner_page table
	public function getAllBannerPage(){
		try {
			$banner_page = banner_page::lists('name', 'id');

			if(count($banner_page) > 0)
			{
				return $banner_page;
			}
			else
			{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}

	public function getAllBannerType(){
		try {
			$banner_type = banner_type::lists('name', 'id');

			if(count($banner_type) > 0)
			{
				return $banner_type;
			}
			else
			{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}
	//get home banners
	public function homeBanner(){
		try {
			$home_banner = banner::select('id as banner_id', 'img_path as banner_path', 'link as banner_link')
			->whereNull('deleted_at')
			->orderBy('updated_at', 'DESC')
			->get();

			if(count($home_banner) > 0)
			{
				return $home_banner;
			}
			else
			{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message : '.$e->getMessage());
		}
	}



}
<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */
 
namespace App\Modules\BannerManage\Requests;

use App\Http\Requests\Request;

class BannerRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;
		$rules = [
			'banner_page'     => 'required',
			'banner_position' => 'required',
			'banner_type'     => 'required'
		];
		
		return $rules;
	}

}

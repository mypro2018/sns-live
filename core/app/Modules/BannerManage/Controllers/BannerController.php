<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\BannerManage\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\BannerManage\BannerLogic\BannerLogic;
use App\Modules\BannerManage\BannerRepository\BannerRepository;
use App\Classes\Functions;
use Illuminate\Http\Request;
use App\Modules\BannerManage\Requests\BannerRequest;
use Input;

class BannerController extends Controller
{
    //declare variables for object
    public $bannerRepo;
    public $bannerLogic;
    public $commonFunction;

    public function __construct(BannerRepository $bannerRepo, BannerLogic $bannerLogic, Functions $commonFunction){
        //initialize object
        $this->bannerRepo     = $bannerRepo;
        $this->bannerLogic    = $bannerLogic;
        $this->commonFunction = $commonFunction;
    }

    /**
     * Display a listing of the banners.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $banner_pages    = $this->bannerRepo->getAllBannerPage();
        $banner_types    = $this->bannerRepo->getAllBannerType();
        $banner_position = [];
        $banners         = [];
        $display         = 'none';

        return view('BannerManage::banner.index',  compact('banner_pages', 'banner_types', 'banners', 'display', 'banner_position'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $banner_pages    = $this->bannerRepo->getAllBannerPage();
        $banner_types    = $this->bannerRepo->getAllBannerType();

        return view('BannerManage::banner.create', compact('banner_pages', 'banner_types'));
    }

    /**
     * Store banners.
     *
     * @param Request $request
     *
     * @return Redirector
     */
    public function store(BannerRequest $request)
    {
        try {
            $banner_page_id     = $request->input('banner_page');
            $banner_position_id = $request->input('banner_position');
            $banner_type_id     = $request->input('banner_type');
            $link               = $request->input('link');
            $upload_images      = Input::file('upload_image');
            $dir_path           = PATH_TO_BANNER;
            $image_count        = count($upload_images);
            $uploaded_img       = 0;

            if(!empty($banner_page_id) && !empty($banner_position_id) && !empty($banner_type_id))
            {
                /*****************************************************************************************
                * @return array with data
                * @param ($banner_position_id, $banner_page_id) for group multiple records
                ******************************************************************************************/
                $banner_section_rules = $this->bannerRepo->getBannerSectionRules($banner_position_id, $banner_page_id);

                if(count($banner_section_rules) > 0)
                {
                    //get banner type data by id
                    $banner_data = $this->bannerRepo->getBannerTypeById($banner_type_id);

                    if(count($banner_data) > 0)
                    {
                        //storage path
                        $full_dir_path = $dir_path.'/'.strtolower($banner_data->banner_type);
                        //make a directory if not exist
                        $isExist = $this->bannerLogic->makeDirNotExist($full_dir_path, 0777, true);

                        if($isExist)
                        {
                            foreach($upload_images as $image)
                            {
                                list($width, $height) = getimagesize($image->getRealPath());

                                /*****************************************************************************************
                                * @return ['path' => '', 'count' => '']
                                * @param (instance of loop image, directory path, min with, expected with, image quality)
                                ******************************************************************************************/
                                $saved = $this->commonFunction->saveImage($image, $full_dir_path, $width, $banner_section_rules->width, 100);

                                if(count($saved) > 0)
                                {
                                    $uploaded_img += $saved['count'];  
                                
                                    //save data to banner tabe
                                    $data = [
                                        'img_path'   => $saved['path'], 
                                        'link'       => $link, 
                                        'status'     => 1,
                                        'created_at' => date('Y-m-d h:i:s')
                                    ];

                                    //saved data in banner table
                                    $savedBanner = $this->bannerRepo->saveBanner($data);

                                    if(count($savedBanner) > 0)
                                    {   
                                        $data = [
                                            'section_banner_id' => $banner_section_rules->id, 
                                            'banner_type_id'    => $banner_type_id, 
                                            'banner_id'         => $savedBanner->id,
                                            'status'            => 1,
                                            'created_at'        => date('Y-m-d h:i:s')
                                        ];

                                        //save data in section_banner_image table
                                        $savedBannerImage = $this->bannerRepo->saveBannerImage($data);
                                    }
                                    else
                                    {
                                        return $this->commonFunction->redirectWithAlert(
                                            'banner.create', 
                                            'error', 
                                            'Error!.', 
                                            'banner couldn\'t saved successfully.'
                                        );
                                    }
                                }
                                else
                                {
                                    return $this->commonFunction->redirectWithAlert(
                                            'banner.create', 
                                            'error', 
                                            'Error!.', 
                                            'Something went wrong!, couldn\'t moved image to server.'
                                        );
                                }
                            }

                            if($image_count == $uploaded_img)
                            {
                                return $this->commonFunction->redirectWithAlert(
                                    'banner.create', 
                                    'success', 
                                    'Done!.', 
                                    $uploaded_img." of ".$image_count.' images was successfully uploaded'
                                );
                            }
                            else
                            {
                                return $this->commonFunction->redirectWithAlert(
                                    'banner.create', 
                                    'success', 
                                    'Done!.', 
                                    $uploaded_img." of ".$image_count.' images was successfully uploaded'
                                );
                            }
                        }
                        else
                        {
                            return $this->commonFunction->redirectWithAlert(
                                'banner.create', 
                                'error', 
                                'Error!.', 
                                'Something went wrong!, couldn\'t created directory in server.'
                            );
                        }
                    }
                    else
                    {
                        return $this->commonFunction->redirectWithAlert(
                            'banner.create', 
                            'error', 
                            'Error!.', 
                            'Something went wrong!.'
                        );
                    }
                }
                else
                {
                    return $this->commonFunction->redirectWithAlert(
                        'banner.create', 
                        'error', 
                        'Error!.', 
                        'Something went wrong!, banner table maybe empty or tables not mapped.'
                    );
                }
            }
            else
            {
                return $this->commonFunction->redirectWithAlert(
                    'banner.create', 
                    'error', 
                    'Error!.', 
                    'Something went wrong.'
                );
            }   
        } catch (\Exception $e) {
            return $this->commonFunction->redirectWithAlert(
                'banner.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified banner from database.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        try {
            $selected_img = (array) $request->input('img_id')?: $request->input('check');
            $action       = $request->input('action');
                
            $deleted = $this->bannerRepo->deleteImage($selected_img, $action);

            if(count($deleted) > 0)
            {
                return $this->commonFunction->redirectWithAlert('banner.index', 'success', 'Done!.', $deleted['action_count']." of ".$deleted['selected_count']." image was successfully ".ucfirst($action)."!.");
            }
            else
            {
                return $this->commonFunction->redirectWithAlert('banner.index', 'success', 'Done!.', $deleted['action_count']." of ".$deleted['selected_count']." image was successfully ".ucfirst($action)."!.");
            }   

        } catch (\Exception $e) {
            return $this->commonFunction->redirectWithAlert('banner.index', 'error', 'Error!.', $e->getMessage());
        }
    }

    /**
     * load data to select box when we select other select box
     *
     * @param  Request
     * @return Response json
     */
    public function load_data(Request $request)
    {
        try {

            $banner_page_id = $request->input('banner_page_id');
            return $this->bannerRepo->loadData($banner_page_id);

        } catch (\Exception $e) {
            return $this->commonFunction->redirectWithAlert('banner.create', 'error', 'Error!.', $e->getMessage());
        }
    }

    //filter banners
    public function banner_filter(Request $request)
    {
        try {
            $banner_page_id     = $request->input('banner_page');
            $banner_position_id = $request->input('banner_position');
            $banner_type_id     = $request->input('banner_type');
            $status             = $request->input('status');
            $banner_pages       = $this->bannerRepo->getAllBannerPage();
            $banner_types       = $this->bannerRepo->getAllBannerType();
            $display            = 'block';
            $count_of_slider    = '';
            $count_of_ads       = '';

            $banner_position       = $this->bannerRepo->loadPageRules($banner_page_id);
            $banners               = $this->bannerRepo->filterBanner($banner_page_id, $banner_position_id, $banner_type_id, $status);

            if(count($banners) > 0)
            {   
                $getAdsImgCountText    = $this->bannerLogic->getImageCountText('ads', $banners);
                $getSliderImgCountText = $this->bannerLogic->getImageCountText('sliders', $banners);

                return view('BannerManage::banner.index',  compact('banner_pages', 'banner_types', 'banners', 'display', 'banner_position', 'getAdsImgCountText', 'getSliderImgCountText'));
            }
            else
            {
                return $this->commonFunction->redirectWithAlert('banner.index', 'error', 'Error!.', 'Data not found!..');
            }                
        } catch (Exception $e) {
            return $this->commonFunction->redirectWithAlert('banner.index', 'error', 'Error!.', $e->getMessage());
        }                
    }
}

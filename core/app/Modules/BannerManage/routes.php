<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */
 
Route::group(['middleware' => ['auth']], function(){
    Route::group(['prefix' => 'banner', 'namespace' => 'App\Modules\BannerManage\Controllers'],function(){

    	/*===================================
					GEt request
    	===================================*/
		Route::get('create', [
			'uses' => 'BannerController@create',
			'as'   => 'banner.create'
        ]);

        Route::get('list', [
			'uses' => 'BannerController@index',
			'as'   => 'banner.index'
        ]);

        Route::any('delete', [
			'uses' => 'BannerController@destroy',
			'as'   => 'banner.destroy'
        ]);


        /*===================================
					POST request
    	===================================*/
    	Route::post('store', [
    		'uses' => 'BannerController@store',
    		'as'   => 'banner.store'
    	]);

        //Route::post('store', function(){return 'post';})->name('banner.store');

    	Route::post('update/{id}', [
    		'uses' => 'BannerController@update',
    		'as'   => 'banner.update'
    	]);

        Route::get('filter', [
            'uses' => 'BannerController@banner_filter',
            'as'   => 'banner.filter'
        ]);

        Route::post('load/data', [
            'uses' => 'BannerController@load_data',
            'as'   => 'banner.load.data'
        ]);
	});  	
}); 

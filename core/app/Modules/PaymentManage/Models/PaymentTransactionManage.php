<?php namespace App\Modules\PaymentManage\Models;

/**
*
* PaymentManage Model
* @author Lahiru Madhsankha <lahirumadhusankha@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class PaymentTransactionManage extends Model {
	/**
     * The sns_payment table associated this model.
     */
    protected $table = 'sns_payment_transaction';
    public $timestamps = true;
    protected $guarded = ['id'];

}

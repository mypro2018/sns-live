<?php namespace App\Modules\PaymentManage\Models;

/**
*
* PaymentManage Model
* @author Lahiru Madhsankha <lahirumadhusankha@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class PaymentManage extends Model {
	/**
     * The sns_payment table associated this model.
     */
    protected $table = 'sns_payment';
    public $timestamps = true;
    protected $guarded = ['id'];

	/**
	* This function is used to make relationship with auction
	*/
	public function auction(){
		return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionManage','sns_auction_id','id');
	}
	
	/**
	* This function is used to make relationship with item
	*/
	public function item(){
		return $this->belongsTo('App\Modules\ProductManage\Models\ProductManage','sns_item_id','id');
	}

	/**
	* This function is used to make relationship with user
	*/
	public function user(){
		return $this->belongsTo('App\Models\User','sns_user_id','id');
	}

	/**
	* This function is used to make relationship with user
	*/
	public function payment_transaction(){
		return $this->hasOne('App\Modules\PaymentManage\Models\PaymentTransactionManage','sns_payment_id','id');
	}

	/**
	* This function is used to make relationship with user
	*/
	public function customer(){
		return $this->belongsTo('App\Modules\CustomerManage\Models\CustomerManage','sns_user_id','id');
	}



}

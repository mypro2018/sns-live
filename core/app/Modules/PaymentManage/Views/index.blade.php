@extends('layouts.back_master') @section('title','Payment List')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Payment
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Auction List</li>
	</ol>
</section>


<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('payment/search')}}">
            <div class="box-body">
                <div class="form-group" style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class=" form-control chosen input-sm" name="auction">
                                    <option value="">-- Search All Auctions --</option>
                                    @if($auction_list)
                                        @foreach($auction_list as $key => $val)
                                        <option value="{{$val->id}}" @if($val->id == $auction) selected @endif>{{$val->event_name}} - {{$val->auction_date}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm datetimepicker validate" data-date-format="YYYY-MM-DD" name="date" placeholder="Auction Date" value="{{$date}}" dt>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
                    <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Auction List</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">#</th>
                                <th class="text-center">Auction</td>
                                <th width="10%" class="text-center">Auction Date</th>
                                <th width="10%" class="text-center">Status</th>
                                <th width="10%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;?>
                        @if(count($payment) > 0)
                            @foreach($payment as $result_val)
                                @include('PaymentManage::template.payment')
                            <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="8" class="text-center">No data found.</td></tr>
                        @endif
                        </tbody>
                    </table>        
                    </div>
                    <div class="box-footer">      
                        <div style="float: right;">{!! $payment->appends($_GET)->render() !!}</div>
                    </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop


@section('js')
<script>
    $(function () {
        $('.datetimepicker').datetimepicker();
    });
</script>
@stop

<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val->item_code?:'-'}}</td>
     <td>{{$result_val->register_id}}</td>
     <td>{{$result_val->customer_name}}</td>
     <td>{{$result_val->card_no}}</td>
     <td class="text-right">{{number_format($result_val->amount,2)}}</td>
     <td>{{$result_val->payment_date}}</td>
     @if($result_val->transaction_id != '0000')
      <td>{{$result_val->transaction_id}}</td>
     @else
      <td>{{$result_val->reference_no}}</td>
     @endif
     @if($result_val->sns_item_id != 0)
        <td class="text-center"><span class="text-bold label label-primary">ORDER</span></td>
     @else
        <td class="text-center"><span class="text-bold label label-warning">REGISTRATION</span></td>
     @endif
     @if($result_val->payment_status == 1)
        <td class="text-center text-green">Success</td>
     @elseif($result_val->payment_status == -1)
        <td class="text-center text-red">Error</td>  
     @endif
     <!-- @if($result_val->sns_item_id != 0)
     <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Payment" onClick="viewPayment({{$result_val->sns_item_id}})"><i class="fa fa-eye view"></i></a>
        </div>
     </div>
    </td> -->
    <!-- @else
        <td></td>
    @endif     -->
</tr>


<tr id="{{$i}}">
     <td class="text-center">{{$i}}</td>
     <td class="text-left">{{$result_val->event_name}}</td>
     <td class="text-center">{{$result_val->auction_date}}</td>
     @if($result_val->auction_status == 1 || $result_val->auction_status == 0)
        <td class="text-center">Pending</td>
     @elseif($result_val->auction_status == 2)
        <td class="text-center">Live</td>
     @else
        <td class="text-center">Over</td>   
     @endif
     <td class="text-center">
        <div class="btn-group">
            <a href="{{'payment-list/'.$result_val->sns_auction_id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Payment"><i class="fa fa-eye view"></i></a>
        </div>
    </div>    
</tr>


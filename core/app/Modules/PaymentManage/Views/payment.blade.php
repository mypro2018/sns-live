@extends('layouts.back_master') @section('title','Payment List')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Payment
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('payment/list')}}}">Auction List</a></li>
        <li class="active">Payment List</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
        <form role="form" method="get" action="{{url('payment/search-payment')}}">
            <div class="box-body">
                <div class="row">
                    <input type="hidden"  name="auction" placeholder="Search Item" value="{{$auction_id}}">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" name="item" placeholder="Search Item" value="{{$item}}">
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" name="customer" placeholder="Search Customer" value="{{$customer}}">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm datetimepicker" name="date" placeholder="Select Date" value="{{$date}}" data-date-format="YYYY-MM-DD" dt>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <select class=" form-control chosen" name="type">
                            <option value="0">-- All Type --</option>
                            <option value="1" @if($type == 1) selected @endif>Order Payment</option>
                            <option value="2" @if($type == 2) selected @endif>Registration Payment</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <select class=" form-control chosen" name="status">
                            <option value="0">-- All Status --</option>
                            <option value="1" @if($status == 1) selected @endif>Payment Success</option>
                            <option value="2" @if($status == 2) selected @endif>Payment Error</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" name="date" placeholder="Select Date" value="{{$date}}">
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                        <select class=" form-control chosen" name="status">
                            <option value="0">-- All Status --</option>
                            <option value="1" @if($status == 1) selected @endif>Payment Success</option>
                            <option value="2" @if($status == 1) selected @endif>Payment Error</option>
                        </select>
                        </div>
                    </div> -->
                </div>

            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search"></i> Search</button>
                    <a href="{{ route('payment.list', ['id' => $auction_id])}}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </div>
        </form>
    </div>
    <div class="box">
        <div class="box-header with-border">
            @if($auction)
                <h3 class="box-title">Payment List - <strong>Auction</strong>({{ $auction->event_name}})</h3>
            @endif
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-condensed table-bordered table-responsive" id="orderTable">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Item Code</th>
                                <th>Register ID</th>
                                <th>Customer</th>
                                <th>Card No</th>
                                <th>Payment(Rs)</th>
                                <th>Payment Date</th>
                                <th>Transaction No</th>
                                <th class="text-center">Type</th>
                                <th width="5%">Status</th>
                                <!-- <th width="5%">Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;?>
                        @if(count($auction_payment_list) > 0)
                            @foreach($auction_payment_list as $result_val)
                                @include('PaymentManage::template.auc-payment')
                            <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="10" class="text-center">Cannot found data.</td></tr>
                        @endif
                        </tbody>
                    </table>        
                    </div>
                    @if(count($auction_payment_list) > 0)
                    <div class="box-footer">      
                        <div style="float: right;">{!! $auction_payment_list->appends($_GET)->render() !!}</div>
                    </div>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
<script type="text/javascript">
    $('.datetimepicker').datetimepicker();
//This function is used to view category details in popup model
function viewPayment(item_id) {
  
  $(".box").addClass('panel-refreshing');

  var content = '<div class="form-horizontal">'+
                    '<div class="form-group">'+
                        '<label for="inputEmail3" class="col-sm-5 control-label">Wining Bid Amount(Rs)</label>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control bid_amount" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="inputEmail3" class="col-sm-5 control-label" style="color:green;">Paid Amount(Rs)</label>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control paid_amount" readonly>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="inputEmail3" class="col-sm-5 control-label" style="color:red;">Due Amount(Rs)</label>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control due_amount" readonly>'+
                        '</div>'+
                    '</div>'+
                '</div>';
    $.ajax({
      url: "{{URL::to('payment/item-payment')}}",
      method: 'GET',
      data: {'item_id': item_id},
      async: false,
      success: function (data) {
          console.log(data.item_payment);
        if(data != ''){
          $.confirm({
            title: data.item_payment[0].item.name+' | '+data.item_payment[0].item.item_code,
            theme: 'material',
            content: content,
            columnClass: 'col-md-6 col-md-offset-3',
            onContentReady: function () {
              var self = this;
              var paid = 0;
              self.$content.find('.bid_amount').val(parseFloat(Math.round(data.item_payment[0].item.auction_detail.highest_bid.win_bid.win_amount * 100) / 100).toFixed(2));
              if(data.item_payment.length > 1){
                paid = Number(data.item_payment[0].amount) + Number(data.item_payment[1].amount);   
                self.$content.find('.paid_amount').val(parseFloat(Math.round(paid * 100) / 100).toFixed(2));
              }else{
                 paid = data.item_payment[0].amount; 
                 self.$content.find('.paid_amount').val(parseFloat(Math.round(paid * 100) / 100).toFixed(2)); 
              } 
              var due = Number(data.item_payment[0].item.auction_detail.highest_bid.win_bid.win_amount) - Number(paid);
              self.$content.find('.due_amount').val(parseFloat(Math.round(due * 100) / 100).toFixed(2));
            },
            buttons: {
              close: function () {}
            }
          });
        }else{}

        $(".box").removeClass('panel-refreshing');
      },
      error: function () {
        alert('error');
      }
  });
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@stop

<?php namespace App\Modules\PaymentManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Lahiru Madhusankha <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\PaymentManage\Models\PaymentManage;
use App\Modules\PaymentManage\Models\PaymentTransactionManage;
use App\Modules\AuctionManage\Models\AuctionManage;
use App\Modules\AuctionManage\Models\AuctionDetailsManage;
use DB;
use Exception;

class Logic {

	/**
     * This function is used to add a payment
     * @param array $data, array $items
     * @return object Auction object if success, exception otherwise
     */
    public function add($data) {
        $product_id = 0;
        if(!empty($data['sns_item_id'])){
            $product_id = $data['sns_item_id'];
        }
        $hasPayment = PaymentManage::where('sns_auction_id', $data['sns_auction_id'])
        ->where('sns_user_id', $data['sns_user_id'])
        ->where('sns_item_id', '=', $product_id)
        ->where('payment_status', PAYMENT_SUCCESS)
        ->whereNull('deleted_at')
        ->first();
        if(!$hasPayment){
            $payment = DB::transaction(function() use($data){
                return $payment = PaymentManage::create($data);
            });
            if($payment){
                return $payment;
            }else{
                throw new Exception("Error occurred while adding new payment");
            }
        }else{
            return $hasPayment;
        }
    }

    /**
     * This function is used to update payment
     * @param integer $id and array $data
     * @response is boolean
     */
    public function update($id, $data, $transaction=null){
        $payment = DB::transaction(function() use($id, $data, $transaction){
            $payment = PaymentManage::where('id', $id)->update($data);
            if($payment){
                /*$payment_transaction = PaymentTransactionManage::where('sns_payment_id', $id)
                    ->where('transaction_id', TRANSACTION_ID)
                    ->where('error_code', TRANSACTION_ERROR_CODE)
                    ->first();
                if(!$payment_transaction){*/    
                    $add_transaction = PaymentTransactionManage::create($transaction);
                    if(!$add_transaction){
                        throw new \Exception("Error occurred while updating payment transaction");
                    }else{
                        return  $add_transaction;
                    }
                /*}else{
                    throw new Exception("Payment transaction already exist"); 
                }*/
            }else{
                throw new Exception("Error occurred while updating payment"); 
            }
        });
        if($payment){
            return $payment;
        }else{
            throw new \Exception("Error occurred while updating payment");
        }
    }
	/** 
	 * This function is used to auction item payment
	 * @param 
	 * @return list object
	 */
	public function auction_payment(){
		$payment_list = DB::table('sns_payment')
            ->leftJoin('sns_auction', 'sns_payment.sns_auction_id', '=', 'sns_auction.id')
			->select('sns_payment.sns_auction_id','sns_auction.event_name','sns_auction.auction_date','sns_auction.auction_status')
			->WhereNotNull('sns_payment.payment_status')
            ->groupBy('sns_payment.sns_auction_id')
            ->orderBy('sns_payment.created_at','desc')
            ->paginate(unserialize(SHOW_OPTION)[1]);	
		if($payment_list){
			return $payment_list;
		}else{
			return [];
		}
	}
	/**
     * This function is used to search payment with auction
     * @param integer $auction_id,date $date
     * @return payment object
     */
    public function searchPayment($auction_id,$date) {
        $payment_list = DB::table('sns_payment')
            ->leftJoin('sns_auction', 'sns_payment.sns_auction_id', '=', 'sns_auction.id')
			->select('sns_payment.sns_auction_id','sns_auction.event_name','sns_auction.auction_date','sns_auction.auction_status');        
        if($auction_id > 0){
            $payment_list->where('sns_payment.sns_auction_id',$auction_id);
        }
		if(!empty($date)){
           $payment_list->where('sns_auction.auction_date',$date); 
        }
        $payment_list->groupBy('sns_payment.sns_auction_id');
        $payment_list->orderBy('sns_payment.created_at','desc');
        return $payment_list->paginate(unserialize(SHOW_OPTION)[1]);
    }
	/**
     * This function is used to get total revenue
     * @param 
     * @return revenue 
     */
    
    public function revenue(){
        return $revenue = PaymentManage::select(DB::raw('sum(amount) AS revenue'))->where('payment_status',1)->get();
    }

    public function getPayment($aution_id, $user_id, $amount = null, $item_id = null, $type = null){

        $payment_list = PaymentManage::where('sns_auction_id', $aution_id)
            ->where('sns_user_id', $user_id)
            ->whereNull('payment_status');

        if($item_id){
            $payment_list->where('sns_item_id', $item_id);
        }

        $payment_list = $payment_list->first();

		if($payment_list){
            if($type){
                $payment_list->amount = $amount;
                $payment_list->save();
            }
			return $payment_list;
		}else{
			return null;
		}
	}
    /** 
	 * This function is used to auction payment
	 * @param 
	 * @return list object
	 */
    public function get_auction_item_payment($auction_id){
        $auction_item_payment = DB::table('sns_payment')
        ->leftJoin('sns_auction','sns_auction.id', '=', 'sns_payment.sns_auction_id')
        ->leftJoin('sns_item','sns_item.id', '=', 'sns_payment.sns_item_id')
        ->leftJoin('sns_auction_detail','sns_item.id', '=', 'sns_auction_detail.sns_item_id')
        ->leftJoin('sns_customer_register','sns_payment.sns_user_id', '=', 'sns_customer_register.id')
        ->leftJoin('sns_payment_transaction', 'sns_payment.id', '=', 'sns_payment_transaction.sns_payment_id')
        ->leftJoin('sns_customer_auction', function($query) use($auction_id){
            $query->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id')
                    ->where('sns_customer_auction.auction_id', '=', $auction_id);
        })
        ->select('sns_item.name','sns_item.item_code', DB::raw('CONCAT(sns_customer_register.fname, " ", sns_customer_register.lname) AS customer_name'),'sns_payment.sns_item_id','sns_payment.amount','sns_payment.payment_date','sns_payment.payment_status','sns_payment_transaction.transaction_id', 'sns_payment_transaction.transaction_time','sns_customer_register.id as register_id', 'sns_payment.reference_no', 'sns_customer_auction.card_no')
        ->where('sns_payment.sns_auction_id', $auction_id)
        ->WhereNotNull('sns_payment.payment_status')
        ->orderBy('sns_payment.created_at','desc')
        ->paginate(unserialize(SHOW_OPTION)[1]);
        if($auction_item_payment){
            return $auction_item_payment;
        }else{
            return [];
        }
    }
    /**
     * This function is used to search payment with item
     * @param String $item,String,String $customer,date $date, integer $status integer $type
     * @return payment object
     */
    public function search_auction_item_payment($auction_id,$item,$customer,$type,$date,$status){ 
        $auction_item_payment = DB::table('sns_payment')
                                ->leftJoin('sns_auction','sns_auction.id', '=', 'sns_payment.sns_auction_id')
                                ->leftJoin('sns_item','sns_item.id', '=', 'sns_payment.sns_item_id')
                                ->leftJoin('sns_auction_detail','sns_item.id', '=', 'sns_auction_detail.sns_item_id')
                                ->leftJoin('sns_customer_register','sns_payment.sns_user_id', '=', 'sns_customer_register.id')
                                ->leftJoin('sns_payment_transaction', 'sns_payment.id', '=', 'sns_payment_transaction.sns_payment_id')
                                ->leftJoin('sns_customer_auction', function($query) use($auction_id){
                                    $query->on('sns_customer_auction.user_id', '=', 'sns_customer_register.id')
                                          ->where('sns_customer_auction.auction_id', '=', $auction_id);
                                })
                                ->select('sns_item.name','sns_item.item_code', DB::raw('CONCAT(sns_customer_register.fname, " ", sns_customer_register.lname) AS customer_name'),'sns_payment.sns_item_id','sns_payment.amount','sns_payment.payment_date','sns_payment.payment_status','sns_payment_transaction.transaction_id', 'sns_payment_transaction.transaction_time','sns_customer_register.id as register_id', 'sns_payment.reference_no', 'sns_customer_auction.card_no')
                                ->WhereNotNull('sns_payment.payment_status');
        if(!empty($item)){
            $auction_item_payment = $auction_item_payment->where('sns_item.name','LIKE','%' .$item. '%')->orWhere('sns_item.item_code','LIKE','%' .$item. '%');
        }
        
        if(!empty($date)){

            $auction_item_payment = $auction_item_payment->whereDate('sns_payment.payment_date', '=', $date);
        }
        if($status > 0){
            if($status == 1){
                $auction_item_payment = $auction_item_payment->where('sns_payment.payment_status',1);
            }else{
                $auction_item_payment = $auction_item_payment->where('sns_payment.payment_status',-1);
            }
        }
        if($type > 0){
            if($type == 1){
                $auction_item_payment = $auction_item_payment->where('sns_payment.sns_item_id','<>',0);
            }else{
                $auction_item_payment = $auction_item_payment->where('sns_payment.sns_item_id','=',0);
            }
        }
        $auction_item_payment = $auction_item_payment->where('sns_payment.sns_auction_id',$auction_id);
        $auction_item_payment = $auction_item_payment->orderBy('sns_payment.created_at','desc');
        return $auction_item_payment = $auction_item_payment->paginate(unserialize(SHOW_OPTION)[1]);
    }

    /**
     * This function is used to search payment item details
     * @param String $item_id
     * @return payment object
     */
    public function viewPaymentItem($item_id){
        $payment_manage = PaymentManage::with('item.auction_detail.highest_bid.win_bid')->where('sns_item_id',$item_id)->where('payment_status',1)->get();
        if($payment_manage){
            return $payment_manage;
        }else{
            return [];
        }
    }

    /**
     * This function is used to get payment details by refNo
     * @param String $refNo
     * @return payment object
     */
    public function getPaymentByRef($ref){
        $payment = PaymentManage::with(['payment_transaction'])->where('reference_no',$ref)->where('payment_status',1)->first();
        if($payment){
            return $payment;
        }else{
            return [];
        }
    }

    /**
     * This function is used to get payment details by payment_id
     * @param String $id
     * @return payment object
     */
    public function paymentDetails($id){
        $payment = PaymentManage::with(['payment_transaction','customer.user','auction','item'])->find($id);
        if($payment){
            return $payment;
        }else{
            return [];
        }
    }

    //GET PAYMENT DETAILS
    public function getPaymentDetails($auction_id, $customer_id){
        $payment = PaymentManage::where('sns_auction_id', $auction_id)
            ->where('sns_item_id', 0)
            ->where('payment_status', DEFAULT_STATUS)
            ->where('sns_user_id', $customer_id)
            ->get();
            
        if(sizeof($payment) > 0){
            return $payment;
        }else{
            return [];
        }
    }

    //This function is used to validate payment pending customers
    public function paymentPending($customer_id, $item_id){
        $detail = AuctionDetailsManage::where('sns_item_id', $item_id)->whereNull('deleted_at')->first();
        if($detail && sizeof($detail) > 0){
            $succes_payment_have = PaymentManage::whereIn('sns_item_id', function($query) use($customer_id, $detail) {
            $query->from('sns_auction_detail')
            ->select('sns_item_id')
                  ->whereNull('deleted_at')
                  ->whereIn('id', function($query) use($customer_id, $detail){
                      $query->from('item_approve')
                      ->select('detail_id')
                      ->where('customer_id', $customer_id)
                      ->where('detail_id', $detail->id)
                      ->whereNull('deleted_at');
                    });
                })->where('payment_status', PAYMENT_SUCCESS)
                ->first();
                
                if($succes_payment_have){
                    return $succes_payment_have;
                }else{
                    return [];
                } 
        }else{
            return [];
        }
                
    }

}

<?php

Route::group(['middleware' => ['auth']], function() {
    Route::group(array('prefix'=>'payment', 'namespace' => 'App\Modules\PaymentManage\Controllers'), function() {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'auction.list', 'uses' => 'PaymentManageController@auctionList'
        ]);
        Route::get('search', [
            'as' => 'search.auction.list', 'uses' => 'PaymentManageController@searchAuctionList'
        ]);
        Route::get('payment-list/{id}', [
            'as' => 'payment.list', 'uses' => 'PaymentManageController@auctionPaymentList'
        ]);
        Route::get('search-payment', [
            'as' => 'search.payment.list', 'uses' => 'PaymentManageController@searchAuctionPaymentList'
        ]);
        Route::get('item-payment', [
            'as' => 'view.payment.list', 'uses' => 'PaymentManageController@viewItemPayment'
        ]);
    });
});
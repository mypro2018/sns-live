<?php namespace App\Modules\PaymentManage\Controllers;
/**
* PaymentManageController class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\PaymentManage\BusinessLogics\Logic AS Payment;
use App\Modules\AuctionManage\BusinessLogics\Logic AS Auction;

use Illuminate\Http\Request;

class PaymentManageController extends Controller {

	protected $payment;
	protected $auction;
	public function __construct(Payment $payment,Auction $auction){
        $this->payment = $payment;
		$this->auction = $auction;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

    /**
     * Display a listing of the payment.
     *
     * @return Response
     */
    public function auctionList()
    {
		$auction_list = $this->auction->getAllAuctions();
		$payment_list = $this->payment->auction_payment();
        return view("PaymentManage::index")->with(['auction_list' => $auction_list,'auction' => '','payment' => $payment_list,'date' => '']);
    }

	/**
     * This Function is used to search payment
     * @param  
     * @return Response
     */
    public function searchAuctionList(Request $request)
    {
		$auction_list = $this->auction->getAllAuctions();
		$payment_list = $this->payment->searchPayment($request->get('auction'),$request->get('date'));
        return view("PaymentManage::index")->with(['auction_list' => $auction_list,'payment' => $payment_list,'auction' => $request->get('auction'),'date' => $request->get('date')]);
    }
	/**
     * This Function is used to get auction payment list
     * @param  
     * @return Response
     */
	public function auctionPaymentList($id){
		$auction_payment_list = $this->payment->get_auction_item_payment($id);
		$auction 			  = $this->auction->getAuctionOnly($id);
		return view("PaymentManage::payment")->with([
			'auction_payment_list' => $auction_payment_list, 'item' => '', 'customer' => '', 'type' => '', 'date' => '', 'status' => '', 'auction_id' => $id, 'auction' => $auction]);
	}
	/**
     * This Function is used to search auction payment list
     * @param  
     * @return Response
     */
	public function searchAuctionPaymentList(Request $request){
		$auction_payment_list = $this->payment->search_auction_item_payment($request->get('auction'),$request->get('item'),$request->get('customer'),$request->get('type'),$request->get('date'),$request->get('status'));
		$auction 			  = $this->auction->getAuctionOnly($request->get('auction'));
		return view("PaymentManage::payment")->with(['auction_payment_list' => $auction_payment_list,'item' => $request->get('item'),'customer' => $request->get('customer'),'type' => $request->get('type'),'date' => $request->get('date'),'status' => $request->get('status'),'auction_id' => $request->get('auction'), 'auction' => $auction]);
	}
	/**
     * This Function is used to view item payment
     * @param  
     * @return Response
     */
	 public function viewItemPayment(Request $request){
		$item_payment = $this->payment->viewPaymentItem($request->get('item_id'));
		return response()->json(['item_payment' => $item_payment]);
	 }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

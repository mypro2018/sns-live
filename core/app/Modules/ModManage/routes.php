<?php

Route::group(['middleware' => ['auth']], function(){
	
    Route::group(['prefix' => 'mod', 'namespace' => 'App\Modules\ModManage\Controllers'],function(){

   	 	Route::resource('posts', 'PostsController');
    	
	});  	
}); 


@extends('layouts.back_master') @section('title','Product List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/boostrap-toggle/css/bootstrap-toggle.min.css')}}" type="text/css"/>
<style type="text/css">
.img-circle{
    width:100px;
    height:100px;
}
.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
.toggle.ios .toggle-handle { border-radius: 20px; }
.go-event{
  width: 80px;
  border-radius:0px !important;
}
.toggle.android { border-radius: 0px;}
.toggle.android .toggle-handle { border-radius: 0px; }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Item
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Item List</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
	<div class="box">
    <form role="form" method="get" action="{{url('item/search')}}">	    
      <div class="box-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="search" placeholder="Search Item" value="{{$item}}" autocomplete="off">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="auction">
                @if(count($auction_list) > 0)
                  <option value="">-- Search All Auctions --</option>
                  @foreach($auction_list as $key => $val)
                    <option value="{{$val->id}}" @if($val->id == $select_auction) selected @endif>{{$val->event_name}} - {{$val->name}} - {{$val->auction_date}}</option>
                  @endforeach
                @else
                  <option value="">-- No Auctions --</option>
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-3">
             <div class="form-group">
                 <select class="form-control chosen" name="supplier">
                     <option value="">-- Search All Suppliers --</option>
                     @if($supplier_list)
                        @foreach($supplier_list as $supplier => $val)
                          <option value="{{$val->id}}" @if($val->id == $supplier_id) selected @endif>{{$val->fname.' ('.$val->supplier_code.')'}}</option>
                        @endforeach
                      @endif
                 </select>
             </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
             <select class=" form-control chosen" name="status">
                 <option value="-1">-- All Status --</option>
                 <option value="{{PRODUCT_AVILABLE}}" @if(PRODUCT_TO_BEGIN == $status) selected @endif>Available</option>
                 <option value="{{PRODUCT_TO_LIVE}}" @if(PRODUCT_TO_LIVE == $status) selected @endif>Live</option>
                 <option value="{{PRODUCT_SOLD}}" @if(PRODUCT_SOLD == $status) selected @endif>Sold</option>
                 <option value="{{PRODUCT_NOT_AVAILABLE}}" @if(PRODUCT_NOT_AVAILABLE == $status) selected @endif>Not Available</option>
             </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <select class="form-control input-sm chosen" name="category">
                @if(count($category_list) > 0)
                  <option value="-1" @if(-1 == $category) selected @endif>-- Search All Categories --</option>
                  <option value="0" @if(0 == $category) selected @endif>Not Assigned</option>
                  @foreach($category_list as $key => $val)
                    <option value="{{$key}}" @if($key == $category) selected @endif>{{$val}}</option>
                  @endforeach
                @else
                  <option value="">-- No Categories --</option>
                @endif
              </select>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="input-daterange input-group" id="datepicker">
					    <input type="text" class="input-sm form-control" name="start" placeholder="From" value="{{$start}}" autocomplete="off"/>
					    <span class="input-group-addon">to</span>
					    <input type="text" class="input-sm form-control" name="end" placeholder="To" value="{{$end}}" autocomplete="off"/>
					  </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-sm btn-default" id="plan"><i class="fa fa-search" ></i> Search</button>
          <a href="list" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
        </div>
      </div>
    </form>      
  </div>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Item List</h3>
      <a href="{{url('item/add')}}" class="btn bg-purple btn-sm pull-right">New Item</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
            <thead>
              <tr>
                <th width="5%">#</th>
                <th width="10%">Image</th>
                <th width="6%">Item Code</th>
                <th width="25%">Item Name</td>
                <th width="10%">Category</td>
                <th width="15%">Auction</td>
                <th width="5%">Show/Hide Code</th>
                <th width="10%">Status</th>
                <th width="8%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = ($all_item->currentpage()-1) * $all_item->perpage() + 1; ?>
              @if(count($all_item) > 0)
                @foreach($all_item as $result_val)
                  @include('ProductManage::template.product')
                <?php $i++;?>
                @endforeach
              @else
                <tr><td colspan="7" class="text-center">No data found.</td></tr>
              @endif
            </tbody>
          </table>        
        </div>
      </div>
    </div>
    @if(count($all_item) > 0)
      <div class="box-footer">
        Showing {{$all_item->firstItem()}} to {{$all_item->lastItem()}} of {{$all_item->total()}} Product      
        <div class="pull-right">{!! $all_item->appends($_GET)->render() !!}</div>
      </div>
    @endif
  </div>
</section>
@stop


@section('js')
<script src="{{asset('assets/dist/boostrap-toggle/js/bootstrap-toggle.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
//Show & Hide Online Bidder Name
$('.show-hide-code').on('change', function(e) {
    var $this         = $(this);
    var product_id    = $(this).data('product');
    if(this.checked) {
      $(this).val(1);
    }else{
      $(this).val(0);
    }
    var status = $(this).val();
    $.ajax({
      url   : "{{URL::to('item/show-hide-code')}}",
      method: 'GET',
      data  : {
        'product_id': product_id,
        'status'    : status
      },
      async : false,
      success : function (data) {
        if(status == '{{BIDDING_ACTIVE}}'){
          toastr.success('Showing product code');
        }else{
          toastr.warning('Hiding product code');
        }
      },error: function () {
        toastr.error('Error Occurred !..Try Again');
      }
    });
  });
});

function viewDetail(item_id) {
  
  $(".box").addClass('panel-refreshing');

  var content = '<div class="row">'+
    '<div class="col-lg-12 form-group">'+
      '<label>Item Category</label>'+
        '<input type="text" class="form-control input-sm" id="itemCategory" placeholder="Item Category" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Lot No</label>'+
        '<input type="text" class="form-control input-sm" id="lotNo" placeholder="Lot No" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Item Name</label>'+
        '<input type="text" class="form-control input-sm" id="itemName" placeholder="Item Name" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Display Name</label>'+
        '<input type="text" class="form-control input-sm" id="displayName" placeholder="Display Name" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Supplier</label>'+
        '<input type="text" class="form-control input-sm" id="supplier" placeholder="No Supplier" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Description</label>'+
        '<textarea class="form-control input-sm" rows="1" id="description" placeholder="No Description" readonly></textarea>'+
    '</div>'+
    '<div class="col-lg-12 form-group">'+
      '<label>Created Date</label>'+
      '<input type="text" class="form-control input-sm" id="created_at" placeholder="Created Date" readonly>'+
    '</div>'+
    '<div class="col-lg-12 form-group media-sec">'+
      '<label>Images</label>'+
      '<div class="row media-object"></div>'+
    '</div>'+

  '</div>';

  $.ajax({
      url: "{{URL::to('item/details')}}",
      method: 'GET',
      data: {'item_id': item_id},
      async: false,
      success: function (data) {
        if(data != ''){
          $.confirm({
            title: 'Item Details',
            theme: 'material',
            content: content,
            columnClass: 'large',
            onContentReady: function () {
              var self = this;
              if(data.details[0].category == null){
                self.$content.find('#itemCategory').val('-');
              }else{
                self.$content.find('#itemCategory').val(data.details[0].category.name);
              }
              self.$content.find('#lotNo').val(data.details[0].item_code);
              self.$content.find('#itemName').val(data.details[0].name);
              self.$content.find('#displayName').val(data.details[0].display_name);
              self.$content.find('#supplier').val((data.details[0].supplier)?data.details[0].supplier.fname+' '+data.details[0].supplier.lname:'');
              self.$content.find('#description').val(data.details[0].description);
              self.$content.find('#created_at').val(data.details[0].created_at);
              
              var str = '';
              if(data.details[0].images.length > 0){
                 $.each(data.details[0].images, function( key, value ) {
                  str +='<div class="col-lg-3" style="padding-top:15px;"><img src="{{url()}}'+'/core/storage/uploads/images/item/'+value.image_path+'"></div>';
                });
                self.$content.find('.media-object').html(str); 
              }else{
                self.$content.find('.media-object').html('<center>No Images To Show</center>');
              }
            },
            buttons: {
              close: function () {}
            }
          });
        } else {}

        $(".box").removeClass('panel-refreshing');
      },
      error: function () {
        alert('error');
      }
  });
}

//Delete Item
function deleteItem(item_id){
  $.confirm({
    title: 'Delete Item',
    theme: 'material',
    content: 'Do you want to delete item ?',
    buttons: {
      confirm: function () {
        $.ajax({
          url   : "{{URL::to('item/delete')}}",
          method: 'GET',
          data  : {'item_id': item_id},
          async : false,
          success: function (data) {
            if(data.response){
              $.confirm({
                title   : 'Success!',
                content : 'Item removed',
                type    : 'green',
                buttons: {
                    omg: {
                        text: 'Ok',
                        btnClass: 'btn-green',
                    }
                }
              });
              document.getElementById(item_id).remove();
            }else{
              $.confirm({
                title   : 'Error!',
                content : 'Error Occurred',
                type    : 'red',
                buttons : {
                    omg : {
                        text: 'Ok',
                        btnClass: 'btn-red',
                    }
                }
              });  
            }
          }
          ,error: function () {
            $.confirm({
              title   : 'Error!',
              content : 'Error Occurred',
              type    : 'red',
              buttons : {
                  omg: {
                      text: 'Ok',
                      btnClass: 'btn-red',
                  }
              }
            });
          }
        });
      },
      cancel: function () {}
    }
  });
  
}
</script>
@stop

@extends('layouts.back_master') @section('title','Add Item')
@section('css')
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Item
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('item/list')}}}">Item List</a></li>
		<li class="active">Add Item</li>
	</ol>
</section>


{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
        <form role="form" class="form-horizontal form-validation" method="post" enctype="multipart/form-data">
    		<div class="box-header with-border">
    			<h3 class="box-title">Add Item</h3>
    		</div>
    		<div class="box-body">    		    
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('category')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Category <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <select class="form-control chosen" name="category">
                            <option value="">-- Select Item Category --</option>
                            @if($category_list)
                                @foreach($category_list as $cat => $val)
                                  <option value="{{$cat}}" @if($cat == Input::old('category')) selected @endif>{{$val}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('category'))
                            <label id="label-error" class="error" for="label">Please Select Category</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('lot_no')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Code(Lot No) <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="lot_no" placeholder="Lot No" value="{{Input::old('lot_no')}}">
                        @if($errors->has('lot_no'))
                            <label id="label-error" class="error" for="label">{{$errors->first('lot_no')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('item_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="item_name" placeholder="Item Name" value="{{Input::old('item_name')}}">
                        @if($errors->has('item_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('item_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Display Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="dis_name" placeholder="Item Display Name" value="{{Input::old('dis_name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Supplier</label>
                    <div class="col-sm-10">
                        <select class="form-control chosen" name="supplier">
                            <option value="0">-- Select Supplier --</option>
                            @if($supplier_list)
                                @foreach($supplier_list as $val)
                                  <option value="{{$val->id}}"@if($val->id == Input::old('supplier')) selected @endif>{{$val->fname." (".$val->supplier_code.")"}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                         <textarea class="form-control" rows="1" name="description" placeholder="Write Something About Item">{{Input::old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Image(s)</label>
                    <div class="col-sm-10">
                        <input id="input-id" type="file" name="file[]" multiple>
                    </div>
                    <label class="control-label pull-right" style="margin-right:15px;"><small>Image Size should be 400px * 300px - Use this <a href="https://resizeimage.net/" target="_blank">https://resizeimage.net</a> to resize image</small></label>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="submit" class="btn btn-sm bg-purple pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
	</div>
</section>
@stop


@section('js')
<script type="text/javascript">
    $("#input-id").fileinput({
        uploadUrl: "",
        maxFileSize: 2000,
        allowedFileExtensions: ["jpg", "JPG", "png", "PNG", "Jpeg"],
        allowedFileTypes: ["image"],
        showRemove: false,
        showUpload: false
    });
</script>
@stop

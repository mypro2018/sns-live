<tr id="{{$result_val->id}}">
    <td class="">{{$i}}</td>
    <td>
        @if(!empty($result_val->image_path))
            <img src="{{ url('core/storage/uploads/images/item/'.$result_val->image_path) }}" class="img-circle" alt="Showing Image"/>
        @else
            <img class="img-circle" src="{{ url('core/storage/uploads/images/item/empty.jpg') }}"/>
        @endif
    </td>
    <td><b>{{$result_val->item_code}}</b></td>
    <td>{{$result_val->name}}</td>
    <td>{{$result_val->category}}</td>
    <td>{{$result_val->auction}}</td>
    <td>
        @if($result_val->show_hide_code == BIDDING_ACTIVE)
            <input type="checkbox" data-toggle="toggle" data-size="small" data-on="Hide" data-off="Show" class="show-hide-code" value="{{$result_val->show_hide_code}}" data-product="{{$result_val->id}}" data-style="android" data-onstyle="default" data-offstyle="primary" checked>
        @else
            <input type="checkbox" data-toggle="toggle" data-size="small" data-on="Hide" data-off="Show" class="show-hide-code" value="{{$result_val->show_hide_code}}" data-product="{{$result_val->id}}" data-style="android" data-onstyle="default" data-offstyle="primary">
        @endif
    </td>
    @if($result_val->status == PRODUCT_TO_BEGIN)
        <td><i class="fa fa-gavel" aria-hidden="true"></i> Available</td>
    @elseif($result_val->status == PRODUCT_TO_LIVE)
        <td><i class="fa fa-bullhorn" aria-hidden="true"></i> Live</td>
    @elseif($result_val->status == PRODUCT_TO_OVER)
        <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Sold</td>
    @else
        <td><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Not Available</td>
    @endif
    <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View Item" onclick="viewDetail({{$result_val->id}})"><i class="fa fa-eye view"></i></a>
            <a href="{{ route('item.edit', ['id' => $result_val->id, 'code' => Request::get('code'),'item' => Request::get('item'), 'sup' => Request::get('supplier'), 'status' => Request::get('status')]) }}" class="btn btn-xs btn-default @if($result_val->status == PRODUCT_TO_OVER) disabled @endif" data-toggle="tooltip" data-placement="top" title="Edit Item"><i class="fa fa-pencil"></i></a>
            <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="@if($result_val->status == PRODUCT_TO_LIVE) Item now in live auction @else Delete Item @endif" onclick="@if($result_val->status != PRODUCT_TO_LIVE)deleteItem({{$result_val->id}})@endif" @if($result_val->status == PRODUCT_TO_LIVE) disabled @endif><i class="fa fa-trash-o"></i></a>
        </div>
     </td>
</tr>


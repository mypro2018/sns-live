<?php namespace App\Modules\ProductManage\Controllers;


/**
* ProductManageController class
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\ProductManage\BusinessLogics\Logic;
use App\Modules\CategoryManage\BusinessLogics\Logic as CategoryLogic;
use App\Modules\SupplierManage\BusinessLogics\Logic as SupplierLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use Illuminate\Http\Request;
use Exception;
use Response;
use Intervention\Image\Facades\Image;

class ProductManageController extends Controller {

	protected $product;
	protected $category;
    protected $supplier;
    protected $auction;

    public function __construct(Logic $logic,CategoryLogic $category,SupplierLogic $supplier, AuctionLogic $auction){
        $this->product  = $logic;
        $this->category = $category;
        $this->supplier = $supplier;
        $this->auction  = $auction;
    }

	/**
	 * This function is used to display item add form
	 *
	 * @return Response
	 */
	public function index(){
		$category_list = $this->category->getCategoryName();
		$supplier_list = $this->supplier->getSupplierList();
		return view("ProductManage::add")->with([
			'category_list' => $category_list,
			'supplier_list' => $supplier_list,
            'cat_id' => '',
            'supplier_id' => '']);
	}

	/**
	 * This function is used to add item 
	 */
	public function addItem(Request $request){
		//validate user input
        $this->validate($request,[
            'category' 	=> 'required',
            'lot_no'    => 'required|unique:sns_item,item_code',
            'item_name' => 'required'
        ]);
        //check category display name empty & if empty then assign name otherwise keep as it is.
        empty($request->dis_name) ? $display_name = $request->item_name : $display_name = $request->dis_name;
        $product = $this->product->addProduct([
            'sns_item_category_id' => $request->category,
            'sns_supplier_id'      => $request->supplier,
            'item_code'            => $request->lot_no,
            'name'       		   => $request->item_name,
            'display_name'         => $display_name,
            'description'          => $request->description,
            'status'               => PRODUCT_TO_BEGIN
        ]);
        try {
            //check image is browse
            if($request->hasFile('file')){
                $files = $request->file('file'); //get file
                $i = 0;
                $img_arr = array();
                foreach($files as $file){
                    $extn = $file->getClientOriginalExtension(); //get extension
                    $fileName = 'item-'.$product.date('YmdHis').$i.'.'.$extn; //set file name
                    $destinationPath = storage_path('uploads/images/item'); // destination
                    $file->move($destinationPath, $fileName);//move to file
                    //resize image
                    // $img = Image::make($destinationPath . '/' . $fileName)->fit(400,400, function ($c) {
                    //     $c->upsize();
                    // },"top");
                    //save
                    //$img->save($destinationPath . '/' . $fileName);
                    array_push($img_arr,['sns_item_id' => $product,'image_path' => $fileName]);
                    $i++;
                }
                $images = $this->product->addImages($img_arr);
            }


            return redirect( 'item/add' )->with([
                'success' => true,
                'success.message' => 'Item added successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return redirect('item/add')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
                ])->withInput();
        }
	}

    /**
     * This function is used to display all Items
     */
    public function listItem(){
    	$supplier_list = $this->supplier->getSupplierList();
        $all_item      = $this->product->getAllItem();
        $auction_list  = $this->auction->getAllAuctions();
        $category_list = $this->category->getCategoryName();
        return view("ProductManage::list")->with([
            'supplier_list' => $supplier_list,
            'all_item'      => $all_item,
            'item'          => '',
            'supplier_id'   => '',
            'status'        => '-1',
            'auction_list'  => $auction_list,
            'select_auction'=> '',
            'start'         => '',
            'end'           => '',
            'category_list' => $category_list,
            'category'      => '-1'
        ]);
    }

    /**
     * This function is used to display item images and other details
     * @return Response
     */
    public function getProductDetails(Request $request) {
        if($request->ajax()) {
            $item = $this->product->getItemDetails($request->get('item_id'));
            return Response::json(['details' => $item]);
        }else {
           return Response::json([]);
        }
    }

    /**
     * This function is used to search item
     * @return Response
     */
    public function searchItem(Request $request){
       $search_item   = $this->product->searchItem($request->get('search'),$request->get('supplier'),$request->get('auction'),$request->get('status'),$request->get('start'),$request->get('end'),$request->get('category'));
       $supplier_list = $this->supplier->getSupplierList();
       $auction_list  = $this->auction->getAllAuctions();
       $category_list = $this->category->getCategoryName();

       return view("ProductManage::list")->with([
           'supplier_list' => $supplier_list,
           'all_item'      => $search_item,
           'item'          => $request->get('search'),
           'supplier_id'   => $request->get('supplier'),
           'status'        => $request->get('status'),
           'auction_list'  => $auction_list,
           'select_auction'=> $request->get('auction'),
           'start'         => $request->get('start'),
           'end'           => $request->get('end'),
           'category_list' => $category_list,
           'category'      => $request->get('category')
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
     * This function is used to display the edit fileds.
     *
     * @param  int  $item_id
     * @return Response
     */
    public function edit($item_id,$code = null,$item = null,$status = null){
    	$category_list = $this->category->getCategoryName();
		$supplier_list = $this->supplier->getSupplierList();
        $item_details = $this->product->getItemDetails($item_id);
        $image_list = $this->product->getImage($item_id);
        if($item_details) {
            return view("ProductManage::edit")->with(['category_list' => $category_list,'supplier_list' => $supplier_list,'item_details' => $item_details,'image_list' => $image_list]);
        }else{
            return response()->view('errors.404');
        }
    }
    /**
    * This function is used to remove images .
    *
    * @param  int  $image_id
    * @return Response
    */
    public function removeImage(Request $request){
        $remove = $this->product->removeImage($request->key);
        return $request->key;
    }

	/**
	 *This function is used to update item details
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		//validate user input
        $this->validate($request,[
            'category' 	=> 'required',
            'item_name' => 'required'
        ]);

        //check category display name empty & if empty then assign name otherwise keep as it is.
        empty($request->dis_name) ? $display_name = $request->item_name : $display_name = $request->dis_name;

        try {
            $product = $this->product->updateItem($id,[
                'sns_item_category_id' => $request->category,
                'sns_supplier_id'      => $request->supplier,
                'name'       		   => $request->item_name,
                'display_name'         => $display_name,
                'description'          => $request->description,
                'status'               => PRODUCT_TO_BEGIN 
            ]);

            //check image is browse
            if($request->hasFile('file')){
                $files = $request->file('file'); //get file
                $i = 0;
                $img_arr = array();
                foreach($files as $file){
                    $extn = $file->getClientOriginalExtension(); //get extension
                    $fileName = 'item-'.$i.date('YmdHis').'.'.$extn; //set file name
                    $destinationPath = storage_path('uploads/images/item'); // destination
                    $file->move($destinationPath, $fileName);//move to file
                    //resize image
                    // $img = Image::make($destinationPath . '/' . $fileName)->fit(500,500, function ($c) {
                    //     $c->upsize();
                    // },"top");
                    //save
                    //$img->save($destinationPath . '/' . $fileName);
                    array_push($img_arr,['sns_item_id' => $id,'image_path' => $fileName]);
                    $i++;
                }
                $images = $this->product->addImages($img_arr);
            }
            if(!empty($request->get('code'))){
                return redirect('item/search?code='.$request->get('code'))->with([
                    'success' => true,
                    'success.message' => 'Item updated successfully!',
                    'success.title'   => 'Success..!'
                ]);
            }

            return redirect( 'item/edit/'.$id )->with([
                'success'         => true,
                'success.message' => 'Item updated successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return back()->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
                ])->withInput();
        }
	}
    /**
    * This function is used to approve item for the payment.
    * @param  
    * @return Response
    */
    public function approvePayment(Request $request){
        return $approve = $this->product->approveItem($request->detail_id);
    }
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
	 * This function is used to show & hide product code
	 * @param integer show_hide_status
	 * @param integer item_id
	 * @return void
	 */

	public function showHideCode(Request $request){
		if($request->ajax()){
			$response = $this->product->showHideCode($request);
			if($response){
                return response()->json(['response' => $response]);
			}
		}else{
			return response()->json([]);
		}
	}

    /**
     * This function is used to delete item
     * @param integer item_id
     * @return void
     */

     public function deleteItem(Request $request){
         if($request->ajax()){
            $delete_response = $this->product->deleteItem($request);
            return response()->json(['response' => $delete_response]);
         }else{
             return false;
         }
     }

}

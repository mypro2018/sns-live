<?php namespace App\Modules\ProductManage\Models;

/**
*
* ProductImageManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class ProductImageManage extends Model {

	/**
     * The sns_item table associated this model.
     */
    protected $table = 'sns_item_image';
    public $timestamps = true;
    protected $guarded = ['id'];

}
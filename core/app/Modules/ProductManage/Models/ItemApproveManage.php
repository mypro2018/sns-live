<?php namespace App\Modules\ProductManage\Models;

/**
*
* ItemApproveManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class ItemApproveManage extends Model {

	/**
     * The item_approve table associated this model.
     */
    protected $table = 'item_approve';
    public $timestamps = true;
    protected $guarded = ['id'];

    public function auction(){
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionManage','auction_id','id');
    }
    public function user(){
        return $this->belongsTo('Core\UserManage\Models\User','customer_id','id');
    }
    public function detail(){
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionDetailsManage','detail_id','id');
    }
    public function winItem(){
        return $this->hasOne('App\Modules\AuctionLiveManage\Models\AuctionWinBidManage','item_approve_id','id');
    }


}
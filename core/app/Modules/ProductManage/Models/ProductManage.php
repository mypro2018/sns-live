<?php namespace App\Modules\ProductManage\Models;

/**
*
* ProductManage Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductManage extends Model {
    use SoftDeletes;
	/**
     * The sns_item table associated this model.
     */
    protected $table   = 'sns_item';
    public $timestamps = true;
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutuated to dates
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];

	/** 
	 * This function is used to get a realtion detail between item and category.
	 */
	public function category(){
	    return $this->belongsTo('App\Modules\CategoryManage\Models\CategoryManage','sns_item_category_id','id');
	}

	/** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auction_detail(){
        return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_item_id','id')
        ->whereIn('status',[0,1,2])
        ->orderBy('status', 'desc')
        ->orderBy('order', 'asc');
	}

    /** This function is used to get single item image*/
    public function single_image(){
        return $this->hasOne('App\Modules\ProductManage\Models\ProductImageManage','sns_item_id','id');
    }

    /** This function is used to get all item image*/
    public function images(){
        return $this->hasMany('App\Modules\ProductManage\Models\ProductImageManage','sns_item_id','id');
    }

    /** 
     * This function is used to get a realtion detail of supplier.
     */
    public function supplier(){
        return $this->belongsTo('App\Modules\SupplierManage\Models\SupplierManage','sns_supplier_id','id');
    }

    public function watchList(){
        return $this->hasMany('App\Models\WatchList','sns_item_id','id');
    }

    /**
     * payments belongs to item
     * @return object payments
     */
    public function payment(){
        return $this->hasMany('App\Modules\PaymentManage\Models\PaymentManage','sns_item_id', 'id')
                    ->where('payment_status', '=', 1);
    }

    /**
     * auction belongs to user
     * @return object User
     */
    public function next(){
        return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_item_id','id')
            ->whereIn('status',[0,1,2,3])
            ->orderBy('status', 'desc')
            ->orderBy('order', 'asc');
    }

    /**
     * payments belongs to item
     * @return object payments
     */
    public function paid_amount(){
        return $this->hasMany('App\Modules\PaymentManage\Models\PaymentManage','sns_item_id','id')
                    ->where('payment_status', '=', 1);
                    
    }

    /** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function won_item_detail(){
        return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_item_id','id')
        ->whereIn('status',[0,1,2,3])
        ->whereNull('deleted_at')
        ->orderBy('status', 'desc')
        ->orderBy('order', 'asc');
	}


    /** 
    * This function is used to get auction relation details between auction and its detail
    */
    public function auc_detail(){
        return $this->hasOne('App\Modules\AuctionManage\Models\AuctionDetailsManage','sns_item_id','id')
        ->orderBy('status', 'desc')
        ->orderBy('order', 'asc');
    }

}
<?php namespace App\Modules\ProductManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Modules\ProductManage\Models\ProductManage;
use App\Modules\AuctionManage\Models\AuctionDetailsManage;
use App\Modules\ProductManage\Models\ProductImageManage;
use App\Modules\ProductManage\Models\ItemApproveManage;
use Illuminate\Support\Facades\Storage;
use File;

use Sentinel;


class Logic extends Model {

	/**
     * This function is used to add a item
     * @param array $data Array of data that needed to add item
     * @return object Item object if success, exception otherwise
     */
    public function addProduct($data){

		//DB::transaction(function() use($data){
	        $product = ProductManage::create($data);
            if($product){
                return $product_id = $product->id;
	        }else{
	            throw new Exception("Error occurred while adding new item");
	        }
	   //});
	}


    /**
     * This function is used to add other images in item
     * @param array $data Array of data that needed to add images
     * @return object Item object if success, exception otherwise
     */

    public function addImages($images){
        DB::transaction(function() use($images){
            $image = ProductImageManage::insert($images);
            if($image){
                return true;
            }else{
                throw new Exception("Error occurred while adding new images");
            }
        });
    }


	/**
     * This function is used to get item code
     * @return item code list object
     */
    public function getItemCode(){
        $item_code = ProductManage::lists('item_code','id');
        if(count($item_code) > 0){
            return $item_code;
        }else{
            return [];
        }
    }


    /**
     * This function is used to get item
     * @return item list object
     */
    public function getAvailableItems(){
        $details = AuctionDetailsManage::lists('sns_item_id')->toArray();
        $items   = ProductManage::where('status',0)->lists('id')->toArray();

        $available = ProductManage::select(DB::raw("CONCAT(item_code,' - ',name) AS sns_item"),'id','name')->whereIn('id', array_diff($items,$details))->whereNull('deleted_at')->get();
        /*$items = AuctionDetailsManage::join('sns_item', 'sns_item.id', '!=', 'sns_auction_detail.sns_item_id')
            ->select(['*'])
            ->get();*/

        if(count($available) > 0){
            return $available;
        }
    }


	/**
     * This function is used to get item
     * @return item list object
     */
    public function getItem(){
        $item = ProductManage::get()->lists('name','id');
        if(count($item) > 0){
            return $item;
        }
    }


    /**
     * This function is used to get all item details
     * @return item object
     */
    public function getAllItem($paginate = null){
        $paginate = unserialize(SHOW_OPTION)[1];
        $all_Item = DB::table('sns_item')->leftJoin('sns_item_category',function($join){
            $join->on('sns_item.sns_item_category_id', '=', 'sns_item_category.id');
        })
        ->leftJoin('sns_auction_detail',function($join){
            $join->on('sns_auction_detail.sns_item_id', '=', 'sns_item.id')
                 ->whereNull('sns_auction_detail.deleted_at');
        })
        ->leftJoin('sns_auction', function($join){
            $join->on('sns_auction.id', '=', 'sns_auction_detail.sns_auction_id')
                 ->whereNull('sns_auction.deleted_at');
        })
        
        ->select('sns_item.id','sns_item.item_code', 'sns_item.name', 'sns_item.status', DB::raw('IFNULL(sns_item_category.name,"Not Assigned") as category'), DB::raw('IFNULL(sns_auction.event_name,"Not Assigned") as auction'),
        DB::raw('(select image_path from sns_item_image where sns_item_id = sns_item.id limit 1) as image_path'), 'sns_item.show_hide_code')
        ->whereNull('sns_item.deleted_at')
        ->orderBy('sns_item.created_at', 'DESC')
        ->groupBy('sns_item.item_code')
        ->paginate($paginate);
        return $all_Item;
        
    }


    /**
     * This function is used to search item
     * @param integer $code, interger $supplier,integer $item,integer $status
     * @response category object
     */
    public function searchItem($search,$supplier,$auction,$status,$start,$end, $category,$paginate = null){
        $paginate = unserialize(SHOW_OPTION)[1];
        $search_item = DB::table('sns_item')->leftJoin('sns_item_category',function($join){
            $join->on('sns_item.sns_item_category_id', '=', 'sns_item_category.id');
        })
        ->leftJoin('sns_auction_detail',function($join){
            $join->on('sns_auction_detail.sns_item_id', '=', 'sns_item.id')
                 ->whereNull('sns_auction_detail.deleted_at');
        })
        ->leftJoin('sns_auction', function($join){
            $join->on('sns_auction.id', '=', 'sns_auction_detail.sns_auction_id')
                 ->whereNull('sns_auction.deleted_at');
        })
        ->select('sns_item.id','sns_item.item_code', 'sns_item.name', 'sns_item.status', DB::raw('IFNULL(sns_item_category.name,"Not Assigned") as category'), DB::raw('IFNULL(sns_auction.event_name,"Not Assigned") as auction'), 
        DB::raw('(select image_path from sns_item_image where sns_item_id = sns_item.id limit 1) as image_path'), 'sns_item.show_hide_code')
        ->whereNull('sns_item.deleted_at'); 

        if(!empty(trim($search))){
            $search_item = $search_item->where(function($sql) use($search){
                $sql->where('sns_item.item_code', 'LIKE', '%'.trim($search).'%')
                    ->orWhere('sns_item.name', 'LIKE', '%'.trim($search).'%')
                    ->orWhere('sns_item.display_name', 'LIKE', '%'.trim($search).'%');
            });
        }
        if($supplier > 0){
            $search_item = $search_item->where('sns_item.sns_supplier_id',$supplier);
        }
        
        if($status >= 0){
            $search_item = $search_item->where('sns_item.status',$status);
        }
        if(!empty($start) && !empty($end)){
            $search_item = $search_item->whereDate('sns_item.updated_at', '<=', date('Y-m-d',strtotime($start)))
            ->whereDate('sns_item.updated_at', '>=', date('Y-m-d',strtotime($end)));
        }
        if($category >= 0){
            $search_item = $search_item->where('sns_item.sns_item_category_id', '=', $category);
        }

        if($auction > 0){
            $search_item = $search_item->where('sns_auction_detail.sns_auction_id', '=', $auction);
        }
        $search_item = $search_item->groupBy('sns_item.item_code');
        $search_item = $search_item->orderBy('sns_item.created_at','desc');
        $search_item = $search_item->paginate($paginate);

        return $search_item;
    }


    /**
     * This function is used to get all item details
     * @parm  integer $item_id that need to get item details
     * @return if item details have then item object else return false
     */
    public function getItemDetails($item_id){

        $item_details = ProductManage::with(['next','supplier','images','category','payment.payment_transaction','auction_detail.highest_bid.win_bid.bidding_winner','auction_detail.auction','auction_detail.approved','auction_detail.highest_bid.win_bid','auction_detail.highest_bid.bid_user'])->where('id', $item_id)->get();

        if(count($item_details) > 0){            
            return $item_details;
        }else{
            return false;
        }
    }

    public function getAucDetails($item_id, $auction_id = null, $customer_id = null){

        $item_details = ProductManage::with(['next','supplier','images','category','payment.payment_transaction','auc_detail.highest_bid.win_bid.bidding_winner','auc_detail' => function($query) use($auction_id) {
                $query->where('sns_auction_id', $auction_id);
            },'auc_detail.auction','auc_detail.approved','auc_detail.highest_bid.win_bid' => function($query) use($customer_id) {
                    $query->where('sns_user_id', $customer_id);
            },'auc_detail.highest_bid.bid_user'])
        ->where('id', $item_id)
        ->get();

        if(count($item_details) > 0){            
            return $item_details;
        }else{
            return false;
        }
    }


    /**
     * This function is used to get won item details
     * @parm  integer $item_id that need to get item details
     * @return if item details have then item object else return false
     */
    public function wonItemDetails($item_id, $auction_id = null, $customer_id = null){
        $item_details = ProductManage::with(['next','supplier','images','category','payment.payment_transaction','won_item_detail.highest_bid.win_bid.bidding_winner','auc_detail' => function($query) use($auction_id){
                $query->where('sns_auction_id', $auction_id);
            },'won_item_detail.auction','won_item_detail.approved','won_item_detail.highest_bid.win_bid' => function($query) use($customer_id) {
                    $query->where('sns_user_id', $customer_id);
            },'won_item_detail.highest_bid.bid_user'])
        ->where('id', $item_id)->get();

        if(count($item_details) > 0){            
            return $item_details;
        }else{
            return false;
        }
    }





    /**
     * This function is used to update item
     * @param integer $item_id and array $data
     * @response is boolean
     */
    public function updateItem($item_id,$data){
        DB::transaction(function() use($item_id,$data){
            $update_item = ProductManage::where('id',$item_id)->update($data);
            if($update_item){
                return true;
            }else{
                throw new Exception("Error occurred while updating item");
            }
        });
    }

    /**
     * This function is used to get specific auction detail
     * @return auction detail object
     */
    /*public function getAuctionDetail($id) {
        $detail = AuctionDetailsManage::find($id);
        if($detail){
            return $detail;
        }else{
            return false;
        }
    }*/

    /**
     * This function is used to get specific items by category id
     * @param integer $id
     * @return auction detail object
     */
    public function getItemsByCategory($id) {

        return ProductManage::select('sns_item.*','images.image_path','auctionDetails.start_bid_price','auctionDetails.sns_auction_id')->with(['watchList'])
                ->leftJoin('sns_item_image as images', function($image_join){
                    $image_join->on('sns_item.id', '=', 'images.sns_item_id');
                })
                ->leftJoin('sns_auction_detail as auctionDetails', function($auctionDetail_join){
                    $auctionDetail_join->on('sns_item.id', '=', 'auctionDetails.sns_item_id');
                })
                ->where('sns_item.sns_item_category_id', '=', $id)
                // ->where('auctionDetails.status', '<>', 3)
                ->groupBy('sns_item.id')
                ->get();
    }
    /**
    * This function is used to get item image
    * @param integer $item_id
    * @return image object
    */
    public function getImage($item_id){
        return $images = ProductImageManage::select('image_path','id')->where('sns_item_id',$item_id)->get();
    }
    /**
    * This function is used to remove item image
    * @param integer $image_id
    * @return
    */
    public function removeImage($image_id){
       $remove_image = ProductImageManage::find($image_id);
       if($remove_image->delete()){
            File::delete(storage_path('uploads/images/item/'.$remove_image->image_path));
       }
    }
    /**
    * This function is used to approve item 
    * @param integer $detail_id
    * @return
    */
    public function approveItem($detail_id){
        $get_item_details = AuctionDetailsManage::with('highest_bid.win_bid')->where('id',$detail_id)->get();
        if(count($get_item_details) > 0){
            $approve_validate = ItemApproveManage::where('detail_id',$detail_id)->get();
            if(count($approve_validate) > 0){
                return [];
            }else{
                DB::transaction(function() use($detail_id,$get_item_details){
                    $item_approve = ItemApproveManage::create(['auction_id'  => $get_item_details[0]->sns_auction_id,
                                                               'detail_id'   => $detail_id,
                                                               'customer_id' => $get_item_details[0]->highest_bid->win_bid->sns_user_id]);
                    if($item_approve){
                        return $detail_id;
                    }else{
                        throw new Exception("Error occurred while approving item");
                    }
                });
            }
        }
    }


    /**
     * This function is used to load item
     * @param String $search
     * @param Integer $auction_id 
     * @return if item details have then item object else return empty array
     */
    public function getSearchItem($search,$auction_id){
        $search = trim($search);
        // $item   = ProductManage::select(DB::raw('concat(sns_item.item_code," - ",sns_item.name) as value'), 'sns_item.id AS data',DB::raw('concat(sns_supplier.supplier_code," - ",sns_supplier.fname) as supplier'))
        $item   = ProductManage::select('sns_item.item_code', 'sns_item.name', 'sns_item.id', DB::raw('concat(sns_supplier.supplier_code," - ",sns_supplier.fname) as supplier'))
                ->join('sns_auction_detail',function($join){
                    $join->on('sns_item.id','=','sns_auction_detail.sns_item_id');
                })
                ->leftJoin('sns_supplier',function($join){
                    $join->on('sns_item.sns_supplier_id','=','sns_supplier.id');
                })
                ->where('sns_item.item_code', '=', $search)
                ->whereNull('sns_item.deleted_at') 
                ->whereIn('sns_item.status',[PRODUCT_TO_BEGIN])
                ->whereNull('sns_auction_detail.deleted_at')  
                ->whereIn('sns_auction_detail.status',[PRODUCT_TO_BEGIN])
                ->where('sns_auction_detail.sns_auction_id', '=', $auction_id)
                ->orderBy('sns_auction_detail.id', 'DESC')
                ->groupBy('sns_auction_detail.sns_item_id')
                ->first();
        if(sizeof($item) > 0){            
            return $item;
        }else{
            return [];
        }
    }

    /**
	 * This function is used to show & hide item code
	 * @param integer show_hider_status
	 * @param integer item_id
	 * @return array auction
	 */

    public function showHideCode($request){
        if($request){
            return DB::transaction(function () use($request){
                $product                  = ProductManage::find($request->product_id);
                $product->show_hide_code  = $request->status;
                $product                  = $product->save();
                return $product;
            });
        }else{
            return [];
        }
    }

    /**
	 * This function is used to delete item
	 * @param integer item_id
	 * @return boolean 
	 */

    public function deleteItem($request){
        $delete_response = null;
        if($request){
            //delete first auction allocated product
            $delete_auction_detail = AuctionDetailsManage::where('status', PRODUCT_TO_BEGIN)->where('sns_item_id', $request->item_id)
            ->delete();
            //delete product now
            $delete_product = ProductManage::find($request->item_id);
            $delete_product->delete();
            if($delete_product){
                $delete_response = $delete_product;
            }else{
                $delete_response = $delete_product;
            }
            return $delete_response;
        }else{
            return $delete_response;
        }
    }

}

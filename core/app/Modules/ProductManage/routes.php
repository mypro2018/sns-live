<?php

Route::group(['middleware' => ['auth']], function(){


    Route::group(array('prefix'=>'item', 'namespace' => 'App\Modules\ProductManage\Controllers'), function() {

        /**
         * GET Routes
         */
        Route::get('add', [
            'as' => 'item.add', 'uses' => 'ProductManageController@index'
        ]);

        Route::get('list',[
            'as' => 'item.list','uses' => 'ProductManageController@listItem'
        ]);

        Route::get('edit/{id}',[
            'as' => 'item.edit','uses' => 'ProductManageController@edit'
        ]);

        Route::get('search',[
            'as' => 'item.list','uses' => 'ProductManageController@searchItem'
        ]);

        Route::get('details', [
            'as' => 'item.details', 'uses' => 'ProductManageController@getProductDetails'
        ]);
        Route::get('approve', [
            'as' => 'item.approve', 'uses' => 'ProductManageController@approvePayment'
        ]);
        Route::get('show-hide-code', [
            'as' => 'item-code.show-hide', 'uses' => 'ProductManageController@showHideCode'
        ]);
        Route::get('delete', [
            'as' => 'item.delete', 'uses' => 'ProductManageController@deleteItem'
        ]);
        


        /**
         * POST Routes
         */

        Route::post('add', [
            'as' => 'item.add', 'uses' => 'ProductManageController@addItem'
        ]);

        Route::post('edit/{id}', [
            'as' => 'item.update','uses' => 'ProductManageController@update'
        ]);
        Route::post('remove-image',[
            'as' => 'item.edit.remove','uses' => 'ProductManageController@removeImage'
        ]);

    });



});
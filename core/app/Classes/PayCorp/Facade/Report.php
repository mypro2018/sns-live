<?php
namespace App\Classes\PayCorp\Facade;

use App\Classes\PayCorp\Facade\BaseFacade;
use App\Classes\PayCorp\Helpers\BasicReportJsonHelper;
use App\Classes\PayCorp\Helpers\SettlementReportJsonHelper;

final class Report extends BaseFacade {
    
    public function __construct($config) {
        parent::__construct($config);
    }
    
    public function basic($request){
        $basicReportJsonHelper = new BasicReportJsonHelper();
        return parent::process($request, Operation::$REPORT_BASIC, $basicReportJsonHelper);
        
    }
    
    public function settlement($request){
        $settlementReportJsonHelper = new SettlementReportJsonHelper();
        return parent::process($request, Operation::$REPORT_SETTLEMENT, $settlementReportJsonHelper);
        
    }
    
}

<?php
namespace App\Classes\PayCorp\Enums;

class DateType {
    
    public static $EVENT = "EVENT";
    public static $SETTLEMENT = "SETTLEMENT";
}

<?php
namespace App\Classes\PayCorp\Enums;

class Network {
    
    public static $LIVE = "LIVE";
    public static $TEST = "TEST";
}

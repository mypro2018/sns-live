<?php
namespace App\Classes\PayCorp\Enums;

class SettlementGroupBy {

    public static $CUSTOMER = "CUSTOMER";
    public static $CLIENT = "CLIENT";
    public static $MERCHANT = "MERCHANT";

}

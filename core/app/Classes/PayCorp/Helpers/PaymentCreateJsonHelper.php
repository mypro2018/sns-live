<?php
namespace App\Classes\PayCorp\Helpers;

use App\Classes\PayCorp\Utils\IJsonHelper;
use App\Classes\PayCorp\Payment\PaymentCreateResponse;

class PaymentCreateJsonHelper implements IJsonHelper {
   
    public function fromJson($responseData) {
        $paymentCreateResponse = new PaymentCreateResponse();
        $paymentCreateResponse->setReqid($responseData["responseData"]["reqid"]);
        $paymentCreateResponse->setExpireAt($responseData["responseData"]["expireAt"]);
        $paymentCreateResponse->setPaymentPageUrl($responseData["responseData"]["paymentPageUrl"]);    
        
        return $paymentCreateResponse;
    }

    public function toJson($paycorpRequest) {
        
        $version = $paycorpRequest->getVersion();
        $msgId = $paycorpRequest->getMsgId();
        $operation = $paycorpRequest->getOperation();
        $requestDate = $paycorpRequest->getRequestDate();
        $validateOnly = $paycorpRequest->getValidateOnly();
        $requestData = $paycorpRequest->getRequestData();
        
        $clientId = $requestData->getClientId();
        $transactionType = $requestData->getTransactionType();
        $clientRef = $requestData->getClientRef();
        $comment = $requestData->getComment();
        $tokenize = $requestData->getTokenize();
        $tokenReference = $requestData->getTokenReference();
        $extraData = $requestData->getExtraData();
        
        $transactionAmount = $requestData->getTransactionAmount();
        $totalAmount = $transactionAmount->getTotalAmount();
        $paymentAmount = $transactionAmount->getPaymentAmount();
        $serviceFeeAmount = $transactionAmount->getServiceFeeAmount();
        $withHoldingAmount = $transactionAmount->getWithHoldingAmount();
        $currency = $transactionAmount->getCurrency();
        
        $creditCard = $requestData->getCreditCard();
        $cardType = $creditCard->getType();
        $cardHolderName = $creditCard->getHolderName();
        $cardNumber = $creditCard->getNumber();
        $cardExpiry = $creditCard->getExpiry();
        $secureId = $creditCard->getSecureId();
       
        return array(
            "version" => "$version",
            "msgId" => "$msgId",
            "operation" => "$operation",
            "requestDate" => "$requestDate",
            "validateOnly" => $validateOnly,
            "requestData" => array(
                "clientId" => $clientId,
                "transactionType" => "$transactionType",
                "transactionAmount" => array(
                    "totalAmount" => $totalAmount,
                    "paymentAmount" => $paymentAmount,
                    "serviceFeeAmount" => $serviceFeeAmount,
                    "withholdingAmount" => $withHoldingAmount,
                    "currency" => "$currency"
                ),
                "creditCard" => array(
                    "type" => "$cardType",
                    "holderName" => "$cardHolderName",
                    "number" => "$cardNumber",
                    "expiry" => "$cardExpiry",
                    "secureId" => "$secureId"
                ),
                "clientRef" => "$clientRef",
                "comment" => "$comment",
                "tokenize" => $tokenize,
                "tokenReference" => "$tokenReference", 
                )
            );
    }

}

<?php
namespace App\Classes\PayCorp\Payment;

class PaymentCreateResponse {

    private $reqid;
    private $expireAt;
    private $paymentPageUrl;

    public function getReqid() {
        return $this->reqid;
    }

    public function setReqid($reqid) {
        $this->reqid = $reqid;
    }

    public function getExpireAt() {
        return $this->expireAt;
    }

    public function setExpireAt($expireAt) {
        $this->expireAt = $expireAt;
    }
    
    function getPaymentPageUrl() {
        return $this->paymentPageUrl;
    }

    function setPaymentPageUrl($paymentPageUrl) {
        $this->paymentPageUrl = $paymentPageUrl;
    }



}

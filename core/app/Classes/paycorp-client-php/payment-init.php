<?php include 'au.com.gateway.client/GatewayClient.php'; ?>
<?php include 'au.com.gateway.client.config/ClientConfig.php'; ?>
<?php include 'au.com.gateway.client.component/RequestHeader.php'; ?>
<?php include 'au.com.gateway.client.component/CreditCard.php'; ?>
<?php include 'au.com.gateway.client.component/TransactionAmount.php'; ?>
<?php include 'au.com.gateway.client.component/Redirect.php'; ?>
<?php include 'au.com.gateway.client.facade/BaseFacade.php'; ?>
<?php include 'au.com.gateway.client.facade/Payment.php'; ?>
<?php include 'au.com.gateway.client.payment/PaymentInitRequest.php'; ?>
<?php include 'au.com.gateway.client.payment/PaymentInitResponse.php'; ?>
<?php include 'au.com.gateway.client.root/PaycorpRequest.php'; ?>
<?php include 'au.com.gateway.client.utils/IJsonHelper.php'; ?>
<?php include 'au.com.gateway.client.helpers/PaymentInitJsonHelper.php'; ?>
<?php include 'au.com.gateway.client.utils/HmacUtils.php'; ?>
<?php include 'au.com.gateway.client.utils/CommonUtils.php'; ?>
<?php include 'au.com.gateway.client.utils/RestClient.php'; ?>
<?php include 'au.com.gateway.client.enums/TransactionType.php'; ?>
<?php include 'au.com.gateway.client.enums/Version.php'; ?>
<?php include 'au.com.gateway.client.enums/Operation.php'; ?>
<?php include 'au.com.gateway.client.facade/Vault.php'; ?>
<?php include 'au.com.gateway.client.facade/Report.php'; ?>
<?php include 'au.com.gateway.client.facade/AmexWallet.php'; ?>

<?php

date_default_timezone_set('Asia/Colombo');
error_reporting(E_ALL);
ini_set('display_errors', 0);
/*------------------------------------------------------------------------------
STEP1: Build ClientConfig object
------------------------------------------------------------------------------*/
$clientConfig = new ClientConfig();
$clientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
$clientConfig->setAuthToken("af791e78-469e-4f73-bd94-b8dc3348a772");
$clientConfig->setHmacSecret("IHqpmhM0S0KHMKoE");
/*------------------------------------------------------------------------------
STEP2: Build Client object
------------------------------------------------------------------------------*/
$client = new GatewayClient($clientConfig);
/*------------------------------------------------------------------------------
STEP3: Build PaymentInitRequest object
------------------------------------------------------------------------------*/
$initRequest = new PaymentInitRequest();
$initRequest->setClientId(14000117);
$initRequest->setTransactionType(TransactionType::$PURCHASE);
$initRequest->setClientRef("merchant_reference"); //reference no
$initRequest->setComment("merchant_additional_data"); //reference data
$initRequest->setExtraData(array("ADD-KEY-1" => "ADD-VALUE-1", "ADD-KEY-2" => "ADD-VALUE-2")); //additional data
// sets transaction-amounts details (all amounts are in cents)
$transactionAmount = new TransactionAmount();
$transactionAmount->setTotalAmount(0);
$transactionAmount->setServiceFeeAmount(0);
$transactionAmount->setPaymentAmount(1010);
$transactionAmount->setCurrency("LKR");
$initRequest->setTransactionAmount($transactionAmount);
// sets redirect settings
$redirect = new Redirect();
$redirect->setReturnUrl("http://127.0.0.1:5048/systems/paycorp-client-php/payment-complete.php");
$redirect->setReturnMethod("CLOSEGET");
$initRequest->setRedirect($redirect);

/*------------------------------------------------------------------------------
STEP4: Process PaymentInitRequest object
------------------------------------------------------------------------------*/
$initResponse = $client->payment()->init($initRequest);

/*------------------------------------------------------------------------------
STEP5: Extract PaymentInitResponse object
------------------------------------------------------------------------------*/
//echo '<br><br>PCW Payment-Init Respopnse: --------------------------------------';
//echo '<br>Req Id : ' . $initResponse->getReqid();
//echo '<br>Payment Page Url : ' . $initResponse->getPaymentPageUrl();
//echo '<br>Expire At : ' . $initResponse->getExpireAt();
//echo '<br>------------------------------------------------------------------<br>';
?>

<html>
    <head></head>
    <body>
    	<br><br>
        <div style="margin-left:auto;margin-right:auto;width:550px;text-align: center;border: 1px solid #CCC;">
            <!-- <img src="shockman-logo.png" width="100px" /> -->
            <div style="height: 90px;vertical-align: middle;background-color: #F5F5F5;">
            	<img src="we-logo.png" width="500px" style="vertical-align: middle;margin-top: 13px;" />
            </div>
        	<h1 style="text-align: center;font-family: sans-serif;font-weight: 400;font-size: 25px;">Payment Gateway</h1>
        	<p style="font-family:sans-serif;">Please fill your card information</p>
            <iframe name='iframe1' id="iframe1" style="margin-left:auto;margin-right:auto;border:none;" class="col-lg-12"  height="350px" width="500px" src="<?php echo $initResponse->getPaymentPageUrl(); ?>">
        </div>
    </body>
</html>
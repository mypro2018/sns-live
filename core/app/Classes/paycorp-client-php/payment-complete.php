<?php include 'au.com.gateway.client/GatewayClient.php'; ?>
<?php include 'au.com.gateway.client.config/ClientConfig.php'; ?>
<?php include 'au.com.gateway.client.component/RequestHeader.php'; ?>
<?php include 'au.com.gateway.client.component/CreditCard.php'; ?>
<?php include 'au.com.gateway.client.component/TransactionAmount.php'; ?>
<?php include 'au.com.gateway.client.component/Redirect.php'; ?>
<?php include 'au.com.gateway.client.facade/BaseFacade.php'; ?>
<?php include 'au.com.gateway.client.facade/Payment.php'; ?>
<?php include 'au.com.gateway.client.root/PaycorpRequest.php'; ?>
<?php include 'au.com.gateway.client.root/PaycorpResponse.php'; ?>
<?php include 'au.com.gateway.client.payment/PaymentCompleteRequest.php'; ?>
<?php include 'au.com.gateway.client.payment/PaymentCompleteResponse.php'; ?>
<?php include 'au.com.gateway.client.utils/IJsonHelper.php'; ?>
<?php include 'au.com.gateway.client.helpers/PaymentCompleteJsonHelper.php'; ?>
<?php include 'au.com.gateway.client.utils/HmacUtils.php'; ?>
<?php include 'au.com.gateway.client.utils/CommonUtils.php'; ?>
<?php include 'au.com.gateway.client.utils/RestClient.php'; ?>
<?php include 'au.com.gateway.client.enums/TransactionType.php'; ?>
<?php include 'au.com.gateway.client.enums/Version.php'; ?>
<?php include 'au.com.gateway.client.enums/Operation.php'; ?>
<?php include 'au.com.gateway.client.facade/Vault.php'; ?>
<?php include 'au.com.gateway.client.facade/Report.php'; ?>
<?php include 'au.com.gateway.client.facade/AmexWallet.php'; ?>

<?php
date_default_timezone_set('Asia/Colombo');
error_reporting(E_ALL);
ini_set('display_errors', 1);
/*------------------------------------------------------------------------------
STEP1: Build ClientConfig object
------------------------------------------------------------------------------*/
$clientConfig = new ClientConfig();
$clientConfig->setServiceEndpoint("https://sampath.paycorp.com.au/rest/service/proxy");
$clientConfig->setAuthToken("af791e78-469e-4f73-bd94-b8dc3348a772");
$clientConfig->setHmacSecret("IHqpmhM0S0KHMKoE");
/*------------------------------------------------------------------------------
STEP2: Build Client object
------------------------------------------------------------------------------*/
$client = new GatewayClient($clientConfig);
/*------------------------------------------------------------------------------
STEP3: Build PaymentCompleteRequest object
------------------------------------------------------------------------------*/
$completeRequest = new PaymentCompleteRequest();
$completeRequest->setClientId(14000117);
$completeRequest->setReqid($_GET['reqid']);
/*------------------------------------------------------------------------------
STEP4: Process PaymentCompleteRequest object
------------------------------------------------------------------------------*/
$completeResponse = $client->payment()->complete($completeRequest);

/*------------------------------------------------------------------------------
STEP5: Process PaymentCompleteResponse object
------------------------------------------------------------------------------*/
//echo '<br><br>PCW Payment-Complete Respopnse: ----------------------------------';
//echo '<br>Txn Reference : ' . $completeResponse->getTxnReference();
//echo '<br>Response Code : ' . $completeResponse->getResponseCode();
//echo '<br>Response Text : ' . $completeResponse->getResponseText();
//echo '<br>Settlement Date : ' . $completeResponse->getSettlementDate();
//echo '<br>Auth Code : ' . $completeResponse->getAuthCode();
//echo '<br>Token : ' . $completeResponse->getToken();
//echo '<br>Token Response Text: ' . $completeResponse->getTokenResponseText();
//echo '<br>----------------------------------------------------------------------';
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="https://s3-ap-southeast-2.amazonaws.com/payment-pages/common/css/bootstrap-3.3.2.min.css">
</head>
<body style="height:330px;">
<div class="container">
	<div class="row">
		<div class="col-lg-12" style="height:300px;">
			<?php
				if($completeResponse->getResponseCode() == '00' || $completeResponse->getResponseCode() == '10'){
			?>
				<p style="color:#3EA73E;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Transaction was processed successfully</p>
				<p>Transaction Reference : <?php echo $completeResponse->getTxnReference() ?></p>
				<p>Transaction Time : <?php echo date('Y-m-d H:i:s') ?></p>
				<p>Merchant Reference : <?php echo $completeResponse->getClientRef() ?></p>
				<!-- <p>Response : <?php echo $completeResponse->getResponseCode().'-'.$completeResponse->getResponseText() ?></p> -->
				<!-- <p>Merchant Comment : <?php echo $completeResponse->getComment() ?></p> -->
				<!-- <p>Extra : <?php print_r($completeResponse->getExtraData()) ?></p> -->
				<div style="position:absolute;bottom:0;width:480px;">
					<button class="btn btn-primary pull-right" onclick="window.top.location.href='test.php'">Continue</button>
				</div>
			<?php 
				}elseif($completeResponse->getResponseCode() == '91' ||
					$completeResponse->getResponseCode() == '92' ||
					$completeResponse->getResponseCode() == 'A4' ||
					$completeResponse->getResponseCode() == 'C5' ||
					$completeResponse->getResponseCode() == 'T3' ||
					$completeResponse->getResponseCode() == 'T4' ||
					$completeResponse->getResponseCode() == 'U9' ||
					$completeResponse->getResponseCode() == 'X1' ||
					$completeResponse->getResponseCode() == 'X3' ||
					$completeResponse->getResponseCode() == '-1' ||
					$completeResponse->getResponseCode() == 'C0' ||
					$completeResponse->getResponseCode() == 'A6'){
			?>
				<p style="color:#FF0000;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Banking Network Temporarily Unavailable -Please try again later.</p>

				<p>Transaction Reference : <?php echo $completeResponse->getTxnReference() ?></p>
				<p>Transaction Time : <?php echo date('Y-m-d H:i:s') ?></p>
				<p>Merchant Reference : <?php echo $completeResponse->getClientRef() ?></p>
				<!-- <p>Response : <?php echo $completeResponse->getResponseCode().'-'.$completeResponse->getResponseText() ?></p> -->
				<!-- <p>Merchant Comment : <?php echo $completeResponse->getComment() ?></p> -->
				<!-- <p>Extra : <?php print_r($completeResponse->getExtraData()) ?></p> -->
				<div style="position:absolute;bottom:0;width:480px;">
					<button class="btn btn-danger pull-right" onclick="window.top.location.href='payment-init.php'">Try Again</button>
				</div>
			<?php }else{ ?>
				<p style="color:#FF0000;text-align:center;font-size: 18px;margin-bottom: 25px;margin-top: 15px;">Payment Declined - Please try an alternative card.</p>

				<p>Transaction Reference : <?php echo $completeResponse->getTxnReference() ?></p>
				<p>Transaction Time : <?php echo date('Y-m-d H:i:s') ?></p>
				<p>Merchant Reference : <?php echo $completeResponse->getClientRef() ?></p>
				<!-- <p>Response : <?php echo $completeResponse->getResponseCode().'-'.$completeResponse->getResponseText() ?></p> -->
				<!-- <p>Merchant Comment : <?php echo $completeResponse->getComment() ?></p> -->
				<!-- <p>Extra : <?php print_r($completeResponse->getExtraData()) ?></p> -->
				<div style="position:absolute;bottom:0;width:480px;">
					<button class="btn btn-danger pull-right" onclick="window.top.location.href='payment-init.php'">Alternative Card</button>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</body>
</html>
<?php
namespace App\Classes;

use Image;
use File;

class Functions
{

    //unixTimePhptoJava
    public static function unixTimePhpToJava($time)
    {
        $conTime = $time * 1000;
        return $conTime;
    }

    public static function unixTimeJavaToPhp($time)
    {
        $conTime = $time / 1000;
        return $conTime;
    }

    public static function formatDateToPhp($date)
    {
        $date_format = substr($date, 0, strlen($date) - 3);
        $time_format = substr($date, 13, strlen($date));
        return date("Y-m-d", strtotime(substr($date_format, 0, strlen($date_format) - 8))).' '.date("H:i:s", strtotime($time_format));
    }

    /**
     * Limit a string length if it is more thatn setted characters.
     *
     * @param string $text String that needs to shorten
     * @param int $maxchar Maximum characters allowed to show
     * @param string $end Which text should append at the end of the shortened string.
     *
     * @return string Shortened text
     *
     */
    public static function limit_string($text, $maxchar, $end='...'){
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                }
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        }
        else {
            $output = $text;
        }
        return $output;
    }

    public function redirectWithAlert($routeName, $alertType, $alertTitle, $alertMessage, $type = 'route'){
        try {
            if($alertType == "success")
            {
                $alertTitle = "Done!.";
            }
            else if($alertType == "error")
            {
                $alertTitle = "Notice!.";
            }
            else if($alertType == "warning")
            {
                $alertTitle = "Notice!.";
            }
            else
            {
                $alertTitle = $alertTitle;   
            }
            
            if($type == 'route'){
                return redirect()->route($routeName)->with([
                    $alertType.'.title'   => $alertTitle,
                    $alertType.'.message' => $alertMessage
                ]);
            }elseif($type == 'url'){
                return redirect($routeName)->with([
                    $alertType.'.title'   => $alertTitle,
                    $alertType.'.message' => $alertMessage
                ]);
            }else{
                throw new \Exception("Something went wrong in 'redirectWithAlert' function!.");
            }
            
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    //save image
    public function saveImage($image, $full_dir_path, $width = null, $expectWidth = null, $img_quality = 100){
        try {
            if($width !== null && $expectWidth !== null && $width > $expectWidth)
            {
                $extention = $image->getClientOriginalExtension();

                if($extention == 'gif' || $extention == 'GIF'){
                    $img_real_path = Image::make($image->getRealPath());
                    $img_real_name = $image->getClientOriginalName();
                    $gen_img_name  = date('Ymdhis')."-".$img_real_name;

                    $full_path     = $full_dir_path.'/'.$gen_img_name;
                    $db_path       = str_replace(UPLOADS_DIR, '', $full_dir_path).'/'.$gen_img_name;
                    $saved         = $image->move(storage_path().'/'.$full_dir_path, $gen_img_name);

                    if(count($saved)){
                        return ['path' => $db_path, 'count' => 1];
                    }else{
                        return ['path' => $db_path, 'count' => 0];
                    }
                    
                }else{
                    $img_real_path = Image::make($image->getRealPath());
                    $img_real_name = $image->getClientOriginalName();
                    $gen_img_name  = date('Ymdhis')."-".$img_real_name;
                    
                    $img_real_path->resize($expectWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $full_path     = $full_dir_path.'/'.$gen_img_name;
                    $db_path       = str_replace(UPLOADS_DIR, '', $full_dir_path).'/'.$gen_img_name;
                    $saved         = $img_real_path->save(storage_path($full_path), $img_quality);

                    if(count($saved)){
                        return ['path' => $db_path, 'count' => 1];
                    }else{
                        return ['path' => $db_path, 'count' => 0];
                    }
                }
            }
            else
            {
                $extention = $image->getClientOriginalExtension();

                if($extention == 'gif' || $extention == 'GIF'){
                    $img_real_path = Image::make($image->getRealPath());
                    $img_real_name = $image->getClientOriginalName();
                    $gen_img_name  = date('Ymdhis')."-".$img_real_name;
                    $full_path     = $full_dir_path.'/'.$gen_img_name;
                    $db_path       = str_replace(UPLOADS_DIR, '', $full_dir_path).'/'.$gen_img_name;
                    $saved         = $image->move(storage_path().'/'.$full_dir_path, $gen_img_name);

                    if(count($saved)){
                        return ['path' => $db_path, 'count' => 1];
                    }else{
                        return ['path' => $db_path, 'count' => 0];
                    }
                    
                }else{
                    $img_real_path = Image::make($image->getRealPath());
                    $img_real_name = $image->getClientOriginalName();
                    $gen_img_name  = date('Ymdhis')."-".$img_real_name;
                    $full_path     = $full_dir_path.'/'.$gen_img_name;
                    $db_path       = str_replace(UPLOADS_DIR, '', $full_dir_path).'/'.$gen_img_name;
                    $saved         = $img_real_path->save(storage_path($full_path), $img_quality);

                    if(count($saved)){
                        return ['path' => $db_path, 'count' => 1];
                    }else{
                        return ['path' => $db_path, 'count' => 0];
                    }
                }
            }
        } catch (\Exception $e) {
            throw new \Exception("message : ".$e->getMessage());
        }
    }

}
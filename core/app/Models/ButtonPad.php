<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * ButtonPad Model Class
 *
 *
 * @category   Models
 * @author     Lahiru Madhusanka Perera
 * @copyright  Copyright (c) 2018, <lahirul@orelit.com>
 * @version    v1.0.0
 */

class ButtonPad extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'button_pad';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutuated to dates
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];


}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Menu Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Lahiru Madhusanka Perera
 * @copyright  Copyright (c) 2018, <lahirul@orelit.com>
 * @version    v1.0.0
 */
class ItemComplete extends Model{
 	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'item_approve';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

	public function auction_detail(){
		return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionDetailsManage','detail_id','id');
	}
}

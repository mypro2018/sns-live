<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * CustomerAllowedType Model Class
 *
 *
 * @category   Models
 * @package    CustomerType
 * @author     Lahiru Madhusanka Perera
 * @copyright  Copyright (c) 2018, <lahirul@orelit.com>
 * @version    v1.0.0
 */

class CustomerAllowedType extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_allowed_type';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutuated to dates
     * 
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function customer_types(){
		return $this->belongsTo('App\Models\CustomerType', 'customer_type_id','id');
	}
}

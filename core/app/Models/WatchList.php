<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Menu Model Class
 *
 *
 * @category   Countries
 * @package    Model
 * @author     Nishan Randika <nishanran@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class WatchList extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sns_user_watchlist';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','sns_auction_id','sns_item_id','created_at'];

    public function item(){
        return $this->belongsTo('App\Modules\ProductManage\Models\ProductManage','sns_item_id','id');
    }

    public function auction(){
        return $this->belongsTo('App\Modules\AuctionManage\Models\AuctionManage','sns_auction_id','id');
    }

}

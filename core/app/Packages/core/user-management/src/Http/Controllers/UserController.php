<?php
namespace Core\UserManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;
use Core\permissions\Models\Permission;
use App\Modules\AuctionManage\Models\AuctionManage;
use App\Modules\CustomerManage\Models\CustomerAuctionManage;
use Core\UserManage\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Response;
use Sentinel;
use Hash;
use DB;


class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the User add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$user = User::all()->where('user_type','=',0)->lists('full_name' , 'id' );
		$roles = UserRole::all()->lists('name' , 'id' );	
		$user->prepend('No Supervisor', '0');
		return view( 'userManage::user.add' )->with([ 'users' => $user,
			'roles' => $roles
		 ]);;
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to menu add
	 */
	public function add(UserRequest $request)
	{
		$user = Sentinel::registerAndActivate([
			'first_name'=> $request->get( 'first_name' ),
			'last_name'	=> $request->get('last_name' ),
			'email'		=> $request->get('email' ),
			'username'	=> $request->get('user_name' ),
			'password'	=>  $request->get('password' ),
			
		]);	
		// admin user assign to all auctions
		$mass_arr = [];
		$auction = AuctionManage::all();
		foreach($auction as $auc){
			$customer_auction = CustomerAuctionManage::where('user_id','=',$user->id)->where('auction_id','=',$auc->id)->get();
            if(count($customer_auction) > 0){}else{
				array_push($mass_arr,['user_id' => $user->id,'auction_id' => $auc->id]);
			}
		}
		DB::transaction(function() use($mass_arr){
            $customer_auction = CustomerAuctionManage::insert($mass_arr);
			if(!$customer_auction){
				return redirect('user/add')->with([ 'error' => true,
				'error.message'=> 'Error Occueered !',
				'error.title' => 'Ooops!']);
			}
        });
		if($request->get('supervisor') != 0){
			$supervisor = User::find( $request->get('supervisor') );
		}else{
			$user->makeRoot();
		}

		if(isset($supervisor) && $supervisor != null){
			$user->makeChildOf($supervisor);
		}

		foreach ($request->get('roles') as $key => $value) {
			$role = Sentinel::findRoleById($value);
	   	    $role->users()->attach($user);
		}

		User::rebuild();
		return redirect('user/add')->with([ 'success' => true,
			'success.message'=> 'User added successfully!',
			'success.title' => 'Well Done!']);
	}

	/**
	 * Show the user add screen to the user.
	 *
	 * @return Response
	 */
	public function listView()
	{
		return view( 'userManage::user.list' );
	}

	/**
	 * Show the user add screen to the user.
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			 $data= User::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $user) {
				$dd = array();
				array_push($dd, $i);
				
				if($user->first_name != ""){
					array_push($dd, $user->first_name);
				}else{
					array_push($dd, "-");
				}
				if($user->last_name != ""){
					array_push($dd, $user->last_name);
				}else{
					array_push($dd, "-");
				}

				if($user->email != ""){
					array_push($dd, $user->email);
				}else{
					array_push($dd, "-");
				}
				if($user->username != ""){
					array_push($dd, $user->username);
				}else{
					array_push($dd, "-");
				}
				if($user->supervisor_id != ""){
					array_push($dd, Sentinel::findById($user->supervisor_id)->first_name.' '.Sentinel::findById($user->supervisor_id)->last_name);
				}else{
					array_push($dd, "-");
				}

				if($user->user_type == 1){
					array_push($dd, 'Customer');
				}else{
					array_push($dd, 'Administrator');
				}

				if($user->status == 1){
					array_push($dd, '<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Deactivate"><input class="user-activate" type="checkbox" checked value="'.$user->id.'"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>');
				}else{
					array_push($dd, '<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Activate"><input class="user-activate" type="checkbox" value="'.$user->id.'"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>');
				}

				$permissions = Permission::whereIn('name',['user.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\''.url('user/edit/'.$user->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit User"><i class="fa fa-pencil"></i></a>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['user.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<a href="#" class="red user-delete" data-id="'.$user->id.'" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash-o"></i></a>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate User
	 * @param  Request $request user id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$user = User::find($id);
			if($user){
				$user->status = $status;
				$user->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a User
	 * @param  Request $request user id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$user = User::find($id);
			if($user){
				$user->delete();
				User::rebuild();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the user edit screen to the user.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$user = User::all()->where('user_type','=','0')->lists('full_name' , 'id' );
		$user->prepend('No Supervisor', '0');
	    $curUserold = User::with(['roles'])->find($id);
	    $curUser=$curUserold;  
	    $srole = array();
	    foreach ($curUser->roles as $key => $value) {
	    	array_push($srole, $value->id);
	    }

	    $roles = UserRole::all()->lists('name' , 'id' );
		if($curUser){
			return view( 'userManage::user.edit' )->with([ 
				'curUser' => $curUser,
				'users'=>$user,
				'roles'=>$roles,
				'srole'=>$srole]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to menu add
	 */
	public function edit(UserRequest $request, $id)
	{
   
		$userOld =  User::with(['roles'])->find($id);
		$user=$userOld;			
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email' );
		$user->username	=$request->get('user_name' );

		if($request->get('supervisor') != 0){
			$supervisor = User::find( $request->get('supervisor') );
		}else{
			$supervisor = null;
			$user->makeRoot();
		}

		if($supervisor && ($supervisor->id != $user->supervisor_id || $supervisor->id != $user->id)){
			$user->makeChildOf($supervisor);
		}

		foreach ($user->roles as $key => $value) {
			$role = Sentinel::findRoleById($value->id);
			$role->users()->detach($user);				
		}		
		//Remove Role for current user		
	   	
		$user->save();
		//attach user for role
		foreach ($request->get( 'roles' ) as $key => $value) {
			$role = Sentinel::findRoleById($value);
	   	    $role->users()->attach($user);
		}

		User::rebuild();
		return redirect( 'user/list' )->with([ 'success' => true,
			'success.message'=> 'User updated successfully!',
			'success.title' => 'Good Job!' ]);
	}
}

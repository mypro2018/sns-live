@extends('layouts.back_master') @section('title','Edit Menu')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.chosen-container{
		font-family: 'FontAwesome', 'Open Sans',sans-serif;
	}
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	User 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('user/list')}}}">User List</a></li>
		<li class="active">Edit User</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit User</h3>
		</div>
		<div class="box-body">
			<form role="form" class=" form-validation" method="post">
      			{!!Form::token()!!}

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              			<div class="form-group">
                    		<label class="control-label required">First Name <span class="require">*</span></label>            		
                			<input type="text" class="form-control input-sm @if($errors->has('first_name')) error @endif" name="first_name" placeholder="First Name" required value="{{$curUser->first_name}}">
                			@if($errors->has('first_name'))
                				<label id="label-error" class="error" for="label">{{$errors->first('first_name')}}</label>
                			@endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="control-label required">Last Name <span class="require">*</span></label> 
                            <input type="text" class="form-control input-sm @if($errors->has('last_name')) error @endif" name="last_name" placeholder="Last Name" required value="{{$curUser->last_name}}">
                            @if($errors->has('last_name'))
                                <label id="label-error" class="error" for="label">{{$errors->first('last_name')}}</label>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                    		<label class="control-label required">E-mail <span class="require">*</span></label>            		
                			<input type="text" class="form-control input-sm @if($errors->has('email')) error @endif" name="email" placeholder="Email" required value="{{$curUser->email}}">
                			@if($errors->has('email'))
                				<label id="label-error" class="error" for="label">{{$errors->first('email')}}</label>
                			@endif            		
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">    
                        <div class="form-group">
                            <label class="control-label required">User Name</label>                    
                            <input type="text" class="form-control input-sm @if($errors->has('user_name')) error @endif" name="user_name" placeholder="User Name" required value="{{$curUser->username}}" readonly>
                            @if($errors->has('user_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('user_name')}}</label>
                            @endif                        
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                    		<label class="control-label">Supervisor</label>            		
                			@if($errors->has('supervisor'))
                				{!! Form::select('supervisor',$users, Input::old('supervisor'),['class'=>'chosen error input-sm','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
                				<label id="supervisor-error" class="error" for="supervisor">{{$errors->first('supervisor')}}</label>
                			@else
                				{!! Form::select('supervisor',$users, $curUser->supervisor_id,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
                			@endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                    		<label class="control-label">Role</label>
                			@if($errors->has('roles[]'))
                				{!! Form::select('roles[]',$roles, $srole,['class'=>'chosen error input-sm', 'multiple','id'=>'roles','style'=>'width:100%;','required']) !!}
                				<label id="label-error" class="error" for="label">{{$errors->first('roles[]')}}</label>
                			@else
                				{!! Form::select('roles[]',$roles,$srole,['class' => 'chosen', 'multiple','id'=>'roles','style'=>'width:100%;','required']) !!}
                			@endif
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>  
                    
                </div>
        	</form>
		</div>
	</div>	
</section>	

@stop
@section('js')


<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});
	
</script>
@stop

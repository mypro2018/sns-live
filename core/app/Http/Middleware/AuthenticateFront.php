<?php namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;
use Route;
use Request;
use Core\UserRoles\Models\UserRole;
use Core\UserRoles\Models\Role;
use Core\Permissions\Models\Permission;
class AuthenticateFront {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{	
		try{
			if (!Sentinel::check())
	    	{
				Session::put('loginRedirect', $request->url());
				return redirect()->route('front.login');
			}else{
				$user = Sentinel::getUser(); 
				if(!empty($user)){
					$role = Role::where('user_id',$user->id)->where('role_id','=',1)->first();					
					if(!empty($role)){
						$user_role = UserRole::find($role->role_id);
					}else{
						$user_role = '';
					}
				}else{
					$user_role = '';
				}
				$action = Route::currentRouteName();
				$permissions = Permission::whereIn('name',[$action,'admin'])->where('status','=',1)->lists('name');
				if (!$user->hasAnyAccess($permissions)) {
					return response()->view('errors.front.404');
				}
			}
		}catch(\Exception $e){
			Session::put('loginRedirect', $request->url());
			return redirect()->route('front.login');
		}
		view()->share('user',$user);
		view()->share('role',$user_role);
		return $next($request);
	}

}

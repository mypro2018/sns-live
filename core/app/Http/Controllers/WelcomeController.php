<?php namespace App\Http\Controllers;

use App\Models\Menu;
use Sentinel;
use App\Modules\AuctionManage\BusinessLogics\Logic AS Auction;
use App\Modules\PaymentManage\BusinessLogics\Logic AS Payment;
use App\Modules\CustomerManage\BusinessLogics\Logic AS Customer;
use Illuminate\Http\Request;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	protected $auction;
	protected $payment;
	protected $customer;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Auction $auction,Payment $payment,Customer $customer)
	{
		$this->auction = $auction;
		$this->payment = $payment;
		$this->customer = $customer;
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		 return 'hi';
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function admin()
	{
		return view('dashboard');
	}


	/**
	 * This function is used to get dashboard data.
	 *
	 * @return Response
	 */
	public function getData()
	{
		$auction_count = $this->auction->auctionCount();
		$item_count = $this->auction->itemCount();
		$customer_count = $this->customer->customerCount();
		$revenue = $this->payment->revenue();
		$auction_details = $this->auction->auction_details();
		return response()->json(['auction_count' => $auction_count,'item_count' => $item_count,'customer_count' => $customer_count,'revenue' => $revenue,'auction_detail' => $auction_details]);
	}

	/**
	 * This function is used to get dashboard data.
	 *
	 * @return Response
	 */
	public function getRequestData(Request $request)
	{
		$auction_details = $this->auction->search_auction_details($request->get('auction_id'));
		return response()->json(['auction_detail' => $auction_details]);
	}
	public function test()
	{
		return 'Hooray';
		//return Menu::create(['label'=>'Add Menu','link'=>'menu/add','icon'=>'','parent'=>'2','menu_sort'=>2,'level'=>1,'permissions'=>'["menu.add"]']);
		//return Sentinel::registerAndActivate(['username'=>'super.admin','password'=>'123456','email'=>'admin@admin.lk','first_name'=>'Super','last_name'=>'Administrator']);
	}



}

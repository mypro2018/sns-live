<?php namespace App\Http\Controllers;

use App\Modules\CategoryManage\BusinessLogics\Logic as CategoryLogic;
use App\Modules\AuctionManage\BusinessLogics\Logic as AuctionLogic;
use App\Modules\BannerManage\BannerRepository\BannerRepository;

use App\Models\Menu;
use Sentinel;

class FrontController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	protected $category;
	protected $auction;
	protected $banner_repo;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(CategoryLogic $category, AuctionLogic $auction,BannerRepository $banner_repo){
        $this->category 	= $category;
		$this->auction 		= $auction;
		$this->banner_repo  = $banner_repo;
    }

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		if(!isset($user)){
			return view('front_ui.index')->with([
				'categories'	=> $this->category->getAllCategories(),
				'auctions'		=> $this->auction->getAvailableAuctions(),
				'user'			=> $user,
				'banners'		=> $this->banner_repo->homeBanner()
			]);
		}elseif(isset($user) && count($user->permissions) == 0){
			return view('front_ui.index')->with([
				'categories'	=> $this->category->getAllCategories(),
				'auctions'		=> $this->auction->getAvailableAuctions(),
				'user'			=> $user,
				'banners'		=> $this->banner_repo->homeBanner()
			]);
		}else{
			return response()->view('errors.404');
		}
	}

}
